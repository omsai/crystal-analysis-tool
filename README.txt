----------------------------------------------------------------------------------------------------------
This is the Crystal Analysis Tool. See the documentation file "doc/manual.pdf" for more info.
Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)

----------------------------------------------------------------------------------------------------------
Copyright Statement: 

Copyright 2012-2013, Alexander Stukowski
All rights reserved. Contact the author for permission to use, copy, modify or redistribute this software.

THIS SOFTWARE IS PROVIDED "AS IS", IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY;
WITHOUT EVEN THE IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PRUPOSE. IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE.

This product includes software developed by Silicon Graphics, Inc.,  
licensed under the SGI Free Software License B (http://oss.sgi.com/projects/FreeB/) 
