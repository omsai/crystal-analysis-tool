#!/bin/bash

# Path of the analysis tool:
exe="../../build/catool/CrystalAnalysis"

# Name of the simulation snapshot file:
inputFile="npAu.dump.gz"

# Path to the pattern catalog file:
patterncatalog="../../patterns/default.db"

# List of structure patterns to search for:
patterns="fcc,fcc_isf,fcc_triple_sf,fcc_quad_sf,fcc_coherent_twin"

# Neighbor list cutoff radius:
cutoff=3.6

# Base name of output files:
outputFile=`basename $inputFile .dump.gz`

# Run analysis program:
${exe} \
	-catalog $patterncatalog \
	-patterns $patterns \
	-cutoff $cutoff \
	-cell ${outputFile}.cell.vtk \
	-atoms id,type,x,y,z,pattern,cluster ${outputFile}.atoms.dump.gz \
	-scounts ${outputFile}.counts.txt \
	-defectsurface ${outputFile}.defectsurface.vtk \
	-defectsurfacecap ${outputFile}.defectsurfacecap.vtk \
	-dislocations ${outputFile}.dislocations.vtk \
	$inputFile
