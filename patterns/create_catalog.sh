#!/bin/bash

# This script generates the default pattern catalog that is distributed with the Crystal Analysis Tool.

executable="../build/catalog_generator/CatalogGenerator"

${executable} default.in default.db | tee default.info
