///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYST_ANALYSIS_STDINC_H
#define __CRYST_ANALYSIS_STDINC_H

/////////////////////////////////////////
// Standard Template Library (STL)

#include <vector>
#include <deque>
#include <list>
#include <limits>
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <stack>
#include <complex>
#include <stdexcept>
#include <math.h>
#include <algorithm>
#include <memory>
#include <functional>
#include <cstdio>
#include <cerrno>
#include <cmath>
#include <cstring>
#include <bitset>
#include <iomanip>

/////////////////////////////////////////
// Standard C Library

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <locale.h>
#include <time.h>

/////////////////////////////////////////
// Boost Library

#include <boost/array.hpp>
#include <boost/utility.hpp>
#include <boost/foreach.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/random/mersenne_twister.hpp>
#if BOOST_VERSION > 146000
#include <boost/random/uniform_real_distribution.hpp>
#else
#include <boost/random/uniform_real.hpp>
#endif
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/serialize_ptr_vector.hpp>
#include <boost/intrusive/slist.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/bind.hpp>
#include <boost/optional.hpp>
#include <boost/math/constants/constants.hpp>

/////////////////////////////////////////
// Eigen library

#ifdef Success
	// X11 header file X.h may have defined the macro "Success", which interferes with the Eigen3 headers.
	// So undefine it first before including Eigen3 headers.
	#undef Success
#endif
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/LU>

/////////////////////////////////////////
// Message Passing Interface (MPI)

#ifdef CALIB_USE_MPI
	#include <mpi.h>
#endif

/////////////////////////////////////////
// Our own default headers

#include "util/FloatType.h"
#include "util/Debug.h"
#include "util/LinAlg.h"
#include "Settings.h"

#endif	// __CRYST_ANALYSIS_STDINC_H
