///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Default constructor.
******************************************************************************/
AtomicStructure::AtomicStructure(CAContext& context, NeighborMode neighborMode, const string& name) :
	SimulationCell(context), _name(name), _neighborMode(neighborMode)
{
	_ghostCutoff = 0;
	_numLocalAtoms = 0;
	_numGhostAtoms = 0;
	_numHelperAtoms = 0;
	_numTotalAtoms = 0;
	_ghostReceiveCounts[0][0] = _ghostReceiveCounts[0][1] = 0;
	_ghostReceiveCounts[1][0] = _ghostReceiveCounts[1][1] = 0;
	_ghostReceiveCounts[2][0] = _ghostReceiveCounts[2][1] = 0;
}

/******************************************************************************
* Computes the bounding box of the atoms.
******************************************************************************/
void AtomicStructure::computeBoundingBox()
{
	Vector3 rpMin(Vector3::Zero());
	Vector3 rpMax(Vector3::Constant(1));

	for(int atomIndex = 0; atomIndex < numLocalAtoms(); atomIndex++) {
		Vector3 rp = absoluteToReducedPoint(atomPosition(atomIndex));
		if(rp.x() < CAFloat(0) || rp.y() < CAFloat(0) || rp.z() < CAFloat(0) ||
				rp.x() > CAFloat(1) || rp.y() > CAFloat(1) || rp.z() > CAFloat(1)) {
			for(int dim = 0; dim < 3; dim++) {
				rpMin[dim] = min(rpMin[dim], rp[dim]);
				rpMax[dim] = max(rpMax[dim], rp[dim]);
			}
		}
	}

	Vector3 rpMinGlobal;
	Vector3 rpMaxGlobal;
	context().allreduce_min(rpMin.data(), rpMinGlobal.data(), 3);
	context().allreduce_max(rpMax.data(), rpMaxGlobal.data(), 3);

	if((rpMinGlobal.x() < 0 || rpMinGlobal.y() < 0 || rpMinGlobal.z() < 0 || rpMaxGlobal.x() > 1.0 || rpMaxGlobal.y() > 1.0 || rpMaxGlobal.z() > 1.0)) {
		if(context().isMaster())
			context().msgLogger() << "WARNING: Not all atoms are inside the simulation cell. Bounding box: [" << rpMinGlobal << " - " << rpMaxGlobal << "]" << endl;

		_boundingBox = _simulationCell * Eigen::DiagonalMatrix<CAFloat,3>(rpMaxGlobal - rpMinGlobal);
	}
	else {
		_boundingBox = _simulationCell;
	}
}

/******************************************************************************
* Is called before adding atoms to the structure.
******************************************************************************/
void AtomicStructure::beginAddingAtoms(int estimatedNumberOfAtomsToBeAdded)
{
	_atomPositions.reserve(_atomPositions.size() + estimatedNumberOfAtomsToBeAdded);
	_atomSpecies.reserve(_atomSpecies.size() + estimatedNumberOfAtomsToBeAdded);
	_atomTags.reserve(_atomTags.size() + estimatedNumberOfAtomsToBeAdded);
	_atomProcCoords.reserve(_atomProcCoords.size() + estimatedNumberOfAtomsToBeAdded);
}

/******************************************************************************
* Adds a single atom to the atom array.
******************************************************************************/
int AtomicStructure::addAtom(const Vector3& pos, AtomInteger tag, int species, const Vector3I& procCoord, bool isGhostAtom, bool isHelperAtom, int pbcImageNumber)
{
	CALIB_ASSERT(pbcImageNumber >= 0);

	AtomInteger uniqueTag = tag;
	if(isHelperAtom)
		_numHelperAtoms++;
	else if(isGhostAtom)
		_numGhostAtoms++;
	else {
		_numLocalAtoms++;

		if(tag < 0)
			context().raiseErrorOne("Detected negative atom tag: %i", (int)tag);
		if(tag >= (1<<CA_ATOM_IMAGE_SHIFT))
			context().raiseErrorOne("Detected out-of-bounds atom tag number: %i. According to the current CA_ATOM_IMAGE_SHIFT setting, atom tags must be smaller than %i.", (int)tag, 1 << CA_ATOM_IMAGE_SHIFT);
		if(pbcImageNumber >= 1 << (sizeof(AtomInteger)*8 - CA_ATOM_IMAGE_SHIFT - 1))
			context().raiseErrorOne("Too many copies of the input structure. The -multiply option can only be used to create a maximum of %i copies. You can change this limit in the source code.", 1 << (sizeof(AtomInteger)*8 - CA_ATOM_IMAGE_SHIFT - 1));

		// Generate a unique tag if there are additional images of atoms.
		uniqueTag = tag | (AtomInteger(pbcImageNumber) << CA_ATOM_IMAGE_SHIFT);
	}
	int index = _atomPositions.size();
	_atomPositions.push_back(pos);
	_atomSpecies.push_back(species);
	_atomTags.push_back(uniqueTag);
	_atomProcCoords.push_back(procCoord);
	return index;
}

/******************************************************************************
* Is called after all atoms have been added to the structure.
******************************************************************************/
void AtomicStructure::endAddingAtoms()
{
	_numTotalAtoms = context().allreduce_sum((AtomInteger)_numLocalAtoms);
}

/// Structure used to exchange ghost atoms with neighboring processors.
struct AtomGhostListComm {
	Vector3 pos;
	Vector3I procCoord;
	AtomInteger tag;
	int species;
};

/******************************************************************************
* Transfers local atoms to neighboring processors.
******************************************************************************/
void AtomicStructure::communicateGhostAtoms()
{
	for(int dim = 0; dim < 3; dim++) {
		if(pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		// Calculate normal vector of the current simulation cell side.
		Vector3 normal = cellNormalVector(dim);

		if(ghostCutoff() * parallel().processorGrid(dim) * 2.0 >= normal.dot(cellVector(dim)))
			context().raiseErrorAll("Local processor domains are too small for ghost cutoff (in dimension %i). Please reduce number of processors or ghost cutoff.", dim+1);

		int numAtoms = numLocalAtoms() + numGhostAtoms();
		for(int dir = 0; dir <= 1; dir++) {
			CAFloat cut;
			Vector3 dirNormal;
			if(dir == 0) {
				dirNormal = normal;
				cut = ghostCutoff() + dirNormal.dot(processorDomainMatrix() * Vector3::Zero());
			}
			else {
				dirNormal = -normal;
				cut = ghostCutoff() + dirNormal.dot(processorDomainMatrix() * Vector3::Unit(dim));
			}

			vector<int>& sendIndices = _ghostCommunicationLists[dim][dir];
			CALIB_ASSERT(sendIndices.empty());
			CALIB_ASSERT(ghostReceiveCount(dim, dir) == 0);

			// Select atoms which need to be communicated to the neighboring processors in the current direction.
			vector<AtomGhostListComm> sendBuffer;
			if(parallel().shouldSendOrReceive(dim, dir, pbc(dim))) {
				for(int i = 0; i < numAtoms; i++) {
					CAFloat t = atomPosition(i).dot(dirNormal);
					if(t <= cut) {
						sendIndices.push_back(i);

						AtomGhostListComm a = { atomPosition(i), atomProcCoord(i), atomTag(i), atomSpecies(i) };
						sendBuffer.push_back(a);
					}
				}
			}

			// Determine image shift vector due to periodic boundaries.
			Vector3 shiftVector(Vector3::Zero());
			Vector3I procCoordShiftVector(Vector3I::Zero());
			if(pbc(dim) && dir == 1 && parallel().processorLocation(dim) == 0) {
				shiftVector = -cellVector(dim);
				procCoordShiftVector[dim] = -parallel().processorGrid(dim);
			}
			else if(pbc(dim) && dir == 0 && parallel().processorLocation(dim) == parallel().processorGrid(dim) - 1) {
				shiftVector = cellVector(dim);
				procCoordShiftVector[dim] = parallel().processorGrid(dim);
			}

			// Communicate number of ghost atoms to be exchanged with neighbor processor.
			parallel().exchange(dim, dir, pbc(dim), sendIndices.size(), _ghostReceiveCounts[dim][dir]);

			// Send atom coordinates of selected atoms to neighbor processor.
			vector<AtomGhostListComm> receiveBuffer(ghostReceiveCount(dim, dir));
			parallel().exchange(dim, dir, pbc(dim), sendBuffer, receiveBuffer);

			// Insert received ghost atoms into local array.
			beginAddingAtoms(receiveBuffer.size());
			BOOST_FOREACH(const AtomGhostListComm& receivedAtom, receiveBuffer) {
				addAtom(receivedAtom.pos + shiftVector, receivedAtom.tag, receivedAtom.species, receivedAtom.procCoord + procCoordShiftVector, true);
			}
		}
	}
}

/******************************************************************************
* Creates extra helper atoms in vacuum regions of the simulation cell to
* make the Delaunay triangulation in the overlap region between processors consistent.
******************************************************************************/
AtomInteger AtomicStructure::createExtraTessellationPoints(CAFloat maxPointDistance)
{
	Vector3 cellVectors[3] = {
			_boundingBox.linear().col(0),
			_boundingBox.linear().col(1),
			_boundingBox.linear().col(2)
	};

	// Determine global point grid.
	Vector3I gridDim;				// The number of grid points in each spatial direction.
	for(int dim = 0; dim < 3; dim++) {
		// Calculate normal vector of the current simulation cell face.
		Vector3 normal = cellVectors[(dim+1)%3].cross(cellVectors[(dim+2)%3]).normalized();
		// Calculate number of bins in the current dimension.
		gridDim[dim] = (int)floor(fabs(normal.dot(cellVectors[dim])) / maxPointDistance);
		// We always have at least one bin in each direction.
		gridDim[dim] = max(gridDim[dim], 1);
	}

	// Shape of a single grid cell.
	AffineTransformation gridCellToAbsolute = _boundingBox *
			Eigen::DiagonalMatrix<CAFloat,3>(1.0 / gridDim[0], 1.0 / gridDim[1], 1.0 / gridDim[2]) *
			Eigen::Translation<CAFloat,3>(Vector3::Constant(-1));
	AffineTransformation absoluteToGridCell = gridCellToAbsolute.inverse();

	AffineTransformation gridPointToAbsolute = gridCellToAbsolute * Eigen::Translation<CAFloat,3>(Vector3::Constant(0.5));
	AffineTransformation absoluteToGridPoint = gridPointToAbsolute.inverse();

	// Add extra points to completely cover the simulation cell and the margin.
	for(int dim = 0; dim < 3; dim++) {
		if(!pbc(dim))
			gridDim[dim] += 3;
		else
			gridDim[dim] += 1;
	}

	// Determine which part of the global point grid is covered by the processor's domain.
	Vector3 corner1_world = processorGridToAbsolute(parallel().processorLocationPoint());
	Vector3 corner2_world = processorGridToAbsolute(parallel().processorLocationPoint() + Vector3I::Constant(1));
	Vector3 corner1_grid = absoluteToGridPoint * corner1_world;
	Vector3 corner2_grid = absoluteToGridPoint * corner2_world;
	Vector3I corner1_rounded;
	Vector3I corner2_rounded;
	Vector3I localGridDim;
	for(int dim = 0; dim < 3; dim++) {
		corner1_rounded[dim] = max((int)floor(corner1_grid[dim]) - 1, 0);
		corner2_rounded[dim] = min((int)ceil(corner2_grid[dim]) + 1, gridDim[dim] - 1);
		if(parallel().isParallelMode() == false) {
			// This is for pseudo-parallelized mode:
			if(corner1_rounded[dim] != 0) corner1_rounded[dim]--;
			if(corner2_rounded[dim] != gridDim[dim] - 1) corner2_rounded[dim]++;
		}
		localGridDim[dim] = corner2_rounded[dim] - corner1_rounded[dim] + 1;
	}
	CALIB_ASSERT(corner1_rounded.x() <= corner2_rounded.x());
	CALIB_ASSERT(corner1_rounded.y() <= corner2_rounded.y());
	CALIB_ASSERT(corner1_rounded.z() <= corner2_rounded.z());
	int numBins = localGridDim.prod();

	// Allocate bin array to determine which grid cells are completely empty.
	CALIB_ASSERT(numBins > 0 && numBins < 100000000);
	vector<bool> bins(numBins, false);

	// Sort atoms into bins.
	for(int atomIndex = 0; atomIndex < numLocalAtoms() + numGhostAtoms(); atomIndex++) {
		Vector3 rp = absoluteToGridCell * atomPosition(atomIndex);
		int binCoord[3];
		bool isClipped = false;
		for(int dim = 0; dim < 3; dim++) {
			binCoord[dim] = (int)floor(rp[dim]);
			if(binCoord[dim] < corner1_rounded[dim] || binCoord[dim] > corner2_rounded[dim])
				isClipped = true;
		}
		if(isClipped) continue;

		int binIndex = (binCoord[2] - corner1_rounded[2]) * localGridDim[0] * localGridDim[1] +
				(binCoord[1] - corner1_rounded[1]) * localGridDim[0] + (binCoord[0] - corner1_rounded[0]);
		CALIB_ASSERT(binIndex >= 0 && binIndex < bins.size());

		// Mark bin as non-empty.
		bins[binIndex] = true;
	}

	// Initialize random number generator with unique seed on each processor.
	srand48(parallel().processor());

	// Create a helper point in every empty grid cell.
	vector<bool>::const_iterator biter = bins.begin();
	for(int z = corner1_rounded.z(); z <= corner2_rounded.z(); z++) {
		for(int y = corner1_rounded.y(); y <= corner2_rounded.y(); y++) {
			for(int x = corner1_rounded.x(); x <= corner2_rounded.x(); x++) {
				CALIB_ASSERT(biter < bins.end());
				// Check if grid cell is empty.
				if(*biter == false) {
					Vector3 gridPoint = gridPointToAbsolute * Vector3(x,y,z);

					// Determine the processor domain the grid point belongs to.
					Vector3I procCoordi = absoluteToProcessorGrid(gridPoint, false);
					Vector3I clampedProcCoordi = procCoordi;
					for(int dim = 0; dim < 3; dim++) {
						if(!pbc(dim))
							clamp(clampedProcCoordi[dim], 0, parallel().processorGrid(dim) - 1);
					}

					// Only create helper point if cell belongs to the local processor domain.
					if(clampedProcCoordi == parallel().processorLocationPoint() || !parallel().isParallelMode()) {

						// Generate a unique tag for the helper atom.
						AtomInteger tag = numTotalAtoms() + z*gridDim[0]*gridDim[1] + y*gridDim[0] + x;
						CALIB_ASSERT(tag >= numTotalAtoms());
						CALIB_ASSERT(tag < (1<<CA_ATOM_IMAGE_SHIFT));

						if(parallel().isParallelMode() == false) {
							// This is for pseudo-parallelized operation: Make the random displacement reproducible.
							srand48(z*gridDim[0]*gridDim[1] + y*gridDim[0] + x);
						}

						// Add a small random perturbation.
						const CAFloat pmag = maxPointDistance * 1e-3;
						gridPoint.x() += (drand48() - 0.5) * pmag;
						gridPoint.y() += (drand48() - 0.5) * pmag;
						gridPoint.z() += (drand48() - 0.5) * pmag;

						// Create a helper atom.
						addAtom(gridPoint, tag, 0, procCoordi, false, true);
					}
				}
				++biter;
			}
		}
	}
	CALIB_ASSERT(biter == bins.end());

	// Determine total number of created helper atoms across all processors.
	AtomInteger totalNumHelperAtoms = parallel().allreduce_sum((AtomInteger)numHelperAtoms());
	if(totalNumHelperAtoms != 0 && parallel().isParallelMode()) {

		// Exchange helper atoms between processors.

		Vector3 domainOrigin = corner1_world;
		for(int dim = 0; dim < 3; dim++) {
			if(pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

			// Calculate normal vector of the current simulation cell side.
			Vector3 normal = cellNormalVector(dim);

			int oldNumHelperAtoms = numHelperAtoms();
			for(int dir = 0; dir <= 1; dir++) {
				CAFloat cut;
				Vector3 dirNormal;
				if(dir == 0) {
					dirNormal = normal;
					cut = ghostCutoff() + dirNormal.dot(domainOrigin);
				}
				else {
					dirNormal = -normal;
					cut = ghostCutoff() + dirNormal.dot(domainOrigin + cellVector(dim) * (CAFloat(1) / parallel().processorGrid(dim)));
				}

				// Select atoms which need to be communicated to the neighboring processors in the current direction.
				vector<AtomGhostListComm> sendBuffer;
				if(parallel().shouldSendOrReceive(dim, dir, pbc(dim))) {
					for(int i = numLocalAtoms() + numGhostAtoms(); i < numLocalAtoms() + numGhostAtoms() + oldNumHelperAtoms; i++) {
						CAFloat t = atomPosition(i).dot(dirNormal);
						if(t <= cut) {
							AtomGhostListComm a = { atomPosition(i), atomProcCoord(i), atomTag(i) };
							sendBuffer.push_back(a);
						}
					}
				}

				// Determine image shift vector due to periodic boundaries.
				Vector3 shiftVector(Vector3::Zero());
				Vector3I procCoordShiftVector(Vector3I::Zero());
				if(pbc(dim) && dir == 1 && parallel().processorLocation(dim) == 0) {
					shiftVector = -cellVector(dim);
					procCoordShiftVector[dim] = -parallel().processorGrid(dim);
				}
				else if(pbc(dim) && dir == 0 && parallel().processorLocation(dim) == parallel().processorGrid(dim) - 1) {
					shiftVector = cellVector(dim);
					procCoordShiftVector[dim] = parallel().processorGrid(dim);
				}

				// Communicate number of ghosts to be exchanged with neighbor processor.
				int receiveCount = 0;
				parallel().exchange(dim, dir, pbc(dim), sendBuffer.size(), receiveCount);

				// Send atom coordinates of selected atoms to neighbor processor.
				vector<AtomGhostListComm> receiveBuffer(receiveCount);
				parallel().exchange(dim, dir, pbc(dim), sendBuffer, receiveBuffer);

				// Insert received ghosts into local array.
				beginAddingAtoms(receiveCount);
				BOOST_FOREACH(const AtomGhostListComm& receivedAtom, receiveBuffer) {
					addAtom(receivedAtom.pos + shiftVector, receivedAtom.tag, 0, receivedAtom.procCoord + procCoordShiftVector, true, true);
				}
			}
		}
	}

	return totalNumHelperAtoms;
}

}; // End of namespace
