///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_GHOST_ATOM_COMMUNICATION_HELPER_H
#define __CA_GHOST_ATOM_COMMUNICATION_HELPER_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "AtomicStructure.h"

namespace CALib {

/******************************************************************************
* Helper class that encapsulates the communication of per-atom data from
* local atoms to the ghost atoms of neighboring processors.
******************************************************************************/
template<typename T>
class GhostAtomCommunicationHelper : public ContextReference
{
public:

	/// Constructor.
	GhostAtomCommunicationHelper(const AtomicStructure& structure) : ContextReference(structure.context()), _structure(structure),
		_currentAtomIndex(-1), _currentItem(NULL), _receiveAtomIndex(structure.numLocalAtoms()) {}

	/// Prepares the communication in one dimension/direction.
	void prepare(int dimension, int direction) {
		CALIB_ASSERT(_currentAtomIndex == -1);
		CALIB_ASSERT(_currentItem == NULL);
		const std::vector<int>& sendIndices = _structure.ghostCommunicationList(dimension, direction);
		_sendIndicesIter = sendIndices.begin();
		_sendBuffer.resize(sendIndices.size());
		_sendBufferIter = _sendBuffer.begin();
	}

	/// Positions the pointers to the next local atom and the corresponding data item to be sent.
	bool nextSendItem() {
		if(_sendBufferIter == _sendBuffer.end()) {
			_currentAtomIndex = -1;
			_currentItem = NULL;
			return false;
		}
		_currentAtomIndex = *_sendIndicesIter++;
		_currentItem = &*_sendBufferIter++;
		return true;
	}

	/// Positions the pointers to the next atom and the corresponding data item to be sent.
	void communicate(int dimension, int direction) {
		CALIB_ASSERT(_currentAtomIndex == -1);
		CALIB_ASSERT(_currentItem == NULL);
		CALIB_ASSERT(_sendBufferIter == _sendBuffer.end());
		_receiveBuffer.resize(_structure.ghostReceiveCount(dimension, direction));
		parallel().exchange(dimension, direction, _structure.pbc(dimension), _sendBuffer, _receiveBuffer);
		_receiveBufferIter = _receiveBuffer.begin();
	}

	/// Positions the pointers to the next ghost atom and the corresponding data item received.
	bool nextReceiveItem() {
		if(_receiveBufferIter == _receiveBuffer.end()) {
			_currentAtomIndex = -1;
			_currentItem = NULL;
			return false;
		}
		_currentAtomIndex = _receiveAtomIndex++;
		_currentItem = &*_receiveBufferIter++;
		return true;
	}

	int sendAtomIndex() const { CALIB_ASSERT(_currentAtomIndex != -1); return _currentAtomIndex; }
	T& sendItem() { CALIB_ASSERT(_currentItem != NULL); return *_currentItem; }

	int receiveAtomIndex() { CALIB_ASSERT(_structure.isGhostAtom(_currentAtomIndex)); return _currentAtomIndex; }
	const T& receiveItem() const { CALIB_ASSERT(_currentItem != NULL); return *_currentItem; }

private:

	/// The atomic structure.
	const AtomicStructure& _structure;

	/// The send-item buffer.
	std::vector<T> _sendBuffer;

	/// The iterator over all atoms to send.
	typename std::vector<int>::const_iterator _sendIndicesIter;

	/// The iterator over all per-atom data items to send.
	typename std::vector<T>::iterator _sendBufferIter;

	/// The current atom.
	int _currentAtomIndex;

	/// The current item;
	T* _currentItem;

	/// The receive-item buffer.
	std::vector<T> _receiveBuffer;

	/// The iterator over all atoms to receive.
	int _receiveAtomIndex;

	/// The iterator over all per-atom data items received.
	typename std::vector<T>::iterator _receiveBufferIter;
};

}; // End of namespace

#endif // __CA_GHOST_ATOM_COMMUNICATION_HELPER_H
