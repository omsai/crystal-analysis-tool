///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_NEIGHBOR_LIST_H
#define __CA_NEIGHBOR_LIST_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../AtomicStructure.h"

namespace CALib {


/**
 * Builds and stores the neighbor lists of atoms.
 */
class NeighborList : public ContextReference
{
public:

	/// Describes one neighbor bond of a central atom.
	struct neighbor_info {
		int index;			///< The index of the neighbor atom in the atomic structure.
		Vector3 delta;		///< Bond vector to neighbor atom.
		CAFloat distsq;		///< Squared distance of neighbor from central atom.

		/// Used for sorting neighbor lists.
		bool operator<(const neighbor_info& other) const { return distsq < other.distsq; }
	};

	/// An iterator over the neighbors of a central atom.
	typedef std::vector<neighbor_info>::const_iterator neighbor_iterator;

	/// A range object that contains all neighbors of a given central atom.
	typedef boost::iterator_range<neighbor_iterator> neighbor_range;

public:

	/// Constructor. Takes a reference to the AtomicStructure for which the neighbor lists will be built.
	NeighborList(const AtomicStructure& structure) : ContextReference(structure.context()), _structure(structure), _cutoff(0), _cutoffSquared(0), _isSorted(false) {}

	/// Returns a const-reference to the atomic structure to which this neighbor list belongs.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the cutoff radius used when generating the neighbor list.
	CAFloat cutoff() const { return _cutoff; }

	/// Returns the squared cutoff radius used when generating the neighbor list.
	CAFloat cutoffSquared() const { return _cutoffSquared; }

	/// Generates the neighbor lists for the atoms of the atomic structure.
	///
	/// If 'sortNeighborLists==true', the per-atom neighbor lists are sorted according
	/// to neighbor distance in ascending order.
	///
	/// The parameter 'generateGhostNeighborLists' controls whether neighbor lists of
	/// ghost atoms are generated. If not using ghost atom, the parameter is ignored.
	///
	/// When building sorted neighbors lists, the 'numNearestNeighbors' parameter can be used to
	/// restrict the generated lists to the first N neighbors of each atom.
	/// This can save memory and bandwidth if only a predetermined number of nearest neighbors is needed.
	void buildNeighborLists(CAFloat cutoff, bool sortNeighborLists, bool generateGhostNeighborLists = true, int numNearestNeighbors = 100000);

	/// Indicates whether neighbors lists are sorted according to neighbor distance.
	bool isSorted() const { return _isSorted; }

	/// Returns the number of neighbors of a central atom.
	int neighborCount(int centralAtomIndex) const {
		CALIB_ASSERT(centralAtomIndex >= 0 && centralAtomIndex < (int)_neighborCountArray.size());
		return _neighborCountArray[centralAtomIndex];
	}

	/// Returns a range object that contains all neighbor bonds of the central atom.
	neighbor_range neighbors(int centralAtomIndex) const {
		CALIB_ASSERT(centralAtomIndex >= 0 && centralAtomIndex < (int)_neighborListHeads.size());
		neighbor_iterator n_begin = _neighborLists.begin() + _neighborListHeads[centralAtomIndex];
		neighbor_iterator n_end   = n_begin + neighborCount(centralAtomIndex);
		return boost::make_iterator_range(n_begin, n_end);
	}

	/// Tests whether another atom is a nearest neighbor of the given central atom.
	bool isNeighbor(int centralAtomIndex, int neighborAtomIndex) const {
		BOOST_FOREACH(const neighbor_info& n, neighbors(centralAtomIndex)) {
			if(n.index == neighborAtomIndex) return true;
		}
		return false;
	}

	/// Returns the index of a neighbor atom.
	/// The neighbor index is an index into the central atom's list of neighbors,
	/// while the returned index is an index into the full list of atoms in the input structure.
	int neighbor(int centralAtomIndex, int neighborIndex) const {
		CALIB_ASSERT(neighborIndex >= 0 && neighborIndex < neighborCount(centralAtomIndex));
		return (boost::const_begin(neighbors(centralAtomIndex)) + neighborIndex)->index;
	}

	/// Returns the squared distance of the given neighbor atom from the central atom.
	/// The neighbor index is an index into the central atom's list of neighbors.
	CAFloat neighborDistanceSquared(int centralAtomIndex, int neighborIndex) const {
		CALIB_ASSERT(neighborIndex >= 0 && neighborIndex < neighborCount(centralAtomIndex));
		return (boost::const_begin(neighbors(centralAtomIndex)) + neighborIndex)->distsq;
	}

	/// Returns the distance of the given neighbor atom from the central atom.
	/// The neighbor index is an index into the central atom's list of neighbors.
	CAFloat neighborDistance(int centralAtomIndex, int neighborIndex) const {
		return sqrt(neighborDistanceSquared(centralAtomIndex, neighborIndex));
	}

	/// Returns the vector from the given central atom to the given neighbor.
	/// The neighbor index is an index into the central atom's list of neighbors.
	const Vector3& neighborVector(int centralAtomIndex, int neighborIndex) const {
		CALIB_ASSERT(neighborIndex >= 0 && neighborIndex < neighborCount(centralAtomIndex));
		return (boost::const_begin(neighbors(centralAtomIndex)) + neighborIndex)->delta;
	}

private:

	/// The atomic structure to which this neighbor list belongs.
	const AtomicStructure& _structure;

	/// The cutoff radius used for generating the neighbor list.
	CAFloat _cutoff;

	/// The squared cutoff radius used for generating the neighbor list.
	CAFloat _cutoffSquared;

	/// Stores the number of neighbors for each atom.
	std::vector<int> _neighborCountArray;

	/// Stores each atom's head position in the following large array.
	std::vector<int> _neighborListHeads;

	/// Large array that stores all bond vectors to neighbors.
	std::vector<neighbor_info> _neighborLists;

	/// Indicates if neighbors lists are sorted according to neighbor distance.
	bool _isSorted;
};

}; // End of namespace

#endif // __CA_NEIGHBOR_LIST_H
