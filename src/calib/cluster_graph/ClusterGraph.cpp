///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ClusterGraph.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor. Creates an empty cluster graph.
******************************************************************************/
ClusterGraph::ClusterGraph(const AtomicStructure& structure, const PatternCatalog& catalog) : ContextReference(structure.context()),
	_structure(structure), _catalog(catalog), _maximumClusterDistance(2)
{
	_numPathQueries = 0;
	_numDisconnectedPairsQuick = 0;
	_numDisconnectedPairsSlow = 0;
	_numConnectedQuick = 0;
	_numConnectedSlow = 0;
	_numIsolated = 0;
}

/******************************************************************************
* Clears all clusters.
******************************************************************************/
void ClusterGraph::clear()
{
	// Remove transitions from clusters.
	BOOST_FOREACH(ClusterTransition* t, clusterTransitions()) {
		t->cluster1->removeTransition(t);
		if(t->isSelfTransition() == false)
			t->cluster2->removeTransition(t->reverse);
	}

	_disconnectedClusters.clear();
	_clusterTransitions.clear();
	_clusterTransitionPool.clear();
	_clusters.clear();
	_clusterPool.clear();
	_clusterMap.clear();
}

/******************************************************************************
* Outputs performance statistics to the log file.
******************************************************************************/
void ClusterGraph::printSearchStatistics()
{
	cout << "Cluster graph query performance statistics:" << endl;
	cout << "PROC " << parallel().processor() << " - numPathQueries = " << _numPathQueries << endl;
	cout << "PROC " << parallel().processor() << " - numDisconnectedPairsQuick = " << _numDisconnectedPairsQuick << endl;
	cout << "PROC " << parallel().processor() << " - numDisconnectedPairsSlow = " << _numDisconnectedPairsSlow << endl;
	cout << "PROC " << parallel().processor() << " - numConnectedQuick = " << _numConnectedQuick << endl;
	cout << "PROC " << parallel().processor() << " - numConnectedSlow = " << _numConnectedSlow << endl;
	cout << "PROC " << parallel().processor() << " - numIsolated = " << _numIsolated << endl;
}

/******************************************************************************
* Inserts a new cluster into the graph.
* A unique ID for the new cluster is automatically generated.
******************************************************************************/
Cluster* ClusterGraph::createCluster(SuperPattern const* pattern)
{
	// Generate new unique ID.
	ClusterId id(clusters().size() + 1, parallel().processor());

	// Create cluster in the graph.
	return createCluster(id, pattern);
}

/******************************************************************************
* Inserts a new cluster into the graph with the given ID.
******************************************************************************/
Cluster* ClusterGraph::createCluster(ClusterId id, SuperPattern const* pattern)
{
	// Construct new Cluster instance and insert it into the list of clusters.
	Cluster* cluster = _clusterPool.construct(id, pattern);
	_clusters.push_back(cluster);

	// Register cluster in ID lookup map.
	bool isUniqueId = _clusterMap.insert(make_pair(id, cluster)).second;
	CALIB_ASSERT(isUniqueId);

	// The new cluster is a root cluster.
	cluster->parentTransition = createSelfTransition(cluster);

	return cluster;
}

/******************************************************************************
* Looks up the cluster with the given ID.
* Returns NULL if the cluster with the given ID does not exist or is not
* known to this processor.
* ******************************************************************************/
Cluster* ClusterGraph::findCluster(ClusterId id) const
{
	map<ClusterId, Cluster*>::const_iterator iter = _clusterMap.find(id);
	if(iter == _clusterMap.end())
		return NULL;
	else {
		CALIB_ASSERT(iter->second->id == id);
		return iter->second;
	}
}

/******************************************************************************
* Registers a new cluster transition between two clusters A and B.
* This will create a new edge in the cluster graph unless an identical edge with the
* same transformation matrix already exists.
* The reverse transition B->A will also be created automatically.
******************************************************************************/
ClusterTransition* ClusterGraph::createClusterTransition(Cluster* clusterA, Cluster* clusterB, const Matrix3& tm, int distance)
{
	// Handle trivial case (the self-transition).
	if(clusterA == clusterB && (tm - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON)) {
		return createSelfTransition(clusterA);
	}

	CALIB_ASSERT(distance >= 1);
	CALIB_ASSERT(distance < 10000);

	// Look for existing transition connecting the same pair of clusters and having the same transition matrix.
	for(ClusterTransition* t = clusterA->transitions; t != NULL; t = t->next) {
		if(t->cluster2 == clusterB && (t->tm - tm).isZero(CA_TRANSITION_MATRIX_EPSILON))
			return t;
	}

	// Create a new transition for the pair of clusters.
	ClusterTransition* tAB = _clusterTransitionPool.construct();
	ClusterTransition* tBA = _clusterTransitionPool.construct();
	tAB->cluster1 = clusterA;
	tAB->cluster2 = clusterB;
	tBA->cluster1 = clusterB;
	tBA->cluster2 = clusterA;
	tAB->tm = tm;
	tBA->tm = tm.inverse();
	tAB->reverse = tBA;
	tBA->reverse = tAB;
	tAB->distance = distance;
	tBA->distance = distance;

	// Insert the new transition and its reverse into the linked lists of the two clusters.
	clusterA->insertTransition(tAB);
	clusterB->insertTransition(tBA);

	// Register pair of new transitions in global list.
	// For this, we need to add only one of them.
	_clusterTransitions.push_back(tAB);

	// When inserting an edge that is not the concatenation of other edges, then
	// the topology of disconnected graph components may have changed.
	// This will invalidate our cache.
	if(distance == 1)
		_disconnectedClusters.clear();

#if 0
	context().msgLogger() << "-- Created cluster transition: " << clusterA->id.id << " <--> " << clusterB->id.id << endl;
	context().msgLogger() << tm;
#endif

	return tAB;
}

/******************************************************************************
* Creates the self-transition for a cluster (or returns the existing one).
* ******************************************************************************/
ClusterTransition* ClusterGraph::createSelfTransition(Cluster* cluster)
{
	// Check for existing self-transition.
	if(cluster->transitions != NULL && cluster->transitions->isSelfTransition()) {
		return cluster->transitions;
	}
	else {
		// Create the self-transition.
		ClusterTransition* t = _clusterTransitionPool.construct();
		t->cluster1 = t->cluster2 = cluster;
		t->tm.setIdentity();
		t->reverse = t;
		t->distance = 0;
		t->next = cluster->transitions;
		cluster->transitions = t;
		CALIB_ASSERT(t->isSelfTransition());
		CALIB_ASSERT(t->next == NULL || t->next->distance >= 1);
		return t;
	}
}

/******************************************************************************
* Determines the transformation matrix that transforms vectors from cluster A to cluster B.
* For this, the cluster graph is searched for the shortest path connecting the two cluster nodes.
* If the two clusters are part of different disconnected components of the graph, then NULL is returned.
* Once a new transition between A and B has been found, it is cached by creating a new edge in the graph between
* the clusters A and B. Future queries for the same pair can then be answered efficiently.
******************************************************************************/
ClusterTransition* ClusterGraph::determineClusterTransition(Cluster* clusterA, Cluster* clusterB)
{
	CALIB_ASSERT(clusterA != NULL && clusterB != NULL);
	_numPathQueries++;

	// Handle trivial case (self-transition).
	if(clusterA == clusterB)
		return createSelfTransition(clusterA);

	// Check if there is a direct transition to the target cluster.
	for(ClusterTransition* t = clusterA->transitions; t != NULL; t = t->next) {
		// Verify that the linked list is ordered.
		CALIB_ASSERT(t->next == NULL || !clusterTransitionDistanceCompare(t->next, t));

		if(t->cluster2 == clusterB) {
			_numConnectedQuick++;
			return t;
		}
	}

	// Check if either the start or the destination cluster has no transitions to other clusters.
	// Then there cannot be a path connecting them.
	if(clusterA->transitions == NULL || (clusterA->transitions->isSelfTransition() && clusterA->transitions->next == NULL)) {
		_numIsolated++;
		return NULL;
	}
	if(clusterB->transitions == NULL || (clusterB->transitions->isSelfTransition() && clusterB->transitions->next == NULL)) {
		_numIsolated++;
		return NULL;
	}

	// Make sure the algorithm always finds the same path, independent of whether we are searching for
	// the connection A->B or B->A.
	bool reversedSearch = false;
	if(clusterA->id > clusterB->id) {
		reversedSearch = true;
		swap(clusterA, clusterB);
	}

	// Check if the transition between the same pair of clusters has been requested
	// in the past and we already found that they are part of disconnected components of the graph.
	if(_disconnectedClusters.find(make_pair(clusterA, clusterB)) != _disconnectedClusters.end()) {
		_numDisconnectedPairsQuick++;
		return NULL;
	}

	if(_maximumClusterDistance == 2) {

		// A hardcoded shortest path search for maximum depth 2:
		int shortestDistance = _maximumClusterDistance + 1;
		ClusterTransition* shortestPath1 = NULL;
		ClusterTransition* shortestPath2;
		for(ClusterTransition* t1 = clusterA->transitions; t1 != NULL; t1 = t1->next) {
			CALIB_ASSERT(t1->cluster2 != clusterB);
			if(t1->cluster2 == clusterA) continue;
			CALIB_ASSERT(t1->distance >= 1);
			for(ClusterTransition* t2 = t1->cluster2->transitions; t2 != NULL; t2 = t2->next) {
				if(t2->cluster2 == clusterB) {
					CALIB_ASSERT(t2->distance >= 1);
					int distance = t1->distance + t2->distance;
					if(distance < shortestDistance) {
						shortestDistance = distance;
						shortestPath1 = t1;
						shortestPath2 = t2;
					}
					break;
				}
			}
		}
		if(shortestPath1) {
			// Create a direct transition (edge) between the two nodes in the cluster graph to speed up subsequent path queries.
			CALIB_ASSERT(shortestDistance >= 1);
			CALIB_ASSERT(shortestDistance < 100000);
			ClusterTransition* newTransition = createClusterTransition(clusterA, clusterB, shortestPath2->tm * shortestPath1->tm, shortestDistance);
			_numConnectedSlow++;
			if(reversedSearch == false) return newTransition;
			else return newTransition->reverse;
		}
	}
	else if(_maximumClusterDistance > 2) {

		// Perform general Dijkstra's shortest path search in cluster graph.
		deque<Cluster*> queue;
		set<Cluster*> visitedClusters;
		clusterA->predecessor = NULL;
		clusterA->distanceFromStart = 0;

		// Add first nearest neighbors to search queue.
		for(ClusterTransition* t = clusterA->transitions; t != NULL; t = t->next) {
			CALIB_ASSERT(t->cluster2 != clusterB);
			if(t->cluster2 == clusterA) continue;
			if(visitedClusters.insert(t->cluster2).second) {
				CALIB_ASSERT(t->distance > 0);
				t->cluster2->distanceFromStart = t->distance;
				t->cluster2->predecessor = t;
				queue.push_back(t->cluster2);
			}
		}

		do {
			Cluster* currentCluster = queue.front();
			queue.pop_front();

			// Recursive step in shortest path search algorithm.
			for(ClusterTransition* t = currentCluster->transitions; t != NULL; t = t->next) {

				// Verify that node's edge list is ordered.
				CALIB_ASSERT(t->next == NULL || t->distance <= t->next->distance);

				// Did we go back to the start cluster? Don't visit it again.
				if(t->cluster2 == t->cluster1 || t->cluster2 == clusterA)
					continue;
				CALIB_ASSERT(t->distance > 0);

				Cluster* neighbor = t->cluster2;
				int distance = currentCluster->distanceFromStart + t->distance;
				if(distance > _maximumClusterDistance)
					continue;

				// Check if we have reached the destination cluster.
				if(neighbor == clusterB) {

					CALIB_ASSERT(distance >= 2);
					CALIB_ASSERT(currentCluster->predecessor != NULL);

					// Determine transformation matrix by reconstructing the shortest path and
					// concatenating the matrices of all edges in the path.
					ClusterTransition* edge = currentCluster->predecessor;
					Matrix3 tm = t->tm * edge->tm;
					for(;;) {
						edge = edge->cluster1->predecessor;
						if(edge == NULL) break;
						tm = tm * edge->tm;
					}

					// Create a direct transition (edge) between the two nodes in the cluster graph to speed up subsequent path queries.
					CALIB_ASSERT(distance >= 1);
					ClusterTransition* newTransition = createClusterTransition(clusterA, clusterB, tm, distance);

					// Return result.
					_numConnectedSlow++;
					if(reversedSearch == false) return newTransition;
					else return newTransition->reverse;
				}

				// Update step of Dijkstra's path search algorithm.
				if(distance < _maximumClusterDistance) {
					if(visitedClusters.insert(neighbor).second) {
						neighbor->distanceFromStart = distance;
						neighbor->predecessor = t;
						queue.push_back(neighbor);
					}
					else {
						if(neighbor->distanceFromStart > distance) {
							neighbor->distanceFromStart = distance;
							neighbor->predecessor = t;
						}
					}
				}
			}
			boost::sort(queue, clusterGraphDistanceCompare);
		}
		while(queue.empty() == false);
	}

	// Flag this pair as disconnected to speed up subsequent queries for the same pair.
	_disconnectedClusters.insert(make_pair(clusterA, clusterB));

	_numDisconnectedPairsSlow++;
	return NULL;
}

/******************************************************************************
* Returns the concatenation of two cluster transitions (A->B->C ->  A->C)
******************************************************************************/
ClusterTransition* ClusterGraph::concatenateClusterTransitions(ClusterTransition* tAB, ClusterTransition* tBC)
{
	CALIB_ASSERT(tAB != NULL && tBC != NULL);
	CALIB_ASSERT(tAB->cluster2 == tBC->cluster1);

	// Just return A->B if B->C is a self-transition (B==C).
	if(tBC->isSelfTransition())
		return tAB;

	// Just return B->C if A->B is a self-transition (A==B).
	if(tAB->isSelfTransition())
		return tBC;

	// Return A->A self-transition in case A->B->A.
	if(tAB->reverse == tBC)
		return createSelfTransition(tAB->cluster1);

	CALIB_ASSERT(tAB->distance >= 1);
	CALIB_ASSERT(tBC->distance >= 1);

	// Actually concatenate transition matrices by multiplying the transformation matrices.
	return createClusterTransition(tAB->cluster1, tBC->cluster2, tBC->tm * tAB->tm, tAB->distance + tBC->distance);
}

}; // End of namespace
