///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CLUSTER_GRAPH_H
#define __CA_CLUSTER_GRAPH_H

#include "Cluster.h"
#include "../util/MemoryPool.h"
#include "../context/CAContext.h"

namespace CALib {

class AtomicStructure;
class PatternCatalog;
struct SuperPattern;

/**
 * This class stores the graph of atomic clusters.
 */
class ClusterGraph : public ContextReference
{
public:

	/// Constructor. Creates an empty cluster graph.
	ClusterGraph(const AtomicStructure& structure, const PatternCatalog& catalog);

	/// Destructor.
	~ClusterGraph() { clear(); }

	/// Clears all clusters.
	void clear();

	/// Returns a const-reference to the atomic structure to which this graph belongs.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns a const-reference to the catalog of patterns.
	const PatternCatalog& catalog() const { return _catalog; }

	/// Returns the list of all clusters known to this processor.
	const std::vector<Cluster*>& clusters() const { return _clusters; }

	/// Returns the list of all cluster transitions known to this processor.
	const std::list<ClusterTransition*>& clusterTransitions() const { return _clusterTransitions; }

	/// Inserts a new cluster into the graph.
	/// A unique ID for the new cluster is automatically generated.
	Cluster* createCluster(SuperPattern const* pattern);

	/// Inserts a new cluster into the graph with the given ID.
	/// It's the responsibility of the caller to ensure the uniqueness of the ID.
	Cluster* createCluster(ClusterId id, SuperPattern const* pattern);

	/// Looks up the cluster with the given ID.
	/// Returns NULL if the cluster with the given ID does not exist or is not known to this processor.
	Cluster* findCluster(ClusterId id) const;

	/// Registers a new cluster transition between two clusters A and B.
	/// This will create a new edge in the cluster graph unless an identical edge with the
	/// same transformation matrix already exists.
	/// The reverse transition B->A will also be created automatically.
	ClusterTransition* createClusterTransition(Cluster* clusterA, Cluster* clusterB, const Matrix3& tm, int distance = 1);

	/// Determines the transformation matrix that transforms vectors from cluster A to cluster B.
	/// For this, the cluster graph is searched for the shortest path connecting the two cluster nodes.
	/// If the two clusters are part of different disconnected components of the graph, then NULL is returned.
	/// Once a new transition between A and B has been found, it is cached by creating a new edge in the graph between
	/// the clusters A and B. Future queries for the same pair can then be answered efficiently.
	ClusterTransition* determineClusterTransition(Cluster* clusterA, Cluster* clusterB);

	/// Creates and returns the self-transition for a cluster (or returns the existing one).
	ClusterTransition* createSelfTransition(Cluster* cluster);

	/// Returns the concatenation of two cluster transitions (A->B->C ->  A->C).
	ClusterTransition* concatenateClusterTransitions(ClusterTransition* tAB, ClusterTransition* tBC);

	/// This will make every processor exchange its list of clusters with its neighboring processors.
	void mirrorAdjacentProcClusters(const PatternCatalog& catalog);

	/// Builds the complete cluster graph on the master processor by transmitting
	/// all nodes and edges from the remote processors to the master processor.
	void buildFullGraph();

	/// Replicates the entire cluster graph on every processor by
	/// broadcasting the master processor's complete list of nodes and
	/// transitions to all remote processors.
	void replicateGraphOnAllProcs();

	/// Combines split clusters from multiple processors into a single parent cluster.
	void mergeDistributedClusters();

	/// Returns whether this cluster was created on a remote processor.
	bool isRemoteCluster(Cluster* cluster) const { return cluster->id.processor != parallel().processor(); }

	/// Outputs performance statistics to the log file.
	void printSearchStatistics();

private:

	/// Adds another child cluster to the root cluster.
	void mergeDistributedClustersRecursive(ClusterTransition* childTransition);

	/// The atomic structure to which this graph belongs.
	const AtomicStructure& _structure;

	/// Catalog of patterns.
	const PatternCatalog& _catalog;

	/// The list of clusters. This includes clusters residing on this processors
	/// and clusters created on remote processors that are already known to this processor.
	std::vector<Cluster*> _clusters;

	/// Map from unique IDs to clusters. This is used for fast lookup of clusters
	/// in the findCluster() function.
	std::map<ClusterId, Cluster*> _clusterMap;

	/// The list of transitions between clusters.
	/// This list doesn't contain the self-transitions.
	std::list<ClusterTransition*> _clusterTransitions;

	/// Memory pool for Cluster instances.
	MemoryPool<Cluster> _clusterPool;

	/// Memory pool for ClusterTransition instances.
	MemoryPool<ClusterTransition> _clusterTransitionPool;

	/// This lists all pairs of clusters which have been found to be part of
	/// separate super clusters.
	std::set< std::pair<Cluster*, Cluster*> > _disconnectedClusters;

	/// Limits the maximum number of (original) transitions between two clusters
	/// when creating a direct transition between them.
	int _maximumClusterDistance;

	// Statistic counters:
	unsigned long _numPathQueries;
	unsigned long _numDisconnectedPairsQuick;
	unsigned long _numDisconnectedPairsSlow;
	unsigned long _numConnectedQuick;
	unsigned long _numConnectedSlow;
	unsigned long _numIsolated;
};

}; // End of namespace

#endif // __CA_CLUSTER_GRAPH_H
