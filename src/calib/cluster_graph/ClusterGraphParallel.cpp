///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ClusterGraph.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"
#include "../pattern/catalog/PatternCatalog.h"

using namespace std;

namespace CALib {

/// This structure is used by the function ClusterGraph::mirrorAdjacentProcClusters()
/// to exchange clusters with neighboring processors.
struct MirrorAdjacentClusterComm
{
	ClusterId id;			// The identifier the cluster.
	int superPatternIndex;	// The super pattern of the cluster.
};

/******************************************************************************
* This will make every processor exchange its list of clusters with its
* neighboring processors.
******************************************************************************/
void ClusterGraph::mirrorAdjacentProcClusters(const PatternCatalog& catalog)
{
	CALIB_ASSERT(parallel().isParallelMode());

	for(int dim = 0; dim < 3; dim++) {
		if(parallel().processorGrid(dim) == 1) continue;

		// Pack local clusters into send buffer.
		vector<MirrorAdjacentClusterComm> sendBuffer(clusters().size());
		vector<MirrorAdjacentClusterComm>::iterator sendItem = sendBuffer.begin();
		BOOST_FOREACH(Cluster* cluster, clusters()) {
			CALIB_ASSERT(cluster->pattern != NULL);
			sendItem->id = cluster->id;
			sendItem->superPatternIndex = cluster->pattern->index;
			++sendItem;
		}

		for(int dir = 0; dir <= 1; dir++) {
			// Don't need to exchange twice if there are only two processors in this direction.
			if(parallel().processorGrid(dim) == 2 && dir == 1 && structure().pbc(dim)) break;

			// Transfer cluster data.
			vector<MirrorAdjacentClusterComm> receiveBuffer;
			parallel().exchangeWithSize(dim, dir, structure().pbc(dim), sendBuffer, receiveBuffer);

			// Unpack receive list.
			BOOST_FOREACH(const MirrorAdjacentClusterComm& receivedCluster, receiveBuffer) {
				// The received clusters should not be known to this processor yet.
				CALIB_ASSERT(findCluster(receivedCluster.id) == NULL);
				// Create copy of the remote cluster on this processor.
				createCluster(receivedCluster.id, &catalog.superPatternByIndex(receivedCluster.superPatternIndex));
			}
		}
	}
}

/// This structure is used by the function ClusterGraph::buildFullGraph()
/// to send clusters to the master processor.
struct GatherClustersComm
{
	ClusterId id;			// The identifier the cluster.
	int superPatternIndex;	// The super pattern of the cluster.
	AtomInteger atomCount;	// The number of atoms that are part of the cluster.
	Matrix3	orientation;	// The average lattice orientation of the cluster.
};

/// This structure is used by the function ClusterGraph::buildFullGraph()
/// to send cluster transitions to the master processor.
struct TransitionComm
{
	ClusterId cluster1;			// The identifier of the first cluster.
	ClusterId cluster2;			// The identifier of the second cluster.
	Matrix3 transitionTM;		// The transition matrix.
};

/******************************************************************************
* Builds the complete cluster graph on the master processor by transmitting
* all nodes and edges from the remote processors to the master processor.
******************************************************************************/
void ClusterGraph::buildFullGraph()
{
	CALIB_ASSERT(parallel().isParallelMode());

	// If not the master processors, transfer all clusters and transitions to master processor.
	if(parallel().isMaster() == false) {

		// Collect all local clusters and cluster transitions which need to be sent to the master processor.
		vector<GatherClustersComm> clusterSendBuffer;
		clusterSendBuffer.reserve(clusters().size());
		BOOST_FOREACH(Cluster* cluster, clusters()) {
			// Do only transmit local clusters.
			if(isRemoteCluster(cluster))
				continue;
			// Pack the cluster into the send buffer.
			CALIB_ASSERT(cluster->pattern != NULL);
			GatherClustersComm sendItem;
			sendItem.id = cluster->id;
			sendItem.atomCount = cluster->atomCount;
			sendItem.orientation = cluster->orientation;
			sendItem.superPatternIndex = cluster->pattern->index;
			clusterSendBuffer.push_back(sendItem);
		}

		// Send cluster list to master processor.
		parallel().sendWithSize(clusterSendBuffer, 0);

		// Pack transitions into a send buffer.
		vector<TransitionComm> transitionSendBuffer(clusterTransitions().size());
		vector<TransitionComm>::iterator sendItem = transitionSendBuffer.begin();
		BOOST_FOREACH(ClusterTransition* t, clusterTransitions()) {
			sendItem->cluster1 = t->cluster1->id;
			sendItem->cluster2 = t->cluster2->id;
			sendItem->transitionTM = t->tm;
			CALIB_ASSERT(t->distance == 1);
			++sendItem;
		}

		// Send transitions to the master processor.
		parallel().sendWithSize(transitionSendBuffer, 0);
	}
	else {

		// On the master processor, receive clusters from all other processors.
		for(int proc = 1; proc < parallel().processorCount(); proc++) {

			// Receive cluster info.
			vector<GatherClustersComm> clusterReceiveBuffer;
			parallel().receiveWithSize(clusterReceiveBuffer, proc);

			// Unpack cluster list.
			BOOST_FOREACH(const GatherClustersComm& receivedCluster, clusterReceiveBuffer) {
				Cluster* cluster = findCluster(receivedCluster.id);
				const SuperPattern* remoteClusterPattern = &_catalog.superPatternByIndex(receivedCluster.superPatternIndex);
				if(cluster == NULL)
					cluster = createCluster(receivedCluster.id, remoteClusterPattern);
				CALIB_ASSERT(cluster->pattern == remoteClusterPattern);
				cluster->orientation = receivedCluster.orientation;
				cluster->atomCount = receivedCluster.atomCount;
			}
		}

		// On the master processor, receive cluster transitions from all other processors.
		for(int proc = 1; proc < parallel().processorCount(); proc++) {

			// Receive transition info.
			vector<TransitionComm> transitionsReceiveBuffer;
			parallel().receiveWithSize(transitionsReceiveBuffer, proc);

			// Unpack transitions list.
			BOOST_FOREACH(const TransitionComm& receivedTransition, transitionsReceiveBuffer) {

				// Lookup cluster 1.
				Cluster* cluster1 = findCluster(receivedTransition.cluster1);
				CALIB_ASSERT(cluster1 != NULL);

				// Lookup cluster 2.
				Cluster* cluster2 = findCluster(receivedTransition.cluster2);
				CALIB_ASSERT(cluster2 != NULL);

				// Create transition between the two clusters if it does not exist yet.
				createClusterTransition(cluster1, cluster2, receivedTransition.transitionTM);
			}
		}

		// Combine split clusters from multiple processors into a single parent cluster.
		mergeDistributedClusters();
	}
}

/******************************************************************************
* Combines split clusters from multiple processors into a single parent cluster.
******************************************************************************/
void ClusterGraph::mergeDistributedClusters()
{
	// Create child-root hierarchies of clusters.
	BOOST_FOREACH(Cluster* rootCluster, _clusters) {
		if(rootCluster->isRoot() == false) continue;
		for(ClusterTransition* t = rootCluster->transitions; t != NULL; t = t->next) {
			if(t->cluster2 == rootCluster) continue;
			if(t->cluster2->pattern != rootCluster->pattern) continue;
			if(t->cluster2->isRoot()) {
				CALIB_ASSERT(t->distance >= 1);
				mergeDistributedClustersRecursive(t);
				CALIB_ASSERT(t->cluster2->parentCluster() == rootCluster);
				CALIB_ASSERT(rootCluster->isRoot());
			}
		}
	}

	// Create root-root transitions from child-child transitions.
	BOOST_FOREACH(Cluster* rootCluster1, _clusters) {
		if(rootCluster1->isRoot() == false) continue;

		// Iterate over all children.
		for(ClusterTransition* childTransition1 = rootCluster1->transitions; childTransition1 != NULL; childTransition1 = childTransition1->next) {
			Cluster* child1 = childTransition1->cluster2;
			if(child1 == rootCluster1) continue;
			if(child1->parentTransition != childTransition1->reverse) continue;
			CALIB_ASSERT(child1->parentCluster() == rootCluster1);

			// Iterate over all transitions of the child cluster.
			for(ClusterTransition* t = child1->transitions; t != NULL; t = t->next) {
				Cluster* child2 = t->cluster2;
				if(child2->parentCluster() == rootCluster1) continue;
				CALIB_ASSERT(t->isSelfTransition() == false);

				// Create a transition from root to root.
				ClusterTransition* part = concatenateClusterTransitions(childTransition1, t);
				ClusterTransition* rootToRoot = concatenateClusterTransitions(part, child2->parentTransition);
				CALIB_ASSERT(rootToRoot->cluster1 == rootCluster1);
				CALIB_ASSERT(rootToRoot->cluster2->isRoot());
				CALIB_ASSERT(rootToRoot->isSelfTransition() == false);
			}
		}
	}
}

/******************************************************************************
* Adds another child cluster to the root cluster.
******************************************************************************/
void ClusterGraph::mergeDistributedClustersRecursive(ClusterTransition* childTransition)
{
	Cluster* rootCluster = childTransition->cluster1;
	Cluster* childCluster = childTransition->cluster2;
	CALIB_ASSERT(rootCluster->isRoot());
	CALIB_ASSERT(rootCluster == childTransition->cluster1);
	CALIB_ASSERT(childCluster->isRoot());

	childCluster->parentTransition = childTransition->reverse;
	CALIB_ASSERT(childCluster->parentCluster() == rootCluster);

	// Calculate weighted average of orientation matrix and center of mass.
	CAFloat w1 = (CAFloat)childCluster->atomCount / (CAFloat)max(childCluster->atomCount + rootCluster->atomCount, 1);
	CAFloat w2 = CAFloat(1) - w1;
	rootCluster->orientation = (w1 * (childCluster->orientation * childTransition->tm)) + (w2 * rootCluster->orientation);
	rootCluster->centerOfMass = (w1 * childCluster->centerOfMass) + (w2 * rootCluster->centerOfMass);
	rootCluster->atomCount += childCluster->atomCount;

	// Recursively add more child clusters to the root.
	for(ClusterTransition* t = childCluster->transitions; t != NULL; t = t->next) {
		if(t->cluster2 == rootCluster) continue;
		if(t->cluster2->pattern == rootCluster->pattern) {
			if(t->cluster2->isRoot()) {
				CALIB_ASSERT(t->distance >= 1);
				ClusterTransition* newTransition = concatenateClusterTransitions(childTransition, t);
				mergeDistributedClustersRecursive(newTransition);
				CALIB_ASSERT(t->cluster2->parentCluster() == rootCluster);
				CALIB_ASSERT(rootCluster->isRoot());
			}
		}
	}
}

/// This structure is used by the function ClusterGraph::replicateGraphOnAllProcs()
/// to broadcast clusters from the master processor to all other processors.
struct BroadcastClustersComm
{
	ClusterId id;			// The local identifier and processor of the cluster.
	int outputId;			// The root cluster id.
	int superPatternIndex;	// The super pattern of the cluster.
	AtomInteger atomCount;	// The number of atoms that are part of the cluster.
	Matrix3	orientation;	// The average lattice orientation of the cluster.
};

/// This structure is used by the function ClusterGraph::replicateGraphOnAllProcs()
/// to send cluster transitions to the other processor.
struct BroadcastTransitionComm
{
	ClusterId cluster1;			// The identifier of the first cluster.
	ClusterId cluster2;			// The identifier of the second cluster.
	Matrix3 transitionTM;		// The transition matrix.
	bool isParentTransition;	// True if this is a transition from a child to a parent cluster.
	bool isChildTransition;		// True if this is a transition from a parent to a child cluster.
};

/******************************************************************************
* Replicates the entire cluster graph on every processor by
* broadcasting the master processor's complete list of nodes and
* transitions to all remote processors.
******************************************************************************/
void ClusterGraph::replicateGraphOnAllProcs()
{
	CALIB_ASSERT(parallel().isParallelMode());

	if(parallel().isMaster()) {
		// Assign index IDs to root clusters.
		int outputId = 0;
		BOOST_FOREACH(Cluster* cluster, _clusters) {
			if(cluster->isRoot())
				cluster->outputId = outputId++;
		}
	}
	else {
		// Clear old cluster transitions.
		BOOST_FOREACH(Cluster* cluster, _clusters) {
			cluster->transitions = NULL;
			cluster->parentTransition = NULL;
		}
		_clusterTransitions.clear();
		_clusterTransitionPool.clear();
	}

	if(parallel().processorCount() != 1) {

		// Broadcast full list of clusters back to other processors.
		vector<BroadcastClustersComm> clusterBuffer(clusters().size());

		// On the master processor, pack all clusters into a send buffer.
		if(parallel().isMaster()) {
			vector<BroadcastClustersComm>::iterator sendItem = clusterBuffer.begin();
			BOOST_FOREACH(Cluster* cluster, clusters()) {
				CALIB_ASSERT(cluster->pattern != NULL);
				sendItem->id = cluster->id;
				sendItem->superPatternIndex = cluster->pattern->index;
				sendItem->atomCount = cluster->atomCount;
				sendItem->orientation = cluster->orientation;
				sendItem->outputId = cluster->outputId;
				++sendItem;
			}
		}
		parallel().broadcastWithSize(clusterBuffer);

		// Unpack cluster information on other processors.
		if(parallel().isMaster() == false) {
			BOOST_FOREACH(const BroadcastClustersComm& receivedCluster, clusterBuffer) {
				Cluster* cluster = findCluster(receivedCluster.id);
				const SuperPattern* remoteClusterPattern = &_catalog.superPatternByIndex(receivedCluster.superPatternIndex);
				if(cluster == NULL)
					cluster = createCluster(receivedCluster.id, remoteClusterPattern);
				else {
					CALIB_ASSERT(cluster->parentTransition == NULL);
					CALIB_ASSERT(cluster->pattern == remoteClusterPattern);
					cluster->parentTransition = createSelfTransition(cluster);
				}
				cluster->outputId = receivedCluster.outputId;
				cluster->atomCount = receivedCluster.atomCount;
				cluster->orientation = receivedCluster.orientation;
			}
		}

		// Broadcast full list of transitions back to other processors.
		vector<BroadcastTransitionComm> transitionsBuffer(clusterTransitions().size());

		// On the master processor, pack all cluster transitions into a send buffer.
		if(parallel().isMaster()) {
			vector<BroadcastTransitionComm>::iterator sendItem = transitionsBuffer.begin();
			BOOST_FOREACH(ClusterTransition* transition, clusterTransitions()) {
				sendItem->cluster1 = transition->cluster1->id;
				sendItem->cluster2 = transition->cluster2->id;
				sendItem->transitionTM = transition->tm;
				sendItem->isParentTransition = (transition->cluster1->parentTransition == transition);
				sendItem->isChildTransition = (transition->cluster2->parentTransition == transition->reverse);
				++sendItem;
			}
			CALIB_ASSERT(sendItem == transitionsBuffer.end());
		}
		parallel().broadcastWithSize(transitionsBuffer);

		// Unpack transition information on other processors.
		if(parallel().isMaster() == false) {
			BOOST_FOREACH(const BroadcastTransitionComm& receivedTransition, transitionsBuffer) {
				// Lookup cluster 1.
				Cluster* cluster1 = findCluster(receivedTransition.cluster1);
				CALIB_ASSERT(cluster1 != NULL);

				// Lookup cluster 2.
				Cluster* cluster2 = findCluster(receivedTransition.cluster2);
				CALIB_ASSERT(cluster2 != NULL);
				CALIB_ASSERT(cluster1 != cluster2);

				// Create transition between the two clusters if it does not exist yet.
				ClusterTransition* transition = createClusterTransition(cluster1, cluster2, receivedTransition.transitionTM);

				if(receivedTransition.isParentTransition) {
					CALIB_ASSERT(!receivedTransition.isChildTransition);
					cluster1->parentTransition = transition;
					CALIB_ASSERT(transition->cluster1->pattern == transition->cluster2->pattern);
				}
				else if(receivedTransition.isChildTransition) {
					cluster2->parentTransition = transition->reverse;
					CALIB_ASSERT(transition->cluster1->pattern == transition->cluster2->pattern);
				}
			}

			BOOST_FOREACH(Cluster* cluster, _clusters) {
				CALIB_ASSERT(cluster->isRoot() == (cluster->outputId != -1));
			}
		}
	}
}


}; // End of namespace
