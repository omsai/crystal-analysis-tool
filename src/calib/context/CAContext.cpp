///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CAContext.h"
#include "../util/NULLStream.h"

using namespace std;

namespace CALib {

static NullStream nullStream;

/******************************************************************************
* CAContext constructor.
*****************************************************************************/
CAContext::CAContext() : _parallelMode(false), _allProcessorsOutput(false)
{
#ifdef CALIB_USE_MPI
	_mpiIsInitialized = false;
#endif

	// Send all log and error messages to nowhere by default.
	_msgLogger = &nullStream;
}

/******************************************************************************
* Destructor.
******************************************************************************/
CAContext::~CAContext()
{
	// Reset console color on exit.
	if(isParallelMode() && allProcOutput()) {
		*_msgLogger << "\033[0m";
	}

#ifdef CALIB_USE_MPI
	if(_mpiIsInitialized) {
		// Shutdown MPI.
		MPI_Finalize();
	}
#endif
}

#ifdef CALIB_USE_MPI

/*********************************************************************
* Initializes the main object.
**********************************************************************/
void CAContext::initContext(int argc, char* argv[], bool parallelMode, MPI_Comm communicator)
{
	// Initialize MPI.
	if(MPI_Init(&argc, &argv) != MPI_SUCCESS) {
		raiseErrorOne("Failed to initialize Message Passing Interface (MPI).");
	}
	_mpiIsInitialized = true;
	this->_parallelMode = parallelMode;

	_mpiComm = communicator;

	MPI_Comm_rank(_mpiComm, &_processorMe);
	MPI_Comm_size(_mpiComm, &_processorCount);

	for(int dim = 0; dim < 3; dim++) {
		_processorGrid[dim] = -1;
		_processorLocation[dim] = -1;
		_processorNeighbors[dim][0] = -1;
		_processorNeighbors[dim][1] = -1;
	}

	if(isMaster())
		setMsgLogger(std::cout);
}

#else

/*********************************************************************
* Initializes the main object.
**********************************************************************/
void CAContext::initContext(int argc, char* argv[], bool parallelMode)
{
	this->_parallelMode = parallelMode;
	setMsgLogger(std::cout);
	for(int dim = 0; dim < 3; dim++) {
		_processorGrid[dim] = 1;
		_processorLocation[dim] = 0;
	}
}

#endif

/*********************************************************************
* Controls whether log messages from all processors are sent to the console.
**********************************************************************/
void CAContext::setAllProcOutput(bool enable)
{
	_allProcessorsOutput = enable;
	if(!isMaster()) {
		if(enable)
			setMsgLogger(std::cout);
		else
			_msgLogger = &nullStream;
	}
}

/*********************************************************************
* Returns the output stream to which log messages are sent.
**********************************************************************/
ostream& CAContext::msgLogger() const
{
	if(isParallelMode() && allProcOutput()) {
		if(processor()) {
			*_msgLogger << "\033[" << (30 + (processor() % 7)) << "m";
		}
		else {
			*_msgLogger << "\033[0m";
		}
		*_msgLogger << "PROC " << processor() << ": ";
	}
	return *_msgLogger;
}

/*********************************************************************
* Assigns spatial domains to the available processors.
**********************************************************************/
void CAContext::setupProcessorGrid(const Matrix3& simulationCell)
{
#ifdef CALIB_USE_MPI
	CALIB_ASSERT(processorCount() >= 1);

	// Check if already set up.
	if(processorGrid(0) >= 1) {
		CALIB_ASSERT(processorGrid(0) * processorGrid(1) * processorGrid(2) == processorCount());
		return;
	}

	// Calculate surface areas of simulation cell.
	CAFloat areas[3];
	areas[0] = simulationCell.col(0).cross(simulationCell.col(1)).norm();
	areas[1] = simulationCell.col(0).cross(simulationCell.col(2)).norm();
	areas[2] = simulationCell.col(1).cross(simulationCell.col(2)).norm();

	CAFloat bestsurf = CAFLOAT_MAX;
	for(int ipx = 1; ipx <= processorCount(); ipx++) {
		if(processorCount() % ipx) continue;
		for(int ipy = 1; ipy <= processorCount()/ipx; ipy++) {
			if((processorCount() / ipx) % ipy) continue;
			int ipz = processorCount() / ipx / ipy;
			CAFloat surf = areas[0]/ipx/ipy + areas[1]/ipx/ipz + areas[2]/ipy/ipz;
			if(surf < bestsurf) {
				bestsurf = surf;
				_processorGrid[0] = ipx;
				_processorGrid[1] = ipy;
				_processorGrid[2] = ipz;
			}
		}
	}
	CALIB_ASSERT(processorGrid(0) * processorGrid(1) * processorGrid(2) == processorCount());

	MPI_Comm cartesian;
	int periods[3] = {1,1,1};
	MPI_Cart_create(communicator(), 3, _processorGrid, periods, 0, &cartesian);
	int cartesian_rank;
	MPI_Comm_rank(cartesian, &cartesian_rank);
	if(cartesian_rank != processor())
		raiseErrorOne("The Cartesian processor rank differs from the original rank.");
	MPI_Cart_get(cartesian, 3, _processorGrid, periods, &_processorLocation[0]);

	for(int dim = 0; dim < 3; dim++) {
		MPI_Cart_shift(cartesian, dim, 1, &_processorNeighbors[dim][0], &_processorNeighbors[dim][1]);
		_processorStrideNeighbors[dim][0].resize(processorGrid(dim));
		_processorStrideNeighbors[dim][1].resize(processorGrid(dim));
		_processorStrideNeighbors[dim][0][0] = processor();
		_processorStrideNeighbors[dim][1][0] = processor();
		for(int stride = 1; stride < processorGrid(dim); stride++)
			MPI_Cart_shift(cartesian, dim, stride, &_processorStrideNeighbors[dim][0][stride], &_processorStrideNeighbors[dim][1][stride]);
	}

	MPI_Comm_free(&cartesian);
#endif
}

#ifndef CALIB_USE_MPI

/*********************************************************************
* Sets the decomposition grid in pseudo-parallel mode.
**********************************************************************/
void CAContext::setupPseudoProcessorGrid(int dimensions[3], const Vector3I& mylocation)
{
	_processorGrid[0] = dimensions[0];
	_processorGrid[1] = dimensions[1];
	_processorGrid[2] = dimensions[2];
	_processorLocation = mylocation;

	if(_processorGrid[0] < 1 || _processorLocation[0] < 0 || _processorLocation[0] >= _processorGrid[0] ||
			_processorGrid[1] < 1 || _processorLocation[1] < 0 || _processorLocation[1] >= _processorGrid[1] ||
			_processorGrid[2] < 1 || _processorLocation[2] < 0 || _processorLocation[2] >= _processorGrid[2])
		raiseErrorAll("Invalid spatial decomposition.");
}

#endif

#ifdef CALIB_USE_MPI

/*********************************************************************
* Performs data exchange with neighboring processors.
**********************************************************************/
void CAContext::exchange(unsigned int dim, unsigned int dir, bool pbc, void* sendBuffer, int sendCount, void* receiveBuffer, int receiveCount, MPI_Datatype dataType) const
{
	MPI_Status status;
	unsigned int oppositeDir = dir ? 0 : 1;
	if(shouldSendOrReceive(dim, dir, pbc) == false) {
		CALIB_ASSERT(shouldSendOrReceive(dim, oppositeDir, pbc) == true);
		MPI_Recv(receiveBuffer, receiveCount, dataType, processorNeighbor(dim, oppositeDir), 0, communicator(), &status);
#ifdef DEBUG_CRYSTAL_ANALYSIS
		int actuallyReceived;
		MPI_Get_count(&status, dataType, &actuallyReceived);
		CALIB_ASSERT(actuallyReceived == receiveCount);
#endif
	}
	else if(shouldSendOrReceive(dim, oppositeDir, pbc) == false) {
		CALIB_ASSERT(shouldSendOrReceive(dim, dir, pbc) == true);
		MPI_Send(sendBuffer, sendCount, dataType, processorNeighbor(dim, dir), 0, communicator());
	}
	else {
		MPI_Sendrecv(sendBuffer, sendCount, dataType, processorNeighbor(dim, dir), 0,
				receiveBuffer, receiveCount, dataType, processorNeighbor(dim, oppositeDir), 0,
					 communicator(), &status);
#ifdef DEBUG_CRYSTAL_ANALYSIS
		int actuallyReceived;
		MPI_Get_count(&status, dataType, &actuallyReceived);
		CALIB_ASSERT(actuallyReceived == receiveCount);
#endif
	}
}
#endif

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function must be called by all processors simultaneously.
**********************************************************************/
void CAContext::raiseErrorAll(const char* errorFormatString, ...)
{
	va_list ap;
	va_start(ap, errorFormatString);
	char buffer[2048];
	vsprintf(buffer, errorFormatString, ap);
	va_end(ap);
	raiseErrorAllImpl(buffer);
}

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function can be called by a single processor.
**********************************************************************/
void CAContext::raiseErrorOne(const char* errorFormatString, ...)
{
	va_list ap;
	va_start(ap, errorFormatString);
	char buffer[2048];
	vsprintf(buffer, errorFormatString, ap);
	va_end(ap);
	raiseErrorOneImpl(buffer);
}

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function must be called by all processors simultaneously.
**********************************************************************/
void CAContext::raiseErrorAllImpl(const char* errorMessage)
{
	barrier();
	if(isMaster()) {
		std::cout << flush;
		std::cerr << "ERROR: " << errorMessage << endl;
	}
#ifdef CALIB_USE_MPI
	if(_mpiIsInitialized)
		MPI_Finalize();
#endif
	exit(1);
}

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function can be called by a single processor.
**********************************************************************/
void CAContext::raiseErrorOneImpl(const char* errorMessage)
{
	std::cout << flush;
	if(isParallelMode())
		std::cerr << "ERROR on processor " << processor() << ": " << errorMessage << endl;
	else
		std::cerr << "ERROR: " << errorMessage << endl;
#ifdef CALIB_USE_MPI
	if(_mpiIsInitialized)
		MPI_Abort(_mpiComm, 1);
#endif
	exit(1);
}

}; // End of namespace
