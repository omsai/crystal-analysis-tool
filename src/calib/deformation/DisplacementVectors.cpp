///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DisplacementVectors.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor.
******************************************************************************/
DisplacementVectors::DisplacementVectors(CAContext& context, const AtomicStructure& structure1, const AtomicStructure& structure2) :
		ContextReference(context), _structure1(structure1), _structure2(structure2)
{
}

/******************************************************************************
* Sets up the mapping between atoms in the initial and final configuration.
******************************************************************************/
void DisplacementVectors::prepare()
{
	// Build two-way map between atoms in the initial configuration and the final configuration.
	mapAtoms(structure1(), structure2(), _initialToFinal);
	mapAtoms(structure2(), structure1(), _finalToInitial);
}

/******************************************************************************
* Build two-way map between atoms in the initial configuration and the final
* configuration.
******************************************************************************/
void DisplacementVectors::mapAtoms(const AtomicStructure& s1, const AtomicStructure& s2, std::vector<int>& map)
{
	CALIB_ASSERT(s1.neighborMode() == AtomicStructure::GHOST_ATOMS);
	CALIB_ASSERT(s2.neighborMode() == AtomicStructure::GHOST_ATOMS);

	// Make sure both configurations have the same number of atoms, i.e. represent the same system.
	if(s1.numTotalAtoms() != s2.numTotalAtoms())
		context().raiseErrorAll("Cannot calculate atomic displacements. The initial and final configurations do not contain the same number of atoms.");

	// Build two-way map between atoms in the initial configuration and the final configuration.
	int atomCount1 = s1.numLocalAtoms() + s1.numGhostAtoms();
	int atomCount2 = s2.numLocalAtoms() + s2.numGhostAtoms();

	// Start with building a lookup table for atom tags to atom indices for the second structure.
	multimap<AtomInteger, int> tagMap;
	for(int i = 0; i < atomCount2; i++)
		tagMap.insert(make_pair(s2.atomOriginalTag(i), i));

	// Use lookup table to determine the mapping.
	map.resize(atomCount1);
	for(int atom1 = 0; atom1 < atomCount1; atom1++) {
		map[atom1] = -1;
		// Find the image of the atom that is closest to its original position.
		CAFloat minDistSq = CAFLOAT_MAX;
		pair<multimap<AtomInteger, int>::const_iterator, multimap<AtomInteger, int>::const_iterator > range = tagMap.equal_range(s1.atomOriginalTag(atom1));
		for(multimap<AtomInteger, int>::const_iterator atom2 = range.first; atom2 != range.second; ++atom2) {
			CALIB_ASSERT(s2.atomOriginalTag(atom2->second) == s1.atomOriginalTag(atom1));

			Vector3 delta = s2.absoluteToReducedPoint(s2.atomPosition(atom2->second))
					 - s1.absoluteToReducedPoint(s1.atomPosition(atom1));
			if(s1.isReducedWrappedVector(delta)) continue;

			CAFloat distSq = delta.squaredNorm();
			if(distSq < minDistSq) {
				map[atom1] = atom2->second;
				minDistSq = distSq;
			}
		}
	}
}

}; // End of namespace
