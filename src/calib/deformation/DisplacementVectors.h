///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DISPLACEMENT_VECTORS_H
#define __CA_DISPLACEMENT_VECTORS_H

#include "../CALib.h"
#include "../atomic_structure/AtomicStructure.h"

namespace CALib {

/**
 * Calculates the displacement vectors of atoms.
 */
class DisplacementVectors : public ContextReference
{
public:

	/// Constructor.
	DisplacementVectors(CAContext& context, const AtomicStructure& structure1, const AtomicStructure& structure2);

	/// Sets up the mapping between atoms in the initial and final configuration.
	void prepare();

	/// Returns the initial configuration of the system.
	const AtomicStructure& structure1() const { return _structure1; }

	/// Returns the final configuration of the system.
	const AtomicStructure& structure2() const { return _structure2; }

	/// Returns an index map that maps atom indices from the reference to the deformed configuration.
	const std::vector<int>& initialToFinalMap() const { return _initialToFinal; }

	/// Returns an index map that maps atom indices from the deformed to reference configuration.
	const std::vector<int>& finalToInitialMap() const { return _finalToInitial; }

	/// Given an atom from the first configuration, returns the corresponding atom from the second configuration.
	/// Returns -1 if the corresponding atom could not be determined (because it moved too far and migrated to
	/// a different processor or the mapping is ambiguous).
	inline int mapInitialToFinal(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < _initialToFinal.size());
		return _initialToFinal[atomIndex];
	}

	/// Given an atom from the second configuration, returns the corresponding atom from the first configuration.
	/// Returns -1 if the corresponding atom could not be determined (because it has moved too far and migrated to
	/// a different processor or the mapping is ambiguous).
	inline int mapFinalToInitial(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < _finalToInitial.size());
		return _finalToInitial[atomIndex];
	}

private:

	/// Generate mapping between atoms in the first configuration and the second configuration.
	void mapAtoms(const AtomicStructure& s1, const AtomicStructure& s2, std::vector<int>& map);

private:

	/// The initial configuration of the system.
	const AtomicStructure& _structure1;

	/// The final configuration of the system.
	const AtomicStructure& _structure2;

	/// Maps atom indices from initial to final configuration.
	std::vector<int> _initialToFinal;

	/// Maps atom indices from final to initial configuration.
	std::vector<int> _finalToInitial;
};

}; // End of namespace

#endif // __CA_DISPLACEMENT_VECTORS_H

