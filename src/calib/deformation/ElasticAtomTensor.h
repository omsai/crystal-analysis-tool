///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ELASTIC_ATOM_TENSOR_H
#define __CA_ELASTIC_ATOM_TENSOR_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../pattern/superpattern/SuperPatternAnalysis.h"

namespace CALib {

/**
 * Calculates the elastic deformation gradient tensor for an atom.
 */
class ElasticAtomTensor : public ContextReference
{
public:

	/// Constructor.
	ElasticAtomTensor(const SuperPatternAnalysis& patternAnalysis) :
		ContextReference(patternAnalysis.context()), _structure(patternAnalysis.structure()), _patternAnalysis(patternAnalysis),
		_neighborList(patternAnalysis.neighborList()) {}

	/// Returns the results of the pattern analysis.
	const SuperPatternAnalysis& patternAnalysis() const { return _patternAnalysis; }

	/// Calculates the elastic deformation tensor for the given atom.
	/// Returns true if the tensor was calculated. Returns false if it could not be calculated for this atom.
	bool calculateElasticTensor(int atomIndex, Matrix3& Fe) const {
		if(patternAnalysis().atomCluster(atomIndex) == NULL)
			return false;

		const SuperPatternNode& node = patternAnalysis().atomPatternNode(atomIndex);
		FrameTransformation tm;
		for(int nodeNeighborIndex = 0; nodeNeighborIndex < node.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
			int atomNeighborIndex = patternAnalysis().nodeNeighborToAtomNeighbor(atomIndex, nodeNeighborIndex);
			tm.addVector(
					node.neighbors[nodeNeighborIndex].referenceVector,
					_neighborList.neighborVector(atomIndex, atomNeighborIndex));
		}
		Fe = tm.computeAverageTransformation();
		return true;
	}

private:

	/// The atomic structure.
	const AtomicStructure& _structure;

	/// The results of the pattern analysis.
	const SuperPatternAnalysis& _patternAnalysis;

	/// The neighbor list for the atomic structure.
	const NeighborList& _neighborList;
};

}; // End of namespace

#endif // __CA_ELASTIC_ATOM_TENSOR_H

