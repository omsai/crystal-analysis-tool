///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ELASTIC_FIELD_H
#define __CA_ELASTIC_FIELD_H

#include "../CALib.h"
#include "Field.h"
#include "../tessellation/ElasticMapping.h"

namespace CALib {

/**
 * Calculates the elastic gradient field based on the tessellation of the atomistic system.
 */
class ElasticField : public Field
{
public:

	/// Constructor.
	ElasticField(CAContext& context, AtomicStructure& structure, DelaunayTessellation& tessellation);

	/// Calculates the elastic deformation tensor field.
	void calculateElasticField(const ElasticMapping& elasticMapping);

	/// Returns the elastic deformation gradient tensors calculated for the local tessellation elements.
	const std::vector<Matrix3>& elasticDeformationGradients() const { return _elasticDeformationGradients; }

private:

	/// The elastic deformation gradient tensors.
	std::vector<Matrix3> _elasticDeformationGradients;
};

}; // End of namespace

#endif // __CA_ELASTIC_FIELD_H

