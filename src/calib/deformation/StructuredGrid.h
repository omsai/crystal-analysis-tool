///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_STUCTURED_GRID_H
#define __CRYSTAL_ANALYSIS_STUCTURED_GRID_H

#include "../CALib.h"
#include "../tessellation/Tessellation.h"
#include "../context/CAContext.h"
#include "StrainCalculator.h"

namespace CALib {

class AtomicStructure;

/**
 * A coarse structured element grid onto which the values of the
 * unstructured Delaunay grid are projected.
 */
class StructuredGrid : public ContextReference
{
public:

	struct DataPoint {

		/// The total deformation gradient tensor.
		Matrix3 deformationGradient;

		/// The plastic deformation gradient tensor.
		Matrix3 plasticDeformationGradient;

		/// The total volume of material projected to this data point.
		CAFloat volume;
	};

public:

	/// Constructor,
	StructuredGrid(CAContext& _context, AtomicStructure& _structure, CAFloat cellSize);

	/// Projects the cell values of the unstructured grid onto this structured grid.
	void project(const StrainCalculator& strainCalculator);

	/// Outputs the grid to a VTK file for visualization.
	void writeToVTKFile(std::ostream& stream) const;

	/// Determine the grid point that is closest to the given world space point.
	Point3I absoluteToGrid(const Point3& p) const;

private:

	/// Project the volume of a single tetrahedron cell onto one point of the structured grid.
	CAFloat projectCell(const Point3I& gridPoint, Tessellation::CellHandle cell);

	/// Clips a tetrahedron at a single clipping plane.
	static CAFloat clipTetrahedron(const Point3 vertices[4], const Vector3* clipPlaneNormals, const CAFloat* clipPlaneDists, int clipPlaneIndex);

private:

	/// The simulation cell.
	AtomicStructure& structure;

	/// The dimension of the grid in each direction.
	Vector3I dimensions;

	/// The data points of this grid.
	std::vector<DataPoint> data;
	std::vector<DataPoint> globalData;

	/// Transforms absolute coordinates to grid coordinates and vice versa.
	Matrix3 absoluteToGridTM;
	Matrix3 gridToAbsoluteTM;
	Point3 absoluteToGridOffset;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_STUCTURED_GRID_H
