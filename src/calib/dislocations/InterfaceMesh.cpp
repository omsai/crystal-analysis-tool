///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "InterfaceMesh.h"
#include "../tessellation/DelaunayTessellation.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Builds the interface mesh.
******************************************************************************/
void InterfaceMesh::build()
{
	// Assign tetrahedra to good or bad crystal region.
	classifyTetrahedra();

	// Create the triangular mesh facets that separate good from bad tetrahedra.
	createSeparatingFacets();

	// Link half-edges to opposite half-edges.
	linkHalfEdges();
}

/******************************************************************************
* Generates the list of tetrahedra in the tessellation and classifies each one
* as being either good or bad.
******************************************************************************/
void InterfaceMesh::classifyTetrahedra()
{
	// Create an internal list of tetrahedra by mirroring all cells from the DelaunayTessellation class.
	_tetrahedra.resize(tessellation().number_of_tetrahedra());
	DelaunayTessellation::CellIterator cell = tessellation().begin_cells();
	BOOST_FOREACH(Tetrahedron& tet, _tetrahedra) {
		tet.cell = cell;
		CALIB_ASSERT(tessellation().cellIndex(cell) < _tetrahedra.size());

		// Determine whether the tetrahedron belongs to the good or the bad crystal region.
		tet.isGood = elasticMapping().isElasticMappingCompatible(cell);

		++cell;
	}
	CALIB_ASSERT(cell == tessellation().end_cells());
}

/******************************************************************************
* Creates the mesh facets separating good and bad tetrahedra.
******************************************************************************/
void InterfaceMesh::createSeparatingFacets()
{
	// Map from atoms to mesh nodes.
	vector<Node*> atomToNodes(structure().numLocalAtoms() + structure().numGhostAtoms());

	// Look for pairs of adjacent tessellation cells, with one being a bad and one being a good cell.
	// Add a triangle facet to the interface mesh that separates the two cell.
	BOOST_FOREACH(Tetrahedron& tet, _tetrahedra) {
		// Start with the bad tetrahedra.
		if(tet.isGood) continue;

		// Iterate over the four faces of the tetrahedron cell.
		for(int f = 0; f < 4; f++) {
			tet.meshFacets[f] = NULL;

			// Check if the neighboring tetrahedron belongs to the lattice region.
			DelaunayTessellation::CellHandle adjacentCell = tessellation().mirrorCell(tet.cell, f);
			const Tetrahedron& adjacentTet = _tetrahedra[tessellation().cellIndex(adjacentCell)];
			if(adjacentTet.isGood == false)
				continue;

			// Look up or, if necessary, create mesh nodes for the three vertices.
			Node* facetNodes[3];
			int facetAtoms[3];
			for(int facetVertex = 0; facetVertex < 3; facetVertex++) {
				DelaunayTessellation::VertexHandle vertex = tet.cell->vertex(DelaunayTessellation::cellFacetVertexIndex(f, facetVertex));
				int atomIndex = vertex->info();
				facetAtoms[facetVertex] = atomIndex;
				CALIB_ASSERT(atomIndex >= 0 && atomIndex < atomToNodes.size());
				if(atomToNodes[atomIndex] == NULL) {
					atomToNodes[atomIndex] = createNode(structure().atomTag(atomIndex),
							structure().atomPosition(atomIndex), structure().isGhostAtom(atomIndex));
				}
				facetNodes[facetVertex] = atomToNodes[atomIndex];
			}

			// Create a new mesh facet and three half-edges.
			Facet* facet = createFacet(isGhostMeshFacet(facetNodes));
			for(int facetVertex = 0; facetVertex < 3; facetVertex++) {
				Node* node1 = facetNodes[facetVertex];
				Node* node2 = facetNodes[(facetVertex+1) % 3];
				int atom1 = facetAtoms[facetVertex];
				int atom2 = facetAtoms[(facetVertex+1) % 3];
				ElasticMapping::TessellationEdge* edge = elasticMapping().findEdge(atom1, atom2);
				CALIB_ASSERT(edge != NULL && edge->hasClusterVector());
				facet->edges[facetVertex] = createHalfEdge(node1, node2, edge->clusterVector, edge->clusterTransition);
				facet->edges[facetVertex]->facet = facet;
			}
			tet.meshFacets[f] = facet;
		}
	}
}

/******************************************************************************
* Links half-edges to opposite half-edges together.
******************************************************************************/
void InterfaceMesh::linkHalfEdges()
{
	BOOST_FOREACH(Tetrahedron& tet, _tetrahedra) {
		if(tet.isGood) continue;
		for(int f = 0; f < 4; f++) {
			Facet* facet = tet.meshFacets[f];
			if(facet == NULL) continue;

			for(int e = 0; e < 3; e++) {
				int vertexIndex1 = DelaunayTessellation::cellFacetVertexIndex(f, e);
				int vertexIndex2 = DelaunayTessellation::cellFacetVertexIndex(f, (e+1)%3);
#if 1
				DelaunayTessellation::FacetCirculator circulator_start = tessellation().incident_facets(tet.cell, vertexIndex2, vertexIndex1, tet.cell, f);
				DelaunayTessellation::FacetCirculator circulator = circulator_start;
				CALIB_ASSERT(circulator->first == tet.cell);
				CALIB_ASSERT(circulator->second == f);
				--circulator;
				CALIB_ASSERT(circulator != circulator_start);
				Facet* adjacentFacet = NULL;
				do {
					const Tetrahedron& tet = _tetrahedra[tessellation().cellIndex(circulator->first)];
					if(tet.isGood) {
						pair<DelaunayTessellation::CellHandle,int> mirrorFacet = tessellation().mirrorFacet(circulator);
						const Tetrahedron& mirrorTet = _tetrahedra[tessellation().cellIndex(mirrorFacet.first)];
						CALIB_ASSERT(!mirrorTet.isGood);
						Facet* facet2 = mirrorTet.meshFacets[mirrorFacet.second];
						CALIB_ASSERT(facet2 != NULL);
						adjacentFacet = facet2;
						break;
					}
					--circulator;
				}
				while(circulator != circulator_start);
#else
				DelaunayTessellation::FacetCirculator circulator_start = tessellation().incident_facets(tet.cell, vertexIndex2, vertexIndex1, tet.cell, f);
				DelaunayTessellation::FacetCirculator circulator = circulator_start;
				CALIB_ASSERT(circulator->first == tet.cell);
				CALIB_ASSERT(circulator->second == f);
				++circulator;
				CALIB_ASSERT(_tetrahedra[tessellation().cellIndex(circulator->first)].isGood);
				Facet* adjacentFacet = NULL;
				do {
					pair<DelaunayTessellation::CellHandle,int> mirrorFacet = tessellation().mirrorFacet(circulator);
					const Tetrahedron& mirrorTet = _tetrahedra[tessellation().cellIndex(mirrorFacet.first)];
					if(mirrorTet.isGood == false) {
						Facet* facet2 = mirrorTet.meshFacets[mirrorFacet.second];
						CALIB_ASSERT(facet2 != NULL);
						adjacentFacet = facet2;
						break;
					}
					++circulator;
				}
				while(circulator != circulator_start);
#endif
				CALIB_ASSERT(adjacentFacet != NULL);
				CALIB_ASSERT(adjacentFacet != facet);
				CALIB_ASSERT(adjacentFacet->hasVertex(facet->vertex(e)));
				CALIB_ASSERT(adjacentFacet->hasVertex(facet->vertex((e+1)%3)));
				Edge* oppositeEdge = adjacentFacet->edges[adjacentFacet->vertexIndex(facet->vertex((e+1)%3))];
				CALIB_ASSERT(oppositeEdge->node2() == facet->vertex(e));
				CALIB_ASSERT(oppositeEdge->clusterTransition == facet->edges[e]->clusterTransition->reverse);
				facet->edges[e]->oppositeEdge = oppositeEdge;
			}
		}
	}
}


/******************************************************************************
* Determines whether the triangular facet specified by the three nodes is
* not owned by the local processor.
******************************************************************************/
bool InterfaceMesh::isGhostMeshFacet(Node* facetNodes[3])
{
	// Find node with lowest ID.
	Node* head = facetNodes[0];
	if(facetNodes[1]->tag < head->tag) head = facetNodes[1];
	if(facetNodes[2]->tag < head->tag) head = facetNodes[2];
	// This node determines whether this is a ghost facet.
	return head->flags.test(Node::IS_GHOST_NODE);
}


/******************************************************************************
* Adds a new node to the mesh.
******************************************************************************/
InterfaceMesh::Node* InterfaceMesh::createNode(AtomInteger tag, const Vector3& pos, bool isGhostNode)
{
	Node* node = _nodePool.construct(pos);
	node->index = _nodes.size();
	node->tag = tag;
	if(isGhostNode)
		node->flags.set(Node::IS_GHOST_NODE);
	else
		_numLocalNodes++;
	_nodes.push_back(node);
	return node;
}

/******************************************************************************
* Adds a new facet to the mesh.
******************************************************************************/
InterfaceMesh::Facet* InterfaceMesh::createFacet(bool isGhostFacet)
{
	Facet* facet = _facetPool.construct();
	if(isGhostFacet)
		facet->flags.set(Facet::IS_GHOST_FACET);
	else
		_numLocalFacets++;
	_facets.push_back(facet);
	return facet;
}

/******************************************************************************
* Creates a new double edge from node A to node B.
******************************************************************************/
InterfaceMesh::Edge* InterfaceMesh::createHalfEdge(Node* A, Node* B, const Vector3& clusterVector, ClusterTransition* transition)
{
	Edge* edgeAB = _edgePool.construct();
	edgeAB->clusterVector = clusterVector;
	edgeAB->clusterTransition = transition;
	edgeAB->physicalVector = structure().wrapVector(B->pos - A->pos);
	edgeAB->oppositeEdge = NULL;
	edgeAB->facet = NULL;
	edgeAB->_node2 = B;
	edgeAB->circuit = NULL;
	edgeAB->outputEdge = NULL;
	edgeAB->nextCircuitEdge = NULL;
	edgeAB->nextNodeEdge = A->edges;
	A->edges = edgeAB;
	return edgeAB;
}

/******************************************************************************
* Duplicates mesh nodes which are part of multiple manifolds.
******************************************************************************/
size_t InterfaceMesh::duplicateSharedMeshNodes()
{
	size_t numSharedNodes = 0;
	size_t oldNodeCount = nodes().size();
	for(size_t nodeIndex = 0; nodeIndex < oldNodeCount; nodeIndex++) {
		Node* node = nodes()[nodeIndex];

		// Count edges connected to the node.
		int numNodeEdges = 0;
		for(Edge* edge = node->edges; edge != NULL; edge = edge->nextNodeEdge)
			numNodeEdges++;
		CALIB_ASSERT(numNodeEdges >= 2);

		// Go in positive direction around node, facet by facet.
		Edge* currentEdge = node->edges;
		int numManifoldEdges = 0;
		do {
			CALIB_ASSERT(currentEdge != NULL && currentEdge->facet != NULL);
			currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
			numManifoldEdges++;
		}
		while(currentEdge != node->edges);

		if(numManifoldEdges == numNodeEdges)
			continue;		// Node is not part of multiple manifolds.

		set<Edge*> visitedEdges;
		currentEdge = node->edges;
		do {
			visitedEdges.insert(currentEdge);
			currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
		}
		while(currentEdge != node->edges);

		while(visitedEdges.size() < numNodeEdges) {

			// Create a second node that takes the edges not visited yet.
			Node* secondNode = createNode(node->tag, node->pos, false);

			Edge* startEdge;
			for(startEdge = node->edges; startEdge != NULL; startEdge = startEdge->nextNodeEdge) {
				if(visitedEdges.find(startEdge) == visitedEdges.end())
					break;
			}
			CALIB_ASSERT(startEdge != NULL);

			currentEdge = startEdge;
			do {
				CALIB_ASSERT(visitedEdges.find(currentEdge) == visitedEdges.end());
				visitedEdges.insert(currentEdge);
				for(Edge* previousEdge = node->edges; previousEdge != NULL; previousEdge = previousEdge->nextNodeEdge) {
					if(previousEdge->nextNodeEdge == currentEdge) {
						previousEdge->nextNodeEdge = currentEdge->nextNodeEdge;
						break;
					}
				}
				currentEdge->nextNodeEdge = secondNode->edges;
				secondNode->edges = currentEdge;
				Edge* oppositeEdge = currentEdge->oppositeEdge;
				oppositeEdge->_node2 = secondNode;
				currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
			}
			while(currentEdge != startEdge);
		}

		numSharedNodes++;
	}

	return numSharedNodes;
}

/******************************************************************************
* Performs a thorough check of the topology of the generated interface mesh.
* Raises an error if something wrong is found. Note that this should never happen.
******************************************************************************/
void InterfaceMesh::validate(bool performManifoldCheck) const
{
	// Check if edges and facets are properly linked together.
	for(vector<Node*>::const_iterator nodeIter = _nodes.begin(); nodeIter != _nodes.end(); ++nodeIter) {
		Node* node = *nodeIter;
		if(node->flags.test(Node::INVALID)) continue;

		int numNodeEdges = 0;
		for(Edge* edge = node->edges; edge != NULL; edge = edge->nextNodeEdge) {
			Node* neighbor = edge->node2();
			if(edge->oppositeEdge == NULL)
				context().raiseErrorOne("Detected dangling half edge. Edge node 1: %i  edge node 2: %i", (int)node->tag, (int)neighbor->tag);
			if(edge->oppositeEdge->oppositeEdge != edge)
				context().raiseErrorOne("Detected invalid reference between opposite edges. Edge node 1: %i  edge node 2: %i", (int)node->tag, (int)neighbor->tag);
			if(edge->facet == NULL)
				context().raiseErrorOne("Detected open interface mesh. Edge node 1: %i  edge node 2: %i", (int)node->tag, (int)neighbor->tag);
			if(edge->oppositeEdge->clusterTransition != edge->clusterTransition->reverse)
				context().raiseErrorOne("Invalid cluster transition in opposite edge. Edge node 1: %i  edge node 2: %i", (int)node->tag, (int)neighbor->tag);
			if(edge->clusterTransition->cluster1 != node->edges->clusterTransition->cluster1) {
				for(Edge* edge = node->edges; edge != NULL; edge = edge->nextNodeEdge) {
					context().msgLogger() << "Edge to node " << edge->node2()->tag << " cluster1=" << *edge->clusterTransition->cluster1 << " cluster2=" << *edge->clusterTransition->cluster2 << endl;
				}
				context().raiseErrorOne("Not all edges of node %i have a compatible cluster transition.", (int)node->tag);
			}
			if(!(edge->oppositeEdge->clusterVector + (edge->clusterTransition->tm * edge->clusterVector)).isZero(CAFLOAT_EPSILON))
				context().raiseErrorOne("Invalid lattice vector in opposite edge. Edge node 1: %i  edge node 2: %i", (int)node->tag, (int)neighbor->tag);

			Facet* facet = edge->facet;
			bool found = false;
			for(int v = 0; v < 3; v++) {
				if(facet->edges[v]->node1() == node) {
					if(facet->edges[v] != edge)
						context().raiseErrorOne("Detected invalid reference from facet to edge. Edge: %i - %i", (int)node->tag, (int)neighbor->tag);
					found = true;
					break;
				}
			}
			if(!found)
				context().raiseErrorOne("Facet does not contain vertex to which it is incident. Node %i", (int)node->tag);
			numNodeEdges++;
		}

		if(performManifoldCheck) {
			// Go in positive direction around node, facet by facet.
			Edge* currentEdge = node->edges;
			int numManifoldEdges = 0;
			do {
				CALIB_ASSERT(currentEdge->facet != NULL);
				currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
				numManifoldEdges++;
			}
			while(currentEdge != node->edges);

			if(numManifoldEdges != numNodeEdges)
				context().raiseErrorOne("Interface mesh node %i is part of multiple manifolds.", (int)node->index);
		}
	}

	// Check facets and the cluster vectors
	for(vector<Facet*>::const_iterator facet = _facets.begin(); facet != _facets.end(); ++facet) {
		Facet* lastOppositeFacet = NULL;
		Vector3 burgersVector(Vector3::Zero());
		Matrix3 tm(Matrix3::Identity());
		bool isValidFacet = true;
		for(int v = 0; v < 3; v++) {
			Edge* edge = (*facet)->edges[v];
			CALIB_ASSERT(edge->oppositeEdge != NULL);
			Node* node1 = edge->node1();
			if(node1->flags.test(Node::INVALID))
				isValidFacet = false;
			Node* node2 = (*facet)->edges[(v+1)%3]->node1();
			CALIB_ASSERT(node2 == edge->node2());
			if(edge->facet != (*facet))
				context().raiseErrorOne("Edge is not incident to facet which is part of. Edge: %i - %i.", (int)node1->tag, (int)node2->tag);

			burgersVector += tm * edge->clusterVector;
			tm = tm * edge->clusterTransition->reverse->tm;

			Facet* oppositeFacet = edge->oppositeEdge->facet;
			if(oppositeFacet == (*facet))
				context().raiseErrorOne("Facet is opposite to itself.");

			if(oppositeFacet == lastOppositeFacet && oppositeFacet != NULL) {
				Node* thirdVertex = (*facet)->edges[(v+2)%3]->node1();
				if(oppositeFacet->hasVertex(thirdVertex) == false)
					context().raiseErrorOne("Facet has two neighbor edges to the same other facet. Edge: %i - %i", (int)node1->tag, (int)node2->tag);
			}
			lastOppositeFacet = oppositeFacet;
		}
		if(isValidFacet && (tm - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON) == false)
			context().raiseErrorOne("Mesh facet's Frank matrix is not identity.");
		if(isValidFacet && burgersVector.isZero(CA_LATTICE_VECTOR_EPSILON) == false)
			context().raiseErrorOne("Mesh facet's Burgers vector is non-zero: %f %f %f", burgersVector.x(), burgersVector.y(), burgersVector.z());
	}
}

/// This structure is used to transmit mesh nodes across processors.
struct MeshNodeComm
{
	Vector3 pos;		// The world-space position of the node.
	AtomInteger tag;	// The unique tag of the node.
};

/// This structure is used to transmit mesh facets across processors.
struct MeshFacetComm
{
	AtomInteger nodes[3];		// The three vertices of the triangle facet.
};

/// This structure is used to transmit mesh edge information across processors.
struct MeshEdgeComm
{
	AtomInteger node1;
	AtomInteger node2;
	AtomInteger facet1vertex;
	AtomInteger facet2vertex;
	Vector3 clusterVector;
	ClusterId cluster1;
	ClusterId cluster2;
	Matrix3 transitionTM;
};

/******************************************************************************
* Merges partial meshes from all processors into a single mesh.
******************************************************************************/
void InterfaceMesh::mergeParallel(InterfaceMesh& outputMesh)
{
	CALIB_ASSERT(&outputMesh.structure() == &this->structure());
	CALIB_ASSERT(outputMesh._nodes.empty() && outputMesh._facets.empty());

	// Pack local mesh nodes into buffer.
	vector<MeshNodeComm> nodeBuffer(_numLocalNodes);
	vector<MeshNodeComm>::iterator nodeItem = nodeBuffer.begin();
	for(vector<Node*>::const_iterator node = _nodes.begin(); node != _nodes.end(); ++node) {
		if(!(*node)->flags.test(Node::IS_GHOST_NODE)) {
			nodeItem->pos = (*node)->pos;
			nodeItem->tag = (*node)->tag;
			++nodeItem;
		}
	}
	CALIB_ASSERT(nodeItem == nodeBuffer.end());

	// Determine the total number of nodes.
	size_t totalNodeCount = parallel().reduce_sum(nodeBuffer.size());
	// Determine the maximum number of nodes per processor.
	size_t maxNodeCount = parallel().reduce_max(nodeBuffer.size());

	// Tag to node map.
	map<AtomInteger, Node*> tagToNodeMap;

	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(nodeBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = nodeBuffer.size();
		outputMesh._nodes.reserve(totalNodeCount);

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				nodeBuffer.resize(maxNodeCount);
				nread = parallel().receive(nodeBuffer, proc);
			}

			// Re-create nodes in the output mesh.
			for(vector<MeshNodeComm>::const_iterator nodeItem = nodeBuffer.begin(); nodeItem != nodeBuffer.begin() + nread; ++nodeItem) {
				Node* meshNode = outputMesh._nodePool.construct(nodeItem->pos);
				meshNode->tag = nodeItem->tag;
				meshNode->index = outputMesh._nodes.size();
				outputMesh._nodes.push_back(meshNode);
				bool newlyInserted = tagToNodeMap.insert(make_pair(nodeItem->tag, meshNode)).second;

				// Tag must be unique:
				CALIB_ASSERT(newlyInserted);
			}
		}

		outputMesh._numLocalNodes = outputMesh._nodes.size();
	}

	// Pack local mesh facets into buffer.
	vector<MeshFacetComm> facetBuffer(_numLocalFacets);
	vector<MeshFacetComm>::iterator facetItem = facetBuffer.begin();
	for(vector<Facet*>::const_iterator facet = _facets.begin(); facet != _facets.end(); ++facet) {
		if(!(*facet)->flags.test(Facet::IS_GHOST_FACET)) {
			for(int v = 0; v < 3; v++) {
				Edge* e = (*facet)->edges[v];
				facetItem->nodes[v] = e->node1()->tag;
			}
			++facetItem;
		}
	}
	CALIB_ASSERT(facetItem == facetBuffer.end());

	// Determine the total number of facets.
	size_t totalFacetCount = parallel().reduce_sum(facetBuffer.size());
	// Determine the maximum number of facets per processor.
	size_t maxFacetCount = parallel().reduce_max(facetBuffer.size());

	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(facetBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = facetBuffer.size();
		outputMesh._facets.reserve(totalFacetCount);

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				facetBuffer.resize(maxFacetCount);
				nread = parallel().receive(facetBuffer, proc);
			}

			// Re-create facets in the output mesh.
			for(vector<MeshFacetComm>::const_iterator facetItem = facetBuffer.begin(); facetItem != facetBuffer.begin() + nread; ++facetItem) {
				Facet* facet = outputMesh._facetPool.construct();
				outputMesh._facets.push_back(facet);
				Node* facetNodes[3];
				for(int v = 0; v < 3; v++) {
					facetNodes[v] = tagToNodeMap[facetItem->nodes[v]];
					CALIB_ASSERT(facetNodes[v] != NULL);
				}
				for(int v = 0; v < 3; v++) {
					Edge* e = outputMesh.createHalfEdge(facetNodes[v], facetNodes[(v+1)%3], Vector3::Zero(), NULL);
					e->facet = facet;
					facet->edges[v] = e;
				}
			}
		}

		outputMesh._numLocalFacets = outputMesh._facets.size();
	}

	// Pack local mesh edges into buffer.
	vector<MeshEdgeComm> edgeBuffer;
	for(vector<Node*>::const_iterator node = _nodes.begin(); node != _nodes.end(); ++node) {
		if((*node)->flags.test(Node::IS_GHOST_NODE))
			continue;

		for(Edge* edge = (*node)->edges; edge != NULL; edge = edge->nextNodeEdge) {
			MeshEdgeComm edgeItem;
			edgeItem.node1 = (*node)->tag;
			edgeItem.node2 = edge->node2()->tag;
			if(edgeItem.node2 < edgeItem.node1) continue;
			edgeItem.clusterVector = edge->clusterVector;
			edgeItem.cluster1 = edge->clusterTransition->cluster1->id;
			edgeItem.cluster2 = edge->clusterTransition->cluster2->id;
			edgeItem.transitionTM = edge->clusterTransition->tm;
			edgeItem.facet1vertex = edge->facet->nextEdge(edge)->node2()->tag;
			edgeItem.facet2vertex = edge->oppositeEdge->facet->nextEdge(edge->oppositeEdge)->node2()->tag;
			edgeBuffer.push_back(edgeItem);
		}
	}

	// Determine the maximum number of edges per processor.
	size_t maxEdgeCount = parallel().reduce_max(edgeBuffer.size());

	// Transmit edges.
	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(edgeBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = edgeBuffer.size();

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				edgeBuffer.resize(maxEdgeCount);
				nread = parallel().receive(edgeBuffer, proc);
			}

			for(vector<MeshEdgeComm>::const_iterator edgeItem = edgeBuffer.begin(); edgeItem != edgeBuffer.begin() + nread; ++edgeItem) {

				Node* node1 = tagToNodeMap[edgeItem->node1];
				CALIB_ASSERT(node1 != NULL);

				Edge* edge = node1->edges;
				while(edge != NULL) {
					if(edge->node2()->tag == edgeItem->node2) {
						Node* thirdNode = edge->facet->nextEdge(edge)->node2();
						if(thirdNode->tag == edgeItem->facet1vertex) {
							CALIB_ASSERT(edge->oppositeEdge == NULL);

							Edge* oppositeEdge = NULL;
							for(Edge* otherEdge = edge->node2()->edges; otherEdge != NULL; otherEdge = otherEdge->nextNodeEdge) {
								if(otherEdge->node2() == node1) {
									Node* thirdNode = otherEdge->facet->nextEdge(otherEdge)->node2();
									if(thirdNode->tag == edgeItem->facet2vertex) {
										oppositeEdge = otherEdge;
										break;
									}
								}
							}
							CALIB_ASSERT(oppositeEdge != NULL);
							CALIB_ASSERT(oppositeEdge->oppositeEdge == NULL);

							edge->oppositeEdge = oppositeEdge;
							oppositeEdge->oppositeEdge = edge;
							Cluster* cluster1 = clusterGraph().findCluster(edgeItem->cluster1);
							Cluster* cluster2 = clusterGraph().findCluster(edgeItem->cluster2);
							CALIB_ASSERT(cluster1 != NULL && cluster2 != NULL);
							edge->clusterVector = edgeItem->clusterVector;
							edge->clusterTransition = clusterGraph().createClusterTransition(cluster1, cluster2, edgeItem->transitionTM);
							oppositeEdge->clusterTransition = edge->clusterTransition->reverse;
							if(edge->clusterTransition->isSelfTransition())
								oppositeEdge->clusterVector = -edge->clusterVector;
							else
								oppositeEdge->clusterVector = edge->clusterTransition->tm * -edge->clusterVector;

							break;
						}
					}
					edge = edge->nextNodeEdge;
				}
				CALIB_ASSERT(edge != NULL);
			}
		}

#ifdef DEBUG_CRYSTAL_ANALYSIS
		for(vector<Facet*>::const_iterator facet = outputMesh._facets.begin(); facet != outputMesh._facets.end(); ++facet) {
			for(int v = 0; v < 3; v++) {
				Edge* edge = (*facet)->edges[v];
				CALIB_ASSERT(edge->oppositeEdge != NULL);
				CALIB_ASSERT(edge->oppositeEdge->oppositeEdge == edge);
			}
		}
#endif
	}
}

}; // End of namespace
