///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_HARDCODED_COORDINATION_PATTERN_ANALYSIS_H
#define __CA_HARDCODED_COORDINATION_PATTERN_ANALYSIS_H

#include "../CALib.h"
#include "../pattern/coordinationpattern/CoordinationPatternAnalysis.h"

namespace CALib {

/**
 * Assign a lattice type to atoms in an AtomicStructure.
 * This class uses a hardcoded common neighbor analysis
 * to identify FCC, HCP and BCC lattices.
 */
class HardcodedCoordinationPatternAnalysis : public CoordinationPatternAnalysis
{
public:

	/// Constructor. Takes a reference to the AtomicStructure in which the patterns will be searched.
	HardcodedCoordinationPatternAnalysis(const AtomicStructure& structure, const NeighborList& neighborList, const PatternCatalog& catalog)
		: CoordinationPatternAnalysis(structure, neighborList, catalog) {}

	/// Searches the input structure for the hardcoded patterns.
	void searchHardcodedCoordinationPatterns();

protected:

	/// Exchange coordination pattern matches with neighboring processors.
	void exchangeHardcodedCoordinationPatternMatches();
};

}; // End of namespace

#endif // __CA_HARDCODED_COORDINATION_PATTERN_ANALYSIS_H
