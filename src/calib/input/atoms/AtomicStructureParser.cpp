///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"
#include "../../util/FILEStream.h"

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#ifdef CALIB_USE_ZLIB
	#include <boost/iostreams/filter/gzip.hpp>
#endif
#include <boost/iostreams/filtering_stream.hpp>

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file.
******************************************************************************/
AtomicStructureParser::FileFormatType AtomicStructureParser::readAtomsFile(const char* filename, istream* stdinStream)
{
	ifstream fileStream;
	istringstream nullStream;
	istream* inStream;
	boost::iostreams::filtering_stream<boost::iostreams::input> decompressionStream;

	// Open a stream to the input file (only on the master processor).
	if(parallel().isMaster()) {

		context().msgLogger() << "Opening file '" << filename << "' for reading." << endl;

		// If filename is '-', read from stdin instead of a file.
		// If filename ends with '.gz', decompress data on the fly.

		if(strcmp(filename, "-") == 0 && stdinStream != NULL) {
			inStream = stdinStream;
		}
		else {
			const char* suffix = filename + max((int)strlen(filename) - 3, 0);
			if(strcmp(suffix, ".gz") == 0) {
#ifdef CALIB_USE_ZLIB
				fileStream.open(filename, ios_base::in | ios_base::binary);
				if(!fileStream.is_open())
					context().raiseErrorOne("Failed to open input file for reading. Filename was '%s'.", filename);
				decompressionStream.push(boost::iostreams::gzip_decompressor());
				decompressionStream.push(fileStream);
				inStream = &decompressionStream;
#else
				context().raiseErrorOne("Cannot open compressed file '%s'. Program has not been built with support for compressed files.", filename);
#endif
			}
			else {
				fileStream.open(filename);
				if(!fileStream.is_open())
					context().raiseErrorOne("Failed to open input file for reading. Filename was '%s'.", filename);
				inStream = &fileStream;
			}
		}
	}
	else inStream = &nullStream;

	// Create our own wrapper stream for parsing.
	TextParserStream parserStream(*inStream, filename);

	// Actually parse file contents.
	FileFormatType fileType = readAtomsFile(parserStream);

	return fileType;
}

/******************************************************************************
* Reads the atomic coordinates from a stream.
******************************************************************************/
AtomicStructureParser::FileFormatType AtomicStructureParser::readAtomsFile(TextParserStream& stream)
{
	if(stream.filename().compare(stream.filename().length() - 3, 3, ".ss") == 0) {
		readCompactBinaryAtomsFile(stream);
		return FILETYPE_COMPACT;
	}

	FileFormatType fileType = FILETYPE_UNKNOWN;
	CAFloat scaling_factor;
	Vector3 cellVector;
	if(parallel().isMaster()) {

		// Parse first line.
		stream.readline();
		if(stream.eof())
			context().raiseErrorOne("Invalid input file. File contains only a single text line.");

		// Perform auto-detection of file format.
		// Inspect first line(s) of input file.
		if(stream.line().find("ITEM: TIMESTEP") != string::npos) {
			fileType = FILETYPE_LAMMPS_DUMP;
		}
		else if(stream.line().compare(0, 4, "#F A") == 0) {
			fileType = FILETYPE_IMD;
		}
		else if(stream.line().compare(0, 19, "particle FILEHEADER") == 0) {
			fileType = FILETYPE_DDCMD;
		}
		else {
			// Check for POSCAR format.
			stream.readline();
			if(!stream.eof() && stream.line().empty() == false && (scaling_factor = atof(stream.line().c_str())) > 0.0) {
				stream.readline();
				if(!stream.eof() && sscanf(stream.line().c_str(), CAFLOAT_SCANF_STRING_3, &cellVector.x(), &cellVector.y(), &cellVector.z()) == 3) {
					fileType = FILETYPE_POSCAR;
				}
			}
		}

		if(fileType == FILETYPE_UNKNOWN)
			context().raiseErrorOne("Invalid input file. File format could not be recognized.");
	}

	int fileTypeInt = static_cast<int>(fileType);
	parallel().broadcast(fileTypeInt);
	fileType = static_cast<FileFormatType>(fileTypeInt);

	switch(fileType) {
	case FILETYPE_LAMMPS_DUMP:
		readLAMMPSAtomsFile(stream);
		break;
	case FILETYPE_IMD:
		readIMDAtomsFile(stream);
		break;
	case FILETYPE_DDCMD:
		readDDCMDAtomsFile(stream);
		break;
	case FILETYPE_POSCAR:
		parallel().broadcast(scaling_factor);
		parallel().broadcast(cellVector);
		readPOSCARAtomsFile(stream, scaling_factor, cellVector);
		break;
	default: CALIB_ASSERT(false); break;
	}

	return fileType;
}

/******************************************************************************
* Sets up the simulation cell of the atomic structure.
******************************************************************************/
void AtomicStructureParser::setupSimulationCell(AffineTransformation simulationCell, boost::array<bool,3> pbc)
{
	if(parallel().isMaster()) {

		// User-specified PBCs.
		if(_preprocessing.overridePBC) {
			pbc[0] = _preprocessing.pbc[0];
			pbc[1] = _preprocessing.pbc[1];
			pbc[2] = _preprocessing.pbc[2];
		}

		// Multiply system size.
		simulationCell.scale(Vector3(_preprocessing.replicate[0], _preprocessing.replicate[1], _preprocessing.replicate[2]));

		// Add extra padding.
		if(_preprocessing.boxPadding != 0) {
			for(int dim = 0; dim < 3; dim++) {
				if(!pbc[dim]) {
					simulationCell
						.translate(+0.5 * Vector3::Unit(dim))
						.scale(Vector3::Constant(1) + Vector3::Unit(dim) * 2 * _preprocessing.boxPadding)
						.translate(-0.5 * Vector3::Unit(dim));
				}
			}
		}

		// Transform cell to match the prescribed shape.
		if(_preprocessing.cellShape != Matrix3::Zero()) {
			_preprocessing.cellDeformation = _preprocessing.cellShape * simulationCell.linear().inverse();
		}

		// Apply user deformation to simulation cell.
		simulationCell = _preprocessing.cellDeformation * simulationCell;
	}

	_structure.setupSimulationCellAndDecomposition(simulationCell, pbc);
}

/******************************************************************************
* Starts the distribution of atoms from the master processor to all processors.
******************************************************************************/
void AtomicStructureParser::beginDistributeAtoms()
{
	CALIB_ASSERT(_preprocessing.replicate[0] >= 1);
	CALIB_ASSERT(_preprocessing.replicate[1] >= 1);
	CALIB_ASSERT(_preprocessing.replicate[2] >= 1);

	_structure.beginAddingAtoms(max(0, _numInputAtoms * _preprocessing.replicate[0] * _preprocessing.replicate[1] * _preprocessing.replicate[2] / parallel().processorCount()));

	// If the number of input atoms is not known then we cannot replicate it properly.
	if(_numInputAtoms < 0 && _preprocessing.replicate[0] * _preprocessing.replicate[1] * _preprocessing.replicate[2] > 1)
		context().raiseErrorAll("Cannot use periodic replicate option with this type of input file.");

	_atomDistributionBuffer.resize(2048);

	_numAtomsRead = 0;
	_currentDistributionAtom = _atomDistributionBuffer.begin();

	// Flag that indicates whether the atomic coordinates are being transformed during parsing.
	_applyCellTransformation = (_preprocessing.cellDeformation != Matrix3::Identity());
}

/******************************************************************************
* Distributes an atom to all processors.
******************************************************************************/
void AtomicStructureParser::distributeAtom(const AtomInfo& atom)
{
	CALIB_ASSERT(_numAtomsRead < _numInputAtoms);
	CALIB_ASSERT(_currentDistributionAtom != _atomDistributionBuffer.end());

	if(parallel().isMaster()) {

		*_currentDistributionAtom = atom;

		// Add random displacements.
		if(_preprocessing.jitterMagnitude != 0) {
			_currentDistributionAtom->pos.x() += ((CAFloat)rand()/RAND_MAX - 0.5) * _preprocessing.jitterMagnitude;
			_currentDistributionAtom->pos.y() += ((CAFloat)rand()/RAND_MAX - 0.5) * _preprocessing.jitterMagnitude;
			_currentDistributionAtom->pos.z() += ((CAFloat)rand()/RAND_MAX - 0.5) * _preprocessing.jitterMagnitude;
		}

		// Apply affine transformation.
		if(_applyCellTransformation)
			_currentDistributionAtom->pos = _preprocessing.cellDeformation * _currentDistributionAtom->pos;

		// Add constant offset.
		_currentDistributionAtom->pos += _preprocessing.atomsOffset;
	}

	// Advance pointer and counter.
	++_currentDistributionAtom;
	++_numAtomsRead;

	// After the chunk of atoms is complete, broadcast it to all processors.
	if(_currentDistributionAtom == _atomDistributionBuffer.end() || _numAtomsRead == _numInputAtoms) {

		size_t nchunk = _currentDistributionAtom - _atomDistributionBuffer.begin();
		parallel().broadcast(&_atomDistributionBuffer[0], sizeof(AtomInfo) * nchunk);
		CALIB_ASSERT(_numInputAtoms >= 0);

		for(int iz = 0; iz < _preprocessing.replicate[2]; iz++) {
			for(int iy = 0; iy < _preprocessing.replicate[1]; iy++) {
				for(int ix = 0; ix < _preprocessing.replicate[0]; ix++) {

					Vector3 positionShift = _structure.simulationCellMatrix().linear() *
							Vector3(CAFloat(ix) / _preprocessing.replicate[0],
									CAFloat(iy) / _preprocessing.replicate[1],
									CAFloat(iz) / _preprocessing.replicate[2]);

					int pbcImageNumber = iz * _preprocessing.replicate[0] * _preprocessing.replicate[1] + iy * _preprocessing.replicate[0] + ix;

					vector<AtomInfo>::const_iterator a = _atomDistributionBuffer.begin();
					for(size_t i = 0; i < nchunk; i++, ++a) {

						// Wrap at periodic boundaries.
						Vector3 wrappedPos = _structure.wrapPoint(a->pos + positionShift);

						// Determine the processor domain the atom is located in.
						Vector3I procCoordi = _structure.absoluteToProcessorGrid(wrappedPos);

						// If the atom is in our local domain, then add it to our local list of atoms.
						if(procCoordi == parallel().processorLocationPoint()) {
							_structure.addAtom(wrappedPos, a->tag, a->species, procCoordi,
									false, false, pbcImageNumber);
						}
					}
				}
			}
		}

		// Reset pointer to start next chunk.
		_currentDistributionAtom = _atomDistributionBuffer.begin();
	}
}

/******************************************************************************
* Finishes the distribution of atoms from the master processor to all processors.
******************************************************************************/
void AtomicStructureParser::endDistributeAtoms()
{
	CALIB_ASSERT(_numAtomsRead == _numInputAtoms);
	_atomDistributionBuffer.clear();

#if 0
	// Shuffle atoms.
	context().msgLogger() << "WARNING: SHUFFLING ATOMS!" << endl;
	srand(0);
	std::random_shuffle(inputAtoms.begin(), inputAtoms.end());
#endif

	// Check if every atom has been assigned to a processor.
	_structure.endAddingAtoms();
	if(_structure.numTotalAtoms() != _numInputAtoms * _preprocessing.replicate[0] * _preprocessing.replicate[1] * _preprocessing.replicate[2])
		context().raiseErrorAll("File loading error. Not all input atoms could be assigned to a processor (%i != %i).", (int)_structure.numTotalAtoms(), (int)_numInputAtoms);

	// Determine bounding box of atoms.
	_structure.computeBoundingBox();
}

}; // End of namespace
