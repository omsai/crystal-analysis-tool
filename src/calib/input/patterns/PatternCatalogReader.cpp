///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "PatternCatalogReader.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Deserializes a PatternCatalog object from a file.
******************************************************************************/
void PatternCatalogReader::read(std::istream& stream, PatternCatalog& catalog)
{
	// Check file header.
	string line;
	getline(stream, line);
	if(line != "# CRYSTAL ANALYSIS TOOL PATTERN CATALOG")
		context().raiseErrorOne("Pattern catalog file was written by a different program version or has an invalid format. Please regenerate the pattern catalog by running the 'create_catalog.sh' script.");
	getline(stream, line);
	stream >> _fileFormatVersion;
	if(_fileFormatVersion != CA_PATTERN_CATALOG_FILEFORMAT_VERSION)
		context().raiseErrorOne("Pattern catalog file was written by a different program version or has an invalid format. Please regenerate the pattern catalog by running the 'create_catalog.sh' script.");
	int maxPatternNeighbors;
	stream >> maxPatternNeighbors;
	if(maxPatternNeighbors != CA_MAX_PATTERN_NEIGHBORS)
		throw std::runtime_error("Pattern catalog file was written on a different platform or by a different program version. The value of the CA_MAX_PATTERN_NEIGHBORS setting is not compatible. Please regenerate the pattern catalog file.");

	int numCoordinationPatterns;
	stream >> numCoordinationPatterns;
	for(int i = 0; i < numCoordinationPatterns; i++) {
		catalog.addCoordinationPattern(readCoordinationPattern(stream, catalog));
	}
	int numSuperPatterns;
	stream >> numSuperPatterns;
	for(int i = 0; i < numSuperPatterns; i++) {
		catalog.addSuperPattern(readSuperPattern(stream, catalog));
	}
}

CoordinationPattern* PatternCatalogReader::readCoordinationPattern(std::istream& stream, PatternCatalog& catalog)
{
	auto_ptr<CoordinationPattern> pattern;
	string patternType;
	stream >> patternType;
	if(patternType == "CNA") pattern.reset(new CNACoordinationPattern());
	else if(patternType == "ACNA") pattern.reset(new AdaptiveCNACoordinationPattern());
	else if(patternType == "NDA") pattern.reset(new NDACoordinationPattern());
	else if(patternType == "DIAMOND") pattern.reset(new DiamondCoordinationPattern());
	else context().raiseErrorOne("Cannot deserialize coordination pattern of unknown type: %s", patternType.c_str());
	stream >> pattern->name;
	int numNeighbors;
	stream >> numNeighbors;
	pattern->neighbors.resize(numNeighbors);
	BOOST_FOREACH(CoordinationPatternNeighbor& n, pattern->neighbors) {
		readVector(stream, n.vec);
		stream >> n.scaleFactor;
	}
	stream >> pattern->cutoffGap;
	stream >> pattern->lastInnerShellRadius;
	stream >> pattern->firstOuterShellRadius;
	stream >> pattern->numShells;
	int numPermutations;
	stream >> numPermutations;
	for(size_t j = 0; j < numPermutations; j++) {
		CoordinationPermutation p, pinv;
		p.resize(numNeighbors);
		pinv.resize(numNeighbors);
		for(int i = 0; i < numNeighbors; i++) {
			stream >> p[i];
			pinv[p[i]] = i;
		}
		Matrix3 tm;
		readMatrix(stream, tm);
		pattern->permutations.push_back(p);
		pattern->inversePermutations.push_back(pinv);
		pattern->permutationMatrices.push_back(tm);
	}

	if(pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_CNA || pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA || pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
		CNACoordinationPattern* cnaPattern = static_cast<CNACoordinationPattern*>(pattern.get());
		for(int i = 0; i < numNeighbors; i++) {
			stream >> cnaPattern->cnaNeighbors[i].numCommonNeighbors >> cnaPattern->cnaNeighbors[i].numBonds >> cnaPattern->cnaNeighbors[i].maxChainLength;
			cnaPattern->cnaNeighborsSorted[i] = cnaPattern->cnaNeighbors[i];
		}
		sort(cnaPattern->cnaNeighborsSorted, cnaPattern->cnaNeighborsSorted + numNeighbors);
		for(int i = 0; i < numNeighbors; i++) {
			for(int j = 0; j < i; j++) {
				int b;
				stream >> b;
				cnaPattern->neighborArray.setNeighborBond(i,j, b != 0);
			}
		}

		if(pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA) {
			AdaptiveCNACoordinationPattern* acnaPattern = static_cast<AdaptiveCNACoordinationPattern*>(pattern.get());
			stream >> acnaPattern->localCNACutoff;
		}
		else if(pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
			DiamondCoordinationPattern* diamondPattern = static_cast<DiamondCoordinationPattern*>(pattern.get());
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 3; j++) {
					stream >> diamondPattern->secondToFirstMap[i][j];
				}
			}
		}
	}
	else if(pattern->coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_NDA) {
		NDACoordinationPattern* ndaPattern = static_cast<NDACoordinationPattern*>(pattern.get());
		int bondCount = 0;
		for(int i = 0; i < numNeighbors; i++) {
			for(int j = 0; j < i; j++) {
				stream >> ndaPattern->bonds[i][j].minDistanceSquared >> ndaPattern->bonds[i][j].maxDistanceSquared;
				ndaPattern->bonds[j][i].minDistanceSquared = ndaPattern->bonds[i][j].minDistanceSquared;
				ndaPattern->bonds[j][i].maxDistanceSquared = ndaPattern->bonds[i][j].maxDistanceSquared;
				ndaPattern->bondRangeDistribution[bondCount++] = ndaPattern->bonds[i][j];
			}
		}
		CALIB_ASSERT(bondCount == numNeighbors * (numNeighbors - 1) / 2);
		sort(ndaPattern->bondRangeDistribution, ndaPattern->bondRangeDistribution + bondCount);
	}

	return pattern.release();
}

SuperPattern* PatternCatalogReader::readSuperPattern(std::istream& stream, PatternCatalog& catalog)
{
	auto_ptr<SuperPattern> pattern(new SuperPattern());
	stream >> pattern->name;
	getline(stream, pattern->fullName);
	getline(stream, pattern->fullName);
	stream >> pattern->numNodes >> pattern->numCoreNodes;
	pattern->nodes.resize(pattern->numNodes);
	BOOST_FOREACH(SuperPatternNode& node, pattern->nodes) {
		stream >> node.species;
		int coordPatternIndex;
		stream >> coordPatternIndex;
		node.coordinationPattern = &catalog.coordinationPatternByIndex(coordPatternIndex);
		for(int i = 0; i < node.coordinationPattern->numNeighbors(); i++) {
			SuperPatternBond& bond = node.neighbors[i];
			stream >> bond.neighborNodeIndex;
			readVector(stream, bond.referenceVector);
			stream >> bond.reverseNodeNeighborIndex;
			stream >> bond.numCommonNeighbors;
			for(int j = 0; j < bond.numCommonNeighbors; j++) {
				stream >> bond.commonNeighborNodeIndices[j][0] >> bond.commonNeighborNodeIndices[j][1];
			}
		}
		int overlapPatternIndex;
		stream >> overlapPatternIndex;
		if(overlapPatternIndex)
			node.overlapPattern = &catalog.superPatternByIndex(overlapPatternIndex);
		else
			node.overlapPattern = NULL;
		stream >> node.overlapPatternNode;
		node.nodeNodeDistances.resize(pattern->numNodes);
		for(int i = 0; i < pattern->numNodes; i++) {
			stream >> node.nodeNodeDistances[i];
		}
		int numSpaceGroupEntries;
		stream >> numSpaceGroupEntries;
		node.spaceGroupEntries.resize(numSpaceGroupEntries);
		BOOST_FOREACH(SpaceGroupElement& sge, node.spaceGroupEntries) {
			stream >> sge.targetNodeIndex;
			stream >> sge.permutationIndex;
			readMatrix(stream, sge.tm);
		}
	}
	stream >> pattern->periodicity;
	string vicinityCriterionString;
	stream >> vicinityCriterionString;
	if(vicinityCriterionString == "lattice") pattern->vicinityCriterion = SuperPattern::VICINITY_CRITERION_LATTICE;
	else if(vicinityCriterionString == "interface") pattern->vicinityCriterion = SuperPattern::VICINITY_CRITERION_INTERFACE;
	else if(vicinityCriterionString == "pointdefect") pattern->vicinityCriterion = SuperPattern::VICINITY_CRITERION_POINTDEFECT;
	else if(vicinityCriterionString == "shortestpath") pattern->vicinityCriterion = SuperPattern::VICINITY_CRITERION_SHORTESTPATH;
	else context().raiseErrorOne("Cannot deserialize super pattern with unknown vicinity criterion: %s", vicinityCriterionString.c_str());
	readVector(stream, pattern->color);
	int numBurgersVectorFamilies;
	stream >> numBurgersVectorFamilies;
	pattern->burgersVectorFamilies.resize(numBurgersVectorFamilies);
	BOOST_FOREACH(BurgersVectorFamily& family, pattern->burgersVectorFamilies) {
		readVector(stream, family.burgersVector);
		stream >> family.id;
		getline(stream, family.name);
		getline(stream, family.name);
		readVector(stream, family.color);
	}
	readMatrix(stream, pattern->misorientationMatrix);
	int numParentLatticePatterns;
	stream >> numParentLatticePatterns;
	for(int i = 0; i < numParentLatticePatterns; i++) {
		int index;
		stream >> index;
		pattern->parentLatticePatterns.insert(&catalog.superPatternByIndex(index));
	}
	readMatrix(stream, pattern->periodicCell);
	int marker;
	stream >> marker;
	if(marker != 0xFFABCD)
		context().raiseErrorOne("Pattern catalog file is invalid. Please regenerate the pattern catalog by running the 'create_catalog.sh' script.");
	return pattern.release();
}


}; // End of namespace
