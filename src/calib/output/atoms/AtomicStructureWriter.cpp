///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureWriter.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../pattern/coordinationpattern/CoordinationPatternAnalysis.h"
#include "../../context/CAContext.h"
#include "../../cluster_graph/Cluster.h"
#include "../../util/MPIGatherHelper.h"
#include "../../deformation/ElasticAtomTensor.h"
#include "../../deformation/TotalAtomTensor.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Writes the header of a LAMMPS dump file.
******************************************************************************/
void AtomicStructureWriter::writeLAMMPSFileHeader(std::ostream& stream, const AtomicStructure& structure)
{
	stream << "ITEM: TIMESTEP\n";
	stream << structure.timestep() << "\n";
	const AffineTransformation::MatrixType& cell = structure.simulationCellMatrix().matrix();
	if(cell(0,1) == 0.0 && cell(0,2) == 0.0 && cell(1,2) == 0.0) {
		stream << "ITEM: BOX BOUNDS";
		if(structure.pbc(0)) stream << " pp"; else stream << " ff";
		if(structure.pbc(1)) stream << " pp"; else stream << " ff";
		if(structure.pbc(2)) stream << " pp"; else stream << " ff";
		stream << "\n";
		stream << cell.col(3).x() << " " << (cell.col(3).x() + cell(0,0)) << "\n";
		stream << cell.col(3).y() << " " << (cell.col(3).y() + cell(1,1)) << "\n";
		stream << cell.col(3).z() << " " << (cell.col(3).z() + cell(2,2)) << "\n";
	}
	else {
		stream << "ITEM: BOX BOUNDS xy xz yz";
		if(structure.pbc(0)) stream << " pp"; else stream << " ff";
		if(structure.pbc(1)) stream << " pp"; else stream << " ff";
		if(structure.pbc(2)) stream << " pp"; else stream << " ff";
		stream << "\n";
		CAFloat xlo = cell.col(3).x();
		CAFloat ylo = cell.col(3).y();
		CAFloat zlo = cell.col(3).z();
		CAFloat xhi = cell.col(0).x() + xlo;
		CAFloat yhi = cell.col(1).y() + ylo;
		CAFloat zhi = cell.col(2).z() + zlo;
		CAFloat xy = cell.col(1).x();
		CAFloat xz = cell.col(2).x();
		CAFloat yz = cell.col(2).y();
		xlo = min(xlo, xlo+xy);
		xlo = min(xlo, xlo+xz);
		ylo = min(ylo, ylo+yz);
		xhi = max(xhi, xhi+xy);
		xhi = max(xhi, xhi+xz);
		yhi = max(yhi, yhi+yz);
		stream << xlo << " " << xhi << " " << xy << "\n";
		stream << ylo << " " << yhi << " " << xz << "\n";
		stream << zlo << " " << zhi << " " << yz << "\n";
	}
}


/******************************************************************************
* Outputs all atoms to a LAMMPS dump file.
******************************************************************************/
void AtomicStructureWriter::writeLAMMPSFile(ostream& stream, const std::vector<DataField>& fields,
		const AtomicStructure& structure,
		const SuperPatternAnalysis* patternAnalysis,
		const GrainIdentification* grainAnalysis,
		const AtomicStructure* structure2,
		CAFloat cutoffRadius)
{
	if(parallel().isMaster()) {
		// Write LAMMPS file header.
		writeLAMMPSFileHeader(stream, structure);
		stream << "ITEM: NUMBER OF ATOMS\n";
		stream << structure.numTotalAtoms() << "\n";
		stream << "ITEM: ATOMS";
		BOOST_FOREACH(DataField field, fields) {
			switch(field) {
			case ATOM_ID: stream << " id"; break;
			case ATOM_TYPE: stream << " type"; break;
			case POS_X: stream << " x"; break;
			case POS_Y: stream << " y"; break;
			case POS_Z: stream << " z"; break;
			case SUPER_PATTERN: stream << " pattern"; break;
			case CLUSTER_ID: stream << " cluster"; break;
			case CLUSTER_LOCAL_ID: stream << " cluster_local_id"; break;
			case CLUSTER_PROC: stream << " cluster_proc"; break;
			case GRAIN_ID: stream << " grain"; break;
			case ORIENTATION_001_AXIS: stream << " orient001"; break;
			case F_11: case F_12: case F_13:
			case F_21: case F_22: case F_23:
			case F_31: case F_32: case F_33:
				stream << " F_" << (((field-F_11)/3)+1) << (((field-F_11)%3)+1);
				break;
			case SHEAR_STRAIN: stream << " shear_strain"; break;
			case VOLUMETRIC_STRAIN: stream << " volumetric_strain"; break;
			case IS_F_VALID: stream << " is_F_valid"; break;
			case FE_11: case FE_12: case FE_13:
			case FE_21: case FE_22: case FE_23:
			case FE_31: case FE_32: case FE_33:
				stream << " FE_" << (((field-FE_11)/3)+1) << (((field-FE_11)%3)+1);
				break;
			default: CALIB_ASSERT(false);
			}
		}
		stream << "\n";
	}

	int numInts = 0;
	int numFloats = 0;
	bool requiresF = false;
	bool requiresFe = false;
	BOOST_FOREACH(DataField field, fields) {
		switch(field) {
		case ATOM_ID:
		case ATOM_TYPE:
		case SUPER_PATTERN:
		case CLUSTER_ID:
		case CLUSTER_LOCAL_ID:
		case CLUSTER_PROC:
		case GRAIN_ID:
		case IS_F_VALID:
			numInts++; break;
		case POS_X:
		case POS_Y:
		case POS_Z:
			numFloats++;
			break;
		case F_11: case F_12: case F_13:
		case F_21: case F_22: case F_23:
		case F_31: case F_32: case F_33:
		case SHEAR_STRAIN:
		case VOLUMETRIC_STRAIN:
			requiresF = true;
			numFloats++;
			break;
		case ORIENTATION_001_AXIS:
		case FE_11: case FE_12: case FE_13:
		case FE_21: case FE_22: case FE_23:
		case FE_31: case FE_32: case FE_33:
			requiresFe = true;
			numFloats++;
			break;
		}
	}
	if(numInts + numFloats == 0) return;

	vector<int> intBuffer(structure.numLocalAtoms() * numInts);
	vector<CAFloat> floatBuffer(structure.numLocalAtoms() * numFloats);

	// Set up calculation of atomic elastic tensors.
	Matrix3 Fe = Matrix3::Zero();
	auto_ptr<ElasticAtomTensor> elasticTensorComp;
	if(requiresFe) {
		if(!patternAnalysis)
			context().raiseErrorAll("Cannot output elastic field if it has not been computed.");
		elasticTensorComp.reset(new ElasticAtomTensor(*patternAnalysis));
	}

	// Set up calculation of atomic total deformation tensors.
	Matrix3 F = Matrix3::Zero();
	auto_ptr<NeighborList> neighborList;
	auto_ptr<TotalAtomTensor> totalTensorComp;
	if(requiresF) {
		if(!structure2)
			context().raiseErrorAll("Cannot output deformation field if it has not been computed.");
		neighborList.reset(new NeighborList(structure));
		neighborList->buildNeighborLists(cutoffRadius, false, false);
		totalTensorComp.reset(new TotalAtomTensor(context(), structure, *structure2, *neighborList));
	}

	// Pack local atom data into send buffer.
	vector<int>::iterator intItem = intBuffer.begin();
	vector<CAFloat>::iterator floatItem = floatBuffer.begin();
	for(int atomIndex = 0; atomIndex < structure.numLocalAtoms(); atomIndex++) {

		bool isFValid = false;
		bool isFeValid = false;
		if(requiresF) {
			CALIB_ASSERT(totalTensorComp.get() != NULL);
			if(!(isFValid = totalTensorComp->calculateDeformationTensor(atomIndex, F)))
				F.setZero();
		}
		if(requiresFe) {
			CALIB_ASSERT(elasticTensorComp.get() != NULL);
			if(!(isFeValid = elasticTensorComp->calculateElasticTensor(atomIndex, Fe)))
				Fe.setZero();
		}

		BOOST_FOREACH(DataField field, fields) {
			switch(field) {
			case ATOM_ID: *intItem++ = structure.atomTag(atomIndex); break;
			case ATOM_TYPE: *intItem++ = structure.atomSpecies(atomIndex); break;
			case POS_X: *floatItem++ = structure.atomPosition(atomIndex).x(); break;
			case POS_Y: *floatItem++ = structure.atomPosition(atomIndex).y(); break;
			case POS_Z: *floatItem++ = structure.atomPosition(atomIndex).z(); break;
			case SUPER_PATTERN:
				CALIB_ASSERT(patternAnalysis != NULL);
				if(patternAnalysis->atomCluster(atomIndex) != NULL)
					*intItem++ = patternAnalysis->atomPattern(atomIndex).index;
				else
					*intItem++ = 0;
				break;
			case CLUSTER_ID:
				CALIB_ASSERT(patternAnalysis != NULL);
				if(patternAnalysis->atomCluster(atomIndex) != NULL)
					*intItem++ = patternAnalysis->atomCluster(atomIndex)->parentCluster()->outputId;
				else
					*intItem++ = -1;
				break;
			case CLUSTER_LOCAL_ID:
				CALIB_ASSERT(patternAnalysis != NULL);
				if(patternAnalysis->atomCluster(atomIndex) != NULL)
					*intItem++ = patternAnalysis->atomCluster(atomIndex)->id.id;
				else
					*intItem++ = -1;
				break;
			case CLUSTER_PROC:
				CALIB_ASSERT(patternAnalysis != NULL);
				if(patternAnalysis->atomCluster(atomIndex) != NULL)
					*intItem++ = patternAnalysis->atomCluster(atomIndex)->id.processor;
				else
					*intItem++ = -1;
				break;
			case GRAIN_ID:
				CALIB_ASSERT(grainAnalysis != NULL);
				*intItem++ = grainAnalysis->atomGrainId(atomIndex);
				break;
			case ORIENTATION_001_AXIS:
				CALIB_ASSERT(requiresFe);
				if(isFeValid) {
					Vector3 v = Fe * Vector3(0,0,1);
					CAFloat angle = acos(fabs(v.normalized().dot(Vector3(0,0,1))));
					*floatItem++ = angle * (180.0/CAFLOAT_PI);
				}
				else *floatItem++ = 0;
				break;
			case F_11: case F_12: case F_13:
			case F_21: case F_22: case F_23:
			case F_31: case F_32: case F_33:
				*floatItem++ = F((field - F_11)/3, (field - F_11)%3);
				break;
			case SHEAR_STRAIN:
				{
				Matrix3 strain = (F.transpose() * F - Matrix3::Identity()) * 0.5;
				*floatItem++ = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) +
						(square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
				}
				break;
			case VOLUMETRIC_STRAIN:
				*floatItem++ = ((F.transpose() * F - Matrix3::Identity()) * 0.5).trace() / 3.0;
				break;
			case IS_F_VALID:
				*intItem++ = isFValid;
				break;
			case FE_11: case FE_12: case FE_13:
			case FE_21: case FE_22: case FE_23:
			case FE_31: case FE_32: case FE_33:
				*floatItem++ = Fe((field - FE_11)/3, (field - FE_11)%3);
				break;
			}
		}
	}
	CALIB_ASSERT(intItem == intBuffer.end());
	CALIB_ASSERT(floatItem == floatBuffer.end());

	int maxAtomsPerProc = parallel().reduce_max(structure.numLocalAtoms());
	if(parallel().isMaster() == false) {
		// Send buffers to master processor.
		parallel().send(intBuffer, 0);
		parallel().send(floatBuffer, 0);
	}
	else {
		// If master, receive buffer data from remote processors.
		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			int numReceivedInts;
			int numReceivedFloats;
			if(proc == 0) {
				numReceivedInts = intBuffer.size();
				numReceivedFloats = floatBuffer.size();
			}
			else {
				intBuffer.resize(maxAtomsPerProc * numInts);
				floatBuffer.resize(maxAtomsPerProc * numFloats);
				numReceivedInts = parallel().receive(intBuffer, proc);
				numReceivedFloats = parallel().receive(floatBuffer, proc);
			}

			// Write per-atom data to output stream.
			int natoms = (numReceivedInts + numReceivedFloats) / (numInts + numFloats);
			vector<int>::const_iterator intItem = intBuffer.begin();
			vector<CAFloat>::const_iterator floatItem = floatBuffer.begin();
			for(int atomIndex = 0; atomIndex < natoms; atomIndex++) {
				BOOST_FOREACH(DataField field, fields) {
					switch(field) {
					case ATOM_ID:
					case ATOM_TYPE:
					case CLUSTER_ID:
					case CLUSTER_LOCAL_ID:
					case CLUSTER_PROC:
					case GRAIN_ID:
					case SUPER_PATTERN:
					case IS_F_VALID:
						stream << *intItem++ << " ";
						break;
					case POS_X:
					case POS_Y:
					case POS_Z:
					case ORIENTATION_001_AXIS:
					case F_11: case F_12: case F_13:
					case F_21: case F_22: case F_23:
					case F_31: case F_32: case F_33:
					case SHEAR_STRAIN:
					case VOLUMETRIC_STRAIN:
					case FE_11: case FE_12: case FE_13:
					case FE_21: case FE_22: case FE_23:
					case FE_31: case FE_32: case FE_33:
						stream << *floatItem++ << " ";
						break;
					default:
						CALIB_ASSERT(false);	// Unknown data field.
						break;
					}
				}
				stream << "\n";
			}
			CALIB_ASSERT(intItem <= intBuffer.end());
			CALIB_ASSERT(floatItem <= floatBuffer.end());
			CALIB_ASSERT(intItem - intBuffer.begin() == numReceivedInts);
			CALIB_ASSERT(floatItem - floatBuffer.begin() == numReceivedFloats);
		}
	}
}

/******************************************************************************
*  Outputs all local atoms including ghost atoms to a LAMMPS dump file.
******************************************************************************/
void AtomicStructureWriter::writeLocalLAMMPSFile(ostream& stream, const AtomicStructure& structure, const SuperPatternAnalysis* patternAnalysis)
{
	int numAtoms = structure.numLocalAtoms() + structure.numGhostAtoms() + structure.numHelperAtoms();

	// Write LAMMPS file header.
	writeLAMMPSFileHeader(stream, structure);
	stream << "ITEM: NUMBER OF ATOMS\n";
	stream << numAtoms << "\n";
	if(patternAnalysis)
		stream << "ITEM: ATOMS tag type x y z isghost ishelper numneighbors SuperPattern Cluster SuperPatternNodeIndex CoordinationPatterns\n";
	else
		stream << "ITEM: ATOMS tag type x y z isghost ishelper\n";

	for(int atomIndex = 0; atomIndex < numAtoms; atomIndex++) {
		stream        << structure.atomTag(atomIndex);
		stream << " " << structure.atomSpecies(atomIndex);
		const Vector3& p = structure.atomPosition(atomIndex);
		stream << " " << p.x() << " " << p.y() << " " << p.z();
		stream << " " << structure.isGhostAtom(atomIndex);
		stream << " " << structure.isHelperAtom(atomIndex);
		if(patternAnalysis) {
			stream << " " << patternAnalysis->neighborList().neighborCount(atomIndex);
			if(patternAnalysis->atomCluster(atomIndex) != NULL) {
				stream << " " << patternAnalysis->atomPattern(atomIndex).index;
				stream << " " << patternAnalysis->atomCluster(atomIndex)->id.id;
				stream << " " << patternAnalysis->atomPatternNodeIndex(atomIndex);
			}
			else {
				stream << " 0";
				stream << " 0";
				stream << " 0";
			}
			unsigned coordPatterns = 0;
			BOOST_FOREACH(const CoordinationPatternAnalysis::coordmatch& match, patternAnalysis->coordAnalysis().coordinationPatternMatches(atomIndex)) {
				if(match.pattern->index < 32)
					coordPatterns |= (1 << match.pattern->index);
			}
			stream << " " << coordPatterns;
		}
		stream << "\n";
	}
	stream << flush;
}

/******************************************************************************
* Outputs all atoms to a POSCAR file.
******************************************************************************/
void AtomicStructureWriter::writePOSCARFile(std::ostream& stream, const AtomicStructure& structure)
{
	CALIB_ASSERT(parallel().processorCount() == 1);

	stream << "POSCAR file written by Crystal Analysis Tool\n";
	stream << "1.0\n";
	for(int i = 0; i < 3; i++) {
		stream << structure.cellVector(i).x() << " " << structure.cellVector(i).y() << " " << structure.cellVector(i).z() << "\n";
	}
	stream << structure.numTotalAtoms() << "\n";
	stream << "Cartesian\n";
	for(int i = 0; i < structure.numTotalAtoms(); i++) {
		const Vector3& p = structure.atomPosition(i);
		stream << p.x() << " " << p.y() << " " << p.z() << "\n";
	}
}

}; // End of namespace
