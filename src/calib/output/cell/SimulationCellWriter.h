///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_SIMULATION_CELL_WRITER_H
#define __CA_SIMULATION_CELL_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"

namespace CALib {

class SimulationCell;

/**
 * Writes the geometry of a SimulationCell to a file, which can be used for visualization purposes.
 */
class SimulationCellWriter : public ContextReference
{
public:

	/// Constructor.
	SimulationCellWriter(CAContext& context) : ContextReference(context) {}

	/// Writes the cell geometry to the given file stream in VTK format.
	void writeVTKFile(std::ostream& stream, const SimulationCell& cell);
};

}; // End of namespace

#endif // __CA_SIMULATION_CELL_WRITER_H
