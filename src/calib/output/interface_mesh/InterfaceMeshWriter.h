///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_INTERFACE_MESH_WRITER_H
#define __CA_INTERFACE_MESH_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../dislocations/InterfaceMesh.h"

namespace CALib {

/**
 * Writes an InterfaceMesh to a file, which can be visualized.
 */
class InterfaceMeshWriter : public ContextReference
{
public:

	/// Constructor.
	InterfaceMeshWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs the interface mesh to a VTK file.
	void writeVTKFile(std::ostream& stream, const InterfaceMesh& mesh);

private:

	/// Determines whether a mesh triangle facet crosses a periodic boundary.
	bool isWrappedFacet(const AtomicStructure& structure, const InterfaceMesh::Facet* facet) const;
};

}; // End of namespace

#endif // __CA_INTERFACE_MESH_WRITER_H
