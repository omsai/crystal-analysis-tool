///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "PatternCatalogWriter.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Serializes a PatternCatalog object to a file.
******************************************************************************/
void PatternCatalogWriter::write(std::ostream& stream, const PatternCatalog& catalog)
{
	if(!parallel().isMaster())
		return;

	// Write some compile-time settings to file header.
	stream << "# CRYSTAL ANALYSIS TOOL PATTERN CATALOG" << endl;
	stream << "# FILE FORMAT" << endl << CA_PATTERN_CATALOG_FILEFORMAT_VERSION << endl;
	stream << CA_MAX_PATTERN_NEIGHBORS << endl;
	stream << setprecision(16);

	stream << catalog.coordinationPatterns().size() << endl;
	BOOST_FOREACH(const CoordinationPattern& pattern, catalog.coordinationPatterns()) {
		writeCoordinationPattern(stream, pattern);
	}
	stream << catalog.superPatterns().size() << endl;
	BOOST_FOREACH(const SuperPattern& pattern, catalog.superPatterns()) {
		writeSuperPattern(stream, pattern);
	}
}

void PatternCatalogWriter::writeCoordinationPattern(std::ostream& stream, const CoordinationPattern& pattern)
{
	switch(pattern.coordinationPatternType) {
	case CoordinationPattern::COORDINATION_PATTERN_CNA: stream << "CNA" << endl; break;
	case CoordinationPattern::COORDINATION_PATTERN_ACNA: stream << "ACNA" << endl; break;
	case CoordinationPattern::COORDINATION_PATTERN_NDA: stream << "NDA" << endl; break;
	case CoordinationPattern::COORDINATION_PATTERN_DIAMOND: stream << "DIAMOND" << endl; break;
	default: context().raiseErrorOne("Cannot serialize coordination pattern of unknown type: %i", pattern.coordinationPatternType);
	}
	stream << pattern.name << endl;
	stream << pattern.neighbors.size() << endl;
	BOOST_FOREACH(const CoordinationPatternNeighbor& n, pattern.neighbors) {
		writeVector(stream, n.vec);
		stream << n.scaleFactor << endl;
	}
	stream << pattern.cutoffGap << endl;
	stream << pattern.lastInnerShellRadius << endl;
	stream << pattern.firstOuterShellRadius << endl;
	stream << pattern.numShells << endl;
	stream << pattern.permutations.size() << endl;
	for(size_t j = 0; j < pattern.permutations.size(); j++) {
		const CoordinationPermutation& p = pattern.permutations[j];
		CALIB_ASSERT(pattern.neighbors.size() == p.size());
		for(int i = 0; i < p.size(); i++)
			stream << p[i] << ' ';
		stream << endl;
		writeMatrix(stream, pattern.permutationMatrices[j]);
	}

	if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_CNA || pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA || pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
		const CNACoordinationPattern& cnaPattern = static_cast<const CNACoordinationPattern&>(pattern);
		for(int i = 0; i < pattern.numNeighbors(); i++) {
			stream << cnaPattern.cnaNeighbors[i].numCommonNeighbors << " " << cnaPattern.cnaNeighbors[i].numBonds << " " << cnaPattern.cnaNeighbors[i].maxChainLength << endl;
		}
		for(int i = 0; i < pattern.numNeighbors(); i++) {
			for(int j = 0; j < i; j++) {
				stream << cnaPattern.neighborArray.neighborBond(i,j) << ' ';
			}
			stream << endl;
		}

		if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA) {
			const AdaptiveCNACoordinationPattern& acnaPattern = static_cast<const AdaptiveCNACoordinationPattern&>(pattern);
			stream << acnaPattern.localCNACutoff << endl;
		}
		else if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
			const DiamondCoordinationPattern& diamondPattern = static_cast<const DiamondCoordinationPattern&>(pattern);
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 3; j++) {
					stream << diamondPattern.secondToFirstMap[i][j] << endl;
				}
			}
		}
	}
	else if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_NDA) {
		const NDACoordinationPattern& ndaPattern = static_cast<const NDACoordinationPattern&>(pattern);
		for(int i = 0; i < pattern.numNeighbors(); i++) {
			for(int j = 0; j < i; j++) {
				stream << ndaPattern.bonds[i][j].minDistanceSquared << " " << ndaPattern.bonds[i][j].maxDistanceSquared << " ";
			}
			stream << endl;
		}
	}
}

void PatternCatalogWriter::writeSuperPattern(std::ostream& stream, const SuperPattern& pattern)
{
	stream << pattern.name << endl;
	stream << pattern.fullName << endl;
	stream << pattern.numNodes << " " << pattern.numCoreNodes << endl;
	BOOST_FOREACH(const SuperPatternNode& node, pattern.nodes) {
		stream << node.species << endl;
		stream << node.coordinationPattern->index << endl;
		for(int i = 0; i < node.coordinationPattern->numNeighbors(); i++) {
			const SuperPatternBond& bond = node.neighbors[i];
			stream << bond.neighborNodeIndex << endl;
			writeVector(stream, bond.referenceVector);
			stream << bond.reverseNodeNeighborIndex << endl;
			stream << bond.numCommonNeighbors << endl;
			for(int j = 0; j < bond.numCommonNeighbors; j++) {
				stream << bond.commonNeighborNodeIndices[j][0] << " " << bond.commonNeighborNodeIndices[j][1] << endl;
			}
		}
		stream << (node.overlapPattern ? node.overlapPattern->index : 0) << endl;
		stream << node.overlapPatternNode << endl;
		CALIB_ASSERT(node.nodeNodeDistances.size() == pattern.numNodes);
		for(int i = 0; i < pattern.numNodes; i++) {
			stream << node.nodeNodeDistances[i] << ' ';
		}
		stream << endl;
		stream << node.spaceGroupEntries.size() << endl;
		BOOST_FOREACH(const SpaceGroupElement& sge, node.spaceGroupEntries) {
			stream << sge.targetNodeIndex << endl;
			stream << sge.permutationIndex << endl;
			writeMatrix(stream, sge.tm);
		}
	}
	stream << pattern.periodicity << endl;
	switch(pattern.vicinityCriterion) {
	case SuperPattern::VICINITY_CRITERION_LATTICE: stream << "lattice" << endl; break;
	case SuperPattern::VICINITY_CRITERION_INTERFACE: stream << "interface" << endl; break;
	case SuperPattern::VICINITY_CRITERION_POINTDEFECT: stream << "pointdefect" << endl; break;
	case SuperPattern::VICINITY_CRITERION_SHORTESTPATH: stream << "shortestpath" << endl; break;
	default: context().raiseErrorOne("Cannot serialize super pattern with unknown vicinity criterion: %i", pattern.vicinityCriterion);
	}
	writeVector(stream, pattern.color);
	stream << pattern.burgersVectorFamilies.size() << endl;
	BOOST_FOREACH(const BurgersVectorFamily& family, pattern.burgersVectorFamilies) {
		writeVector(stream, family.burgersVector);
		stream << family.id << endl;
		stream << family.name << endl;
		writeVector(stream, family.color);
	}
	writeMatrix(stream, pattern.misorientationMatrix);
	stream << pattern.parentLatticePatterns.size() << endl;
	BOOST_FOREACH(const SuperPattern* p, pattern.parentLatticePatterns) {
		stream << p->index << " ";
	}
	stream << endl;
	writeMatrix(stream, pattern.periodicCell);
	stream << 0xFFABCD << endl;
}


}; // End of namespace
