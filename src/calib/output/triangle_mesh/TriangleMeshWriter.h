///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_TRIANGLE_MESH_WRITER_H
#define __CA_TRIANGLE_MESH_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../trianglemesh/TriangleMesh.h"

namespace CALib {

/**
 * Writes a TriangleMesh to a file, which can be visualized.
 */
class TriangleMeshWriter : public ContextReference
{
public:

	/// Constructor.
	TriangleMeshWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs a triangle mesh to a VTK file.
	void writeVTKFile(std::ostream& stream, const TriangleMesh& mesh);
};

}; // End of namespace

#endif // __CA_TRIANGLE_MESH_WRITER_H
