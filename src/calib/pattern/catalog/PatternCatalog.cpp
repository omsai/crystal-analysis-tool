///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "PatternCatalog.h"
#include "../../context/CAContext.h"
#include "../../output/patterns/PatternCatalogWriter.h"
#include "../../input/patterns/PatternCatalogReader.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Returns the super pattern with the given name (or NULL if it doesn't exist).
******************************************************************************/
SuperPattern const* PatternCatalog::superPatternByName(const string& name) const
{
	BOOST_FOREACH(const SuperPattern& pattern, _superPatterns) {
		if(pattern.name == name)
			return &pattern;
	}
	return NULL;
}

/******************************************************************************
* Uses MPI to broadcast the catalog from the master processor to all
* other processors.
******************************************************************************/
void PatternCatalog::broadcast()
{
	if(parallel().processorCount() == 1)
		return;	// Nothing to do.

	if(parallel().isMaster()) {
		// To transmit the catalog data, first serialize data into a memory buffer.
		ostringstream memory_stream;
		PatternCatalogWriter(context()).write(memory_stream, *this);

		// Broadcast data to other processors.
		string data(memory_stream.str());
		parallel().broadcast(data);
	}
	else {
		// Receive serialized version of catalog from master.
		string data;
		parallel().broadcast(data);

		// Deserialize catalog data.
		istringstream memory_stream(data);
		PatternCatalogReader(context()).read(memory_stream, *this);
	}
}


}; // End of namespace
