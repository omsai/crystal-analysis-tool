///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CALIB_PATTERN_CATALOG_H
#define __CALIB_PATTERN_CATALOG_H

#include "../../CALib.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../coordinationpattern/CoordinationPattern.h"
#include "../coordinationpattern/cna/CNACoordinationPattern.h"
#include "../coordinationpattern/acna/AdaptiveCNACoordinationPattern.h"
#include "../coordinationpattern/nda/NDACoordinationPattern.h"
#include "../coordinationpattern/diamond/DiamondCoordinationPattern.h"
#include "../superpattern/SuperPattern.h"

namespace CALib {

/**
 * A collection of coordination patterns and super patterns.
 */
class PatternCatalog : public ContextReference
{
public:

	/********************************* Initialization ************************************/

	/// Constructor.
	PatternCatalog(CAContext& context) : ContextReference(context) {}

	/// Uses MPI to broadcast the catalog's data from the master processor to all other processors.
	void broadcast();

	/****************************** Coordination patterns ********************************/

	/// Returns the list of coordination patterns stored in the catalog.
	const boost::ptr_vector<CoordinationPattern>& coordinationPatterns() const {
		return _coordinationPatterns;
	}

	/// Returns the coordination pattern with the given index (1-based).
	const CoordinationPattern& coordinationPatternByIndex(int index) const {
		CALIB_ASSERT(index >= 1 && index <= (int)_coordinationPatterns.size());
		CALIB_ASSERT(_coordinationPatterns[index-1].index == index);
		return _coordinationPatterns[index-1];
	}

	/// Insert a new pattern into the catalog.
	void addCoordinationPattern(CoordinationPattern* pattern) {
		_coordinationPatterns.push_back(pattern);
		pattern->index = _coordinationPatterns.size();
	}

	/// Insert a new pattern into the catalog.
	void addSuperPattern(SuperPattern* pattern) {
		_superPatterns.push_back(pattern);
		pattern->index = _superPatterns.size();
	}

	/********************************* Super patterns ************************************/

	/// Returns the list of super patterns in the catalog.
	const boost::ptr_vector<SuperPattern>& superPatterns() const {
		return _superPatterns;
	}

	/// Returns the super pattern with the given name (or NULL if it doesn't exist).
	SuperPattern const* superPatternByName(const std::string& name) const;

	/// Returns the super pattern with the given index (1-based).
	const SuperPattern& superPatternByIndex(int index) const {
		CALIB_ASSERT(index >= 1 && index <= (int)_superPatterns.size());
		CALIB_ASSERT(_superPatterns[index-1].index == index);
		return _superPatterns[index-1];
	}

protected:

	/// List of all coordination patterns.
	boost::ptr_vector<CoordinationPattern> _coordinationPatterns;

	/// List of all super patterns.
	boost::ptr_vector<SuperPattern> _superPatterns;
};

}; // End of namespace

#endif // __CALIB_PATTERN_CATALOG_H

