///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_COORDINATION_PATTERN_H
#define __CA_COORDINATION_PATTERN_H

#include "../../CALib.h"
#include "../../util/FixedCapacityVector.h"

namespace CALib {

/**
 * Stores information about a neighbor bond from the central atom to one of its neighbors.
 */
struct CoordinationPatternNeighbor
{
	/// The vector to the neighbor atom.
	Vector3 vec;

	/// The factor is multiplied with the bond length in the actual crystal to determine a local scaling factor.
	CAFloat scaleFactor;
};

/// Stores the neighbor bonds of a coordination pattern.
typedef FixedCapacityVector<CoordinationPatternNeighbor, CA_MAX_PATTERN_NEIGHBORS> CoordinationPatternNeighborList;

/// A vector that stores a permutation of atom neighbors.
typedef FixedCapacityVector<int, CA_MAX_PATTERN_NEIGHBORS> CoordinationPermutation;

/**
 * Stores a coordination pattern that characterizes the local arrangement of neighbors
 * around a central atom.
 */
class CoordinationPattern
{
public:

	/// Declare a virtual destructor for proper polymorphism.
	virtual ~CoordinationPattern() {}

	/// The types of coordination patterns.
	enum Type {
		COORDINATION_PATTERN_NONE,	// Undefined coordination pattern type (not used)
		COORDINATION_PATTERN_NDA,		// Neighbor distance analysis
		COORDINATION_PATTERN_CNA,		// Common neighbor analysis
		COORDINATION_PATTERN_ACNA,		// Adaptive common neighbor analysis
		COORDINATION_PATTERN_DIAMOND,	// Diamond-specific common neighbor analysis
	};

	/// The type of the coordination pattern.
	Type coordinationPatternType;

	/// The minimum distance between the last shell of atoms within the cutoff radius and the first shell outside the cutoff.
	CAFloat cutoffGap;

	/// The distance of the outermost neighbor of the central atom taken into account by the pattern.
	CAFloat lastInnerShellRadius;

	/// The distance of the neighbor next to the outermost neighbor taken into account by the pattern.
	CAFloat firstOuterShellRadius;

	/// Number of neighbors shells.
	int numShells;

	/// The human-readable name of this coordination pattern.
	std::string name;

	/// The index of this pattern in the database of coordination patterns.
	int index;

	/// List of neighbor atoms.
	CoordinationPatternNeighborList neighbors;

	/// The list of permutations of the pattern that map it to itself.
	std::vector<CoordinationPermutation> permutations;

	/// The list of inverse permutations of the pattern that map it to itself.
	std::vector<CoordinationPermutation> inversePermutations;

	/// The transformations corresponding to the neighbor index permutations.
	std::vector<Matrix3> permutationMatrices;

	/// Returns the number of neighbors of the central atom.
	int numNeighbors() const { return neighbors.size(); }

	/// Returns the maximum number of neighbors taken into account by this pattern.
	virtual int maxNeighbors() const { return numNeighbors(); }

protected:

	/// Constructor.
	CoordinationPattern(Type _coordinationPatternType = COORDINATION_PATTERN_NONE) : coordinationPatternType(_coordinationPatternType) {
		// This (abstract) base class should never be default-constructed:
		CALIB_ASSERT(_coordinationPatternType != COORDINATION_PATTERN_NONE);
	}
};

/**
 * A bit-flag array indicating which pairs of neighbors are bonded
 * and which are not.
 */
struct NeighborBondArray
{
	/// Default constructor.
	NeighborBondArray() {
		memset(neighborArray, 0, sizeof(neighborArray));
	}

	/// Two-dimensional bit array that stores the bonds between neighbors.
	unsigned int neighborArray[CA_MAX_PATTERN_NEIGHBORS];

	/// Returns whether two nearest neighbors have a bond between them.
	inline bool neighborBond(int neighborIndex1, int neighborIndex2) const {
		CALIB_ASSERT(neighborIndex1 < CA_MAX_PATTERN_NEIGHBORS);
		CALIB_ASSERT(neighborIndex2 < CA_MAX_PATTERN_NEIGHBORS);
		return (neighborArray[neighborIndex1] & (1<<neighborIndex2));
	}

	/// Sets whether two nearest neighbors have a bond between them.
	void setNeighborBond(int neighborIndex1, int neighborIndex2, bool bonded) {
		CALIB_ASSERT(neighborIndex1 < CA_MAX_PATTERN_NEIGHBORS);
		CALIB_ASSERT(neighborIndex2 < CA_MAX_PATTERN_NEIGHBORS);
		if(bonded) {
			neighborArray[neighborIndex1] |= (1<<neighborIndex2);
			neighborArray[neighborIndex2] |= (1<<neighborIndex1);
		}
		else {
			neighborArray[neighborIndex1] &= ~(1<<neighborIndex2);
			neighborArray[neighborIndex2] &= ~(1<<neighborIndex1);
		}
	}
};

}; // End of namespace

#endif // __CA_COORDINATION_PATTERN_H

