///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_COORDINATION_PATTERN_ANALYSIS_H
#define __CA_COORDINATION_PATTERN_ANALYSIS_H

#include "../../CALib.h"
#include "../../util/MemoryPool.h"
#include "../../context/CAContext.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../atomic_structure/neighbor_list/NeighborList.h"
#include "CoordinationPattern.h"

namespace CALib {

class PatternCatalog;

/**
 * Searches for coordination patterns in an AtomicStructure
 * and assigns one or more patterns to an atom.
 */
class CoordinationPatternAnalysis : public ContextReference
{
public:

	/**
	 * A record of the match between the neighbors of a central atom
	 * and a coordination pattern.
	 */
	struct coordmatch
	{
		CoordinationPattern const* pattern;		///< The pattern that matches with the atom.
		int mapping[CA_MAX_PATTERN_NEIGHBORS];		///< Permutation array that maps neighbor indices of the pattern to atom neighbors.
	};

	/// An iterator over the coordination pattern matches of an atom.
	typedef std::vector<coordmatch>::const_iterator coordmatch_iterator;

	/// A range object that contains all coordination pattern matches of a given atom.
	typedef boost::iterator_range<coordmatch_iterator> coordmatch_range;

public:

	/// Constructor. Takes a reference to the AtomicStructure in which the patterns will be searched.
	CoordinationPatternAnalysis(const AtomicStructure& structure, const NeighborList& neighborList, const PatternCatalog& catalog)
		: ContextReference(structure.context()), _structure(structure), _neighborList(neighborList), _catalog(catalog) {}

	/// Returns a const-reference to the atomic structure in which patterns are searched.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns a const-reference to the neighbor list used for pattern matching.
	const NeighborList& neighborList() const { return _neighborList; }

	/// Returns a const-reference to the catalog of coordination patterns.
	const PatternCatalog& catalog() const { return _catalog; }

	/// Searches the structure for the given patterns.
	void searchCoordinationPatterns(const std::vector<CoordinationPattern const*>& patternsToSearchFor, CAFloat cutoffRadius);

	/// Returns the number of coordination pattern matches found for an atom.
	int coordinationPatternMatchCount(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_coordinationMatchCounts.size());
		return _coordinationMatchCounts[atomIndex];
	}

	/// Returns a range object containing all coordination pattern matches of an atom.
	coordmatch_range coordinationPatternMatches(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_coordinationMatchesHeads.size());
		coordmatch_iterator n_begin = _coordinationPatternMatches.begin() + _coordinationMatchesHeads[atomIndex];
		coordmatch_iterator n_end = n_begin + coordinationPatternMatchCount(atomIndex);
		return boost::make_iterator_range(n_begin, n_end);
	}

	/// Returns the match record for the given atom and coordination pattern.
	boost::optional<coordmatch_iterator> findCoordinationPatternMatch(int atomIndex, const CoordinationPattern& pattern) const {
		coordmatch_range range = coordinationPatternMatches(atomIndex);
		for(coordmatch_iterator i = range.begin(); i != range.end(); ++i)
			if(i->pattern == &pattern) return i;
		return boost::optional<coordmatch_iterator>();
	}

	/// Determines the coordination pattern that best matches an atom.
	coordmatch const* bestMatchingCoordinationPattern(int atomIndex) const;

	/// Quantifies the deviation of an atom's coordination structure from the given
	/// ideal pattern.
	CAFloat calculateDeviationFromCoordinationPattern(const coordmatch& mapping, int centerAtomIndex) const;

protected:

	/// Exchange coordination pattern matches with neighboring processors.
	void exchangeCoordinationPatternMatches();

protected:

	/// The atomic structure in which patterns are searched.
	const AtomicStructure& _structure;

	/// The neighbor list for the atomic structure.
	const NeighborList& _neighborList;

	/// Catalog of patterns.
	const PatternCatalog& _catalog;

	/// Stores the number of pattern matches for each atom.
	std::vector<int> _coordinationMatchCounts;

	/// Stores each atom's head position in the coordination pattern match array.
	std::vector<int> _coordinationMatchesHeads;

	/// Stores the pattern matches for all atoms in one consecutive array.
	std::vector<coordmatch> _coordinationPatternMatches;
};

}; // End of namespace

#endif // __CA_COORDINATION_PATTERN_ANALYSIS_H
