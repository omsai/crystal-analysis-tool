///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __ADAPTIVE_CNA_COORDINATION_PATTERN_H
#define __ADAPTIVE_CNA_COORDINATION_PATTERN_H

#include "../cna/CNACoordinationPattern.h"

namespace CALib {

/**
 * Stores an adaptive common neighbor analysis (a-CNA) pattern.
 */
struct AdaptiveCNACoordinationPattern : public CNACoordinationPattern
{
	/// Constructor.
	AdaptiveCNACoordinationPattern() : CNACoordinationPattern(COORDINATION_PATTERN_ACNA) {}

	/// The cutoff radius used for the CNA.
	CAFloat localCNACutoff;
};

}; // End of namespace

#endif // __ADAPTIVE_CNA_COORDINATION_PATTERN_H

