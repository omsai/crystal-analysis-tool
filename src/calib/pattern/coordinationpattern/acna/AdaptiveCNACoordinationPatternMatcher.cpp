///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AdaptiveCNACoordinationPatternMatcher.h"
#include "AdaptiveCNACoordinationPattern.h"
#include "../../../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

bool AdaptiveCNACoordinationPatternMatcher::initializeNeighborPermutations()
{
	CALIB_ASSERT(atomIndex() >= 0 && atomIndex() < structure().numLocalAtoms());
	CALIB_ASSERT(neighborList().isSorted());

	// Check for sufficient number of neighbors.
	int nn = pattern().numNeighbors();
	if(neighborList().neighborCount(atomIndex()) < nn)
		return false;

	// Determine scale factor that relates the actual distance distribution with the reference distribution.
	CAFloat localScale = 0;
	NeighborList::neighbor_iterator n = neighborList().neighbors(atomIndex()).begin();
	for(int ni = 0; ni < nn; ni++, ++n)
		localScale += sqrt(n->distsq) * pattern().neighbors[ni].scaleFactor;
	CAFloat scaling_factor_sq = square(1.0 / localScale);

	// Check if the (N+1)-th neighbor is far enough away from the central atom.
	if(neighborList().neighborCount(atomIndex()) > nn) {
		CAFloat localGap = neighborList().neighborDistance(atomIndex(), nn) - neighborList().neighborDistance(atomIndex(), nn - 1);
		if(localGap <= pattern().cutoffGap * localScale) {
			return false;
		}
	}

	const AdaptiveCNACoordinationPattern& cnaPattern = static_cast<const AdaptiveCNACoordinationPattern&>(pattern());
	CAFloat cutoffSquared = square(cnaPattern.localCNACutoff) / scaling_factor_sq;

	// Compute bond bit-flag array.
	NeighborList::neighbor_iterator n1 = neighborList().neighbors(atomIndex()).begin();
	for(int ni1 = 0; ni1 < nn; ni1++, ++n1) {
		_neighborArray.setNeighborBond(ni1, ni1, false);
		NeighborList::neighbor_iterator n2 = n1 + 1;
		for(int ni2 = ni1 + 1; ni2 < nn; ni2++, ++n2) {
			bool bonded = (n2->delta - n1->delta).squaredNorm() < cutoffSquared;
			_neighborArray.setNeighborBond(ni1, ni2, bonded);
		}
	}

	// Calculate CNA signatures.
	CNABond cnaSignaturesSorted[CA_MAX_PATTERN_NEIGHBORS];
	for(int ni = 0; ni < nn; ni++) {
		CNACoordinationPattern::calculateCNASignature(_neighborArray, ni, _cnaSignatures[ni], nn);
		cnaSignaturesSorted[ni] = _cnaSignatures[ni];
	}
	sort(cnaSignaturesSorted, cnaSignaturesSorted + nn);

	// Check if sorted CNA signatures are identical.
	for(int ni = 0; ni < nn; ni++) {
		if(cnaSignaturesSorted[ni] != cnaPattern.cnaNeighborsSorted[ni]) {
			return false;
		}
	}

	// Reset permutation.
	_previousIndices.assign(nn, -1);
	_atomNeighborIndices.resize(nn);
	for(int n = 0; n < nn; n++)
		_atomNeighborIndices[n] = n;

	// Find first matching permutation.
	for(;;) {
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), nn);
		if(!boost::next_permutation(_atomNeighborIndices))
			break;
	}

	return false;
}

}; // End of namespace
