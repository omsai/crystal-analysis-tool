///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CNACoordinationPattern.h"

#if defined(__xlC__)
	#include <builtins.h>
#endif

namespace CALib {

/******************************************************************************
* Find all atoms that are nearest neighbors of the given pair of atoms.
******************************************************************************/
int CNACoordinationPattern::findCommonNeighbors(const NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors)
{
	commonNeighbors = neighborArray.neighborArray[neighborIndex];
	CALIB_STATIC_ASSERT(sizeof(commonNeighbors) == 4);
#ifdef __GNUC__
	// Count the number of bits set in neighbor bit field.
	return __builtin_popcount(commonNeighbors);
#elif defined(__xlC__)
	// Count the number of bits set in neighbor bit field.
	return __popcnt4(commonNeighbors);
#else
	// Count the number of bits set in neighbor bit field.
	unsigned int v = commonNeighbors - ((commonNeighbors >> 1) & 0x55555555);
	v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
	return ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
#endif
}

/******************************************************************************
* Finds all bonds between common nearest neighbors.
******************************************************************************/
int CNACoordinationPattern::findNeighborBonds(const NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds)
{
	int numBonds = 0;

	int nib[CA_MAX_PATTERN_NEIGHBORS];
	int nibn = 0;
	int ni1b = 1;
	for(int ni1 = 0; ni1 < numNeighbors; ni1++, ni1b <<= 1) {
		if(commonNeighbors & ni1b) {
			unsigned int b = commonNeighbors & neighborArray.neighborArray[ni1];
			for(int n = 0; n < nibn; n++) {
				if(b & nib[n]) {
					CALIB_ASSERT(numBonds < CA_MAX_PATTERN_NEIGHBORS*CA_MAX_PATTERN_NEIGHBORS);
					neighborBonds[numBonds++] = ni1b | nib[n];
				}
			}

			nib[nibn++] = ni1b;
		}
	}
	return numBonds;
}

/******************************************************************************
* Find all chains of bonds.
******************************************************************************/
static int getAdjacentBonds(unsigned int atom, CNAPairBond* bondsToProcess, int& numBonds, unsigned int& atomsToProcess, unsigned int& atomsProcessed)
{
	int adjacentBonds = 0;
	for(int b = numBonds - 1; b >= 0; b--) {
		if(atom & *bondsToProcess) {
            ++adjacentBonds;
   			atomsToProcess |= *bondsToProcess & (~atomsProcessed);
   			memmove(bondsToProcess, bondsToProcess + 1, sizeof(CNAPairBond) * b);
   			numBonds--;
		}
		else ++bondsToProcess;
	}
	return adjacentBonds;
}

/******************************************************************************
* Find all chains of bonds between common neighbors and determine the length
* of the longest continuous chain.
******************************************************************************/
int CNACoordinationPattern::calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds)
{
    // Group the common bonds into clusters.
	int maxChainLength = 0;
	while(numBonds) {
        // Make a new cluster starting with the first remaining bond to be processed.
		numBonds--;
        unsigned int atomsToProcess = neighborBonds[numBonds];
        unsigned int atomsProcessed = 0;
        int clusterSize = 1;
        do {
        	// Determine the number of trailing 0-bits in atomsToProcess,
        	// starting at the least significant bit position.
#if defined(__GNUC__)
        	// This code uses the intrinsic function provided by GNU compatible compilers.
        	int nextAtomIndex = __builtin_ctz(atomsToProcess);
#elif defined(_MSC_VER)
        	// This code uses the MSVC compiler intrinsic:
			unsigned long nextAtomIndex;
			_BitScanForward(&nextAtomIndex, atomsToProcess);
			CALIB_ASSERT(nextAtomIndex >= 0 && nextAtomIndex < 32);
#elif defined(__xlC__)
			int nextAtomIndex = __cnttz4(atomsToProcess);			
#else
	#error "This code currently supports only GNU compatible compilers and the MSVC compiler."
#endif
			unsigned int nextAtom = 1 << nextAtomIndex;
        	atomsProcessed |= nextAtom;
			atomsToProcess &= ~nextAtom;
			clusterSize += getAdjacentBonds(nextAtom, neighborBonds, numBonds, atomsToProcess, atomsProcessed);
		}
        while(atomsToProcess);
        if(clusterSize > maxChainLength)
        	maxChainLength = clusterSize;
	}
	return maxChainLength;
}

/******************************************************************************
* Calculates the CNA signature for a bond.
******************************************************************************/
void CNACoordinationPattern::calculateCNASignature(const NeighborBondArray& neighborArray, int neighborIndex, CNABond& cnaBond, int nneighbors)
{
	// Determine number of neighbors the pair of atoms has in common.
	unsigned int commonNeighbors;
	cnaBond.numCommonNeighbors = findCommonNeighbors(neighborArray, neighborIndex, commonNeighbors, nneighbors);

	// Determine the number of bonds among the shared neighbors.
	CNAPairBond neighborBonds[CA_MAX_PATTERN_NEIGHBORS*CA_MAX_PATTERN_NEIGHBORS];
	cnaBond.numBonds = findNeighborBonds(neighborArray, commonNeighbors, nneighbors, neighborBonds);

	// Determine the number of bonds in the longest continuous chain.
	cnaBond.maxChainLength = calcMaxChainLength(neighborBonds, cnaBond.numBonds);
}

}; // End of namespace
