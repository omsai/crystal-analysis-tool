///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_NDA_COORDINATION_PATTERN_H
#define __CA_NDA_COORDINATION_PATTERN_H

#include "../CoordinationPattern.h"

namespace CALib {

/**
 * Describes a bond between two neighbors of the central atom.
 */
struct NDABond
{
	/// The minimum distance between the two neighbors.
	CAFloat minDistanceSquared;

	/// The maximum distance between the two neighbors.
	CAFloat maxDistanceSquared;

	/// Used to sort NDA bonds w.r.t to their length.
	bool operator<(const NDABond& other) const { return minDistanceSquared < other.minDistanceSquared; }
};

/**
 * Stores a neighbor distance analysis (NDA) pattern.
 */
struct NDACoordinationPattern : public CoordinationPattern
{
	/// Constructor.
	NDACoordinationPattern() : CoordinationPattern(COORDINATION_PATTERN_NDA) {}

	/// The bonds between each pair of neighbors.
	NDABond bonds[CA_MAX_PATTERN_NEIGHBORS][CA_MAX_PATTERN_NEIGHBORS];

	/// The sorted list of bonds.
	NDABond bondRangeDistribution[CA_MAX_PATTERN_NEIGHBORS*CA_MAX_PATTERN_NEIGHBORS];
};

}; // End of namespace

#endif // __CA_NDA_COORDINATION_PATTERN_H

