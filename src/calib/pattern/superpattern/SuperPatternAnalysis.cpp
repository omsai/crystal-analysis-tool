///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "SuperPatternAnalysis.h"
#include "SuperPatternMatcher.h"
#include "../catalog/PatternCatalog.h"
#include "../../atomic_structure/GhostAtomCommunicationHelper.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Searches the structure for the given super patterns.
******************************************************************************/
vector<SuperPatternAnalysis::AnalysisResultsRecord> SuperPatternAnalysis::searchSuperPatterns(const vector<SuperPattern const*>& patternsToSearchFor)
{
	vector<AnalysisResultsRecord> results;

	// Reset internal data structures.
	reset();
	_clusterGraph.clear();

	// Look only for lattice patterns first.
	BOOST_REVERSE_FOREACH(SuperPattern const* pattern, patternsToSearchFor) {
		CALIB_ASSERT(pattern != NULL);
		if(pattern->isLattice() == true)
			results.push_back(searchSuperPattern(*pattern));
	}

	// Then search for defect patterns.
	BOOST_REVERSE_FOREACH(SuperPattern const* pattern, patternsToSearchFor) {
		if(pattern->isLattice() == false)
			results.push_back(searchSuperPattern(*pattern));
	}

	return results;
}

/******************************************************************************
* Searches the input structure for a single super pattern.
******************************************************************************/
SuperPatternAnalysis::AnalysisResultsRecord SuperPatternAnalysis::searchSuperPattern(const SuperPattern& pattern)
{
	// For collecting statistics:
	boost::array<AtomInteger,3> counters;
	counters.assign(0);
	AtomInteger& numClusters = counters[0];
	AtomInteger& numAtoms = counters[1];
	AtomInteger& numDisclinations = counters[2];

	/// This helper object is passed to the SuperPatternMatcher to filter
	/// the atoms that may become part of a cluster.
	class AtomFilter : public AtomMatchFilter {
		const SuperPatternAnalysis& _analysis;
	public:
		/// Constructor.
		AtomFilter(const SuperPatternAnalysis& analysis) : _analysis(analysis) {}
		/// Returns true if the given atom may be mapped to the given super pattern node.
		bool acceptAtom(int atomIndex, int nodeIndex, const SuperPattern& pattern) {
			// Always allow overlap atoms/nodes to be matched:
			if(nodeIndex >= pattern.numCoreNodes) {
				return _analysis.atomCluster(atomIndex) != NULL && &_analysis.atomPattern(atomIndex) == pattern.nodes[nodeIndex].overlapPattern;
				//return true;
			}
			// Allow atoms which are not part of a cluster yet.
			else if(_analysis.atomCluster(atomIndex) == NULL) return true;
			// Allow defect clusters to overlap with lattice clusters.
			else if(pattern.isLattice() == false && _analysis.atomCluster(atomIndex)->pattern->isLattice()) return true;
			else return false;
		}
	};
	AtomFilter filter(*this);

	SuperPatternMatcher matcher(_coordAnalysis, _catalog, pattern, &filter);
	while(matcher.findNextCluster()) {

		// Create a new cluster for the match found by the super pattern matcher.
		numClusters++;
		Cluster* cluster = _clusterGraph.createCluster(&pattern);
		cluster->centerOfMass.setZero();

		// Calculate orientation matrix of cluster.
		cluster->orientation = matcher.calculateOrientation();

		// Add matching atoms to the cluster.
		BOOST_FOREACH(const SuperPatternMatcher::MatchedAtom& atom, matcher.matchingAtoms()) {
			if(matcher.isCoreNodeAtom(atom)) {
				int atomIndex = atom.atomIndex;
				if(structure().isGhostAtom(atomIndex) == false) {
					cluster->centerOfMass += structure().atomPosition(atomIndex);
					cluster->atomCount++;
				}

				CALIB_ASSERT(atomCluster(atomIndex) == NULL || atomPattern(atomIndex).isLattice());

				// Register atom as part of the current cluster.
				_clusterAtoms[atomIndex].cluster = cluster;
				_clusterAtoms[atomIndex].nodeIndex = atom.nodeIndex;
				_clusterAtoms[atomIndex].permutationIndex = atom.permutationIndex;
				_clusterAtoms[atomIndex].coordination = atom.coordination;
			}
		}

		// Calculate center of mass of the current cluster.
		if(cluster->atomCount != 0)
			cluster->centerOfMass = structure().wrapPoint(cluster->centerOfMass / cluster->atomCount);

#if 0
		context().msgLogger() << "* Created cluster id=" << cluster->id.id << ":" << endl;
		context().msgLogger() << "    Pattern: [" << pattern.index << "]" << pattern.name << endl;
		context().msgLogger() << "    Number of atoms: " << cluster->atomCount << endl;
		context().msgLogger() << "    Orientation matrix: Det = " << cluster->orientation.determinant() << " " << cluster->orientation.row(0) << " " << cluster->orientation.row(1) << " " << cluster->orientation.row(2) << endl;
#endif

		if(pattern.isLattice() == false) {

			// Find transformation matrices connecting this cluster with surrounding clusters.
			BOOST_FOREACH(const SuperPatternMatcher::MatchedAtom& atom, matcher.matchingAtoms()) {
				if(matcher.isCoreNodeAtom(atom)) continue;

				const SuperPatternNode& node = pattern.nodes[atom.nodeIndex];
				const ClusterAtom& clusterAtom = _clusterAtoms[atom.atomIndex];
				const SuperPattern& latticePattern = *node.overlapPattern;

				if(clusterAtom.cluster == NULL || clusterAtom.cluster->pattern != &latticePattern)
					continue;	// It's not a valid overlap at this atom.

				// Check if the right nodes overlap.
				bool isOverlapValid = false;
				BOOST_FOREACH(const SpaceGroupElement& sge, latticePattern.nodes[node.overlapPatternNode].spaceGroupEntries) {
					if(clusterAtom.nodeIndex == sge.targetNodeIndex) {
						isOverlapValid = true;
						break;
					}
				}
				if(!isOverlapValid) continue;

				CALIB_ASSERT(atom.coordinationPattern() == clusterAtom.patternNode().coordinationPattern);
				CALIB_ASSERT(atom.coordination == clusterAtom.coordination);

				// Create a transition edge in the cluster graph between the defect cluster and the parent lattice cluster.
				_clusterGraph.createClusterTransition(cluster, clusterAtom.cluster,
						computeTransformationFromOverlapAtom(atom, node, clusterAtom));
			}
		}

		numAtoms += cluster->atomCount;
		if(matcher.foundDisclination())
			numDisclinations++;
	}

	boost::array<AtomInteger,3> totalCounters = parallel().reduce_sum(counters);
	if(parallel().isMaster()) {
		CALIB_ASSERT(pattern.numCoreNodes > 0);

		context().msgLogger() << "Super pattern '[" << pattern.index << "]" << pattern.name << "': ";
		AtomInteger unitCells = totalCounters[1] / pattern.numCoreNodes;
		context().msgLogger() << totalCounters[1] << " matching atoms (" << totalCounters[0] << " clusters, " << unitCells << " units)" << endl;
		if(totalCounters[2] != 0)
			context().msgLogger() << "Disclinations have been detected in " << totalCounters[2] << " clusters." << endl;
	}

	AnalysisResultsRecord results;
	results.pattern = &pattern;
	results.numMatchingAtoms = totalCounters[1];
	results.numMatchingUnits = totalCounters[1] / pattern.numCoreNodes;
	results.numDisclinations = totalCounters[2];
	return results;
}

/******************************************************************************
* Given an atom where two clusters overlap, computes the crystallographic
* orientation relationship between the two.
******************************************************************************/
Matrix3 SuperPatternAnalysis::computeTransformationFromOverlapAtom(const SuperPatternMatcher::MatchedAtom& atom, const SuperPatternNode& node, const ClusterAtom& otherAtom)
{
	int nn = atom.coordinationPattern()->numNeighbors();
	const SuperPatternNode& otherNode = otherAtom.patternNode();

	CALIB_ASSERT(otherNode.coordinationPattern == node.coordinationPattern);

	// Generate mapping from atom neighbors to node neighbors.
	boost::array<int,CA_MAX_PATTERN_NEIGHBORS*4> inverseMapping;
	inverseMapping.fill(-1);
	for(int n = 0; n < nn; n++) {
		int i = nodeNeighborToAtomNeighbor(otherAtom, n);
		if(i < inverseMapping.size())
			inverseMapping[i] = n;
	}

	FrameTransformation tm;
	for(int n = 0; n < nn; n++) {
		int i = atom.nodeNeighborToAtomNeighbor(n);
		if(i < inverseMapping.size() && inverseMapping[i] != -1) {
			tm.addVector(node.neighbors[n].referenceVector,
					otherNode.neighbors[inverseMapping[i]].referenceVector);
		}
	}

	return tm.computeTransformation();
}

/// This structure is used to exchange per-atom cluster information with neighboring processors.
struct AtomSuperPatternComm
{
	ClusterId clusterId;		// The ID of the super cluster the atom belongs to.
	short nodeIndex;			// The super pattern node assigned to the atom.
	short permutationIndex;		// The permutation of the node neighbors.
};

/******************************************************************************
* After the pattern analysis phase, every processor has generated a local
* cluster graph that contains only local nodes. This function will
* establish graph edges between nodes from different processors by exchanging
* information about cluster assignments of ghost atoms between neighboring
* processors.
******************************************************************************/
void SuperPatternAnalysis::connectDistributedClusterGraph()
{
	GhostAtomCommunicationHelper<AtomSuperPatternComm> commHelper(structure());
	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		for(int dir = 0; dir <= 1; dir++) {

			// Exchange the clusters assigned to local atoms.
			commHelper.prepare(dim, dir);
			while(commHelper.nextSendItem()) {
				const ClusterAtom& atom = _clusterAtoms[commHelper.sendAtomIndex()];
				if(atom.cluster != NULL) {
					commHelper.sendItem().clusterId = atom.cluster->id;
					commHelper.sendItem().nodeIndex = atom.nodeIndex;
					commHelper.sendItem().permutationIndex = atom.permutationIndex;
				}
				else commHelper.sendItem().clusterId.id = -1;
			}
			commHelper.communicate(dim, dir);

			// Usually, there are many atoms where two clusters from different processors
			// overlap. To speed up the connection process, we keep track of which clusters have already
			// been connected, and don't do it a second time.
			set< pair<Cluster*, Cluster*> > connectedClusterPairs;

			// Unpack received data from the receive buffer.
			while(commHelper.nextReceiveItem()) {
				ClusterAtom& localAtom = _clusterAtoms[commHelper.receiveAtomIndex()];
				const AtomSuperPatternComm& remoteAtom = commHelper.receiveItem();

				// Check if two clusters overlap at this atom.
				if(remoteAtom.clusterId.id == -1) continue;	// Remote atom has no cluster assigned.
				if(localAtom.cluster == NULL) continue;		// Local atom has not cluster assigned.

				// Look up the two clusters.
				Cluster* localCluster = localAtom.cluster;
				Cluster* remoteCluster = clusterGraph().findCluster(remoteAtom.clusterId);
				CALIB_ASSERT(remoteCluster != NULL);

				// Both cluster should match to the same atomic pattern.
				SuperPattern const* pattern = localCluster->pattern;
				CALIB_ASSERT(pattern != NULL);
				if(pattern != remoteCluster->pattern) continue;

				// A cluster may span the entire processor domain and overlap with itself.
				// In this case we have to do nothing.
				if(remoteCluster == localCluster)
					continue;

				// Check if have connected these two clusters already.
				if(connectedClusterPairs.find(make_pair(localCluster, remoteCluster)) != connectedClusterPairs.end())
					continue;

				const SuperPatternNode& localNode = localAtom.patternNode();
				BOOST_FOREACH(const SpaceGroupElement& sge, localNode.spaceGroupEntries) {

					if(remoteAtom.nodeIndex != sge.targetNodeIndex)
						continue;

					const SuperPatternNode& remoteNode = pattern->nodes[remoteAtom.nodeIndex];
					CALIB_ASSERT(remoteNode.coordinationPattern == localNode.coordinationPattern);

					const CoordinationPattern* coordPattern = localNode.coordinationPattern;
					CALIB_ASSERT(localAtom.coordination->pattern == coordPattern);
					CALIB_ASSERT(sge.permutationIndex < coordPattern->permutations.size());

					const CoordinationPermutation& spaceGroupPermutation = coordPattern->permutations[sge.permutationIndex];
					const CoordinationPermutation& remotePermutation = coordPattern->permutations[remoteAtom.permutationIndex];
					bool permutationMatches = true;
					for(int n = 0; n < coordPattern->numNeighbors(); n++) {
						int remoteAtomNeighborIndex = localAtom.coordination->mapping[remotePermutation[n]];
						int localAtomNeighborIndex = nodeNeighborToAtomNeighbor(localAtom, spaceGroupPermutation[n]);
						if(remoteAtomNeighborIndex != localAtomNeighborIndex) {
							permutationMatches = false;
							break;
						}
					}
					if(!permutationMatches) continue;

					// Create transition between local and remote cluster.
					_clusterGraph.createClusterTransition(localCluster, remoteCluster, sge.tm);
					connectedClusterPairs.insert(make_pair(localCluster, remoteCluster));

					break;
				}
			}
		}
	}
}

/***************************************** of clusters*************************************
* After the full distributed cluster graph has been generated, this will
* synchronize the assignment of clusters to atoms of all processors.
* That is, ghost atoms will become part of remote clusters.
******************************************************************************/
void SuperPatternAnalysis::synchronizeClusterAssignments()
{
	CALIB_ASSERT(parallel().isParallelMode());

	GhostAtomCommunicationHelper<AtomSuperPatternComm> commHelper(structure());
	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		for(int dir = 0; dir <= 1; dir++) {

			// Exchange the clusters assigned to local atoms.
			commHelper.prepare(dim, dir);
			while(commHelper.nextSendItem()) {
				const ClusterAtom& atom = _clusterAtoms[commHelper.sendAtomIndex()];
				if(atom.cluster != NULL) {
					CALIB_ASSERT(atom.cluster->atomCount != 0);
					commHelper.sendItem().clusterId = atom.cluster->id;
					commHelper.sendItem().nodeIndex = atom.nodeIndex;
					commHelper.sendItem().permutationIndex = atom.permutationIndex;
				}
				else commHelper.sendItem().clusterId.id = -1;
			}
			commHelper.communicate(dim, dir);

			// Unpack received data from receive buffer.
			while(commHelper.nextReceiveItem()) {
				int ghostAtomIndex = commHelper.receiveAtomIndex();
				ClusterAtom& ghostAtom = _clusterAtoms[ghostAtomIndex];
				const AtomSuperPatternComm& remoteAtom = commHelper.receiveItem();

				if(remoteAtom.clusterId.id == -1 || neighborList().neighborCount(ghostAtomIndex) == 0) {
					ghostAtom.cluster = NULL;
					continue;
				}
				Cluster* remoteCluster = clusterGraph().findCluster(remoteAtom.clusterId);
				CALIB_ASSERT(remoteCluster != NULL);
				ghostAtom.cluster = remoteCluster;
				ghostAtom.nodeIndex = remoteAtom.nodeIndex;
				ghostAtom.permutationIndex = remoteAtom.permutationIndex;

				const CoordinationPattern& coordPattern = *ghostAtom.patternNode().coordinationPattern;
				boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> match = _coordAnalysis.findCoordinationPatternMatch(ghostAtomIndex, coordPattern);
				CALIB_ASSERT(match);
				ghostAtom.coordination = &**match;
			}
		}
	}
}

/******************************************************************************
* Determines whether the given atom (belonging to a cluster) is part of an
* anti-phase boundary.
******************************************************************************/
bool SuperPatternAnalysis::isAntiPhaseBoundaryAtom(int atomIndex) const
{
	CALIB_ASSERT(atomIndex >= 0 && atomIndex < _clusterAtoms.size());
	const ClusterAtom& atom = _clusterAtoms[atomIndex];
	const SuperPattern& pattern = atom.pattern();
	const SuperPatternNode& node = atom.patternNode();

	for(int nodeNeighborIndex = 0; nodeNeighborIndex < node.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
		int atomNeighborIndex = nodeNeighborToAtomNeighbor(atom, nodeNeighborIndex);
		int neighborAtomIndex = neighborList().neighbor(atomIndex, atomNeighborIndex);
		Cluster* otherCluster = atomCluster(neighborAtomIndex);
		if(otherCluster == NULL) continue;
		if(otherCluster->pattern != &pattern) continue;
		if(otherCluster != atom.cluster)
			return true;
		if(atomPatternNodeIndex(neighborAtomIndex) != node.neighbors[nodeNeighborIndex].neighborNodeIndex)
			return true;
	}

	return false;
}


}; // End of namespace
