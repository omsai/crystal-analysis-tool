///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_SUPER_PATTERN_MATCHER_H
#define __CA_SUPER_PATTERN_MATCHER_H

#include "../../CALib.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../coordinationpattern/CoordinationPatternAnalysis.h"
#include "SuperPattern.h"

namespace CALib {

class PatternCatalog;

/**
 * Abstract base class for filter objects that can be passed to the SuperPatternMatcher to
 * restrict the pattern search to certain atoms..
 */
class AtomMatchFilter {
public:
	/// No-op destructor.
	virtual ~AtomMatchFilter() {}
	/// Returns true if the given atom may be mapped to the given super pattern node.
	virtual bool acceptAtom(int atomIndex, int nodeIndex, const SuperPattern& pattern) = 0;
};

/**
 * Given a super pattern and an input structure, finds all occurrences of the
 * pattern in the input structure.
 */
class SuperPatternMatcher
{
public:

	struct MatchedAtom;
	struct visited_tag;
	struct tobechecked_tag;

	typedef boost::intrusive::slist_base_hook<
			boost::intrusive::tag<visited_tag>,
			boost::intrusive::link_mode< boost::intrusive::normal_link>
	> visited_slist_hook;
	typedef boost::intrusive::slist<MatchedAtom,
			boost::intrusive::base_hook<visited_slist_hook>,
			boost::intrusive::constant_time_size<false>,
			boost::intrusive::linear<true>
	> visited_slist;

	typedef boost::intrusive::slist_base_hook<
			boost::intrusive::tag<tobechecked_tag>,
			boost::intrusive::link_mode< boost::intrusive::normal_link>
	> tobechecked_slist_hook;
	typedef boost::intrusive::slist<MatchedAtom,
			boost::intrusive::base_hook<tobechecked_slist_hook>,
			boost::intrusive::constant_time_size<false>,
			boost::intrusive::linear<true>
	> tobechecked_slist;

	/// Stores information about an atom from the input structure that has been matched
	/// to a super pattern node.
	struct MatchedAtom : public visited_slist_hook, tobechecked_slist_hook
	{
		enum BitFlags {
			MATCHES,
			IS_IN_QUEUE,
			DISCLINATION_PATH,
			DISCLINATION_BARRIER,
			NUM_FLAGS
		};

		/// The index of the atom in the input structure.
		int atomIndex;

		/// The index of the super pattern node that has been matched to the atom.
		int nodeIndex;

		/// The coordination pattern match record of the input atom.
		CoordinationPatternAnalysis::coordmatch const* coordination;

		/// The permutation from the list of permutations of the coordination pattern that maps
		/// neighbor indices from the super pattern node to neighbor indices of the coordination pattern.
		int permutationIndex;

		/// Next entry in the linked list of atoms which still need to be visited
		MatchedAtom* nextInQueue;

		/// The predecessor in the recursive algorithm.
		MatchedAtom* predecessor;

		/// The bit flags array.
		std::bitset<NUM_FLAGS> flags;

		/// Returns a pointer to the coordination pattern associated with the atom.
		inline CoordinationPattern const* coordinationPattern() const {
			return coordination->pattern;
		}

		/// Returns the number of neighbors of the atom.
		inline int numNeighbors() const {
			return coordinationPattern()->numNeighbors();
		}

		/// For a given neighbor of a super pattern node, returns the
		/// corresponding index in the atom's neighbor list.
		int nodeNeighborToAtomNeighbor(int superPatternNeighborIndex) const {
			CALIB_ASSERT(permutationIndex < (int)coordinationPattern()->permutations.size());
			int coordPatternNeighborIndex = coordinationPattern()->permutations[permutationIndex][superPatternNeighborIndex];
			return coordination->mapping[coordPatternNeighborIndex];
		}

		/// Returns true if match between this atom and the super pattern node was successful and if
		/// the atom has not been marked as a disclination barrier.
		bool isValidMatch() const {
			return (flags.test(MatchedAtom::MATCHES) == true) &&
					(flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);
		}
	};

	/// This function object is passed to the filter_iterator to control which matched atoms are
	/// exposed to the caller.
	struct MatchedAtomFilter {
		bool operator ()(const MatchedAtom& atom) const {
			// Return only atoms with a successful match and which are not marked as disclination barrier atoms.
			return atom.isValidMatch();
		}
	};

	/// Iterator over all matching atoms.
	typedef boost::filter_iterator<MatchedAtomFilter, visited_slist::const_iterator> match_iterator;

public:

	/// Constructor.
	SuperPatternMatcher(const CoordinationPatternAnalysis& coordAnalysis, const PatternCatalog& catalog, const SuperPattern& pattern, AtomMatchFilter* filter = NULL);

	/// Returns the pattern that this matcher object searches for.
	const SuperPattern& pattern() const { return _pattern; }

	/// Returns the atomic structure in which this matcher object searches for the pattern.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the results of the coordination pattern analysis which serves as the basis for the super pattern analysis.
	const CoordinationPatternAnalysis& coordAnalysis() const { return _coordAnalysis; }

	/// Finds the next occurrence of the super pattern in the input structure.
	/// Returns false if no more matches were found.
	bool findNextCluster();

	/// Tries to match the pattern starting at the given atom.
	bool checkPermutation(int seedAtomIndex, int seedNodeIndex, int seedPermutationIndex);

	/// Returns a range object that contains all successfully matched atoms that form the current cluster.
	boost::iterator_range<match_iterator> matchingAtoms() const {
		return boost::make_iterator_range(
				match_iterator(_visitedAtoms.begin(), _visitedAtoms.end()),
				match_iterator(_visitedAtoms.end(), _visitedAtoms.end()));
	}

	/// After a cluster of atoms matching the pattern has been found,
	/// this methods calculates an average orientation matrix that transforms
	/// vectors from the ideal reference frame of the cluster to the global simulation frame.
	Matrix3 calculateOrientation() const;

	/// Returns true if a disclination has been encountered in the last cluster found.
	bool foundDisclination() const { return _foundDisclination; }

	/// Returns true if this matched atom was mapped to a core node of the super pattern.
	/// Returns false if it was mapped to an overlap node.
	bool isCoreNodeAtom(const MatchedAtom& atom) const {
		CALIB_ASSERT(atom.nodeIndex >= 0 && atom.nodeIndex < pattern().numNodes);
		return atom.nodeIndex < pattern().numCoreNodes;
	}

protected:

	/// A filter method that decides whether an atom may become part of matching cluster.
	bool acceptAtom(int nodeIndex, int atomIndex) const {
		if(!_filter) return true;
		else return _filter->acceptAtom(atomIndex, nodeIndex, pattern());
	}

	/// Checks if all nodes of the super pattern appear in the vicinity of the
	/// given base atom.
	bool coversPatternNodes(MatchedAtom& baseAtom) {
		if(pattern().numNodes == 1)
			return coversPatternNodesSingle(baseAtom);
		else
			return coversPatternNodesMultiple(baseAtom);
	}

	/// Checks if all nodes of the super pattern appear in the vicinity of the given base atom.
	bool coversPatternNodesMultiple(MatchedAtom& baseAtom);

	/// Optimized version of coversPatternNodesMultiple() for super patterns with a single node.
	bool coversPatternNodesSingle(MatchedAtom& baseAtom);

	bool isValidTransitionPermutation(
			MatchedAtom* atomA,
			MatchedAtom* atomB,
			int superPatternNeighborIndexA,
			int atomNeighborIndexA,
			const CoordinationPatternAnalysis::coordmatch& coordMatch,
			const CoordinationPermutation& permutationB,
			CAFloat& deviation);

	/// Returns a neighbor for the given matched atom (if it exists).
	MatchedAtom* lookupNeighbor(const MatchedAtom& atom, int nodeNeighborIndex) const {
		// First map neighbor index of node to neighbor index of atom.
		int atomNeighborIndex = atom.nodeNeighborToAtomNeighbor(nodeNeighborIndex);
		// Then lookup neighbor atom in neighbor list.
		int neighbor = _neighborList.neighbor(atom.atomIndex, atomNeighborIndex);
		// Finally, lookup matched atom for neighbor atom.
		CALIB_ASSERT(neighbor >= 0 && neighbor < (int)_matchedAtoms.size());
		return _matchedAtoms[neighbor];
	}

protected:

	/// Object that stores the coordination patterns assigned to atoms.
	const CoordinationPatternAnalysis& _coordAnalysis;

	/// The structure in which the pattern is searched.
	const AtomicStructure& _structure;

	/// The neighbor list for the atomic structure.
	const NeighborList& _neighborList;

	/// The pattern catalog.linear<bool Enable>
	const PatternCatalog& _catalog;

	/// The pattern that is being searched for.
	const SuperPattern& _pattern;

	/// The first node of the super pattern, which is being matched to an atom of the input structure.
	SuperPatternNode const* _seedNode;

	/// The index of the first node, which is being matched to an atom of the input structure.
	int _seedNodeIndex;

	/// The current atom that we try to match to the initial node of the super pattern.
	int _seedAtomIndex;

	/// The index of the current permutation used for the seed atom.
	int _seedPermutationIndex;

	/// The memory pool used to create MatchedAtom instances.
	MemoryPool<MatchedAtom> _atomPool;

	/// For each atom in the input structure, stores the corresponding match record.
	std::vector<MatchedAtom*> _matchedAtoms;

	/// Linked list of atoms which have been visited.
	visited_slist _visitedAtoms;

	/// Linked list of atoms which still have to be checked.
	tobechecked_slist _toBeChecked;

	/// Bit-array that indicates which of the super pattern nodes have been found in the vicinity of the current atom.
	std::vector<bool> _coveredNodes;

	/// Flag that indicates that a disclination has been encountered in the last cluster.
	bool _foundDisclination;

	/// Optional filter function that controls which atoms may be mapped to the super pattern.
	AtomMatchFilter* _filter;
};

}; // End of namespace

#endif // __CA_SUPER_PATTERN_MATCHER_H

