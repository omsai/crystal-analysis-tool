///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "FreeSurfaceIdentification.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor.
******************************************************************************/
FreeSurfaceIdentification::FreeSurfaceIdentification(const AtomicStructure& structure) :
	ContextReference(structure.context()), _structure(structure), _alpha(0),
	_tessellation(structure.context(), structure), _flipOrientation(true), _solidVolume(0)
{
}

/******************************************************************************
* Generates a geometric representation of the free surfaces of the solid.
******************************************************************************/
void FreeSurfaceIdentification::buildSurfaceMesh(TriangleMesh& mesh)
{
	// Check parameters.
	if(alpha() <= 0)
		context().raiseErrorAll("Alpha parameter must be positive.");

	// Compute the Delaunay tessellation.
	_tessellation.generateTessellation(true);

	// Make sure Delaunay tessellation is consistent.
	if(!tessellation().checkTessellationConsistency())
		context().raiseErrorOne("Detected invalid periodic tessellation. By increasing the ghost cutoff size you may be able to avoid this error.");

	_tetrahedra.resize(tessellation().number_of_tetrahedra());

	// Create the triangular mesh facets separating solid and open tetrahedra.
	createSeparatingFacets(mesh);

	// Links half-edges to opposite half-edges.
	linkHalfEdges(mesh);
}

/******************************************************************************
* Creates the triangular mesh facets separating solid and open tetrahedra.
******************************************************************************/
void FreeSurfaceIdentification::createSeparatingFacets(TriangleMesh& mesh)
{
	CAFloat localSolidVolume = 0;
	vector<TriangleMeshVertex*> vertexMap(structure().numLocalAtoms() + structure().numGhostAtoms());

	for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {
		// Start with the solid tetrahedra.
		if(!isSolidTetrahedron(cell))
			continue;

		if(tessellation().isValidAndLocalCell(cell))
			localSolidVolume += tessellation().cellVolume(cell);

		// Iterate over the four faces of the tetrahedron cell.
		Tetrahedron& tet = _tetrahedra[tessellation().cellIndex(cell)];
		for(int f = 0; f < 4; f++) {
			tet.meshFacets[f] = NULL;

			// Check if the neighboring tetrahedron belongs to the open region.
			DelaunayTessellation::CellHandle adjacentCell = tessellation().mirrorCell(cell, f);
			if(isSolidTetrahedron(adjacentCell))
				continue;

			// Classify current triangle facet as either ghost or local.
			bool isGhostFacet = structure().isGhostAtom(cell->vertex(DelaunayTessellation::cellFacetVertexIndex(f, 0))->info());
			AtomTag headTag = structure().atomTag(cell->vertex(DelaunayTessellation::cellFacetVertexIndex(f, 0))->info());
			for(int v = 1; v < 3; v++) {
				int vertexIndex = cell->vertex(DelaunayTessellation::cellFacetVertexIndex(f, v))->info();
				if(structure().atomTag(vertexIndex) < headTag) {
					headTag = structure().atomTag(vertexIndex);
					isGhostFacet = structure().isGhostAtom(vertexIndex);
				}
			}

			// Look up or, if necessary, create the three vertices of the face.
			TriangleMeshVertex* facetVertices[3];
			for(int v = 0; v < 3; v++) {
				DelaunayTessellation::VertexHandle vertex = cell->vertex(DelaunayTessellation::cellFacetVertexIndex(f, v));
				int vertexIndex = vertex->info();
				CALIB_ASSERT(vertexIndex >= 0 && vertexIndex < vertexMap.size());
				if(vertexMap[vertexIndex] == NULL) {
					vertexMap[vertexIndex] = facetVertices[v] = mesh.createVertex(
							structure().atomPosition(vertexIndex),
							structure().isGhostAtom(vertexIndex),
							structure().atomTag(vertexIndex));
				}
				else
					facetVertices[v] = vertexMap[vertexIndex];
			}

			// Create half-edges.
			TriangleMeshEdge* facetEdges[3];
			for(int v = 0; v < 3; v++) {
				int v1 = _flipOrientation ? (2-v) : v;
				int v2 = (_flipOrientation ? (4-v) : (v+1)) % 3;
				facetEdges[v] = mesh.createHalfEdge(facetVertices[v1], facetVertices[v2]);
			}

			// Create a new triangle facet.
			tet.meshFacets[f] = mesh.createFacet(facetEdges, isGhostFacet);
		}
	}

	_solidVolume = parallel().reduce_sum(localSolidVolume);
}

/******************************************************************************
* Links half-edges to opposite half-edges.
******************************************************************************/
void FreeSurfaceIdentification::linkHalfEdges(TriangleMesh& mesh)
{
	for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {
		if(!isSolidTetrahedron(cell))
			continue;

		Tetrahedron& tet = _tetrahedra[tessellation().cellIndex(cell)];
		for(int f = 0; f < 4; f++) {
			TriangleMeshFacet* facet = tet.meshFacets[f];
			if(facet == NULL) continue;

			for(int e = 0; e < 3; e++) {
				int vertexIndex1 = DelaunayTessellation::cellFacetVertexIndex(f, e);
				int vertexIndex2 = DelaunayTessellation::cellFacetVertexIndex(f, (e+1)%3);
				DelaunayTessellation::FacetCirculator circulator_start = tessellation().incident_facets(cell, vertexIndex2, vertexIndex1, cell, f);
				DelaunayTessellation::FacetCirculator circulator = circulator_start;
				CALIB_ASSERT(circulator->first == cell);
				CALIB_ASSERT(circulator->second == f);
				--circulator;
				CALIB_ASSERT(circulator != circulator_start);
				TriangleMeshFacet* adjacentFacet = NULL;
				do {
					if(!isSolidTetrahedron(circulator->first)) {
						pair<DelaunayTessellation::CellHandle,int> mirrorFacet = tessellation().mirrorFacet(circulator);
						CALIB_ASSERT(isSolidTetrahedron(mirrorFacet.first));
						const Tetrahedron& mirrorTet = _tetrahedra[tessellation().cellIndex(mirrorFacet.first)];
						TriangleMeshFacet* facet2 = mirrorTet.meshFacets[mirrorFacet.second];
						CALIB_ASSERT(facet2 != NULL);
						adjacentFacet = facet2;
						break;
					}
					--circulator;
				}
				while(circulator != circulator_start);

				int v2 = _flipOrientation ? (2-e) : e;
				int v1 = (v2+2) % 3;

				CALIB_ASSERT(adjacentFacet != NULL);
				CALIB_ASSERT(adjacentFacet != facet);
				CALIB_ASSERT(adjacentFacet->hasVertex(facet->vertex(v1)));
				CALIB_ASSERT(adjacentFacet->hasVertex(facet->vertex(v2)));
				CALIB_ASSERT(facet->edges[v1]->vertex2 == facet->vertex(v2));
				CALIB_ASSERT(facet->edges[v1]->oppositeEdge == NULL);
				TriangleMeshEdge* oppositeEdge = adjacentFacet->edges[adjacentFacet->vertexIndex(facet->vertex(v2))];
				CALIB_ASSERT(oppositeEdge->vertex2 == facet->vertex(v1));
				facet->edges[v1]->oppositeEdge = oppositeEdge;
			}
		}
	}
}

}; // End of namespace
