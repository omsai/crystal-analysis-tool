///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DelaunayTessellation.h"
#include "../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

/******************************************************************************
* High-level function that generates the tessellation.
******************************************************************************/
void DelaunayTessellation::generateTessellation(bool perturbation)
{
	vector<DT::Point> cgalPoints;

	int numAtoms = structure().numLocalAtoms() + structure().numGhostAtoms() + structure().numHelperAtoms();
	CALIB_ASSERT(structure().atomPositions().size() == numAtoms);

	cgalPoints.reserve(numAtoms);

	vector<Vector3>::const_iterator pos = structure().atomPositions().begin();
	vector<Vector3>::const_iterator pos_end = pos + numAtoms;

	if(perturbation) {

		// Set up random number generator to generate displacement vectors.
#if BOOST_VERSION > 146000
		boost::random::mt19937 rng;
		boost::random::uniform_real_distribution displacement(-1e-6, +1e-6);
#else
		boost::mt19937 rng;
		boost::uniform_real<> displacement(-1e-6, +1e-6);
#endif

		vector<AtomInteger>::const_iterator tag = structure().atomTags().begin();
		for(; pos != pos_end; ++pos, ++tag) {
			// Add a perturbation to the atomic positions to make the Delaunay triangulation more robust for perfect lattices,
			// which can lead to singular cases.
			// The random perturbation vector for an atom depends on the atom's ID.
			// This ensures that displacements are consistent across processors for an atom and all its ghost images.
			CALIB_ASSERT(*tag >= 1);
			rng.seed(*tag);
			cgalPoints.push_back(DT::Point(pos->x() + displacement(rng), pos->y() + displacement(rng), pos->z() + displacement(rng)));
		}
	}
	else {
		for(; pos != pos_end; ++pos)
			cgalPoints.push_back(DT::Point(pos->x(), pos->y(), pos->z()));
	}
	_dt.insert(boost::make_zip_iterator(boost::make_tuple(cgalPoints.begin(), boost::counting_iterator<int>(0))),
				boost::make_zip_iterator(boost::make_tuple(cgalPoints.end(), boost::counting_iterator<int>(cgalPoints.size()))));

	// Reset atom indices of vertices corresponding to helper atoms and of the infinite vertex.
	_dt.infinite_vertex()->info() = -1;
	for(VertexIterator v = begin_vertices(); v != end_vertices(); ++v) {
		if(_dt.is_infinite(v)) continue;
		if(structure().isHelperAtom(v->info()))
			v->info() = -1;
	}

	// Assign indices to tetrahedra.
	int index = 0;
	for(CellIterator cell = begin_cells(); cell != end_cells(); ++cell, ++index)
		cell->info() = index;
}

/// Structure used to exchange tessellation tetrahedra between processors.
struct TetrahedronComm1 {
	AtomInteger vertexTags[4];
	//Vector3 positions[4];

	bool operator<(const TetrahedronComm1& other) const {
		for(int v = 0; v < 4; v++) {
			if(vertexTags[v] < other.vertexTags[v])
				return true;
			else if(vertexTags[v] > other.vertexTags[v])
				return false;
		}
		return false;
	}
};

/******************************************************************************
* Performs an additional comparison of the overlapping tetrahedra
* to check if the tessellation on all processors is consistent.
******************************************************************************/
bool DelaunayTessellation::checkTessellationConsistency() const
{
	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		// Build list of tetrahedra that must be sent to the neighboring processors.
		// We exchange tetrahedra that at least one vertex inside the local domain and one vertex
		// in the neighboring processor's domain.
		vector<TetrahedronComm1> sendBuffers[2];

		// Determine the grid coordinates of the two neighboring processors in the current spatial dimension.
		Vector3I neighborProcCoords[2];
		neighborProcCoords[0] = parallel().processorLocationPoint();
		neighborProcCoords[1] = parallel().processorLocationPoint();
		neighborProcCoords[0][dim] -= 1;
		neighborProcCoords[1][dim] += 1;

		for(CellIterator cell = begin_cells(); cell != end_cells(); ++cell) {

			// Iterate over the four vertices of the tetrahedron cell.
			bool hasInsideVertex = false;
			bool hasInfiniteVertex = false;
			bool hasOutsideVertex[2] = { false, false };
			bool hasHelperVertex = false;
			TetrahedronComm1 tetComm;
			for(int v = 0; v < 4; v++) {

				// Skip the infinite vertex.
				if(isInfiniteVertex(cell->vertex(v))) {
					hasInfiniteVertex = true;
					continue;
				}

				// Determine the processor domain the vertex is located in.
				int atomIndex = cell->vertex(v)->info();
				if(atomIndex == -1) {
					hasHelperVertex = true;
					continue;
				}

				CALIB_ASSERT(atomIndex >= 0);
				CALIB_ASSERT(atomIndex < structure().numLocalAtoms() + structure().numGhostAtoms());
				const Vector3I& procCoord = structure().atomProcCoord(atomIndex);
				if(procCoord == parallel().processorLocationPoint())
					hasInsideVertex = true;
				else if(procCoord == neighborProcCoords[0])
					hasOutsideVertex[0] = true;
				else if(procCoord == neighborProcCoords[1])
					hasOutsideVertex[1] = true;

				tetComm.vertexTags[v] = structure().atomTag(atomIndex);
				//tetComm.positions[v] = structure().atomPosition(atomIndex);
			}
			if(hasInsideVertex && hasInfiniteVertex) {
				// Detected invalid tessellation. Infinite tetrahedron intersects processor domain.
				return false;
			}

			if(hasOutsideVertex[0] && hasOutsideVertex[1]) {
				// Detected invalid tessellation. Tetrahedron overlaps with more than two processor domains in one dimension.
				return false;
			}

			if(hasHelperVertex == false) {
				if(hasInsideVertex && hasOutsideVertex[0]) {
					sort(tetComm.vertexTags, tetComm.vertexTags + 4);
					sendBuffers[0].push_back(tetComm);
				}
				else if(hasInsideVertex && hasOutsideVertex[1]) {
					sort(tetComm.vertexTags, tetComm.vertexTags + 4);
					sendBuffers[1].push_back(tetComm);
				}
			}
		}

		boost::sort(sendBuffers[0]);
		boost::sort(sendBuffers[1]);

		for(int dir = 0; dir <= 1; dir++) {
			const int oppositeDir = dir ? 0 : 1;

			// Communicate number of tetrahedra to be exchanged with neighbor processor.
			int tetReceiveBufferSize = 0;
			parallel().exchange(dim, dir, structure().pbc(dim), sendBuffers[dir].size(), tetReceiveBufferSize);

			if(parallel().shouldSendOrReceive(dim, oppositeDir, structure().pbc(dim))) {
				// Number of tetrahedra must match.
				if(tetReceiveBufferSize != sendBuffers[oppositeDir].size()) {
					//context().msgLogger() << "Processor " << parallel().processor() << " dim=" << dim << " dir=" << oppositeDir << "   Neighbor processors " << context.processorNeighbor(dim, oppositeDir) << " dir=" << dir << endl;
					//context().msgLogger() << "tetReceiveBufferSize=" << tetReceiveBufferSize << "  sendBuffers[oppositeDir].size()=" << sendBuffers[oppositeDir].size() << endl;
					// Detected invalid tessellation. Numbers of overlapping tetrahedra differ.
					return false;
				}
			}

			// Exchange overlap tetrahedra.
			vector<TetrahedronComm1> tetReceiveBuffer(tetReceiveBufferSize);
			parallel().exchange(dim, dir, structure().pbc(dim), sendBuffers[dir], tetReceiveBuffer);

			// Check if all received tetrahedra are identical to our own tetrahedra.
			vector<TetrahedronComm1>::const_iterator localTet = sendBuffers[oppositeDir].begin();
			for(vector<TetrahedronComm1>::const_iterator remoteTet = tetReceiveBuffer.begin(); remoteTet != tetReceiveBuffer.end(); ++remoteTet, ++localTet) {
				for(int v = 0; v < 4; v++) {
					if(localTet->vertexTags[v] != remoteTet->vertexTags[v]) {
						//context().msgLogger() << "Processor " << parallel().processor() << " dim=" << dim << " dir=" << oppositeDir << "   Neighbor processors " << context.processorNeighbor(dim, oppositeDir) << " dir=" << dir << endl;
						//context().msgLogger() << "tetReceiveBufferSize=" << tetReceiveBufferSize << "  sendBuffers[oppositeDir].size()=" << sendBuffers[oppositeDir].size() << endl;
						// Detected invalid tessellation. Overlapping tetrahedra are not consistent.
						return false;
					}
				}
			}
		}
	}

	return true;
}


/******************************************************************************
* Determines whether the given tetrahedral cell of the tessellation is owned by the
* local processor and connects four physical atoms.
******************************************************************************/
bool DelaunayTessellation::isValidAndLocalCell(CellHandle cell) const
{
	// Find head vertex with the lowest tag number.
	int headVertex = cell->vertex(0)->info();
	if(headVertex == -1) return false;
	AtomInteger headTag = structure().atomTag(headVertex);
	for(int v = 1; v < 4; v++) {
		int index = cell->vertex(v)->info();
		if(index == -1) return false;
		AtomInteger tag = structure().atomTag(index);
		if(tag < headTag) {
			headTag = tag;
			headVertex = index;
		}
	}

	// Cell is local if the head vertex is inside the processor's domain.
	return (structure().atomProcCoord(headVertex) == parallel().processorLocationPoint());
}

/******************************************************************************
* Computes the volume of a tetrahedral cell.
******************************************************************************/
CAFloat DelaunayTessellation::cellVolume(CellHandle cell) const
{
	CALIB_ASSERT(isValidCell(cell));
	const Vector3& p3 = structure().atomPosition(cell->vertex(3)->info());
	Vector3 ad = structure().atomPosition(cell->vertex(0)->info()) - p3;
	Vector3 bd = structure().atomPosition(cell->vertex(1)->info()) - p3;
	Vector3 cd = structure().atomPosition(cell->vertex(2)->info()) - p3;
	CAFloat volume = ad.dot(cd.cross(bd)) / 6.0;
	// The element volume should be positive.
	// The random displacements applied to the vertex positions when constructing the
	// Delaunay tessellation may, however, lead to elements with slightly negative volume.
	CAFloat epsilon = 1e-4;
	if(volume < -epsilon)
		context().msgLogger() << "Warning: Delaunay element has negative volume: " << volume << endl;
	return volume;
}

/******************************************************************************
* Computes the aspect ratio of a tetrahedral cell.
******************************************************************************/
CAFloat DelaunayTessellation::cellAspectRatio(CellHandle cell) const
{
	// List of vertices that bound the six edges of a tetrahedron.
	static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

	Vector3 vertexPos[4];
	for(int v = 0; v < 4; v++) {
		CALIB_ASSERT(cell->vertex(v)->info() >= 0);
		vertexPos[v] = structure().atomPosition(cell->vertex(v)->info());
	}

	CAFloat longestEdge = 0.0;
	for(int eindex = 0; eindex < 6; eindex++) {
		longestEdge = max(longestEdge,
				(vertexPos[edgeVertices[eindex][0]] - vertexPos[edgeVertices[eindex][1]]).squaredNorm());
	}

	CAFloat shortestNormal = CAFLOAT_MAX;
	for(int findex = 0; findex < 4; findex++) {
		const Vector3& atom1 = vertexPos[cellFacetVertexIndex(findex,0)];
		const Vector3& atom2 = vertexPos[cellFacetVertexIndex(findex,1)];
		const Vector3& atom3 = vertexPos[cellFacetVertexIndex(findex,2)];
		const Vector3& atom4 = vertexPos[findex];
		Vector3 normal = (atom1 - atom2).cross(atom1 - atom3);
		if(normal != Vector3::Zero()) {
			CAFloat dist = fabs(normal.dot(atom4 - atom1));
			shortestNormal = min(shortestNormal, dist);
		}
	}
	return (longestEdge / shortestNormal);
}

}; // End of namespace
