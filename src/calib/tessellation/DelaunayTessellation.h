///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DELAUNAY_TESSELLATION_H
#define __CA_DELAUNAY_TESSELLATION_H

#include "../CALib.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"
#include "../util/MemoryPool.h"

#ifdef __clang__
	#ifndef CGAL_CFG_ARRAY_MEMBER_INITIALIZATION_BUG
		#define CGAL_CFG_ARRAY_MEMBER_INITIALIZATION_BUG
	#endif
#endif
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>

namespace CALib {

/**
 * Generates a Delaunay tessellation of the atomic system.
 *
 * This is just a wrapper for the CGAL Delaunay tessellation engine.
 */
class DelaunayTessellation : public ContextReference
{
public:

	typedef CGAL::Exact_predicates_inexact_constructions_kernel 	K;
	typedef CGAL::Triangulation_cell_base_with_info_3<int,K> 		CbDS;
	typedef CGAL::Triangulation_vertex_base_with_info_3<int,K>		VbDS;

	// Define data types for Delaunay triangulation class.
	typedef K 														GT;
	typedef CGAL::Triangulation_data_structure_3<VbDS, CbDS> 		TDS;
	typedef CGAL::Delaunay_triangulation_3<GT, TDS>					DT;

	// Often-used iterator types.
	typedef DT::Triangulation_data_structure::Cell_iterator 		CellIterator;
	typedef DT::Triangulation_data_structure::Vertex_iterator 		VertexIterator;

	typedef DT::Triangulation_data_structure::Vertex_handle 		VertexHandle;
	typedef DT::Triangulation_data_structure::Cell_handle 			CellHandle;

	typedef DT::Triangulation_data_structure::Facet_circulator		FacetCirculator;

public:

	/// Constructor.
	DelaunayTessellation(CAContext& context, const AtomicStructure& structure) : ContextReference(context), _structure(structure) {}

	/// Generates the Delaunay tessellation.
	void generateTessellation(bool perturbation);

	/// Returns the atomic structure for which the tessellation was built.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the tetrahedron cell on the opposite side of the given cell facet.
	CellHandle mirrorCell(const CellHandle& cell, int facet) const {
		return _dt.tds().mirror_facet(DT::Triangulation_data_structure::Facet(cell, facet)).first;
	}

	/// Returns true if the given vertex is the infinite vertex of the tessellation.
	bool isInfiniteVertex(const VertexHandle& vertex) const {
		return _dt.is_infinite(vertex);
	}

	/// Returns whether the given tessellation cell connects four physical atoms.
	/// Returns true if all four vertices are either local or ghost atoms of the
	/// atomic structure. Returns false if any of the four vertices is the infinite
	/// vertex or a tessellation helper atom.
	bool isValidCell(const CellHandle& cell) const {
		for(int v = 0; v < 4; v++)
			if(cell->vertex(v)->info() == -1) return false;
		return true;
	}

	/// Returns the total number of tetrahedra on this processor (including ghosts and invalid tetrahedra).
	int number_of_tetrahedra() const { return _dt.number_of_cells(); }

	CellIterator begin_cells() const { return _dt.cells_begin(); }
	CellIterator end_cells() const { return _dt.cells_end(); }
	VertexIterator begin_vertices() const { return _dt.vertices_begin(); }
	VertexIterator end_vertices() const { return _dt.vertices_end(); }

	/// Given a cell of the tessellation, returns the index of the cell in the global list of tessellation cells.
	int cellIndex(const CellHandle& cell) const { return cell->info(); }

	/// Returns the cell vertex for the given triangle vertex of the given cell facet.
	static inline int cellFacetVertexIndex(int cellFacetIndex, int facetVertexIndex) {
		return CGAL::Triangulation_utils_3::vertex_triple_index(cellFacetIndex, facetVertexIndex);
	}

	FacetCirculator incident_facets(CellHandle cell, int i, int j, CellHandle start, int f) const {
		return _dt.tds().incident_facets(cell, i, j, start, f);
	}

	/// Returns the same facet seen from the other adjacent cell.
	std::pair<CellHandle,int> mirrorFacet(const FacetCirculator& facet) const {
		return _dt.tds().mirror_facet(*facet);
	}

	/// Returns a reference to the internal CGAL Delaunay triangulation object.
	DT& dt() { return _dt; }

	/// Performs a comparison of the overlapping tetrahedra at processor domain boundaries
	/// to check if the local tessellations of all processors are consistent.
	bool checkTessellationConsistency() const;

	/// Determines whether the given tetrahedral cell of the tessellation is owned by the
	/// local processor and connects four physical atoms.
	bool isValidAndLocalCell(CellHandle cell) const;

	/// Computes the volume of a tetrahedral cell.
	CAFloat cellVolume(CellHandle cell) const;

	/// Computes the aspect ratio of a tetrahedral cell.
	CAFloat cellAspectRatio(CellHandle cell) const;

private:

	/// The atomic structure for which the tessellation is built.
	const AtomicStructure& _structure;

	/// The internal CGAL triangulator object.
	DT _dt;
};

}; // End of namespace

#endif // __CA_DELAUNAY_TESSELLATION_H
