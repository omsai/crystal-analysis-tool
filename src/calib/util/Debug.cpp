///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "../CALib.h"

namespace CALib {

/********************************************************************************
 * This function aborts the program after printing an error message.
 *******************************************************************************/
void handleFatalAssertionError(const char* file, int line, const char* location, const char* msg)
{
	using namespace std;

	// Determine the processor rank.
	int rank = 0;
#ifdef CALIB_USE_MPI
	int mpiinit;
	MPI_Initialized(&mpiinit);
	if(mpiinit)
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

	cerr << endl << "===================================================================================="<< endl;
	cerr << "ASSERTION FAILURE on processor " << rank << " (code version " << CA_LIB_VERSION_STRING << ")." << endl;
	cerr << "Location:" << endl;
	if(location) cerr << location << endl;
	cerr << file << ", line " << line << endl;
	if(msg) cerr << msg << endl;
	cerr << "The program has stumbled across an unexpected condition. Please contact the author." << endl;
	cerr << "The bug report should include the program output up to this point and, if possible," << endl;
	cerr << "the input file(s) and command line options used when running the program." << endl;
	cerr << "====================================================================================" << endl;
	cerr << flush;

	// This invokes the debugger:
#if defined(__GNUC__)
	__builtin_trap();
#endif

#ifdef CALIB_USE_MPI
	// Abort MPI.
	if(mpiinit) {
		MPI_Abort(MPI_COMM_WORLD, 1);
		MPI_Finalize();
	}
#endif

	// Abort program.
	exit(1);
}

/// Global pointer to the handler function that is called on an assertion error.
void (*fatalAssertionErrorHandler)(const char* file, int line, const char* location, const char* msg) = handleFatalAssertionError;

}; // End of namespace
