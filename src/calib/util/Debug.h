///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_DEBUG_H
#define __CRYSTAL_ANALYSIS_DEBUG_H

namespace CALib {

/// This points to a function that is called when an assertion check fails.
extern void (*fatalAssertionErrorHandler)(const char* file, int line, const char* location, const char* msg);

// This is for debugging purposes. The CALIB_ASSERT() macros are used in the code to check
// if everything runs as expected.

#ifdef DEBUG_CRYSTAL_ANALYSIS

	inline void calib_assertion_noop() {}

	#define CALIB_ASSERT(cond) ((!(cond)) ? CALib::fatalAssertionErrorHandler(__FILE__,__LINE__,BOOST_CURRENT_FUNCTION,NULL) : CALib::calib_assertion_noop())
	#define CALIB_ASSERT_MSG(cond,location,msg) ((!(cond)) ? CALib::fatalAssertionErrorHandler(__FILE__,__LINE__,location,msg) : CALib::calib_assertion_noop())

#else

	#define CALIB_ASSERT(cond)
	#define CALIB_ASSERT_MSG(cond,location,msg)

#endif

/// Used for compile-time checks.
/// Use dynamic assertion check if static version is not available.
#ifdef BOOST_STATIC_ASSERT
	#define CALIB_STATIC_ASSERT BOOST_STATIC_ASSERT
#else
	#define CALIB_STATIC_ASSERT CALIB_ASSERT
#endif

}; // End of namespace

#endif	// __CRYSTAL_ANALYSIS_DEBUG_H
