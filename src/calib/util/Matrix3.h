///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_MATRIX3_H
#define __CRYSTAL_ANALYSIS_MATRIX3_H

#include "LinAlg.h"

namespace CALib {

/// Returns a copy of a matrix, where small matrix element close to zero have been
/// rounded to exactly zero.
inline Matrix3 roundZeroElements(const Matrix3& m, CAFloat epsilon = CAFLOAT_EPSILON) {
	Matrix3 roundedMatrix;
	for(int col = 0; col < 3; col++)
		for(int row = 0; row < 3; row++)
			roundedMatrix(row,col) = (fabs(m(row,col)) > epsilon ? m(row,col) : CAFloat(0));
	return roundedMatrix;
}

/// \brief Tests whether a matrix is a pure rotation matrix.
/// \return \c If the matrix is a pure rotation matrix; \c false otherwise.
///
/// Matrix A is a pure rotation matrix if:
///   (1) det(A) = 1  and
///   (2) A * A^T = I
inline bool isRotationMatrix(const Matrix3& m, CAFloat epsilon = CAFLOAT_EPSILON) {
	if(fabs(m(0,0)*m(1,0) + m(0,1)*m(1,1) + m(0,2)*m(1,2)) > epsilon) return false;
	if(fabs(m(0,0)*m(2,0) + m(0,1)*m(2,1) + m(0,2)*m(2,2)) > epsilon) return false;
	if(fabs(m(1,0)*m(2,0) + m(1,1)*m(2,1) + m(1,2)*m(2,2)) > epsilon) return false;
	if(fabs(m(0,0)*m(0,0) + m(0,1)*m(0,1) + m(0,2)*m(0,2) - 1.0) > epsilon) return false;
	if(fabs(m(1,0)*m(1,0) + m(1,1)*m(1,1) + m(1,2)*m(1,2) - 1.0) > epsilon) return false;
	if(fabs(m(2,0)*m(2,0) + m(2,1)*m(2,1) + m(2,2)*m(2,2) - 1.0) > epsilon) return false;
	return(fabs(m.determinant() - 1.0) <= epsilon);
}

/// \brief Tests whether a matrix is a pure rotation and/or reflection matrix.
/// \return \c If the matrix is a pure rotation reflection matrix; \c false otherwise.
///
/// Matrix A is a pure rotation reflection matrix if:
///   (1) |det(A)| = 1  and
///   (2) A * A^T = I
inline bool isRotationReflectionMatrix(const Matrix3& m, CAFloat epsilon = CAFLOAT_EPSILON) {
	if(fabs(m(0,0)*m(1,0) + m(0,1)*m(1,1) + m(0,2)*m(1,2)) > epsilon) return false;
	if(fabs(m(0,0)*m(2,0) + m(0,1)*m(2,1) + m(0,2)*m(2,2)) > epsilon) return false;
	if(fabs(m(1,0)*m(2,0) + m(1,1)*m(2,1) + m(1,2)*m(2,2)) > epsilon) return false;
	if(fabs(m(0,0)*m(0,0) + m(0,1)*m(0,1) + m(0,2)*m(0,2) - 1.0) > epsilon) return false;
	if(fabs(m(1,0)*m(1,0) + m(1,1)*m(1,1) + m(1,2)*m(1,2) - 1.0) > epsilon) return false;
	if(fabs(m(2,0)*m(2,0) + m(2,1)*m(2,1) + m(2,2)*m(2,2) - 1.0) > epsilon) return false;
	return(fabs(fabs(m.determinant()) - 1.0) <= epsilon);
}

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_MATRIX3_H
