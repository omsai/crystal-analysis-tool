####################################################################
#
# This a CMake project file that controls which source files must
# be built and which third-party libraries must be linked.
#
# Use the cmake utility to create a Makefile from this project file.
# See http://www.cmake.org/ for details.
#
####################################################################

####################################################################
# List of source files of the catalog generator tool.
####################################################################
SET(CatalogGeneratorSourceFiles
	Main.cpp
	PatternTemplate.cpp
	CatalogGenerator.cpp
	CoordinationPatternGeneration.cpp
	SuperPatternGeneration.cpp
	SuperPatternGeneration2.cpp
)

###################################################################
# Build catalog generator tool.
###################################################################
ADD_EXECUTABLE(CatalogGenerator ${CatalogGeneratorSourceFiles})
TARGET_LINK_LIBRARIES(CatalogGenerator CrystalAnalysisLibrary)
TARGET_LINK_LIBRARIES(CatalogGenerator ${Boost_LIBRARIES} ${MPI_LIBRARIES})
SET_TARGET_PROPERTIES(CatalogGenerator PROPERTIES COMPILE_DEFINITIONS "${ANALYSIS_COMPILE_DEFINITIONS}")

# This executable will be part of the installation package.
INSTALL(TARGETS CatalogGenerator DESTINATION ".")

# Also install the pattern catalog.
INSTALL(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/../../patterns" DESTINATION ".")
