///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CatalogGenerator.h"
#include <calib/input/atoms/AtomicStructureParser.h>
#include <calib/cluster_graph/ClusterVector.h>

#include <boost/format.hpp>

using namespace std;
using namespace CALib;

/******************************************************************************
* Loads an atomic structure file and generates a new pattern for all
* atoms that do not match to any of the existing patterns.
******************************************************************************/
void CatalogGenerator::generatePattern(PatternTemplate& templ, PatternCatalog& catalog)
{
	context().msgLogger() << "------------------------------------------------------" << endl;
	context().msgLogger() << templ.templateName << endl;
	context().msgLogger() << "------------------------------------------------------" << endl;

	// Load the atomic structure that serves as a template for the pattern.
	AtomicStructure& structure = templ.loadAtomicStructure();

	// Identify atoms matching the existing coordination patterns.
	CoordinationPatternAnalysis coordPatternAnalysis(structure, templ.neighborList(), catalog);
	vector<CoordinationPattern const*> patternsToSearchFor;
	BOOST_FOREACH(const CoordinationPattern& pattern, catalog.coordinationPatterns()) patternsToSearchFor.push_back(&pattern);
	coordPatternAnalysis.searchCoordinationPatterns(patternsToSearchFor, templ.innerCutoff);

	if(templ.coordinationPatternType != CoordinationPattern::COORDINATION_PATTERN_NONE) {

		// Counting the number of new coordination patterns created for the current structure template.
		int numCoordinationPatterns = 0;

		// Create a new coordination pattern for each atom that does not (exactly) match an existing pattern.
		for(int atomIndex = 0; atomIndex < structure.numTotalAtoms(); atomIndex++) {

			// Ignore atom if it has been clipping by one of the user-defined planes.
			if(templ.isClipped(structure.atomPosition(atomIndex))) continue;

			// Check matches to existing patterns.
			// Only if the deviation of the actual coordination structure from the reference is small enough we count
			// it as a good match. For atoms without a perfect match, we will have to create a new coordination pattern.
			bool isPerfectMatch = false;
			BOOST_FOREACH(const CoordinationPatternAnalysis::coordmatch& match, coordPatternAnalysis.coordinationPatternMatches(atomIndex)) {
				if(coordPatternAnalysis.calculateDeviationFromCoordinationPattern(match, atomIndex) <= templ.perfectCoordinationMatchThreshold) {
					isPerfectMatch = true;
					break;
				}
			}
			if(isPerfectMatch) continue;

			// Create a new coordination pattern for this atom.
			numCoordinationPatterns++;
			string name = boost::str(boost::format("%1%.%2%") % templ.templateName % numCoordinationPatterns);
			CoordinationPattern* coordPattern = createCoordinationPatternForAtom(atomIndex, templ, name).release();

			// Add the new pattern to the catalog.
			catalog.addCoordinationPattern(coordPattern);

			// Search for the pattern in the current template structure to mark all equivalent atoms.
			patternsToSearchFor.push_back(coordPattern);
			coordPatternAnalysis.searchCoordinationPatterns(patternsToSearchFor, templ.innerCutoff);

			// Verify that the atom really matches the coordination pattern that was created for it.
			BOOST_FOREACH(const CoordinationPatternAnalysis::coordmatch& match, coordPatternAnalysis.coordinationPatternMatches(atomIndex)) {
				CALIB_ASSERT(match.pattern == coordPattern);
				if(coordPatternAnalysis.calculateDeviationFromCoordinationPattern(match, atomIndex) <= templ.perfectCoordinationMatchThreshold) {
					isPerfectMatch = true;
					break;
				}
			}
			if(!isPerfectMatch)
				context().raiseErrorAll("The newly created coordination pattern does not exactly match the atom (%i) it was created for.", structure.atomTag(atomIndex));
		}
	}

	// Now generate the super pattern.
	if(templ.periodicity == 3)
		templ.generateLatticeGraphPattern(catalog, coordPatternAnalysis);
	else {
		if(templ.coordinationPatternType != CoordinationPattern::COORDINATION_PATTERN_NONE)
			templ.generateDefectGraphPattern(catalog, coordPatternAnalysis);
		else
			templ.generateDefectGraphPattern2(catalog, coordPatternAnalysis);
	}
}

