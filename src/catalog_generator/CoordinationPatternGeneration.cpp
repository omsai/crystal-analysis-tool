///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CatalogGenerator.h"
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/context/CAContext.h>
#include <calib/pattern/coordinationpattern/CoordinationPatternMatcher.h>

using namespace std;
using namespace CALib;

/******************************************************************************
* Creates a new coordination pattern for the given central atom.
******************************************************************************/
auto_ptr<CoordinationPattern> CatalogGenerator::createCoordinationPatternForAtom(int atomIndex, const PatternTemplate& templ, const std::string& patternName)
{
	auto_ptr<CoordinationPattern> pattern;
	switch(templ.coordinationPatternType) {
		case CoordinationPattern::COORDINATION_PATTERN_NDA: pattern.reset(new NDACoordinationPattern()); break;
		case CoordinationPattern::COORDINATION_PATTERN_CNA: pattern.reset(new CNACoordinationPattern()); break;
		case CoordinationPattern::COORDINATION_PATTERN_ACNA: pattern.reset(new AdaptiveCNACoordinationPattern()); break;
		case CoordinationPattern::COORDINATION_PATTERN_DIAMOND: pattern.reset(new DiamondCoordinationPattern()); break;
		default: CALIB_ASSERT(false);
	}
	pattern->name = patternName;

	// Actually generate the pattern.
	buildCoordinationPattern(atomIndex, *pattern, templ);

	// Print some info about the newly created pattern.
	context().msgLogger() << "* Created coordination pattern '" << pattern->name << "' (#neighbors: " << pattern->numNeighbors() << ", #permutations: " << pattern->permutations.size() << ", #shells: " << pattern->numShells;
	context().msgLogger() << ", cutoff gap: " << pattern->cutoffGap;
	context().msgLogger() << ") for atom " << templ.structure().atomTag(atomIndex) << endl;
	context().msgLogger() << "   LastInnerShellRadius=" << pattern->lastInnerShellRadius << "   FirstOuterShellRadius=" << pattern->firstOuterShellRadius << endl;
	for(int n = 0; n < pattern->numNeighbors(); n++) {
		context().msgLogger() << "   Neighbor " << n << ": " << pattern->neighbors[n].vec << " " << pattern->neighbors[n].vec.norm();
		if(templ.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_CNA || templ.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA || templ.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
			const CNACoordinationPattern& cnaPattern = static_cast<CNACoordinationPattern&>(*pattern);
			context().msgLogger() << "  (CNA signature: " << cnaPattern.cnaNeighbors[n].numCommonNeighbors << "-" << cnaPattern.cnaNeighbors[n].numBonds << "-" << cnaPattern.cnaNeighbors[n].maxChainLength << ")";
		}
		context().msgLogger() << endl;
	}
	//for(int i = 0; i < pattern->permutationMatrices.size(); i++)
	//	context().msgLogger() << "Permutation " << i << ": (det=" << pattern->permutationMatrices[i].determinant() << ")" << endl <<
	//		pattern->permutationMatrices[i];

	return pattern;
}

/******************************************************************************
* Builds a coordination pattern from the neighbor bonds of the given central
* atom.
******************************************************************************/
void CatalogGenerator::buildCoordinationPattern(int centerAtomIndex, CoordinationPattern& pattern, const PatternTemplate& templ)
{
	AtomInteger atomTag = templ.structure().atomTag(centerAtomIndex);

	// Create the list of neighbors within the specified cutoff radius.
	int nn = 0;
	for(;;) {
		if(nn == templ.neighborList().neighborCount(centerAtomIndex))
			context().raiseErrorAll("Outer cutoff radius is too small to include at least one more atom than the inner cutoff radius (atom tag = %i).", atomTag);
		if(templ.neighborList().neighborDistanceSquared(centerAtomIndex, nn) >= square(templ.innerCutoff))
			break;
		if(nn == CA_MAX_PATTERN_NEIGHBORS)
			context().raiseErrorAll("Number of neighbors of atom %i exceeds CA_MAX_PATTERN_NEIGHBORS (%i) in structure '%s'. Inner cutoff radius might be too large.", atomTag, CA_MAX_PATTERN_NEIGHBORS, templ.templateFilename.c_str());

		CoordinationPatternNeighbor bond;
		bond.vec = templ.neighborList().neighborVector(centerAtomIndex, nn);
		pattern.neighbors.push_back(bond);
		nn++;
	}
	CALIB_ASSERT(pattern.numNeighbors() == nn);

	// A coordination must include at least three neighbor atoms.
	if(nn <= 2)
		context().raiseErrorAll("Atom %i does not have enough neighbors to build a coordination pattern. Please increase cutoff radius such that it includes at least three neighbors.", atomTag);

	// Initialize random number generator to a fixed seed value to ensure predictive algorithm behavior.
	srand(1);

	// Randomize order of neighbors in each shell to speedup pattern matching.
	pattern.numShells = 0;
	for(CoordinationPatternNeighborList::iterator a = pattern.neighbors.begin(); a != pattern.neighbors.end(); ) {
		CAFloat shellRadius = a->vec.norm();
		for(CoordinationPatternNeighborList::iterator b = a + 1; ; ++b) {
			if(b == pattern.neighbors.end() || fabs(b->vec.norm() - shellRadius) > shellRadius * CA_ATOM_VECTOR_EPSILON) {
				std::random_shuffle(a, b);
				a = b;
				pattern.numShells++;
				break;
			}
		}
	}

	pattern.lastInnerShellRadius = templ.neighborList().neighborDistance(centerAtomIndex, nn - 1);
	pattern.firstOuterShellRadius = templ.neighborList().neighborDistance(centerAtomIndex, nn);

	if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_NDA) {
		buildNDACoordinationPattern(centerAtomIndex, static_cast<NDACoordinationPattern&>(pattern), templ);
	}
	else if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_CNA || pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA) {
		buildCNACoordinationPattern(centerAtomIndex, static_cast<CNACoordinationPattern&>(pattern), templ);
	}
	else if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_DIAMOND) {
		buildDiamondCoordinationPattern(centerAtomIndex, static_cast<DiamondCoordinationPattern&>(pattern), templ);
	}
	pattern.cutoffGap = (pattern.firstOuterShellRadius - pattern.lastInnerShellRadius) * templ.cutoffGapFactor;

	// Compute length scale factors.
	for(CoordinationPatternNeighborList::iterator n = pattern.neighbors.begin(); n != pattern.neighbors.end(); ++n)
		n->scaleFactor = 1.0 / n->vec.norm() / nn;

	// Find and store all equivalent permutations of the coordination pattern.
	// We use the coordination pattern matcher to discover all permutations.
	auto_ptr<CoordinationPatternMatcher> matcher(CoordinationPatternMatcher::createPatternMatcher(templ.structure(), templ.neighborList(), pattern));
	matcher->setCutoff(templ.innerCutoff);
	if(!matcher->findNextPermutation(centerAtomIndex))
		context().raiseErrorAll("Failed create a valid coordination pattern for atom %i of template structure '%s'. Pattern does not match the structure it was created from.", atomTag, templ.templateFilename.c_str());
	CoordinationPermutation firstPermutation = matcher->permutation();
	CoordinationPermutation firstInversePermutation = matcher->inversePermutation();

	do {
		// Save permutation and its inverse.
		CoordinationPermutation permutation(nn);
		CoordinationPermutation inversePermutation(nn);
		for(int n = 0; n < nn; n++) {
			permutation[n] = firstInversePermutation[matcher->atomNeighborIndex(n)];
			inversePermutation[permutation[n]] = n;
		}
		pattern.permutations.push_back(permutation);
		pattern.inversePermutations.push_back(inversePermutation);

		// Calculate transformation matrix corresponding to the permutation.
		Matrix3 V(Matrix3::Zero());
		Matrix3 W(Matrix3::Zero());
		for(int n = 0; n < nn; n++) {
			const Vector3& lv = pattern.neighbors[n].vec;
			const Vector3& nv = pattern.neighbors[permutation[n]].vec;
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					V(i,j) += lv[j] * lv[i];
					W(i,j) += lv[j] * nv[i];
				}
			}
		}
		if(fabs(V.determinant()) < CAFLOAT_EPSILON || fabs(W.determinant()) < CAFLOAT_EPSILON)
			context().raiseErrorAll("Cannot create a valid coordination pattern for atom %i of template structure '%s'. Found degenerate permutation.", atomTag, templ.templateFilename.c_str());

		pattern.permutationMatrices.push_back(V * W.inverse());

		if(fabs(pattern.permutationMatrices.back().determinant()) < CAFLOAT_EPSILON)
			context().raiseErrorAll("Cannot create a valid coordination pattern for atom %i of template structure '%s'. Found degenerate permutation.", atomTag, templ.templateFilename.c_str());

		for(int n = 0; n < nn; n++) {
			CALIB_ASSERT(inversePermutation[permutation[n]] == n);
			CALIB_ASSERT(permutation[inversePermutation[n]] == n);
		}
	}
	while(matcher->findNextPermutation(centerAtomIndex));
}


/******************************************************************************
* Generates data that is specific for an NDA coordination pattern.
******************************************************************************/
void CatalogGenerator::buildNDACoordinationPattern(int centerAtomIndex, NDACoordinationPattern& pattern, const PatternTemplate& templ)
{
	// Calculate minimum and maximum bond lengths.
	CAFloat localMaxDisplacement = templ.maxNDADisplacement * templ.neighborList().neighborDistance(centerAtomIndex, 0);
	int bondCount = 0;
	for(int ni1 = 0; ni1 < pattern.numNeighbors(); ni1++) {
		for(int ni2 = 0; ni2 < ni1; ni2++) {
			CAFloat distance = (pattern.neighbors[ni1].vec - pattern.neighbors[ni2].vec).norm();
			CAFloat minDistance = max(distance - localMaxDisplacement, (CAFloat)0);
			CAFloat maxDistance = distance + localMaxDisplacement;
			pattern.bonds[ni1][ni2].minDistanceSquared = pattern.bonds[ni2][ni1].minDistanceSquared = square(minDistance);
			pattern.bonds[ni1][ni2].maxDistanceSquared = pattern.bonds[ni2][ni1].maxDistanceSquared = square(maxDistance);
			pattern.bondRangeDistribution[bondCount++] = pattern.bonds[ni1][ni2];
		}
	}
	sort(pattern.bondRangeDistribution, pattern.bondRangeDistribution + bondCount);
	CALIB_ASSERT(bondCount == pattern.numNeighbors() * (pattern.numNeighbors() - 1) / 2);
}

/******************************************************************************
* Generates data that is specific for a CNA or ACNA coordination pattern.
******************************************************************************/
void CatalogGenerator::buildCNACoordinationPattern(int centerAtomIndex, CNACoordinationPattern& pattern, const PatternTemplate& templ)
{
	// Position cutoff radius of coordination pattern between the two shells.
	CAFloat localCutoff = (pattern.lastInnerShellRadius + pattern.firstOuterShellRadius) / 2.0;

	// Set up bond arrays and calculate CNA signatures.
	for(int ni1 = 0; ni1 < pattern.numNeighbors(); ni1++) {
		pattern.neighborArray.setNeighborBond(ni1, ni1, false);
		for(int ni2 = 0; ni2 < ni1; ni2++) {
			bool bonded = (pattern.neighbors[ni1].vec - pattern.neighbors[ni2].vec).squaredNorm() < square(localCutoff);
			pattern.neighborArray.setNeighborBond(ni1, ni2, bonded);
		}
	}
	int ncommonneighbors = 0;
	for(int ni = 0; ni < pattern.numNeighbors(); ni++) {
		CNACoordinationPattern::calculateCNASignature(pattern.neighborArray, ni, pattern.cnaNeighbors[ni], pattern.numNeighbors());
		pattern.cnaNeighborsSorted[ni] = pattern.cnaNeighbors[ni];
		ncommonneighbors += pattern.cnaNeighbors[ni].numCommonNeighbors;
	}
	if(ncommonneighbors == 0)
		context().raiseErrorAll("Cannot use CNA to identify coordination structure of atom %i without common neighbors. Please increase cutoff radius such that it includes common neighbors.",
				templ.structure().atomTag(centerAtomIndex));

	sort(pattern.cnaNeighborsSorted, pattern.cnaNeighborsSorted + pattern.numNeighbors());

	if(pattern.coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_ACNA) {
		AdaptiveCNACoordinationPattern& adaptiveCNAPattern = static_cast<AdaptiveCNACoordinationPattern&>(pattern);
		adaptiveCNAPattern.localCNACutoff = localCutoff;
	}
}

/******************************************************************************
* Generates data that is specific for diamond coordination patterns.
******************************************************************************/
void CatalogGenerator::buildDiamondCoordinationPattern(int centerAtomIndex, DiamondCoordinationPattern& pattern, const PatternTemplate& templ)
{
	if(pattern.numNeighbors() != 4 + 12)
		context().raiseErrorAll("Not the right number of neighbors within the cutoff radius. Atoms in diamond structures have 16 neighbors (nearest and second nearest), but found %i.", pattern.numNeighbors());

	// Compute a local CNA cutoff radius from the average distance of the 4 nearest neighbors.
	CAFloat sum = 0;
	for(int i = 0; i < 4; i++) {
		sum += pattern.neighbors[i].vec.norm();
	}
	sum /= 4;

	// Determine the CNA cutoff.
	CAFloat localCutoff = sum / (sqrt(3.0)/4) * (0.5 * (sqrt(0.5) + 1.0));

	// Set up bond arrays and calculate CNA signatures for second nearest neighbors.
	for(int ni1 = 4; ni1 < pattern.numNeighbors(); ni1++) {
		pattern.neighborArray.setNeighborBond(ni1-4, ni1-4, false);
		for(int ni2 = 4; ni2 < ni1; ni2++) {
			bool bonded = (pattern.neighbors[ni1].vec - pattern.neighbors[ni2].vec).squaredNorm() < square(localCutoff);
			pattern.neighborArray.setNeighborBond(ni1-4, ni2-4, bonded);
		}
	}
	for(int ni = 0; ni < 4; ni++) {
		pattern.cnaNeighbors[ni].numCommonNeighbors = 0;
		pattern.cnaNeighbors[ni].numBonds = 0;
		pattern.cnaNeighbors[ni].maxChainLength = 0;
		pattern.cnaNeighborsSorted[ni] = pattern.cnaNeighbors[ni];
	}
	int n421 = 0;
	int n422 = 0;
	for(int ni = 4; ni < pattern.numNeighbors(); ni++) {
		CNABond& bond = pattern.cnaNeighbors[ni];
		CNACoordinationPattern::calculateCNASignature(pattern.neighborArray, ni-4, bond, 12);
		pattern.cnaNeighborsSorted[ni] = bond;
		if(bond.numCommonNeighbors == 4 && bond.numBonds == 2 && bond.maxChainLength == 1)
			n421++;
		if(bond.numCommonNeighbors == 4 && bond.numBonds == 2 && bond.maxChainLength == 2)
			n422++;
	}
	if(n421 != 12 && !(n421 == 6 && n422 == 6))
		context().raiseErrorAll("This is not a cubic or hexagonal diamond structure.");
	sort(pattern.cnaNeighborsSorted, pattern.cnaNeighborsSorted + pattern.numNeighbors());

	for(int j = 0; j < 4; j++) {
		int n = 0;
		for(int i = 0; i < 12; i++) {
			CAFloat d = (pattern.neighbors[j].vec - pattern.neighbors[i+4].vec).norm();
			if(d < sum * 1.1) {
				if(n == 3)
					context().raiseErrorAll("This is not a diamond structure.");
				pattern.secondToFirstMap[j][n++] = i+4;
			}
		}
		if(n < 3)
			context().raiseErrorAll("This is not a diamond structure.");
	}
}
