///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/util/Timer.h>
#include <calib/context/CAContext.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/output/patterns/PatternCatalogWriter.h>

#include "InputFileParserStream.h"
#include "PatternTemplate.h"
#include "CatalogGenerator.h"

using namespace CALib;
using namespace std;

/**
 * Prints program usage instructions to the console.
 */
void printHelp(ostream& stream)
{
	stream << "Pattern Catalog Generator" << endl;
	stream << "Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)" << endl;
	stream << "Version: " << CA_LIB_VERSION_STRING  << endl << endl;
	stream << "Usage: CatalogGenerator <inputfile> <outputfile>" << endl << endl;
	stream << "Parameters:" << endl << endl;
	stream << "    inputfile : Pattern definition file" << endl;
	stream << "    outputfile: Compiled catalog file" << endl;
}

int main(int argc, char* argv[])
{
	// Check and parse command line arguments.
	if(argc != 3) {
		printHelp(cerr);
		return 1;
	}

	string inputFilename = argv[1];
	string outputFilename = argv[2];

	try {
		// Create global context object.
		CAContext context;
		context.initContext(argc, argv, false);

		if(context.isParallelMode())
			context.raiseErrorAll("The pattern generator can only be run in serial mode.");

		// Open input file for reading.
		ifstream instream(inputFilename.c_str());
		if(!instream)
			context.raiseErrorAll("Failed to open pattern definition file for reading. Filename was '%s'.", inputFilename.c_str());
		InputFileParserStream parserStream(instream, inputFilename);

		// The pattern catalog to be created.
		PatternCatalog catalog(context);
		CatalogGenerator generator(context);

		// Create patterns one by one.
		for(;;) {

			// Parse next template definition from input file.
			PatternTemplate patternTemplate(context, catalog);
			if(!patternTemplate.parseDefinition(parserStream, catalog))
				break;	// End of file has been reached.

			// Create new pattern from template.
			generator.generatePattern(patternTemplate, catalog);
		}

		context.msgLogger() << "------------------------------------------------------" << endl;
		context.msgLogger() << "Saving pattern catalog to file '" << outputFilename << "'." << endl;

		// Open output file for writing.
		ofstream outstream(outputFilename.c_str());
		if(!outstream)
			context.raiseErrorAll("Failed to open output file for writing.");

		// Write catalog to output file.
		PatternCatalogWriter(context).write(outstream, catalog);

		// Done.
		return 0;
	}
	catch(const std::bad_alloc& ex) {
		cerr << "ERROR: Out of memory." << endl;
		return 1;
	}
	catch(const exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}
}
