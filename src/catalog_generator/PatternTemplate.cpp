///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/pattern/catalog/PatternCatalog.h>

#include "InputFileParserStream.h"
#include "PatternTemplate.h"

using namespace CALib;
using namespace std;

/******************************************************************************
* Checks if the first token matches the given keyword,
* and if yes, checks that the right number of parameter tokens is present.
******************************************************************************/
bool PatternTemplate::expectParameters(const vector<string>& tokens, const char* keyword, int numParameters, InputFileParserStream& stream)
{
	if(tokens[0] != keyword)
		return false;

	if(tokens.size() != numParameters + 1) {
		ostringstream ss;
		ss << "Insufficient or too many parameters after keyword " << keyword;
		stream.raiseSyntaxError(context(), ss.str().c_str());
	}
	return true;
}

/******************************************************************************
* Parse the definition of a template from the input file.
******************************************************************************/
bool PatternTemplate::parseDefinition(InputFileParserStream& stream, PatternCatalog& catalog)
{
	// Read first line of definition.
	vector<string> defHeader;
	if(stream.readtokens(defHeader) == 0)
		return false;	// End of file reached.
	if(defHeader.size() != 2 || defHeader[1] != "{")
		stream.raiseSyntaxError(context(), "Invalid template header");
	templateName = defHeader[0];

	// Keep reading lines until end-of-template marker is reached.
	for(;;) {

		// Read and split next line into tokens.
		vector<string> tokens;
		if(stream.readtokens(tokens) == 0)
			stream.raiseSyntaxError(context(), "Unexpected end of file");
		if(tokens[0] == "}") break;	// End-of-template marker.

		if(expectParameters(tokens, "PERIODICITY", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%i", &periodicity) != 1 || periodicity < 0 || periodicity > 3 || periodicity == 1)
				stream.raiseSyntaxError(context(), "Invalid periodicity value. Must be 0, 2, or 3.");
		}
		else if(expectParameters(tokens, "TITLE", 1, stream)) {
			fullName = tokens[1];
		}
		else if(expectParameters(tokens, "FILE", 1, stream)) {
			templateFilename = tokens[1];
		}
		else if(expectParameters(tokens, "UNRELAXED_FILE", 1, stream)) {
			unrelaxedTemplateFilename = tokens[1];
		}
		else if(expectParameters(tokens, "INNER_CUTOFF", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%lg", &innerCutoff) != 1 || innerCutoff <= 0)
				stream.raiseSyntaxError(context(), "Invalid inner cutoff radius");
		}
		else if(expectParameters(tokens, "OUTER_CUTOFF", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%lg", &outerCutoff) != 1 || outerCutoff <= 0)
				stream.raiseSyntaxError(context(), "Invalid outer cutoff radius");
		}
		else if(expectParameters(tokens, "COORD_TYPE", 1, stream)) {
			if(tokens[1] == "acna") coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_ACNA;
			else if(tokens[1] == "cna") coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_CNA;
			else if(tokens[1] == "nda") coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_NDA;
			else if(tokens[1] == "diamond") coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_DIAMOND;
			else if(tokens[1] == "none") coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_NONE;
			else stream.raiseSyntaxError(context(), "Undefined coordination pattern type");
		}
		else if(expectParameters(tokens, "VICINITY_CRITERION", 1, stream)) {
			if(tokens[1] == "lattice") vicinityCriterion = SuperPattern::VICINITY_CRITERION_LATTICE;
			else if(tokens[1] == "interface") vicinityCriterion = SuperPattern::VICINITY_CRITERION_INTERFACE;
			else if(tokens[1] == "defect") vicinityCriterion = SuperPattern::VICINITY_CRITERION_POINTDEFECT;
			else if(tokens[1] == "shortestpath") vicinityCriterion = SuperPattern::VICINITY_CRITERION_SHORTESTPATH;
			else stream.raiseSyntaxError(context(), "Unknown vicinity criterion type");
		}
		else if(expectParameters(tokens, "NDA_MAX_DISPLACEMENT", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%lg", &maxNDADisplacement) != 1 || maxNDADisplacement <= 0)
				stream.raiseSyntaxError(context(), "Invalid NDA maximum displacement");
		}
		else if(tokens[0] == "SURROUNDING_LATTICES") {
			for(int i = 1; i < tokens.size(); i++) {
				SuperPattern const* pattern = catalog.superPatternByName(tokens[i]);
				if(pattern == NULL)
					context().raiseErrorOne("Non-existing lattice referenced in definition of '%s' (line %i). Pattern '%s' does not exist.", templateName.c_str(), stream.lineNumber(), tokens[i].c_str());
				surroundingLatticePatterns.push_back(pattern);
			}
		}
		else if(expectParameters(tokens, "COORD_MATCH_THRESHOLD", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%lg", &perfectCoordinationMatchThreshold) != 1 || perfectCoordinationMatchThreshold <= 0)
				stream.raiseSyntaxError(context(), "Invalid coordination pattern matching threshold");
		}
		else if(expectParameters(tokens, "SUPER_MATCH_THRESHOLD", 1, stream)) {
			if(sscanf(tokens[1].c_str(), "%lg", &superpatternMatchThreshold) != 1 || superpatternMatchThreshold <= 0)
				stream.raiseSyntaxError(context(), "Invalid super pattern matching threshold");
		}
		else if(expectParameters(tokens, "CORE_ATOM", 1, stream)) {
			int i;
			if(sscanf(tokens[1].c_str(), "%i", &i) != 1 || i < 0)
				stream.raiseSyntaxError(context(), "Invalid core atom index");
			userCoreAtoms.push_back(i);
		}
		else if(expectParameters(tokens, "PBC", 3, stream)) {
			preprocessing.pbc[0] = (tokens[1] == "1");
			preprocessing.pbc[1] = (tokens[2] == "1");
			preprocessing.pbc[2] = (tokens[3] == "1");
		}
		else if(expectParameters(tokens, "CLIP_PLANE", 2, stream)) {
			CAFloat dist;
			Vector3 normal;
			if(sscanf(tokens[2].c_str(), CAFLOAT_SCANF_STRING_1, &dist) != 1)
				stream.raiseSyntaxError(context(), "Invalid plane distance");
			if(sscanf(tokens[1].c_str(), "(" CAFLOAT_SCANF_STRING_3 ")", &normal.x(), &normal.y(), &normal.z()) != 3)
				stream.raiseSyntaxError(context(), "Invalid plane normal");
			clippingPlanes.push_back(Plane3(normal.normalized(), -dist));
		}
		else if(expectParameters(tokens, "COLOR", 3, stream)) {
			if(sscanf(tokens[1].c_str(), CAFLOAT_SCANF_STRING_1, &color[0]) != 1 ||
					sscanf(tokens[2].c_str(), CAFLOAT_SCANF_STRING_1, &color[1]) != 1 ||
					sscanf(tokens[3].c_str(), CAFLOAT_SCANF_STRING_1, &color[2]) != 1)
				stream.raiseSyntaxError(context(), "Invalid color");
		}
		else if(expectParameters(tokens, "BURGERS_FAMILY", 9, stream)) {
			BurgersVectorFamily family;
			if(sscanf(tokens[1].c_str(), "%i", &family.id) != 1)
				stream.raiseSyntaxError(context(), "Invalid Burgers vector family identifier.");
			if(family.id <= 0)
				stream.raiseSyntaxError(context(), "Invalid Burgers vector family identifier. Identifier must be positive.");
			BOOST_FOREACH(const BurgersVectorFamily& f, burgersVectorFamilies) {
				if(f.id == family.id)
					stream.raiseSyntaxError(context(), "Duplicate Burgers vector family identifier.");
			}
			int r;
			if(sscanf(tokens[3].c_str(), "1/%i<" CAFLOAT_SCANF_STRING_1, &r, &family.burgersVector.x()) != 2 || r < 1 ||
			   sscanf(tokens[4].c_str(), CAFLOAT_SCANF_STRING_1, &family.burgersVector.y()) != 1 ||
			   sscanf(tokens[5].c_str(), CAFLOAT_SCANF_STRING_1 ">", &family.burgersVector.z()) != 1)
				stream.raiseSyntaxError(context(), "Invalid Burgers vector prototype.");
			sort(family.burgersVector.data(), family.burgersVector.data() + 3);
			if(family.burgersVector.x() < 0)
				stream.raiseSyntaxError(context(), "Invalid Burgers vector prototype. Components must not be negative.");
			family.burgersVector /= r;
			family.name = tokens[6];
			if(sscanf(tokens[7].c_str(), "(" CAFLOAT_SCANF_STRING_1, &family.color.x()) != 1 ||
			   sscanf(tokens[8].c_str(), CAFLOAT_SCANF_STRING_1, &family.color.y()) != 1 ||
			   sscanf(tokens[9].c_str(), CAFLOAT_SCANF_STRING_1 ")", &family.color.z()) != 1)
				stream.raiseSyntaxError(context(), "Invalid color.");
			burgersVectorFamilies.push_back(family);
		}
		else {
			ostringstream ss;
			ss << "Unknown keyword: " << tokens[0];
			stream.raiseSyntaxError(context(), ss.str().c_str());
		}
	}

	// By default, look for all lattices around a crystal defect.
	if(surroundingLatticePatterns.empty()) {
		BOOST_FOREACH(const SuperPattern& pattern, catalog.superPatterns())
			surroundingLatticePatterns.push_back(&pattern);
	}

	// Validate template definition.
	if(periodicity < 0)
		stream.raiseSyntaxError(context(), "Missing PERIODICITY keyword");
	if(innerCutoff == 0)
		stream.raiseSyntaxError(context(), "Missing INNER_CUTOFF keyword");
	if(outerCutoff == 0)
		stream.raiseSyntaxError(context(), "Missing OUTER_CUTOFF keyword");
	if(innerCutoff >= outerCutoff)
		stream.raiseSyntaxError(context(), "Outer cutoff radius must be larger than inner cutoff");
	if(coordinationPatternType == CoordinationPattern::COORDINATION_PATTERN_NDA && maxNDADisplacement <= 0)
		stream.raiseSyntaxError(context(), "Missing NDA_MAX_DISPLACEMENT keyword when using NDA coordination pattern");
	if(periodicity == 3 && !clippingPlanes.empty())
		stream.raiseSyntaxError(context(), "Cannot use clipping planes in the definition of a lattice pattern.");

	return true;
}

/******************************************************************************
* Loads the template structure for the pattern from the specified atoms file.
******************************************************************************/
AtomicStructure& PatternTemplate::loadAtomicStructure()
{
	// Load the atomic structure that serves as a template for the pattern.
	_structure.setName(templateFilename);

	// Parse file.
	AtomicStructureParser(_structure, preprocessing).readAtomsFile(templateFilename.c_str());

	// Build nearest neighbor lists.
	_neighborList.buildNeighborLists(outerCutoff, true);

	// Also load the unrelaxed coordinates of the atoms from a separate file.
	if(unrelaxedTemplateFilename.empty() == false) {
		_unrelaxedStructure.setName(unrelaxedTemplateFilename);

		// Parse file.
		AtomicStructureParser(_unrelaxedStructure, preprocessing).readAtomsFile(unrelaxedTemplateFilename.c_str());

		// Verify that unrelaxed structure is compatible with relaxed structure.
		if(_unrelaxedStructure.numTotalAtoms() != _structure.numTotalAtoms())
			context().raiseErrorAll("Number of atoms in relaxed and unrelaxed template structure does not match.");

		if((_unrelaxedStructure.simulationCellMatrix().matrix() - _structure.simulationCellMatrix().matrix()).isZero(CA_ATOM_VECTOR_EPSILON) == false)
			context().raiseErrorAll("Simulation cell geometries of relaxed and unrelaxed template structure do not match.");

		for(int atomIndex = 0; atomIndex < _structure.numTotalAtoms(); atomIndex++) {
			if(_structure.atomTag(atomIndex) != _unrelaxedStructure.atomTag(atomIndex))
				context().raiseErrorAll("Order of atoms in relaxed and unrelaxed template structure does not match.");
		}
	}
	return _structure;
}

/******************************************************************************
* Given a neighbor of an atom, determines what the connecting vector looks like
* in the unrelaxed template structure.
******************************************************************************/
Vector3 PatternTemplate::unrelaxedNeighborVector(int atomIndex, int neighborIndex) const
{
	Vector3 v = neighborList().neighborVector(atomIndex, neighborIndex);
	if(unrelaxedStructure().numTotalAtoms() != 0) {
		CALIB_ASSERT(structure().atomTag(atomIndex) == unrelaxedStructure().atomTag(atomIndex));
		int neighborAtomIndex = neighborList().neighbor(atomIndex, neighborIndex);
		CALIB_ASSERT(structure().atomTag(neighborAtomIndex) == unrelaxedStructure().atomTag(neighborAtomIndex));
		v += unrelaxedStructure().wrapVector(unrelaxedStructure().atomPosition(neighborAtomIndex) - structure().atomPosition(neighborAtomIndex));
		v -= unrelaxedStructure().wrapVector(unrelaxedStructure().atomPosition(atomIndex) - structure().atomPosition(atomIndex));
	}
	return v;
}
