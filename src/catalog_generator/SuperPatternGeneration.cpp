///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CatalogGenerator.h"
#include <calib/pattern/superpattern/SuperPatternMatcher.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/cluster_graph/ClusterVector.h>
#include <calib/output/atoms/AtomicStructureWriter.h>

using namespace std;
using namespace CALib;

/******************************************************************************
* Create a graph pattern for a lattice structure.
******************************************************************************/
void PatternTemplate::generateLatticeGraphPattern(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis)
{
	// Build a pattern from all atoms of the template cell.
	vector<int> listOfPatternAtoms;
	vector<CoordinationPatternAnalysis::coordmatch const*> coordMappings;
	for(int atomIndex = 0; atomIndex < structure().numTotalAtoms(); atomIndex++) {
		listOfPatternAtoms.push_back(atomIndex);
		coordMappings.push_back(coordPatternAnalysis.bestMatchingCoordinationPattern(atomIndex));
		CALIB_ASSERT(coordMappings.back() != NULL);
	}

	SuperPatternAnalysis superPatternAnalysis(coordPatternAnalysis);

#if 0
	// Make sure no other lattice pattern matches the current template structure.
	vector<SuperPattern const*> patternsToSearchFor;
	BOOST_FOREACH(const SuperPattern& pattern, catalog.superPatterns()) patternsToSearchFor.push_back(&pattern);
	std::vector<SuperPatternAnalysis::AnalysisResultsRecord> results = superPatternAnalysis.searchSuperPatterns(patternsToSearchFor);
	BOOST_FOREACH(const SuperPatternAnalysis::AnalysisResultsRecord& record, results) {
		if(record.numMatchingAtoms != 0)
			context().raiseErrorAll("Pattern %s matches lattice template structure '%s'. Cannot create new lattice pattern from this template.", record.pattern->name.c_str(), templateName.c_str());
	}
#endif

	int numCoreNodes = listOfPatternAtoms.size();
	createGraphPattern(listOfPatternAtoms, coordMappings, numCoreNodes, catalog, superPatternAnalysis).release();
}

/******************************************************************************
* Create a graph pattern for a crystal defect.
******************************************************************************/
void PatternTemplate::generateDefectGraphPattern(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis)
{
	// First look for occurrences of known super patterns in the template structure.
	SuperPatternAnalysis superPatternAnalysis(coordPatternAnalysis);
	superPatternAnalysis.searchSuperPatterns(surroundingLatticePatterns);

	// Generate a new super pattern from all atoms which are not covered by one of the existing super patterns.
	for(int patternCount = 1; ; patternCount++) {
		vector<int> patternAtoms;
		vector<CoordinationPatternAnalysis::coordmatch const*> coordMappings;
		if(userCoreAtoms.empty()) {
			// Find first atom that doesn't match well to an existing super pattern.
			int seedAtomIndex = 0;
			int nclipped = 0;
			for(; seedAtomIndex < structure().numTotalAtoms(); seedAtomIndex++) {
				if(isClipped(seedAtomIndex)) {
					continue;
				}
				if(superPatternAnalysis.atomCluster(seedAtomIndex) == NULL) {
					context().msgLogger() << "Seed atom " << structure().atomTag(seedAtomIndex) << " for new super pattern does not belong to a cluster." << endl;
					break;
				}
				if(superPatternAnalysis.isAntiPhaseBoundaryAtom(seedAtomIndex) && superPatternAnalysis.atomPattern(seedAtomIndex).isLattice())
					break;
				if(calculateSuperPatternDeviation(seedAtomIndex, superPatternAnalysis) > superpatternMatchThreshold) {
					context().msgLogger() << "Seed atom " << structure().atomTag(seedAtomIndex) << " deviation " << calculateSuperPatternDeviation(seedAtomIndex, superPatternAnalysis) << endl;
					break;
				}
			}
			// Reached end of atom list?
			if(seedAtomIndex == structure().numTotalAtoms()) {
				if(patternCount == 1)
					context().raiseErrorAll("Could not create a defect pattern from template structure '%s'. It contains no atoms that form a defect core.", templateName.c_str());
				break;
			}
			CALIB_ASSERT(coordPatternAnalysis.coordinationPatternMatchCount(seedAtomIndex) > 0);

			// Recursively gather all neighboring atoms which also don't match well with an existing super pattern.
			deque<int> toBeProcessed(1, seedAtomIndex);
			do {
				int currentAtom = toBeProcessed.front();
				toBeProcessed.pop_front();
				if(isClipped(currentAtom))
					context().raiseErrorAll("Candidate atom for a new super pattern is in the clipped region (current atom tag %i, seed atom tag %i).", structure().atomTag(currentAtom), structure().atomTag(seedAtomIndex));
				CoordinationPatternAnalysis::coordmatch const* coordinationPattern = coordPatternAnalysis.bestMatchingCoordinationPattern(currentAtom);
				CALIB_ASSERT(coordinationPattern != NULL);
				coordMappings.push_back(coordinationPattern);
				patternAtoms.push_back(currentAtom);

				for(int n = 0; n < coordinationPattern->pattern->numNeighbors(); n++) {
					int neighborAtomIndex = coordPatternAnalysis.neighborList().neighbor(currentAtom, n);
					if(superPatternAnalysis.atomCluster(neighborAtomIndex) != NULL) {
						if(superPatternAnalysis.isAntiPhaseBoundaryAtom(neighborAtomIndex) == false) {
							if(calculateSuperPatternDeviation(neighborAtomIndex, superPatternAnalysis) <= superpatternMatchThreshold)
								continue;
						}
						if(superPatternAnalysis.atomPattern(neighborAtomIndex).isLattice() == false) continue;
					}
					if(boost::find(patternAtoms, neighborAtomIndex) == patternAtoms.end() &&
							boost::find(toBeProcessed, neighborAtomIndex) == toBeProcessed.end()) {
						toBeProcessed.push_back(neighborAtomIndex);
					}
				}
			}
			while(!toBeProcessed.empty());
		}
		else {
			if(patternCount != 1) break;
			for(int i = 0; i < userCoreAtoms.size(); i++) {
				int currentAtom = userCoreAtoms[i];
				if(currentAtom < 0 || currentAtom > structure().numTotalAtoms())
					context().raiseErrorAll("Defect core atom index out of range: %i", currentAtom);
				CoordinationPatternAnalysis::coordmatch const* coordinationPattern = coordPatternAnalysis.bestMatchingCoordinationPattern(currentAtom);
				coordMappings.push_back(coordinationPattern);
				patternAtoms.push_back(currentAtom);
			}
		}

		int numCoreNodes = patternAtoms.size();

		// For grain boundary patterns, these matrices store the misorientations.
		Matrix3 T_A(Matrix3::Identity());	// Transformation from the GB frame to grain A.
		Matrix3 T_B(Matrix3::Identity());	// Transformation from the GB frame to grain B.
		Cluster* grainA = NULL;
		Cluster* grainB = NULL;

		// Now add an extra layer of atoms to the pattern, which overlap with the surrounding lattice atoms.
		set<SuperPattern const*> parentLatticePatterns;
		for(int nodeIndex = 0; nodeIndex < numCoreNodes; nodeIndex++) {
			int currentAtom = patternAtoms[nodeIndex];
			CoordinationPatternAnalysis::coordmatch const* coordinationPattern = coordMappings[nodeIndex];

			for(int n = 0; n < coordinationPattern->pattern->numNeighbors(); n++) {
				int neighborAtomIndex = neighborList().neighbor(currentAtom, n);
				if(superPatternAnalysis.atomCluster(neighborAtomIndex) == NULL) continue;
				if(boost::find(patternAtoms, neighborAtomIndex) != patternAtoms.end()) continue;

				Cluster* latticeCluster = superPatternAnalysis.atomCluster(neighborAtomIndex);
				const SuperPattern& latticePattern = superPatternAnalysis.atomPattern(neighborAtomIndex);
				const SuperPatternNode& latticeNode = superPatternAnalysis.atomPatternNode(neighborAtomIndex);
				boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> neighborCoordinationPatternMapping = coordPatternAnalysis.findCoordinationPatternMatch(neighborAtomIndex, *latticeNode.coordinationPattern);
				CALIB_ASSERT(neighborCoordinationPatternMapping);

				if(latticePattern.isLattice()) {
					FrameTransformation ftm;
					for(int n2 = 0; n2 < latticeNode.coordinationPattern->numNeighbors(); n2++) {
						int atomNeighborIndex = superPatternAnalysis.nodeNeighborToAtomNeighbor(neighborAtomIndex, n2);
						const Vector3 lattice_vector = latticeNode.neighbors[n2].referenceVector;
						const Vector3 gb_vector = unrelaxedNeighborVector(neighborAtomIndex, atomNeighborIndex);
						ftm.addVector(gb_vector, lattice_vector);
					}
					if(grainA == NULL) {
						T_A = ftm.computeTransformation();
						grainA = latticeCluster;
					}
					else if(latticeCluster == grainA) {
						if((T_A - ftm.computeTransformation()).isZero(CA_TRANSITION_MATRIX_EPSILON) == false) {
							context().raiseErrorAll("Elastic field incompatibility detected at interface between the new defect pattern and the parent lattice cluster '%s' at atom %i.",
								latticePattern.name.c_str(), structure().atomTag(neighborAtomIndex));
						}
					}
					else if(grainB == NULL) {
						T_B = ftm.computeTransformation();
						grainB = latticeCluster;
					}
					else if(latticeCluster == grainB) {
						if((T_B - ftm.computeTransformation()).isZero(CA_TRANSITION_MATRIX_EPSILON) == false) {
							context().raiseErrorAll("Elastic field incompatibility detected at interface between the new defect pattern and the parent lattice cluster '%s' at atom %i.",
								latticePattern.name.c_str(), structure().atomTag(neighborAtomIndex));
						}
					}

					patternAtoms.push_back(neighborAtomIndex);
					coordMappings.push_back(&**neighborCoordinationPatternMapping);
					parentLatticePatterns.insert(&latticePattern);
				}
			}
		}

		// Create the new super pattern.
		SuperPattern* superPattern = createGraphPattern(patternAtoms, coordMappings, numCoreNodes, catalog, superPatternAnalysis).release();

		// Compute misorientation matrix of grain boundary.
		if(periodicity == 2) {
			if(grainA != NULL && grainB != NULL) {
				superPattern->misorientationMatrix = T_B * T_A.inverse();
				context().msgLogger() << "Crystal interface misorientation matrix (det = "
						<< superPattern->misorientationMatrix.determinant() << "):" << endl << superPattern->misorientationMatrix;
				CAFloat misorientationAngle = CAFLOAT_MAX;
				BOOST_FOREACH(const SuperPattern* pattern, parentLatticePatterns) {
					for(int nodeIndex = 0; nodeIndex < pattern->numCoreNodes; nodeIndex++) {
						BOOST_FOREACH(const SpaceGroupElement& sge, pattern->nodes[nodeIndex].spaceGroupEntries) {
							Matrix3 tm = T_B * sge.tm * T_A.inverse();
							CAFloat trace = tm.trace() - 1.0;
							Vector3 axis(tm(2,1) - tm(1,2), tm(0,2) - tm(2,0), tm(1,0) - tm(0,1));
							CAFloat angle = atan2(axis.norm(), trace);
							if(angle > CAFLOAT_PI)
								angle = (2.0 * CAFLOAT_PI) - angle;
							if(angle < misorientationAngle)
								misorientationAngle = angle;
						}
					}
				}
				context().msgLogger() << "Crystal interface misorientation angle: " << (misorientationAngle / CAFLOAT_PI * 180.0) << endl;
				if(!isRotationReflectionMatrix(superPattern->misorientationMatrix, CA_TRANSITION_MATRIX_EPSILON)) {
					context().msgLogger() << "M * M^t" << endl << (superPattern->misorientationMatrix * superPattern->misorientationMatrix.transpose());
					context().msgLogger() << "WARNING: Interface misorientation is not a pure rotation/reflection matrix." << endl;
				}
			}
		}
		superPattern->parentLatticePatterns = parentLatticePatterns;

		// Mark all matches of the new pattern in the current template structure.
		vector<SuperPattern const*> patternsToSearchFor;
		for(int i = 0; i < surroundingLatticePatterns.size(); i++)
			if(parentLatticePatterns.find(surroundingLatticePatterns[i]) != parentLatticePatterns.end())
				patternsToSearchFor.push_back(surroundingLatticePatterns[i]);
		patternsToSearchFor.push_back(superPattern);
		vector<SuperPatternAnalysis::AnalysisResultsRecord> paResults = superPatternAnalysis.searchSuperPatterns(patternsToSearchFor);
		BOOST_FOREACH(const SuperPatternAnalysis::AnalysisResultsRecord& record, paResults) {
			if(record.numDisclinations)
				context().raiseErrorAll("Detected a disclination in a cluster matching to the '%s' pattern.", record.pattern->name.c_str());
		}

		// The new super pattern must at least match to the atoms it has been generated from.
		for(int i = 0; i < numCoreNodes; i++) {
			if(superPatternAnalysis.atomCluster(patternAtoms[i]) == NULL)
				context().raiseErrorAll("The newly created super pattern does not match to the atoms it was created from.");
			if(&superPatternAnalysis.atomPattern(patternAtoms[i]) != superPattern)
				context().raiseErrorAll("The atoms that were used to create the super pattern already match to the '%s' pattern.", superPatternAnalysis.atomPattern(patternAtoms[i]).name.c_str());
		}

		// Check elastic compatibility at cluster interfaces.
		for(int atomIndex = 0; atomIndex < structure().numTotalAtoms(); atomIndex++) {
			if(superPatternAnalysis.atomCluster(atomIndex) == NULL) continue;
			if(&superPatternAnalysis.atomPattern(atomIndex) != superPattern) continue;
			const SuperPatternNode& patternNode = superPatternAnalysis.atomPatternNode(atomIndex);
			for(int ni1 = 0; ni1 < patternNode.coordinationPattern->numNeighbors(); ni1++) {
				int neighborAtomIndex = neighborList().neighbor(atomIndex, superPatternAnalysis.nodeNeighborToAtomNeighbor(atomIndex, ni1));
				if(superPatternAnalysis.atomCluster(neighborAtomIndex) == NULL) continue;
				const SuperPattern& otherPattern = superPatternAnalysis.atomPattern(neighborAtomIndex);
				const SuperPatternNode& otherNode = superPatternAnalysis.atomPatternNode(neighborAtomIndex);
				ClusterVector latticeVector(patternNode.neighbors[ni1].referenceVector, superPatternAnalysis.atomCluster(atomIndex));
				for(int ni2 = 0; ni2 < otherNode.coordinationPattern->numNeighbors(); ni2++) {
					if(neighborList().neighbor(neighborAtomIndex, superPatternAnalysis.nodeNeighborToAtomNeighbor(neighborAtomIndex, ni2)) != atomIndex) continue;
					if((neighborList().neighborVector(neighborAtomIndex, superPatternAnalysis.nodeNeighborToAtomNeighbor(neighborAtomIndex, ni2)) +
							neighborList().neighborVector(atomIndex, superPatternAnalysis.nodeNeighborToAtomNeighbor(atomIndex, ni1))).isZero(CA_ATOM_VECTOR_EPSILON) == false) continue;
					ClusterVector latticeVector2(otherNode.neighbors[ni2].referenceVector, superPatternAnalysis.atomCluster(neighborAtomIndex));
					if(latticeVector2.transformToCluster(superPatternAnalysis.atomCluster(atomIndex), superPatternAnalysis.clusterGraph())) {
						if(!(latticeVector2.localVec() + latticeVector.localVec()).isZero(CA_LATTICE_VECTOR_EPSILON)) {
							context().msgLogger() << latticeVector.localVec() << " - " << -latticeVector2.localVec() << endl;
							context().msgLogger() << (latticeVector.localVec() +latticeVector2.localVec()) << endl;
							context().raiseErrorAll("Elastic field incompatibility detected at interface between clusters '%s' and '%s'.", superPattern->name.c_str(), otherPattern.name.c_str());
						}
					}
				}
			}
		}

#if 0
		std::vector<AtomicStructureWriter::DataField> fields;
		fields.push_back(AtomicStructureWriter::ATOM_ID);
		fields.push_back(AtomicStructureWriter::POS_X);
		fields.push_back(AtomicStructureWriter::POS_Y);
		fields.push_back(AtomicStructureWriter::POS_Z);
		fields.push_back(AtomicStructureWriter::SUPER_PATTERN);
		fields.push_back(AtomicStructureWriter::CLUSTER_ID);
		ofstream stream("DEBUG.dump");
		AtomicStructureWriter(context()).writeLAMMPSFile(stream, fields, structure, &superPatternAnalysis);
		//ofstream stream2("DEBUG.poscar");
		//AtomicStructureWriter(context()).writePOSCARFile(stream2, structure);
#endif
	}
}

/******************************************************************************
* Generates a new graph pattern from a set of atoms.
******************************************************************************/
auto_ptr<SuperPattern> PatternTemplate::createGraphPattern(const vector<int>& listOfAtoms, const vector<CoordinationPatternAnalysis::coordmatch const*>& coordMappings, int numCoreNodes,
		PatternCatalog& catalog, const SuperPatternAnalysis& superPatternAnalysis)
{
	CALIB_ASSERT(listOfAtoms.size() >= 1);
	CALIB_ASSERT(listOfAtoms.size() == coordMappings.size());
	CALIB_ASSERT(numCoreNodes <= listOfAtoms.size());
	CALIB_ASSERT(numCoreNodes >= 1);

	auto_ptr<SuperPattern> pattern(new SuperPattern());
	pattern->name = templateName;
	pattern->fullName = fullName;
	pattern->numNodes = listOfAtoms.size();
	pattern->numCoreNodes = numCoreNodes;
	pattern->nodes.resize(listOfAtoms.size());
	pattern->periodicity = periodicity;
	pattern->vicinityCriterion = vicinityCriterion;
	pattern->color = color;
	pattern->burgersVectorFamilies = burgersVectorFamilies;
	pattern->misorientationMatrix.setIdentity();
	pattern->periodicCell = structure().simulationCellMatrix().linear();
	for(int dim = 0; dim < 3; dim++)
		if(!structure().pbc(dim)) pattern->periodicCell.col(dim).setZero();

	// List of atom types that occur in the super pattern.
	set<int> speciesSet;

	// Generate a super pattern node for each input atom.
	for(int nodeIndex = 0; nodeIndex < pattern->numNodes; nodeIndex++) {
		SuperPatternNode& patternNode = pattern->nodes[nodeIndex];
		int atomIndex = listOfAtoms[nodeIndex];

		patternNode.coordinationPattern = coordMappings[nodeIndex]->pattern;
		patternNode.overlapPattern = NULL;

		patternNode.species = structure().atomSpecies(atomIndex);
		speciesSet.insert(patternNode.species);

		context().msgLogger() << "  Created node " << nodeIndex << " from atom " << structure().atomTag(atomIndex);
		if(nodeIndex < numCoreNodes)
			context().msgLogger() << " (core";
		else
			context().msgLogger() << " (overlap=" << superPatternAnalysis.atomPattern(atomIndex).name;
		context().msgLogger() << ", coordination=" << patternNode.coordinationPattern->name	<< ")" << endl;

		// Create bonds to neighbor nodes.
		for(int n = 0; n < patternNode.coordinationPattern->numNeighbors(); n++) {
			int neighborIndex = coordMappings[nodeIndex]->mapping[n];
			int neighborAtomIndex = neighborList().neighbor(atomIndex, neighborIndex);
			int neighborNodeIndex = -1;
			for(int nodeIndex2 = 0; nodeIndex2 < pattern->numNodes; nodeIndex2++) {
				if(listOfAtoms[nodeIndex2] == neighborAtomIndex) {
					neighborNodeIndex = nodeIndex2;
					break;
				}
			}
			patternNode.neighbors[n].neighborNodeIndex = neighborNodeIndex;
			patternNode.neighbors[n].reverseNodeNeighborIndex = -1;
			patternNode.neighbors[n].numCommonNeighbors = 0;
			patternNode.neighbors[n].referenceVector = unrelaxedNeighborVector(atomIndex, neighborIndex);
		}
	}

	// Clear species information if there is not more than one species in the pattern.
	if(speciesSet.size() <= 1) {
		for(int nodeIndex = 0; nodeIndex < pattern->numNodes; nodeIndex++)
			pattern->nodes[nodeIndex].species = 0;
	}

	// Determine reverse neighbor bond indices and common neighbors.
	for(int nodeIndex1 = 0; nodeIndex1 < pattern->numCoreNodes; nodeIndex1++) {
		SuperPatternNode& node1 = pattern->nodes[nodeIndex1];
		for(int n = 0; n < node1.coordinationPattern->numNeighbors(); n++) {
			int nodeIndex2 = node1.neighbors[n].neighborNodeIndex;
			CALIB_ASSERT(nodeIndex2 >= 0);
			const SuperPatternNode& node2 = pattern->nodes[nodeIndex2];
			node1.neighbors[n].numCommonNeighbors = 0;
			Matrix3 coplanarCheck;
			coplanarCheck.col(0) = node1.neighbors[n].referenceVector;
			for(int ni1 = 0; ni1 < node2.coordinationPattern->numNeighbors(); ni1++) {
				if((node2.neighbors[ni1].referenceVector + node1.neighbors[n].referenceVector).isZero(CA_LATTICE_VECTOR_EPSILON)) {
					CALIB_ASSERT(node1.neighbors[n].reverseNodeNeighborIndex == -1);
					node1.neighbors[n].reverseNodeNeighborIndex = ni1;
				}
				else {
					for(int ni2 = 0; ni2 < node1.coordinationPattern->numNeighbors(); ni2++) {
						if(ni2 == n) continue;
						if((node1.neighbors[ni2].referenceVector - node2.neighbors[ni1].referenceVector - node1.neighbors[n].referenceVector).isZero(CA_LATTICE_VECTOR_EPSILON)) {
							CALIB_ASSERT(node1.neighbors[n].numCommonNeighbors < CA_MAX_PATTERN_NEIGHBORS);
							node1.neighbors[n].commonNeighborNodeIndices[node1.neighbors[n].numCommonNeighbors][0] = ni2;
							node1.neighbors[n].commonNeighborNodeIndices[node1.neighbors[n].numCommonNeighbors][1] = ni1;
							if(node1.neighbors[n].numCommonNeighbors == 0) {
								coplanarCheck.col(1) = node1.neighbors[ni2].referenceVector;
								node1.neighbors[n].numCommonNeighbors++;
							}
							else if(node1.neighbors[n].numCommonNeighbors == 1) {
								coplanarCheck.col(2) = node1.neighbors[ni2].referenceVector;
								if(fabs(coplanarCheck.determinant()) > CA_TRANSITION_MATRIX_EPSILON)
									node1.neighbors[n].numCommonNeighbors++;
							}
						}
					}
				}
			}
			if(node1.neighbors[n].reverseNodeNeighborIndex == -1) {
				int atom1tag = structure().atomTag(listOfAtoms[nodeIndex1]);
				int atom2tag = structure().atomTag(listOfAtoms[nodeIndex2]);
				context().raiseErrorAll("Atom %i (%s) has bond to atom %i (%s), but not vice versa. Cannot build a valid super pattern in this case.", atom1tag, node1.coordinationPattern->name.c_str(), atom2tag, node2.coordinationPattern->name.c_str());
			}
			if(node1.neighbors[n].numCommonNeighbors < 2) {
				int atom1tag = structure().atomTag(listOfAtoms[nodeIndex1]);
				int atom2tag = structure().atomTag(listOfAtoms[nodeIndex2]);
				if(node1.neighbors[n].numCommonNeighbors < 1)
					context().raiseErrorAll("No common neighbors between atom %i (%s) and atom %i (%s).", atom1tag, node1.coordinationPattern->name.c_str(), atom2tag, node2.coordinationPattern->name.c_str());
				else
					context().msgLogger() << "WARNING: Number of (non-coplanar) common neighbors between atom " << atom1tag << " (" << node1.coordinationPattern->name << ") and atom " << atom2tag << " (" << node2.coordinationPattern->name << ") is less than two." << endl;
			}
		}
	}

	// Determine node-to-node distances in the super pattern graph.
	for(int seedNodeIndex = 0; seedNodeIndex < pattern->numNodes; seedNodeIndex++) {
		SuperPatternNode& seedNode = pattern->nodes[seedNodeIndex];
		seedNode.nodeNodeDistances.resize(pattern->numNodes, -1);
		seedNode.nodeNodeDistances[seedNodeIndex] = 0;
		vector<int> currentNodes(1, seedNodeIndex);
		for(int iteration = 1; currentNodes.empty() == false; iteration++) {
			CALIB_ASSERT(iteration <= pattern->numNodes);
			vector<int> newlyVisitedNodes;
			for(int nodeIndex = 0; nodeIndex < currentNodes.size(); nodeIndex++) {
				SuperPatternNode& currentNode = pattern->nodes[currentNodes[nodeIndex]];
				for(int n = 0; n < currentNode.coordinationPattern->numNeighbors(); n++) {
					int nodeIndex2 = currentNode.neighbors[n].neighborNodeIndex;
					if(nodeIndex2 < 0) continue;
					if(seedNode.nodeNodeDistances[nodeIndex2] == -1) {
						seedNode.nodeNodeDistances[nodeIndex2] = iteration;
						newlyVisitedNodes.push_back(nodeIndex2);
					}
				}
			}
			currentNodes = newlyVisitedNodes;
		}
	}

	context().msgLogger() << "* Created super pattern '" << pattern->name << "' [" << pattern->numCoreNodes << " core nodes, " << (pattern->numNodes - pattern->numCoreNodes) << " overlap nodes]." << endl;

	// Set up overlapping nodes.
	for(int nodeIndex = pattern->numCoreNodes; nodeIndex < pattern->numNodes; nodeIndex++) {
		int atomIndex = listOfAtoms[nodeIndex];
		SuperPatternNode& patternNode = pattern->nodes[nodeIndex];
		CALIB_ASSERT(superPatternAnalysis.atomCluster(atomIndex) != NULL);
		patternNode.overlapPattern = &superPatternAnalysis.atomPattern(atomIndex);
		patternNode.overlapPatternNode = superPatternAnalysis.atomPatternNodeIndex(atomIndex);
	}

	findSpaceGroupElements(*pattern, catalog, listOfAtoms, superPatternAnalysis.coordAnalysis());

	// Add new pattern to the catalog.
	catalog.addSuperPattern(pattern.get());

	return pattern;
}


/******************************************************************************
* Finds all symmetry elements of the pattern's space group.
******************************************************************************/
void PatternTemplate::findSpaceGroupElements(SuperPattern& pattern, PatternCatalog& catalog, const vector<int>& listOfAtoms, const CoordinationPatternAnalysis& coordAnalysis)
{
	// Do an extensive search: Try to match pattern to every atom of the template structure, starting with every node of the pattern,
	// and try every possible coordination pattern permutation.
	// This will let us discover all elements of the pattern's space group.

	SuperPatternMatcher matcher(coordAnalysis, catalog, pattern);
	int numMatches = 0, numSpaceGroupElements = 0;
	for(int seedAtomIndex = 0; seedAtomIndex < structure().numTotalAtoms(); seedAtomIndex++) {
		if(isClipped(seedAtomIndex))
			continue;
		for(int seedNodeIndex = 0; seedNodeIndex < pattern.numCoreNodes; seedNodeIndex++) {
			const SuperPatternNode& seedNode = pattern.nodes[seedNodeIndex];
			if(seedNode.species != 0 && structure().atomSpecies(seedAtomIndex) != seedNode.species) continue;
			boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> seedCoordinationMapping = coordAnalysis.findCoordinationPatternMatch(seedAtomIndex, *seedNode.coordinationPattern);
			if(!seedCoordinationMapping) continue;
			for(int seedPermutationIndex = 0; seedPermutationIndex != seedNode.coordinationPattern->permutations.size(); ++seedPermutationIndex) {
				if(matcher.checkPermutation(seedAtomIndex, seedNodeIndex, seedPermutationIndex)) {

					if(matcher.foundDisclination())
						context().raiseErrorAll("Detected a disclination while matching the new super pattern to the template structure.");

					numMatches++;
					BOOST_FOREACH(const SuperPatternMatcher::MatchedAtom& atom, matcher.matchingAtoms()) {
						const SuperPatternNode& node = pattern.nodes[atom.nodeIndex];
						FrameTransformation ftm;
						for(int nodeNeighborIndex = 0; nodeNeighborIndex < atom.numNeighbors(); nodeNeighborIndex++) {
							int atomNeighborIndex = atom.nodeNeighborToAtomNeighbor(nodeNeighborIndex);
							Vector3 nv = unrelaxedNeighborVector(atom.atomIndex, atomNeighborIndex);
							const Vector3& lv = node.neighbors[nodeNeighborIndex].referenceVector;
							ftm.addVector(nv, lv);
						}
						Matrix3 orientation = ftm.computeTransformation();

						for(int nodeNeighborIndex = 0; nodeNeighborIndex < atom.numNeighbors(); nodeNeighborIndex++) {
							int atomNeighborIndex = atom.nodeNeighborToAtomNeighbor(nodeNeighborIndex);
							Vector3 nv = unrelaxedNeighborVector(atom.atomIndex, atomNeighborIndex);
							const Vector3& lv = node.neighbors[nodeNeighborIndex].referenceVector;
							CAFloat deviation = (lv - orientation * nv).squaredNorm() / lv.squaredNorm();
							if(deviation > square(CA_LATTICE_VECTOR_EPSILON) * 1e4) {
								context().msgLogger() << "Relative orientation (det=" << orientation.determinant() << "):" << endl << orientation;
								Matrix3 worldOrientation = matcher.calculateOrientation();
								context().msgLogger() << "World space orientation (det=" << worldOrientation.determinant() << "): " << endl << worldOrientation;
								context().msgLogger() << "deviation: " << deviation << "  lv=" << lv << " nv=" << nv << "  orientation*nv=" << (orientation * nv) << endl;
								context().msgLogger() << "threshold: " << square(CA_LATTICE_VECTOR_EPSILON) * 1e4 << endl;

								context().raiseErrorAll("Found permutation of super pattern that is not an exact match.");
							}
						}

						if(!isRotationReflectionMatrix(orientation, 2.0 * CA_TRANSITION_MATRIX_EPSILON)) {
							context().msgLogger() << "Seed node: " << seedNodeIndex << " seed atom: " << structure().atomTag(seedAtomIndex) << " seedPermutationIndex=" << seedPermutationIndex << endl;
							context().msgLogger() << "Transformation matrix (det=" << orientation.determinant() << "):" << endl << orientation;
							context().raiseErrorAll("Detected an orientation matrix is not a pure rotation/reflection matrix during pattern symmetry analysis.");
						}

						// Build up list of space group elements.
						for(int nodeIndex = 0; nodeIndex < pattern.numCoreNodes; nodeIndex++) {
							if(atom.atomIndex == listOfAtoms[nodeIndex]) {
								SuperPatternNode& patternNode = pattern.nodes[nodeIndex];
								if(atom.coordinationPattern() == patternNode.coordinationPattern) {

									bool isNew = true;
									BOOST_FOREACH(const SpaceGroupElement& sge, patternNode.spaceGroupEntries) {
										if(sge.targetNodeIndex == atom.nodeIndex && sge.permutationIndex == atom.permutationIndex) {
											isNew = false;
											CALIB_ASSERT((sge.tm - orientation).isZero(CA_TRANSITION_MATRIX_EPSILON));
											break;
										}
									}

									if(isNew) {
										SpaceGroupElement sge = { atom.nodeIndex, atom.permutationIndex, orientation };
										patternNode.spaceGroupEntries.push_back(sge);
										numSpaceGroupElements++;

#if 0
										context().msgLogger() << "Space group element " << numSpaceGroupElements << ":" << endl;
										context().msgLogger() << "  node: " << atom.nodeIndex << endl;
										context().msgLogger() << "  permutation: " << atom.permutationIndex << endl;
										context().msgLogger() << "  orientation: " << endl << orientation << endl;
#endif
									}
								}
							}
						}
					}
				}
			}
		}
	}
	context().msgLogger() << "Found the new super pattern " << numMatches << " times in the template structure '" << templateName << "'." << endl;
	context().msgLogger() << "Space group of the new pattern consists of " << numSpaceGroupElements << " elements." << endl;
	if(numMatches == 0)
		context().raiseErrorAll("Pattern did not match atoms it was created from. Something is wrong.");
	CALIB_ASSERT(numSpaceGroupElements >= pattern.numCoreNodes);
}

/******************************************************************************
* Quantifies the deviation of an actual coordination structure of an atom from
* the ideal reference pattern.
******************************************************************************/
CAFloat PatternTemplate::calculateSuperPatternDeviation(int atomIndex, const SuperPatternAnalysis& superPatternAnalysis) const
{
	CALIB_ASSERT(superPatternAnalysis.atomCluster(atomIndex) != NULL);

	// Calculate transformation matrix using a least-square fit.
	FrameTransformation ftm;
	const SuperPattern& superPattern = superPatternAnalysis.atomPattern(atomIndex);
	const SuperPatternNode& node = superPatternAnalysis.atomPatternNode(atomIndex);
	const CoordinationPattern* const coordPattern = node.coordinationPattern;
	for(int nodeNeighborIndex = 0; nodeNeighborIndex < coordPattern->numNeighbors(); nodeNeighborIndex++) {
		const Vector3& lv = node.neighbors[nodeNeighborIndex].referenceVector;
		int atomNeighborIndex = superPatternAnalysis.nodeNeighborToAtomNeighbor(atomIndex, nodeNeighborIndex);
		Vector3 nv = unrelaxedNeighborVector(atomIndex, atomNeighborIndex);
		ftm.addVector(nv, lv);
	}
	Matrix3 orientation = ftm.computeAverageTransformation();

	CAFloat maxDeviation = 0;
	for(int nodeNeighborIndex = 0; nodeNeighborIndex < coordPattern->numNeighbors(); nodeNeighborIndex++) {
		int atomNeighborIndex = superPatternAnalysis.nodeNeighborToAtomNeighbor(atomIndex, nodeNeighborIndex);
		Vector3 nv = unrelaxedNeighborVector(atomIndex, atomNeighborIndex);
		const Vector3& lv = node.neighbors[nodeNeighborIndex].referenceVector;
		CAFloat deviation = (lv - orientation * nv).squaredNorm() / coordPattern->neighbors[0].vec.squaredNorm();
		if(deviation > maxDeviation)
			maxDeviation = deviation;
	}

	return sqrt(maxDeviation);
}

