///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CrystalAnalysisTool.h"
#include <calib/input/atoms/AtomicStructureParser.h>
#include <calib/input/patterns/PatternCatalogReader.h>
#include <calib/surface/FreeSurfaceIdentification.h>
#include <calib/util/Timer.h>

/******************************************************************************
* Constructor.
******************************************************************************/
CrystalAnalysisTool::CrystalAnalysisTool() :
	_patternCatalog(_context),
	_tessellation(_context, _input.structure),
	_input(_context, _patternCatalog, _tessellation),
	_deformedConfiguration(_context, _patternCatalog, _tessellation),
	_interfaceMesh(_input.elasticMapping),
	_tracer(_interfaceMesh),
	_surface(_context),
	_surfaceCap(_context),
	_freeSurface(_context),
	_flippedFreeSurface(_context),
	_freeSurfaceCap(_context),
	_deformationField(_context, _input.structure, _deformedConfiguration.structure, _tessellation, _patternCatalog),
	_elasticField(_context, _input.structure, _tessellation),
	_maxNeighbors(0)
{
}

/******************************************************************************
* Initializes MPI etc.
******************************************************************************/
bool CrystalAnalysisTool::initialize(int argc, char* argv[])
{
	context().initContext(argc, argv, true);
	return true;
}

/******************************************************************************
* Loads the pattern catalog and puts together the list
* of coordination patterns and super patterns to search for.
******************************************************************************/
void CrystalAnalysisTool::loadPatterns()
{
	if(context().isMaster()) {
		// Read pattern catalog file (only on the master processor).
		context().msgLogger() << "Loading pattern catalog '" << params().patternCatalogFile << "'" << endl;
		ifstream catalog_stream(params().patternCatalogFile.c_str());
		if(!catalog_stream.is_open())
			context().raiseErrorOne("Failed to open pattern catalog file for reading. Filename was '%s'.", params().patternCatalogFile.c_str());
		try {
			PatternCatalogReader(context()).read(catalog_stream, catalog());
		}
		catch(const std::exception& ex) {
			ostringstream ss;
			ss << "Could not load pattern catalog file: " << ex.what() << endl;
			ss << "Please regenerate the pattern catalog file by running the 'create_catalog.sh' script found in the directory 'patterns/'.";
			throw std::runtime_error(ss.str());
		}
	}

	// Broadcast the catalog's data from the master to other processors.
	catalog().broadcast();

	// Parse list of patterns to search for.
	istringstream ss(params().patternList);
	string patternname;
	set<SuperPattern const*> superPatternsToSearchForSet;
	set<CoordinationPattern const*> coordinationPatternsToSearchForSet;
	while(getline(ss, patternname, ',')) {
		SuperPattern const* pattern = catalog().superPatternByName(patternname);
		if(pattern == NULL)
			context().raiseErrorAll("Pattern with name '%s' not found in pattern catalog.", patternname.c_str());
		superPatternsToSearchForSet.insert(pattern);
		BOOST_FOREACH(const SuperPatternNode& node, pattern->nodes) {
			CALIB_ASSERT(node.coordinationPattern != NULL);
			coordinationPatternsToSearchForSet.insert(node.coordinationPattern);
		}
	}
	BOOST_FOREACH(const SuperPattern& spi, catalog().superPatterns()) {
		if(superPatternsToSearchForSet.find(&spi) != superPatternsToSearchForSet.end()) {
			_superPatterns.push_back(&spi);

			// Make sure for defect patterns that the their parent lattice(s) are also selected.
			for(int nodeIndex = spi.numCoreNodes; nodeIndex < spi.numNodes; nodeIndex++) {
				const SuperPattern* parentLattice = spi.nodes[nodeIndex].overlapPattern;
				CALIB_ASSERT(parentLattice != NULL);
				if(superPatternsToSearchForSet.find(parentLattice) == superPatternsToSearchForSet.end()) {
					context().raiseErrorAll("The selected defect pattern '%s' is embedded in the parent lattice '%s'. Please also select the lattice pattern '%s' from the catalog.",
							patternname.c_str(), parentLattice->name.c_str(), parentLattice->name.c_str());
				}
			}
		}
	}
	BOOST_FOREACH(const CoordinationPattern& cpi, catalog().coordinationPatterns()) {
		if(coordinationPatternsToSearchForSet.find(&cpi) != coordinationPatternsToSearchForSet.end())
			_coordinationPatterns.push_back(&cpi);
	}

	// Determine maximum number of neighbors per atom required for pattern analysis.
	_maxNeighbors = 0;
	BOOST_FOREACH(CoordinationPattern const* pattern, _coordinationPatterns) {
		_maxNeighbors = max(_maxNeighbors, pattern->maxNeighbors());
	}
}

/******************************************************************************
* Loads an input file and builds neighbor lists.
******************************************************************************/
void CrystalAnalysisTool::loadInputStructure(StructureAnalysisSet& s, const string& filename, bool createHelperAtoms)
{
	// Parse input file.
	AtomicStructureParser parser(s.structure, params().preprocessing);
	if(!params().inputCoordinateColumnX.empty())
		parser.setCoordinateColums(params().inputCoordinateColumnX, params().inputCoordinateColumnY, params().inputCoordinateColumnZ);
	parser.readAtomsFile(filename.c_str(), &std::cin);

	context().msgLogger() << "Processor grid with " << parallel().processorCount() << " processors." << endl;
	context().msgLogger() << "Using " << parallel().processorGrid(0) << " x " << parallel().processorGrid(1) << " x " << parallel().processorGrid(2) << " processor grid." << endl;
	if(context().allProcOutput())
		context().msgLogger() << "Processor location is (" << parallel().processorLocation(0) << ", " << parallel().processorLocation(1) << ", " << parallel().processorLocation(2) << ")." << endl;
	context().msgLogger() << "Simulation cell: " << endl << s.structure.simulationCellMatrix();
	context().msgLogger() << "Periodic boundary conditions: " << s.structure.pbc(0) << " " << s.structure.pbc(1) << " " << s.structure.pbc(2) << endl;

	if(context().allProcOutput())
		context().msgLogger() << "Processor has " << s.structure.numLocalAtoms() << " local atoms." << endl;

	// Create ghost atoms.
	s.structure.setGhostCutoff(params().ghostCutoff);
	context().msgLogger() << "Exchanging ghost atoms (ghost cutoff = " << params().ghostCutoff << ")." << endl;
	Timer ghostAtomsTimer;
	s.structure.communicateGhostAtoms();
	context().barrier();
	context().msgLogger() << "Ghost atom time: " << ghostAtomsTimer << endl;

	// Create helper atoms.
	if(createHelperAtoms) {
		context().msgLogger() << "Creating tessellation helper points (distance = " << params().helperPointDistance << ")." << endl;
		Timer tessellationPointsTimer;
		s.structure.createExtraTessellationPoints(params().helperPointDistance);
		context().barrier();
		context().msgLogger() << "Tessellation helper points time: " << tessellationPointsTimer << endl;
	}

	if(params().identifyAtomicStructures) {
		// Build local nearest neighbor lists.
		context().msgLogger() << "Building neighbor lists (cutoff = " << params().neighborCutoff << ")." << endl;
		Timer neighborListTimer;
		s.neighborList.buildNeighborLists(params().neighborCutoff, true, true, _maxNeighbors);
		context().barrier();
		context().msgLogger() << "Neighbor list time: " << neighborListTimer << endl;
	}
}

/******************************************************************************
* Identifies local atomic arrangements.
******************************************************************************/
void CrystalAnalysisTool::performStructureAnalysis(StructureAnalysisSet& s)
{
	// Perform coordination pattern matching.
	context().msgLogger() << "Searching for coordination patterns." << endl;
	Timer coordinationPatternTimer;
	s.coordinationPatternAnalysis.searchCoordinationPatterns(_coordinationPatterns, params().neighborCutoff);
	context().barrier();
	context().msgLogger() << "Coordination pattern time: " << coordinationPatternTimer << endl;

	// Perform super pattern matching.
	context().msgLogger() << "Searching for super patterns." << endl;
	Timer superPatternTimer;
	_patternAnlysisResults = s.superPatternAnalysis.searchSuperPatterns(_superPatterns);
	context().barrier();
	context().msgLogger() << "Super pattern time: " << superPatternTimer << endl;
	if(params().structureCountsOutputFile.empty() == false)
		outputStructureMatchCounts(params().structureCountsOutputFile);

	// Prepare cluster graph.
	ClusterGraph& clusterGraph = s.superPatternAnalysis.clusterGraph();
	context().msgLogger() << "Connecting distributed cluster graph." << endl;
	Timer mirrorClustersTimer;
	clusterGraph.mirrorAdjacentProcClusters(catalog());
	context().barrier();
	context().msgLogger() << "Mirroring clusters time: " << mirrorClustersTimer << endl;
	Timer connectGraphTimer;
	s.superPatternAnalysis.connectDistributedClusterGraph();
	context().barrier();
	context().msgLogger() << "Connect graph time: " << connectGraphTimer << endl;
	Timer buildFullGraphTimer;
	clusterGraph.buildFullGraph();
	context().barrier();
	context().msgLogger() << "Build full graph time: " << buildFullGraphTimer << endl;
	Timer replicateGraphTimer;
	clusterGraph.replicateGraphOnAllProcs();
	context().barrier();
	context().msgLogger() << "Replicate graph time: " << replicateGraphTimer << endl;
	Timer synchronizeClustersTimer;
	s.superPatternAnalysis.synchronizeClusterAssignments();
	context().barrier();
	context().msgLogger() << "Synchronize cluster assignments time: " << synchronizeClustersTimer << endl;
}

/******************************************************************************
* Decomposes the input structure into individual grains.
******************************************************************************/
void CrystalAnalysisTool::performGrainAnalysis(StructureAnalysisSet& s)
{
	context().msgLogger() << "Decomposing structure into crystal grains (misorientation threshold: " << params().grainMisorientationThresholdAngle
							<< ", fluctuation tolerance: " << params().grainOrientationFluctuationTolerance << ", min. grain size: " << params().minimumGrainAtomCount << ")." << endl;

	s.grainAnalysis.setMisorientationThreshold(params().grainMisorientationThresholdAngle * CAFLOAT_PI / 180.0);
	s.grainAnalysis.setFluctuationTolerance(params().grainOrientationFluctuationTolerance * CAFLOAT_PI / 180.0);
	s.grainAnalysis.setMinGrainAtomCount(params().minimumGrainAtomCount);

	Timer grainTimer;
	AtomInteger numGrains = s.grainAnalysis.findGrains();
	context().msgLogger() << "Found " << numGrains << " grains." << endl;
	context().msgLogger() << "Grain identification time: " << grainTimer << endl;
}

/******************************************************************************
* Generates the triangle mesh for the free surfaces of the solid.
******************************************************************************/
void CrystalAnalysisTool::generateFreeSurfaceMesh(StructureAnalysisSet& s)
{
	context().msgLogger() << "Identifying free surfaces (sphere radius = " << params().freeSurfaceSphereRadius << ")." << endl;

	Timer surfaceTimer;
	TriangleMesh localMesh(context());
	FreeSurfaceIdentification surfaceIdentification(s.structure);
	surfaceIdentification.setAlpha(square(params().freeSurfaceSphereRadius));
	surfaceIdentification.buildSurfaceMesh(localMesh);
	_solidVolume = surfaceIdentification.solidVolume();
	context().barrier();
	context().msgLogger() << "Free surface identification time: " << surfaceTimer << endl;

	// Merge meshes from all processor domains.
	// Only the master processor will obtain the complete mesh.
	context().msgLogger() << "Stitching free surface mesh." << endl;
	localMesh.mergeParallel(_freeSurface);
	context().barrier();

	if(context().isMaster()) {
		context().msgLogger() << "Finalizing free surface mesh." << endl;
		_freeSurface.duplicateSharedVertices();

#ifdef DEBUG_CRYSTAL_ANALYSIS
		context().msgLogger() << "Validating surface mesh." << endl;
		_freeSurface.validate(true);
#endif
	}
}

/******************************************************************************
* Computes a Delaunay tessellation for the given structure.
******************************************************************************/
void CrystalAnalysisTool::generateTessellation()
{
	context().msgLogger() << "Generating Delaunay tessellation." << endl;
	Timer tessellationTimer;
	_tessellation.generateTessellation(true);
	context().barrier();
	context().msgLogger() << "Tessellation time: " << tessellationTimer << endl;

	context().msgLogger() << "Checking consistency of distributed tessellation." << endl;
	if(!_tessellation.checkTessellationConsistency())
		context().raiseErrorOne("Detected invalid periodic tessellation. By increasing the ghost cutoff size you may be able to avoid this error.");
	context().barrier();
}

/******************************************************************************
* Computes the elastic mapping for the given structure.
******************************************************************************/
void CrystalAnalysisTool::computeElasticMapping(StructureAnalysisSet& s, bool onlyLocalTetrahedra, bool synchronizeProcessors)
{
	// Generate elastic mapping from current configuration to stress free configuration.
	context().msgLogger() << "Computing elastic mapping." << endl;
	Timer elasticMappingTimer;
	s.elasticMapping.generate(onlyLocalTetrahedra, params().crystalPathSteps, params().dontReconstructIdealEdgeVectors);
	context().barrier();
	context().msgLogger() << "Elastic mapping time: " << elasticMappingTimer << endl;

	if(synchronizeProcessors) {

		// Make sure all edge are consistent across processors.
		context().msgLogger() << "Synchronizing elastic mapping across processors." << endl;
		Timer syncMappingTimer;
		s.elasticMapping.synchronize();
		context().barrier();
		context().msgLogger() << "Sync mapping time: " << syncMappingTimer << endl;
	}
}

/******************************************************************************
* Builds the interface mesh.
******************************************************************************/
void CrystalAnalysisTool::buildInterfaceMesh()
{
	// Build local part of interface mesh on each processor.
	context().barrier();
	context().msgLogger() << "Building interface mesh." << endl;
	InterfaceMesh localMesh(_input.elasticMapping);
	localMesh.build();

	// Merge meshes across processor domains.
	// Only the mast()er processor will obtain the complete mesh.
	context().barrier();
	context().msgLogger() << "Stitching interface mesh." << endl;
	localMesh.mergeParallel(_interfaceMesh);
	context().barrier();

	if(context().isMaster()) {

		// Finish mesh.
		context().msgLogger() << "Finalizing interface mesh." << endl;
		_interfaceMesh.finalizeMesh();

#ifdef DEBUG_CRYSTAL_ANALYSIS
		context().msgLogger() << "Validating interface mesh." << endl;
		_interfaceMesh.validate();
#endif

		context().msgLogger() << "Number of interface mesh nodes: " << _interfaceMesh.nodes().size() << " (" << (sizeof(InterfaceMesh::Node) * _interfaceMesh.nodes().size() / 1024 / 1024) << " mbyte)" << endl;
		context().msgLogger() << "Number of interface mesh facets: " << _interfaceMesh.facets().size() << " (" << (sizeof(InterfaceMesh::Facet) * _interfaceMesh.facets().size() / 1024 / 1024) << " mbyte)" << endl;
	}
}

/******************************************************************************
* Finds the dislocations.
******************************************************************************/
void CrystalAnalysisTool::identifyDislocations()
{
	if(!context().isMaster())
		return;

	// Identify primary dislocation segments.
	context().msgLogger() << "Tracing dislocation lines (max. circuit size: " << params().maxBurgersCircuitSize << ", ext. circuit size: " << params().maxExtendedBurgersCircuitSize << ")." << endl;
	Timer traceTimer;
	pair<size_t,size_t> results = _tracer.traceDislocationSegments();
	size_t numSegs = results.first;
	size_t numJunctions = results.second;

	context().msgLogger() << "Finishing dislocation lines." << endl;
	_tracer.finishDislocationSegments();
	context().msgLogger() << "Dislocation results: " << numSegs << " segments and " << numJunctions << " junctions." << endl;
}

/******************************************************************************
* Generates the defect surface mesh.
******************************************************************************/
void CrystalAnalysisTool::generateDefectSurface()
{
	if(!context().isMaster())
		return;

	context().msgLogger() << "Generating defect surface mesh." << endl;
	Timer surfaceTimer;

	// Generate defect surface mesh for output.
	_surface.generateFromInterfaceMesh(_interfaceMesh, _tracer);

	context().msgLogger() << "Defect surface time: " << surfaceTimer << endl;

#ifdef DEBUG_CRYSTAL_ANALYSIS
	// Check the mesh.
	_surface.validate(false);
#endif
}

/******************************************************************************
* Smoothes and clips the defect surface mesh.
******************************************************************************/
void CrystalAnalysisTool::prepareDefectSurfaceForVisualization()
{
	if(!context().isMaster())
		return;

	// Make defect surface ready for output.

	context().msgLogger() << "Smoothing defect surface mesh." << endl;
	_surface.smoothMesh(params().surfaceSmoothingLevel, inputConfiguration().structure);

	context().msgLogger() << "Calculating normals of defect surface mesh." << endl;
	_surface.calculateNormals(inputConfiguration().structure);

	context().msgLogger() << "Wrapping defect surface mesh at simulation cell boundaries." << endl;
	if(params().surfaceCapOutputFile.empty() == false)
		_surface.wrapMesh(inputConfiguration().structure, &_surfaceCap);
	else
		_surface.wrapMesh(inputConfiguration().structure);
}

/******************************************************************************
* Smoothes and clips the free surface mesh.
******************************************************************************/
void CrystalAnalysisTool::prepareFreeSurfaceForVisualization()
{
	if(!context().isMaster())
		return;

	// Make free surface ready for output.

	context().msgLogger() << "Smoothing free surface mesh (level = " << params().surfaceSmoothingLevel << ")." << endl;
	_freeSurface.smoothMesh(params().surfaceSmoothingLevel, inputConfiguration().structure);

	CAFloat totalVolume = inputConfiguration().structure.cellVolume();
	context().msgLogger() << "Total cell volume: " << totalVolume << endl;
	context().msgLogger() << "Solid volume: " << _solidVolume << endl;
	context().msgLogger() << "Solid volume fraction: " << (_solidVolume / totalVolume) << endl;
	CAFloat surfaceArea = _freeSurface.computeTotalArea(inputConfiguration().structure);
	context().msgLogger() << "Free surface area: " << surfaceArea << endl;
	context().msgLogger() << "Free surface area / total volume: " << (surfaceArea / totalVolume) << endl;
	context().msgLogger() << "Free surface area / solid volume: " << (surfaceArea / _solidVolume) << endl;

	TriangleMesh* mesh = &_freeSurface;
	if(params().flipFreeSurfaceOrientation) {
		_freeSurface.clone(_flippedFreeSurface, true);
		mesh = &_flippedFreeSurface;
	}

	context().msgLogger() << "Calculating normals of free surface mesh." << endl;
	mesh->calculateNormals(inputConfiguration().structure);

	context().msgLogger() << "Wrapping free surface mesh at simulation cell boundaries." << endl;
	if(params().freeSurfaceCapOutputFile.empty() == false)
		mesh->wrapMesh(inputConfiguration().structure, &_freeSurfaceCap);
	else
		mesh->wrapMesh(inputConfiguration().structure);
}

/******************************************************************************
* Smoothes and clips the dislocation lines.
******************************************************************************/
void CrystalAnalysisTool::prepareDislocationsForVisualization()
{
	if(!context().isMaster())
		return;

	_tracer.network().coarsenDislocationSegments(params().linePointInterval);
	_tracer.network().smoothDislocationSegments(params().lineSmoothingLevel);
	_tracer.network().wrapDislocationSegments();
}

/******************************************************************************
* Outputs the total dislocation line length.
******************************************************************************/
void CrystalAnalysisTool::outputDislocationInformation()
{
	if(!context().isMaster())
		return;

	// Total dislocation line lengths per Burgers vector family.
	vector< vector<CAFloat> > familyLengths(_patternCatalog.superPatterns().size());
	for(int i = 0; i < familyLengths.size(); i++) {
		CALIB_ASSERT(_patternCatalog.superPatterns()[i].index == i+1);
		familyLengths[i].resize(_patternCatalog.superPatterns()[i].burgersVectorFamilies.size());
	}

	CAFloat totalLength = 0;
	CAFloat otherLength = 0;
	BOOST_FOREACH(DislocationSegment* segment, _tracer.network().segments()) {
		CAFloat len = segment->calculateLength();
		totalLength += len;
		const SuperPattern* pattern = segment->burgersVector.cluster()->pattern;
		bool isMember = false;
		if(pattern->isLattice()) {
			for(int j = 0; j < pattern->burgersVectorFamilies.size(); j++) {
				if(pattern->burgersVectorFamilies[j].isMember(segment->burgersVector.localVec())) {
					isMember = true;
					familyLengths[pattern->index-1][j] += len;
					break;
				}
			}
		}
		if(!isMember)
			otherLength += len;
	}

	context().msgLogger() << "Simulation cell volume: " << inputConfiguration().structure.cellVolume() << endl;
	context().msgLogger() << "Total dislocation line length: " << totalLength << endl;
	for(int i = 0; i < familyLengths.size(); i++) {
		const SuperPattern& pattern = _patternCatalog.superPatterns()[i];
		if(find(_superPatterns.begin(), _superPatterns.end(), &pattern) == _superPatterns.end()) continue;
		if(pattern.isLattice() && pattern.burgersVectorFamilies.empty() == false) {
			for(int j = 0; j < pattern.burgersVectorFamilies.size(); j++) {
				context().msgLogger() << "  [" << pattern.name << "] " << pattern.burgersVectorFamilies[j].name << ": " << familyLengths[i][j] << endl;
			}
		}
	}
	context().msgLogger() << "  Other Burgers vectors: " << otherLength << endl;
}

/******************************************************************************
* Computes the total deformation gradient field.
******************************************************************************/
void CrystalAnalysisTool::computeDeformationField()
{
	context().msgLogger() << "Computing total deformation field." << endl;
	Timer totalFieldTimer;

	_deformationField.buildLocalCellList();
	_deformationField.calculateTotalDeformation();

	context().barrier();
	context().msgLogger() << "Total deformation field time: " << totalFieldTimer << endl;
}

/******************************************************************************
* Computes the incremental elastic and plastic deformation fields.
******************************************************************************/
void CrystalAnalysisTool::computeElasticPlasticFields()
{
	context().msgLogger() << "Computing elastic and plastic deformation fields." << endl;
	Timer elasticPlasticFieldTimer;

	_deformationField.calculateElasticPlasticDeformation(inputConfiguration().elasticMapping, deformedConfiguration().elasticMapping);

	context().barrier();
	context().msgLogger() << "Elastic-plastic deformation fields time: " << elasticPlasticFieldTimer << endl;
}

/******************************************************************************
* Outputs the macroscopic elastic and plastic deformation gradients.
******************************************************************************/
void CrystalAnalysisTool::outputMacroscopicDeformationGradients()
{
	Matrix3 totalDeformationGradientLocal(Matrix3::Zero());
	Matrix3 totalPlasticDeformationGradientLocal(Matrix3::Zero());
	CAFloat localDeformationCellVolumeSum = 0.0;
	CAFloat localPlasticCellVolumeSum = 0.0;
	int localNumDeformationCells = 0;
	int localNumPlasticCells = 0;
	vector<Matrix3>::const_iterator F = _deformationField.deformationGradients().begin();
	vector<Matrix3>::const_iterator F_plastic = _deformationField.plasticDeformationGradients().begin();
	vector<DelaunayTessellation::CellHandle>::const_iterator cell = _deformationField.localCells().begin();
	for(; F != _deformationField.deformationGradients().end(); ++F, ++F_plastic, ++cell) {
		CAFloat volume = _deformationField.tessellation().cellVolume(*cell);

		if(volume > CAFLOAT_EPSILON) {	// Skip degenerate sliver elements.

			totalDeformationGradientLocal += volume * (*F);
			localDeformationCellVolumeSum += volume;
			localNumDeformationCells++;

			if(params().computeElasticPlasticFields && *F_plastic != Matrix3::Zero()) {
				totalPlasticDeformationGradientLocal += volume * (*F_plastic);
				localPlasticCellVolumeSum += volume;
				localNumPlasticCells++;
			}
		}
	}

	AtomInteger totalNumDeformationCells = context().reduce_sum((AtomInteger)localNumDeformationCells);
	AtomInteger totalNumPlasticCells = context().reduce_sum((AtomInteger)localNumPlasticCells);
	CAFloat totalDeformationCellVolumeSum = context().reduce_sum(localDeformationCellVolumeSum);
	CAFloat totalPlasticCellVolumeSum = context().reduce_sum(localPlasticCellVolumeSum);
	if(context().isMaster()) {
		CAFloat simulationCellVolume = inputConfiguration().structure.cellVolume();
		context().msgLogger() << "Cells with total deformation gradient: " << totalNumDeformationCells << " (volume: " << totalDeformationCellVolumeSum << ", volume fraction: " << (totalDeformationCellVolumeSum / simulationCellVolume) << ")" << endl;
		if(params().computeElasticPlasticFields) {
			context().msgLogger() << "Cells with plastic deformation gradient: " << totalNumPlasticCells << " (volume: " << totalPlasticCellVolumeSum << ", volume fraction: " << (totalPlasticCellVolumeSum / totalDeformationCellVolumeSum) << ")" << endl;
			context().msgLogger() << "Cells without plastic deformation gradient: " << (totalNumDeformationCells - totalNumPlasticCells) << " (volume: " << (totalDeformationCellVolumeSum-totalPlasticCellVolumeSum) << ", volume fraction: " << (1.0 - totalPlasticCellVolumeSum / totalDeformationCellVolumeSum) << ")" << endl;
		}
	}

	Matrix3 totalDeformationGradient = context().reduce_sum(totalDeformationGradientLocal);
	Matrix3 totalPlasticDeformationGradient = context().reduce_sum(totalPlasticDeformationGradientLocal);
	if(context().isMaster()) {
		if(totalDeformationCellVolumeSum != 0) totalDeformationGradient /= totalDeformationCellVolumeSum;
		if(totalPlasticCellVolumeSum != 0) totalPlasticDeformationGradient /= totalPlasticCellVolumeSum;

		Matrix3 cellDeformationGradient =
				(deformedConfiguration().structure.simulationCellMatrix() *
				inputConfiguration().structure.reciprocalSimulationCellMatrix()).linear();

		context().msgLogger() << "Simulation cell deformation: (Det=" << cellDeformationGradient.determinant() << ")" << endl << roundZeroElements(cellDeformationGradient, 1e-12);
		context().msgLogger() << "Total deformation: (Det=" << totalDeformationGradient.determinant() << ")" << endl << roundZeroElements(totalDeformationGradient, 1e-12);
		if(params().computeElasticPlasticFields) {
			context().msgLogger() << "Plastic deformation: (Det=" << totalPlasticDeformationGradient.determinant() << ")" << endl << roundZeroElements(totalPlasticDeformationGradient, 1e-12);
		}
	}
}

/******************************************************************************
* Computes the elastic field.
******************************************************************************/
void CrystalAnalysisTool::computeElasticField()
{
	context().msgLogger() << "Computing elastic deformation gradient field." << endl;
	Timer fieldTimer;

	_elasticField.buildLocalCellList();
	_elasticField.calculateElasticField(inputConfiguration().elasticMapping);
	context().barrier();
	context().msgLogger() << "Elastic field time: " << fieldTimer << endl;
}

