///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_TOOL_H
#define __CRYSTAL_ANALYSIS_TOOL_H

#include <calib/CALib.h>
#include <calib/context/CAContext.h>
#include <calib/atomic_structure/neighbor_list/NeighborList.h>
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/input/atoms/AtomicStructureParser.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/pattern/coordinationpattern/CoordinationPatternAnalysis.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/tessellation/DelaunayTessellation.h>
#include <calib/tessellation/ElasticMapping.h>
#include <calib/dislocations/InterfaceMesh.h>
#include <calib/dislocations/DislocationTracer.h>
#include <calib/trianglemesh/TriangleMesh.h>
#include <calib/deformation/DeformationField.h>
#include <calib/output/field/DeformationFieldWriter.h>
#include <calib/deformation/ElasticField.h>
#include <calib/output/field/ElasticFieldWriter.h>
#include <calib/output/atoms/AtomicStructureWriter.h>
#include <calib/grains/GrainIdentification.h>

using namespace CALib;
using namespace std;

class CrystalAnalysisTool
{
public:

	/// Stores all program parameters that can be changed by the user.
	struct Parameters
	{
		/// Default constructor.
		Parameters();

		/// Outputs all parameter descriptions and prints usage information to the console.
		void printHelp(ostream& stream);

		/// Parses command line parameters.
		bool parseCommandLine(int argc, char* argv[], ostream& err);

		string inputFile;
		string patternCatalogFile;
		string crystalAnalysisOutputFile;
		string atomsOutputFile;
		string cellOutputFile;
		string clustersOutputFile;
		string meshOutputFile;
		string dislocationsOutputFile;
		string surfaceOutputFile;
		string surfaceCapOutputFile;
		string deformedConfigurationFile;
		string deformationFieldOutputFile;
		string elasticFieldOutputFile;
		string structureCountsOutputFile;
		string junctionsOutputFile;
		string grainsOutputFile;
		string freeSurfaceOutputFile;
		string freeSurfaceCapOutputFile;
		string freeSurfaceRawOutputFile;

		string patternList;
		int maxBurgersCircuitSize;
		int maxExtendedBurgersCircuitSize;
		int lineSmoothingLevel;
		CAFloat linePointInterval;
		int surfaceSmoothingLevel;
		CAFloat ghostCutoff;
		CAFloat helperPointDistance;
		CAFloat grainMisorientationThresholdAngle;
		CAFloat grainOrientationFluctuationTolerance;
		CAFloat freeSurfaceSphereRadius;
		bool flipFreeSurfaceOrientation;
		int minimumGrainAtomCount;
		bool allprocOutput;
		string atomsOutputFieldsString;
		vector<AtomicStructureWriter::DataField> atomsOutputFields;
		bitset<DeformationFieldWriter::NUM_COMPONENTS> deformationFieldOutputComponents;
		string deformationFieldOutputComponentsString;
		bitset<ElasticFieldWriter::NUM_COMPONENTS> elasticFieldOutputComponents;
		string elasticFieldOutputComponentsString;
		bool eliminateHomogeneousDeformation;
		int crystalPathSteps;
		bool dontReconstructIdealEdgeVectors;

		string inputCoordinateColumnX;
		string inputCoordinateColumnY;
		string inputCoordinateColumnZ;

		AtomsPreprocessingParameters preprocessing;
		CAFloat neighborCutoff;

		bool deformationAnalysisMode;
		bool identifyAtomicStructures;
		bool identifyDislocations;
		bool generateTessellation;
		bool generateElasticMapping;
		bool generateInterfaceMesh;
		bool generateDefectSurface;
		bool computeDeformationField;
		bool computeElasticPlasticFields;
		bool computeElasticField;
		bool identifyGrains;
		bool generateFreeSurface;
	};


	/// Stores the necessary objects for the structure analysis of an
	/// input dataset.
	struct StructureAnalysisSet {

		/// Constructor.
		StructureAnalysisSet(CAContext& context, PatternCatalog& catalog, const DelaunayTessellation& tessellation) :
			structure(context, AtomicStructure::GHOST_ATOMS), neighborList(structure),
			coordinationPatternAnalysis(structure, neighborList, catalog),
			superPatternAnalysis(coordinationPatternAnalysis),
			elasticMapping(tessellation, superPatternAnalysis),
			grainAnalysis(superPatternAnalysis) {}

		/// The input structure.
		AtomicStructure structure;

		/// The neighbor list for the input structure.
		NeighborList neighborList;

		/// Analysis object that performs coordination pattern matching.
		CoordinationPatternAnalysis coordinationPatternAnalysis;

		/// Analysis object that performs super pattern matching.
		SuperPatternAnalysis superPatternAnalysis;

		/// The elastic mapping for this configuration.
		ElasticMapping elasticMapping;

		/// Analysis object that decomposes the polycrystal into grains.
		GrainIdentification grainAnalysis;
	};

public:

	/// Constructor.
	CrystalAnalysisTool();

	/// Returns the context object of the CA library.
	CAContext& context() { return _context; }

	/// Returns the context object of the CA library.
	CAContext& parallel() { return _context; }

	/// Returns a reference to the user parameters set.
	Parameters& params() { return _params; }

	/// Returns a reference to the catalog of structure patterns.
	PatternCatalog& catalog() { return _patternCatalog; }

	/// Returns the input dataset.
	StructureAnalysisSet& inputConfiguration() { return _input; }

	/// Returns the deformed configuration used for deformation analysis.
	StructureAnalysisSet& deformedConfiguration() { return _deformedConfiguration; }

	/// Returns the deformation field computed from an initial and a deformed configuration of the atomistic system.
	DeformationField& deformationField() { return _deformationField; }

	/// Initializes MPI etc.
	bool initialize(int argc, char* argv[]);

	/// Checks if all command line parameters specified by the user are valid.
	void validateParameters();

	/// Loads the pattern catalog and puts together the list
	/// of coordination patterns and super patterns to search for.
	void loadPatterns();

	/// Loads an input file and builds neighbor lists.
	void loadInputStructure(StructureAnalysisSet& s, const string& filename, bool createHelperAtoms);

	/// Identifies local atomic arrangements.
	void performStructureAnalysis(StructureAnalysisSet& s);

	/// Decomposes the input structure into individual grains.
	void performGrainAnalysis(StructureAnalysisSet& s);

	/// Generates the triangle mesh for the free surfaces of the solid.
	void generateFreeSurfaceMesh(StructureAnalysisSet& s);

	/// Computes a Delaunay tessellation for the given structure.
	void generateTessellation();

	/// Computes the elastic mapping for the given structure.
	void computeElasticMapping(StructureAnalysisSet& s, bool onlyLocalTetrahedra, bool synchronizeProcessors);

	/// Builds the interface mesh.
	void buildInterfaceMesh();

	/// Finds the dislocations.
	void identifyDislocations();

	/// Generates the defect surface mesh.
	void generateDefectSurface();

	/// Smoothes and clips the defect surface mesh.
	void prepareDefectSurfaceForVisualization();

	/// Smoothes and clips the dislocation lines.
	void prepareDislocationsForVisualization();

	/// Smoothes and clips the free surface mesh.
	void prepareFreeSurfaceForVisualization();

	/// Computes the total deformation gradient field.
	void computeDeformationField();

	/// Computes the incremental elastic and plastic deformation fields.
	void computeElasticPlasticFields();

	/// Computes the elastic gradient tensor field.
	void computeElasticField();

	/// Writes the shape of the simulation cell for given structure to an output file.
	void writeCellToFile(StructureAnalysisSet& s, const string& filename);

	/// Writes the processed atoms to an output file.
	void writeAtomsToFile(StructureAnalysisSet& s, const string& filename);

	/// Writes the processed atoms of the local processor to an output file.
	void writeLocalAtomsToFile(StructureAnalysisSet& s, const string& filename);

	/// Writes the clusters to an output file.
	void writeClustersToFile(StructureAnalysisSet& s, const string& filename);

	/// Writes the interface mesh to an output file.
	void writeMeshToFile(const string& filename);

	/// Writes the defect surface to an output file.
	void writeDefectSurfaceToFile(const string& filename);

	/// Writes the defect surface cap to an output file.
	void writeDefectSurfaceCapToFile(const string& filename);

	/// Writes the free surface mesh to an output file.
	void writeFreeSurfaceToFile(const string& filename);

	/// Writes the free surface mesh cap to an output file.
	void writeFreeSurfaceCapToFile(const string& filename);

	/// Writes the original free surface mesh to an output file.
	void writeRawFreeSurfaceToFile(const string& filename);

	/// Writes the dislocation lines to an output file.
	void writeDislocationsToFile(const string& filename);

	/// Outputs the total dislocation line length.
	void outputDislocationInformation();

	/// Writes the extracted dislocation lines and clusters to an output file.
	void writeAnalysisResultsToFile(const string& filename);

	/// Writes the computed deformation field to an output file.
	void writeDeformationFieldToFile(const string& filename);

	/// Writes the computed elastic field to an output file.
	void writeElasticFieldToFile(const string& filename);

	/// Outputs the macroscopic elastic and plastic deformation gradients.
	void outputMacroscopicDeformationGradients();

	/// Outputs the number of atoms that match to each of the structure patterns to a text file.
	void outputStructureMatchCounts(const string& filename);

	/// Outputs the list of dislocation junctions to a text file.
	void outputJunctions(const string& filename);

	/// Outputs the list of grains to a text file.
	void outputGrains(const string& filename);

private:

	/// The context object of the CA library.
	CAContext _context;

	/// The user parameters.
	Parameters _params;

	/// The catalog of structure patterns.
	PatternCatalog _patternCatalog;

	/// The space-filling tessellation.
	DelaunayTessellation _tessellation;

	/// The input structure.
	StructureAnalysisSet _input;

	/// The deformed input structure for deformation analysis.
	StructureAnalysisSet _deformedConfiguration;

	/// The stitched interface mesh (on master processor).
	InterfaceMesh _interfaceMesh;

	/// Dislocation analysis object.
	DislocationTracer _tracer;

	/// The defect surface mesh.
	TriangleMesh _surface;

	/// The defect surface cap.
	TriangleMesh _surfaceCap;

	/// The triangle mesh that represents the free surface of the solid.
	TriangleMesh _freeSurface;

	/// The free surface mesh with flipped orientation.
	TriangleMesh _flippedFreeSurface;

	/// The cap of the triangle mesh that represents the free surface of the solid.
	TriangleMesh _freeSurfaceCap;

	/// The deformation field computed from an initial and a deformed configuration of the atomistic system.
	DeformationField _deformationField;

	/// The elastic field computed from the current configuration of the atomistic system.
	ElasticField _elasticField;

	// Maximum number of neighbors per atom required for coordination pattern analysis.
	int _maxNeighbors;

	/// List of coordination patterns from the catalog that have been selected by the user.
	vector<CoordinationPattern const*> _coordinationPatterns;

	/// List of super patterns from the catalog that have been selected by the user.
	vector<SuperPattern const*> _superPatterns;

	/// Stores the number of atoms that match to each of the structure patterns.
	vector<SuperPatternAnalysis::AnalysisResultsRecord> _patternAnlysisResults;

	/// The volume of the solid region as determined by the free-surface identification algorithm.
	CAFloat _solidVolume;
};

#endif // __CRYSTAL_ANALYSIS_TOOL_H
