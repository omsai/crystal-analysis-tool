///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CrystalAnalysisTool.h"
#include <calib/output/atoms/AtomicStructureWriter.h>
#include <calib/output/interface_mesh/InterfaceMeshWriter.h>
#include <calib/output/triangle_mesh/TriangleMeshWriter.h>
#include <calib/output/dislocations/DislocationsWriter.h>
#include <calib/output/dislocations/AnalysisResultsWriter.h>
#include <calib/output/cell/SimulationCellWriter.h>
#include <calib/output/cluster_graph/ClusterGraphWriter.h>
#include <calib/util/NULLStream.h>

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#ifdef CALIB_USE_ZLIB
	#include <boost/iostreams/filter/gzip.hpp>
#endif
#include <boost/iostreams/filtering_stream.hpp>

/**
 * Helper stream class that can perform on-the-fly data compression.
 */
class OutputStream : public boost::iostreams::filtering_stream<boost::iostreams::output>
{
public:

	/// Constructor that opens the given output file.
	OutputStream(const string& filename, CAContext& context, bool forceCompression = false, bool allProcessors = false) {
		if(context.isMaster() || allProcessors) {
			// Compress output data on the fly if filename ends with ".gz".
			if(forceCompression || filename.compare(max((int)filename.length() - 3, 0), 3, ".gz") == 0) {
#ifdef CALIB_USE_ZLIB
				_fileStream.open(filename.c_str(), ios_base::out | ios_base::binary);
				this->push(boost::iostreams::gzip_compressor());
#else
				context.raiseErrorOne("Cannot write compressed file '%s'. Program has not been built with zlib compression support.", filename.c_str());
#endif
			}
			else {
				_fileStream.open(filename.c_str(), ios_base::out);
			}
			if(!_fileStream.is_open())
				context.raiseErrorOne("Failed to open output file for writing: %s", filename.c_str());
			this->push(_fileStream);
		}
		else this->push(_nullStream);
	}

	/// Destructor. Closes the file stream and removes it from the filter chain.
	/// It's crucial to do this here, because the the file stream will already be destroyed
	/// at the time the base class destructor is called.
	~OutputStream() { reset(); }

private:
	std::ofstream _fileStream;
	std::ostringstream _nullStream;
};

/******************************************************************************
* Writes the processed atoms to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeAtomsToFile(StructureAnalysisSet& s, const string& filename)
{
	context().msgLogger() << "Writing atoms to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	AtomicStructureWriter(context()).writeLAMMPSFile(stream, params().atomsOutputFields,
			s.structure, &s.superPatternAnalysis, &s.grainAnalysis,
			params().deformationAnalysisMode ? &deformedConfiguration().structure : NULL,
			params().neighborCutoff);
}

/******************************************************************************
* Writes the processed atoms of the local processor to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeLocalAtomsToFile(StructureAnalysisSet& s, const string& filename)
{
	context().msgLogger() << "Writing local atoms to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context(), false, true);
	AtomicStructureWriter(context()).writeLocalLAMMPSFile(stream, s.structure, &s.superPatternAnalysis);
}

/******************************************************************************
* Writes the shape of the simulation cell for given structure to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeCellToFile(StructureAnalysisSet& s, const string& filename)
{
	context().msgLogger() << "Writing simulation cell to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	SimulationCellWriter(context()).writeVTKFile(stream, s.structure);
}

/******************************************************************************
* Writes the clusters to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeClustersToFile(StructureAnalysisSet& s, const string& filename)
{
	context().msgLogger() << "Writing clusters to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	ClusterGraphWriter(context()).writeVTKFile(stream, s.superPatternAnalysis.clusterGraph());
}

/******************************************************************************
* Writes interface mesh to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeMeshToFile(const string& filename)
{
	context().msgLogger() << "Writing interface mesh to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	InterfaceMeshWriter(context()).writeVTKFile(stream, _interfaceMesh);
}

/******************************************************************************
* Writes the defect surface to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeDefectSurfaceToFile(const string& filename)
{
	context().msgLogger() << "Writing defect surface to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	TriangleMeshWriter(context()).writeVTKFile(stream, _surface);
}

/******************************************************************************
* Writes the defect surface cap to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeDefectSurfaceCapToFile(const string& filename)
{
	context().msgLogger() << "Writing defect surface cap to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	TriangleMeshWriter(context()).writeVTKFile(stream, _surfaceCap);
}

/******************************************************************************
* Writes the free surface mesh to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeFreeSurfaceToFile(const string& filename)
{
	context().msgLogger() << "Writing free surface to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	TriangleMeshWriter(context()).writeVTKFile(stream, params().flipFreeSurfaceOrientation ? _flippedFreeSurface : _freeSurface);
}

/******************************************************************************
* Writes the free surface mesh cap to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeFreeSurfaceCapToFile(const string& filename)
{
	context().msgLogger() << "Writing free surface cap to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	TriangleMeshWriter(context()).writeVTKFile(stream, _freeSurfaceCap);
}

/******************************************************************************
* Writes the original free surface mesh to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeRawFreeSurfaceToFile(const string& filename)
{
	context().raiseErrorAll("Writing a raw surface file is no longer support by this program version.");
}

/******************************************************************************
* Writes the dislocation lines to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeDislocationsToFile(const string& filename)
{
	context().msgLogger() << "Writing dislocations to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	DislocationsWriter(context()).writeVTKFile(stream, _tracer.network());
}

/******************************************************************************
* Writes the computed deformation field to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeDeformationFieldToFile(const string& filename)
{
	context().msgLogger() << "Writing deformation field to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	DeformationFieldWriter(_tessellation).writeVTKFile(stream, _deformationField, params().deformationFieldOutputComponents);
}

/******************************************************************************
* Writes the computed elastic field to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeElasticFieldToFile(const string& filename)
{
	context().msgLogger() << "Writing elastic field to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());
	ElasticFieldWriter(_tessellation).writeVTKFile(stream, _elasticField, params().elasticFieldOutputComponents);
}

/******************************************************************************
* Outputs the number of atoms that match to each of the structure patterns to a text file.
******************************************************************************/
void CrystalAnalysisTool::outputStructureMatchCounts(const string& filename)
{
	if(!parallel().isMaster()) return;

	context().msgLogger() << "Writing pattern match counts to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());

	stream << "# PatternId AtomCount UnitCount PatternName" << endl;
	BOOST_FOREACH(const SuperPatternAnalysis::AnalysisResultsRecord& record, _patternAnlysisResults) {
		stream << record.pattern->index << "\t";
		stream << record.numMatchingAtoms << "\t";
		stream << record.numMatchingUnits << "\t";
		stream << record.pattern->name << endl;
	}
}

/******************************************************************************
* Outputs the list of dislocation junctions to a text file.
******************************************************************************/
void CrystalAnalysisTool::outputJunctions(const string& filename)
{
	if(!parallel().isMaster()) return;

	context().msgLogger() << "Writing dislocation junctions to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());

	DislocationsWriter dislocWriter(context());
	set<const DislocationNode*> visitedNodes;
	stream << "# PosX PosY PosZ ArmCount SegmentId1 SegmentType1 SegmentId2 SegmentType2 ..." << endl;
	BOOST_FOREACH(DislocationSegment* segment, _tracer.network().segments()) {
		CALIB_ASSERT(segment->replacedWith == NULL);
		for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
			DislocationNode* node = segment->nodes[nodeIndex];
			int armCount = 0;
			DislocationNode* armNode = node;
			do {
				if(visitedNodes.insert(armNode).second == false) {
					armCount = 0;
					break;
				}
				armCount++;
				armNode = armNode->junctionRing;
			}
			while(armNode != node);
			if(armCount <= 2) continue;
			stream << node->position().x() << " " << node->position().y() << " " << node->position().z();
			stream << " " << armCount;
			armNode = node;
			do {
				stream << " " << armNode->segment->id;
				stream << " " << dislocWriter.burgersVectorFamilyId(armNode->segment->burgersVector);
				armNode = armNode->junctionRing;
			}
			while(armNode != node);
			stream << endl;
		}
	}
}

/******************************************************************************
* Outputs the list of grains to a text file.
******************************************************************************/
void CrystalAnalysisTool::outputGrains(const string& filename)
{
	if(!parallel().isMaster()) return;

	context().msgLogger() << "Writing grain list to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());

	stream << "# Number of grains:" << endl;
	stream << "# " << inputConfiguration().grainAnalysis.grainCount() << endl;
	stream << "# grain_id atom_count lattice_atom_count lattice_type" << endl;

	BOOST_FOREACH(const GrainIdentification::Grain& grain, inputConfiguration().grainAnalysis.grains()) {
		stream << grain.id << '\t' << grain.atomCount << '\t' << grain.latticeAtomCount
				<< '\t' << grain.cluster->pattern->name << endl;
	}
}

/******************************************************************************
* Writes the extracted dislocation lines and clusters to an output file.
******************************************************************************/
void CrystalAnalysisTool::writeAnalysisResultsToFile(const string& filename)
{
	context().msgLogger() << "Writing analysis results to output file '" << filename << "'." << endl;
	OutputStream stream(filename, context());

	AnalysisResultsWriter writer(context());
	writer.writeAnalysisFile(stream, _tracer.network(), _superPatterns, _surface, filename, params().atomsOutputFile);
}


