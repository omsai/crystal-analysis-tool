///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "fix_crystalanalysis.h"
#include <lammps/LAMMPSAtomicStructure.h>

#include <calib/pattern/superpattern/SuperPattern.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/pattern/coordinationpattern/CoordinationPattern.h>
#include <calib/pattern/coordinationpattern/CoordinationPatternAnalysis.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/trianglemesh/TriangleMesh.h>
#include <calib/output/atoms/AtomicStructureWriter.h>
#include <calib/output/interface_mesh/InterfaceMeshWriter.h>
#include <calib/output/triangle_mesh/TriangleMeshWriter.h>
#include <calib/output/cell/SimulationCellWriter.h>
#include <calib/output/dislocations/DislocationsWriter.h>
#include <calib/output/dislocations/AnalysisResultsWriter.h>
#include <calib/input/patterns/PatternCatalogReader.h>
#include <calib/tessellation/DelaunayTessellation.h>
#include <calib/tessellation/ElasticMapping.h>
#include <calib/dislocations/InterfaceMesh.h>
#include <calib/dislocations/DislocationTracer.h>
#include <calib/util/Timer.h>

using namespace std;

/******************************************************************************
* Constructor.
******************************************************************************/
CrystalAnalysisFix::CrystalAnalysisFix(LAMMPS *lmp, int narg, char **arg) :
	Fix(lmp, narg, arg), averageFix(NULL), patternCatalog(NULL), maxPatternNeighbors(0),
	analysisOutputStream(NULL)
{
	force_reneighbor = 1;
	next_reneighbor = -1;

	msgLoggerStream.attach(screen);
	setMsgLogger(msgLoggerStream);

	// Install assertion error handler that passes the message on to LAMMPS.
	fatalAssertionErrorHandler = handleFatalAssertionError;

	// Setup MPI and parallelization.
	this->_mpiComm = lmp->world;
	this->_processorMe = comm->me;
	this->_processorCount = comm->nprocs;
	this->_parallelMode = true;
	this->_processorGrid[0] = comm->procgrid[0];
	this->_processorGrid[1] = comm->procgrid[1];
	this->_processorGrid[2] = comm->procgrid[2];
	this->_processorLocation[0] = comm->myloc[0];
	this->_processorLocation[1] = comm->myloc[1];
	this->_processorLocation[2] = comm->myloc[2];
	this->_processorNeighbors[0][0] = comm->procneigh[0][0];
	this->_processorNeighbors[0][1] = comm->procneigh[0][1];
	this->_processorNeighbors[1][0] = comm->procneigh[1][0];
	this->_processorNeighbors[1][1] = comm->procneigh[1][1];
	this->_processorNeighbors[2][0] = comm->procneigh[2][0];
	this->_processorNeighbors[2][1] = comm->procneigh[2][1];

	// Initialize parameters with default values.
	maxBurgersCircuitSize = CA_DEFAULT_MAX_BURGERS_CIRCUIT_SIZE;
	maxExtendedBurgersCircuitSize = CA_DEFAULT_MAX_EXTENDED_BURGERS_CIRCUIT_SIZE;
	lineSmoothingLevel = CA_DEFAULT_LINE_SMOOTHING_LEVEL;
	linePointInterval = CA_DEFAULT_LINE_POINT_INTERVAL;
	surfaceSmoothingLevel = CA_DEFAULT_SURFACE_SMOOTHING_LEVEL;
	neighborCutoff = 0;
	ghostCutoff = 0;

	// Parse fix command parameters.
	int iarg = 3;
	if(narg < 7) error->all(FLERR,"Not enough parameters for fix crystalanalysis.");

	// This parameter specifies the time intervals when to analyze the structure.
	this->nevery = atoi(arg[iarg++]);
	if(this->nevery < 1) error->all(FLERR,"Invalid timestep parameter for fix crystalanalysis.");

	// Parse neighbor list cutoff parameter.
	neighborCutoff = atof(arg[iarg++]);
	if(neighborCutoff <= 0.0) error->all(FLERR,"Invalid cutoff radius specified for fix crystalanalysis.");

	try {
		// Load pattern catalog.
		string patternCatalogFilename = arg[iarg];
		patternCatalog = new PatternCatalog(*this);
		ifstream catalog_stream;
		if(isMaster()) {
			catalog_stream.open(arg[iarg]);
			if(!catalog_stream.is_open())
				raiseErrorOne("Failed to open pattern catalog file for reading. Filename was '%s'.", arg[iarg]);
			PatternCatalogReader(*this).read(catalog_stream, *patternCatalog);
		}
		// Broadcast the catalog's data from the master to other processors.
		patternCatalog->broadcast();

		iarg++;

		// Parse list of patterns to search for.
		istringstream ss(arg[iarg]);
		string patternname;
		set<SuperPattern const*> superPatternsToSearchForSet;
		set<CoordinationPattern const*> coordinationPatternsToSearchForSet;
		while(getline(ss, patternname, ',')) {
			const SuperPattern* pattern = patternCatalog->superPatternByName(patternname);
			if(pattern == NULL)
				raiseErrorAll("Pattern with name '%s' not found in database file.", patternname.c_str());
			superPatternsToSearchForSet.insert(pattern);
			for(vector<SuperPatternNode>::const_iterator node = pattern->nodes.begin(); node != pattern->nodes.end(); ++node) {
				CALIB_ASSERT(node->coordinationPattern != NULL);
				coordinationPatternsToSearchForSet.insert(node->coordinationPattern);
			}
		}
		for(boost::ptr_vector<SuperPattern>::const_iterator spi = patternCatalog->superPatterns().begin(); spi != patternCatalog->superPatterns().end(); ++spi) {
			if(superPatternsToSearchForSet.find(&*spi) != superPatternsToSearchForSet.end())
				superPatternsToSearchFor.push_back(&*spi);
		}
		for(boost::ptr_vector<CoordinationPattern>::const_iterator cpi = patternCatalog->coordinationPatterns().begin(); cpi != patternCatalog->coordinationPatterns().end(); ++cpi) {
			if(coordinationPatternsToSearchForSet.find(&*cpi) != coordinationPatternsToSearchForSet.end()) {
				coordinationPatternsToSearchFor.push_back(&*cpi);

				// Determine maximum number of neighbors per atom required for the pattern analysis.
				maxPatternNeighbors = max(maxPatternNeighbors, cpi->numNeighbors());
			}
		}
		iarg++;

		// Parse extra/optional parameters
		while(iarg < narg) {
			if(strcmp(arg[iarg], "log") == 0) {
				// Parse log filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'log' in fix crystalanalysis.");
				logFilename = arg[iarg+1];
				iarg += 2;
				initOutputStream(logFilename, logStream);
				setMsgLogger(logStream);
			}
			else if(strcmp(arg[iarg], "ave") == 0) {
				// Parse ave/atom fix name.
				if(iarg + 2 > narg) error->all(FLERR,"Missing fix name after keyword 'ave' in fix crystalanalysis.");
				int ifix = modify->find_fix(arg[iarg+1]);
				if(ifix < 0) error->all(FLERR,"ave/atom fix with the given ID does not exist.");
				averageFix = dynamic_cast<FixAveAtom*>(modify->fix[ifix]);
				if(averageFix == NULL) error->all(FLERR,"Fix referenced by 'ave' keyword is not an ave/atom fix.");
				if(averageFix->peratom_flag == 0 || averageFix->size_peratom_cols != 3)
					error->all(FLERR,"Fix ave/atom referenced by the crystalanalysis fix does not calculate three per-atom values.");
				if((this->nevery % averageFix->peratom_freq) != 0)
					error->all(FLERR,"Fix ave/atom referenced by the crystalanalysis fix is not computed at compatible times.");
				if(this->igroup != averageFix->igroup)
					error->all(FLERR,"Fix ave/atom referenced by the crystalanalysis fix is not using the same atom group.");
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "ghost") == 0) {
				if(iarg + 2 > narg) error->all(FLERR,"Missing value after keyword 'cutoff' in fix crystalanalysis.");
				ghostCutoff = atof(arg[iarg + 1]);
				if(ghostCutoff <= 0.0) error->all(FLERR,"Invalid ghost cutoff specified for fix crystalanalysis.");
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "cell") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'cell' in fix crystalanalysis.");
				cellOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "atoms") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'atoms' in fix crystalanalysis.");
				atomsOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "mesh") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'mesh' in fix crystalanalysis.");
				meshOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "defectsurface") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'defectsurface' in fix crystalanalysis.");
				surfaceOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "defectsurfacecap") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'defectsurfacecap' in fix crystalanalysis.");
				surfaceCapOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "dislocations") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'dislocations' in fix crystalanalysis.");
				dislocationsOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "output") == 0) {
				// Parse output filename
				if(iarg + 2 > narg) error->all(FLERR, "Missing filename parameter after keyword 'output' in fix crystalanalysis.");
				analysisOutputFile = arg[iarg+1];
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "maxcircuitsize") == 0) {
				if(iarg + 2 > narg) error->all(FLERR,"Missing value after keyword 'maxcircuitsize' in fix crystalanalysis.");
				maxBurgersCircuitSize = atoi(arg[iarg + 1]);
				if(maxBurgersCircuitSize < 3) error->all(FLERR,"Invalid maxcircuitsize specified for fix crystalanalysis.");
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "extcircuitsize") == 0) {
				if(iarg + 2 > narg) error->all(FLERR,"Missing value after keyword 'extcircuitsize' in fix crystalanalysis.");
				maxExtendedBurgersCircuitSize = atoi(arg[iarg + 1]);
				if(maxExtendedBurgersCircuitSize < 3) error->all(FLERR,"Invalid extcircuitsize specified for fix crystalanalysis.");
				iarg += 2;
			}
			else if(strcmp(arg[iarg], "pointinterval") == 0) {
				if(iarg + 2 > narg) error->all(FLERR,"Missing value after keyword 'pointinterval' in fix crystalanalysis.");
				linePointInterval = atof(arg[iarg + 1]);
				if(linePointInterval <= 0) error->all(FLERR,"Invalid pointinterval parameter specified for fix crystalanalysis.");
				iarg += 2;
			}
			else error->all(FLERR,"Unknown optional parameter in fix crystalanalysis command.");
		}

		// By default, create ghost atoms up to sqrt(6) times the neighbor cutoff radius.
		// User can override this with the -ghost switch.
		if(neighborCutoff > 0.0 && ghostCutoff == 0)
			ghostCutoff = neighborCutoff * sqrt(6.0) * 1.2;
		if(ghostCutoff <= 0.0)
			error->all(FLERR,"Invalid ghost cutoff. Please use the ghost option to specify a valid overlap distance.");
		if(ghostCutoff < neighborCutoff * 2.0)
			error->all(FLERR,"Invalid ghost cutoff. Ghost cutoff must not be be smaller than twice the neighbor cutoff.");

		msgLogger() << "Pattern catalog file: " << patternCatalogFilename << endl;
		msgLogger() << "Searching for the following patterns:";
		for(vector<SuperPattern const*>::const_iterator pattern = superPatternsToSearchFor.begin(); pattern != superPatternsToSearchFor.end(); ++pattern)
			msgLogger() << " " << (*pattern)->name;
		msgLogger() << endl;

		// Open output files.
		initOutputStream(atomsOutputFile, atomsOutputStream);
		initOutputStream(cellOutputFile, cellOutputStream);
	}
	catch(const exception& ex) {
		error->one(FLERR,ex.what());
	}
}

/******************************************************************************
* Destructor.
******************************************************************************/
CrystalAnalysisFix::~CrystalAnalysisFix()
{
	delete patternCatalog;
}

/*********************************************************************
 * Prepares an output file for writing during the initialization phase.
 *********************************************************************/
void CrystalAnalysisFix::initOutputStream(const string& filename, ofstream& stream)
{
	if(filename.empty()) return;
	size_t pos = filename.find('*');
	if(pos == string::npos) {

		pos = filename.find('%');
		string actualFilename;
		if(pos == string::npos) {
			if(comm->me != 0) return;
			actualFilename = filename;
		}
		else {
			actualFilename.append(filename, 0, pos);
			stringstream ss;
			ss << comm->me;
			actualFilename.append(ss.str());
			actualFilename.append(filename, pos+1, filename.length());
		}

		stream.open(actualFilename.c_str(), ios::out);
		if(!stream.is_open())
			raiseErrorOne("Fix crystalanalysis could not open output file '%s' for writing.", filename.c_str());
	}
}

/*********************************************************************
 * Replaces placeholder characters in a filename.
 *********************************************************************/
std::string CrystalAnalysisFix::resolveFilename(const std::string& filename)
{
	// If one file per processor, replace '%' with processor ID.
	size_t pos = filename.find('%');
	string actualFilename1;
	if(pos == string::npos) {
		if(comm->me != 0) return std::string();
		actualFilename1 = filename;
	}
	else {
		actualFilename1.append(filename, 0, pos);
		stringstream ss;
		ss << comm->me;
		actualFilename1.append(ss.str());
		actualFilename1.append(filename, pos+1, filename.length());
	}

	// If one file per timestep, replace '*' with current timestep
	string actualFilename2;
	pos = actualFilename1.find('*');
	if(pos != string::npos) {
		actualFilename2.append(actualFilename1, 0, pos);
		stringstream ss;
		ss << update->ntimestep;
		actualFilename2.append(ss.str());
		actualFilename2.append(actualFilename1, pos+1, actualFilename1.length());
		return actualFilename2;
	}
	else {
		return std::string();
	}
}

/*********************************************************************
 * Opens an output file for writing during the simulation phase.
 *********************************************************************/
bool CrystalAnalysisFix::openOutputStream(const string& filename, ofstream& stream)
{
	std::string actualFilename = resolveFilename(filename);
	if(actualFilename.empty()) return false;
	if(stream.is_open()) return true;
	stream.open(actualFilename.c_str(), ios::out);
	if(!stream.is_open())
		raiseErrorOne("Failed to open output file '%s' for writing.", actualFilename.c_str());
	return true;
}

/*********************************************************************
 * Opens an output file for writing during the simulation phase.
 *********************************************************************/
bool CrystalAnalysisFix::openOutputStream(const string& filename, FILE** stream)
{
	std::string actualFilename = resolveFilename(filename);
	if(actualFilename.empty()) return false;
	if(*stream != NULL) return true;
	*stream = fopen(actualFilename.c_str(), "w");
	if(!*stream)
		raiseErrorOne("Failed to open output file '%s' for writing.", actualFilename.c_str());
	return true;
}

/*********************************************************************
 * The return value of this method specifies at which points the
 * fix is invoked during the simulation.
 *********************************************************************/
int CrystalAnalysisFix::setmask()
{
	return FixConst::END_OF_STEP | FixConst::INITIAL_INTEGRATE;
}

/*********************************************************************
 * This gets called by the system before the simulation starts.
 *********************************************************************/
void CrystalAnalysisFix::init()
{
	if(atom->tag_enable == 0)
		error->all(FLERR,"Fix crystalanalysis requires atoms having IDs.");
}

/*********************************************************************
 * This is called by LAMMPS and the beginning of a run.
 *********************************************************************/
void CrystalAnalysisFix::setup(int)
{
	end_of_step();
}

/*********************************************************************
 * This is called by LAMMPS and the beginning of each MD integration step.
 *********************************************************************/
void CrystalAnalysisFix::initial_integrate(int vflag)
{
	// Force a reneighboring/exchange of atoms between processors
	// before each analysis step.
	if(update->ntimestep % nevery == 0)
		next_reneighbor = update->ntimestep;
}

/*********************************************************************
 * Called by LAMMPS at the end of each selected MD time integration step.
 *********************************************************************/
void CrystalAnalysisFix::end_of_step()
{
	try {
		// Open logging stream for writing.
		bool closeLogStream = false;
		if(logFilename.empty() == false)
			closeLogStream = openOutputStream(logFilename, logStream);

		msgLogger() << "------------------------------------------------------------------------" << endl;
		msgLogger() << "Performing crystal analysis on timestep " << update->ntimestep << "." << endl;

		// Measure analysis time.
		CALib::Timer analysisTimer;

		// Create an atomic structure from the atoms in LAMMPS.
		LAMMPSAtomicStructure structure(lmp, *this);
		// Copy LAMMPS atoms to internal storage.
		structure.transferLAMMPSAtoms();

		// Create ghost atoms.
		msgLogger() << "Creating ghost atoms (ghost cutoff: " << ghostCutoff << ")." << endl;
		structure.setGhostCutoff(ghostCutoff);
		structure.communicateGhostAtoms();

		// Create helper atoms.
		CAFloat helperPointDistance = neighborCutoff * 1.2;
		msgLogger() << "Creating tessellation helper points (distance = " << helperPointDistance << ")." << endl;
		structure.createExtraTessellationPoints(helperPointDistance);

		// Dump simulation cell shape to file.
		if(cellOutputFile.empty() == false && isMaster()) {
			msgLogger() << "Writing simulation cell to output file '" << cellOutputFile << "'." << endl;
			bool closeStream = openOutputStream(cellOutputFile, cellOutputStream);
			SimulationCellWriter(*this).writeVTKFile(cellOutputStream, structure);
			if(closeStream) cellOutputStream.close();
		}

		// Build neighbor lists.
		msgLogger() << "Building neighbor lists (neighbor cutoff: " << neighborCutoff << ")." << endl;
		NeighborList neighborList(structure);
		neighborList.buildNeighborLists(neighborCutoff, true, true, maxPatternNeighbors);

		// Do coordination pattern matching.
		msgLogger() << "Performing coordination pattern analysis." << endl;
		CoordinationPatternAnalysis coordinationAnalysis(structure, neighborList, *patternCatalog);
		coordinationAnalysis.searchCoordinationPatterns(coordinationPatternsToSearchFor, neighborCutoff);

		// Perform super pattern analysis.
		msgLogger() << "Performing super pattern analysis." << endl;
		SuperPatternAnalysis superPatternAnalysis(coordinationAnalysis);
		superPatternAnalysis.searchSuperPatterns(superPatternsToSearchFor);

		// Prepare cluster graph.
		msgLogger() << "Preparing cluster graph." << endl;
		ClusterGraph& clusterGraph = superPatternAnalysis.clusterGraph();
		clusterGraph.mirrorAdjacentProcClusters(*patternCatalog);
		superPatternAnalysis.connectDistributedClusterGraph();
		clusterGraph.buildFullGraph();
		clusterGraph.replicateGraphOnAllProcs();
		superPatternAnalysis.synchronizeClusterAssignments();

		// Dump processed atoms to output file.
		if(atomsOutputFile.empty() == false) {
			msgLogger() << "Writing processed atoms to output file '" << atomsOutputFile << "' " << endl;
			bool closeStream = false;
			if(isMaster()) closeStream = openOutputStream(atomsOutputFile, atomsOutputStream);
			AtomicStructureWriter writer(*this);
			vector<AtomicStructureWriter::DataField> atomsOutputFields;
			atomsOutputFields.push_back(AtomicStructureWriter::ATOM_ID);
			atomsOutputFields.push_back(AtomicStructureWriter::ATOM_TYPE);
			atomsOutputFields.push_back(AtomicStructureWriter::POS_X);
			atomsOutputFields.push_back(AtomicStructureWriter::POS_Y);
			atomsOutputFields.push_back(AtomicStructureWriter::POS_Z);
			atomsOutputFields.push_back(AtomicStructureWriter::SUPER_PATTERN);
			writer.writeLAMMPSFile(atomsOutputStream, atomsOutputFields, structure, &superPatternAnalysis);
			if(closeStream) atomsOutputStream.close();
		}

		// Build Delaunay tessellation.
		msgLogger() << "Generating Delaunay tessellation." << endl;
		DelaunayTessellation tessellation(*this, structure);
		tessellation.generateTessellation(true);

		// Compute elastic mapping from current configuration to stress-free configuration.
		msgLogger() << "Computing elastic mapping." << endl;
		ElasticMapping elasticMapping(tessellation, superPatternAnalysis);
		elasticMapping.generate(true);

		// Make sure all edges are consistent across processors.
		msgLogger() << "Synchronizing elastic mapping across processors." << endl;
		elasticMapping.synchronize();

		// Build local part of interface mesh on each processor.
		msgLogger() << "Building interface mesh." << endl;
		InterfaceMesh mesh(elasticMapping);
		mesh.build();

		// Merge interface meshes from different processors.
		// Only the master processor will receive the complete mesh.
		msgLogger() << "Stitching interface mesh." << endl;
		InterfaceMesh stitchedMesh(elasticMapping);
		mesh.mergeParallel(stitchedMesh);

		if(isMaster()) {

			// Finish interface mesh.
			msgLogger() << "Finalizing interface mesh." << endl;
			stitchedMesh.finalizeMesh();

			msgLogger() << "Number of interface mesh nodes: " << stitchedMesh.nodes().size() << " (" << (sizeof(InterfaceMesh::Node) * stitchedMesh.nodes().size() / 1024 / 1024) << " mbyte)" << endl;
			msgLogger() << "Number of interface mesh facets: " << stitchedMesh.facets().size() << " (" << (sizeof(InterfaceMesh::Facet) * stitchedMesh.facets().size() / 1024 / 1024) << " mbyte)" << endl;

			// Dump mesh to output file.
			if(meshOutputFile.empty() == false && isMaster()) {
				msgLogger() << "Writing mesh to output file '" << meshOutputFile << "' " << flush;
				bool closeStream = openOutputStream(meshOutputFile, meshOutputStream);
				InterfaceMeshWriter(*this).writeVTKFile(meshOutputStream, stitchedMesh);
				if(closeStream) meshOutputStream.close();
			}

			// Identify dislocation segments.
			msgLogger() << "Tracing dislocation lines (max. circuit size: " << maxBurgersCircuitSize << ", ext. circuit size: " << maxExtendedBurgersCircuitSize << ")." << endl;
			DislocationTracer tracer(stitchedMesh);
			tracer.setMaximumBurgersCircuitSize(maxBurgersCircuitSize);
			tracer.setMaximumExtendedBurgersCircuitSize(maxExtendedBurgersCircuitSize);
			pair<size_t,size_t> dislocationInfo = tracer.traceDislocationSegments();
			tracer.finishDislocationSegments();
			msgLogger() << "Found " << dislocationInfo.first << " segments and " << dislocationInfo.second << " junctions." << endl;

			// Generate defect surface mesh for output.
			TriangleMesh surface(*this);
			TriangleMesh surfaceCap(*this);
			msgLogger() << "Generating defect surface mesh." << endl;
			surface.generateFromInterfaceMesh(stitchedMesh, tracer);

			// Dump analysis results to output file.
			if(analysisOutputFile.empty() == false) {
				msgLogger() << "Writing analysis results to output file '" << analysisOutputFile << "' " << flush;
				bool closeStream = openOutputStream(analysisOutputFile, &analysisOutputStream);
				AnalysisResultsWriter(*this).writeAnalysisFile(analysisOutputStream, tracer.network(), superPatternsToSearchFor, surface,
						resolveFilename(analysisOutputFile), resolveFilename(atomsOutputFile));
				if(closeStream) {
					fclose(analysisOutputStream);
					analysisOutputStream = NULL;
				}
			}

			if(surfaceOutputFile.empty() == false || surfaceCapOutputFile.empty() == false || dislocationsOutputFile.empty() == false) {

				// Finish defect surface for output.
				msgLogger() << "Smoothing defect surface mesh." << endl;
				surface.smoothMesh(surfaceSmoothingLevel, structure);
				msgLogger() << "Calculating normals of defect surface mesh." << endl;
				surface.calculateNormals(structure);
				msgLogger() << "Wrapping defect surface mesh at simulation cell boundaries." << endl;
				if(surfaceCapOutputFile.empty() == false)
					surface.wrapMesh(structure, &surfaceCap);
				else
					surface.wrapMesh(structure);

				// Coarsen and smooth extracted dislocation lines.
				msgLogger() << "Coarsening and smoothing dislocation lines." << endl;
				DislocationNetwork& network = tracer.network();
				network.coarsenDislocationSegments(linePointInterval);
				network.smoothDislocationSegments(lineSmoothingLevel);
				network.wrapDislocationSegments();

				// Dump defect surface to output file.
				if(surfaceOutputFile.empty() == false) {
					msgLogger() << "Writing defect surface to output file '" << surfaceOutputFile << "' " << flush;
					bool closeStream = openOutputStream(surfaceOutputFile, surfaceOutputStream);
					TriangleMeshWriter(*this).writeVTKFile(surfaceOutputStream, surface);
					if(closeStream) surfaceOutputStream.close();
				}

				// Dump surface capping to output file.
				if(surfaceCapOutputFile.empty() == false) {
					msgLogger() << "Writing defect surface cap to output file '" << surfaceCapOutputFile << "' " << flush;
					bool closeStream = openOutputStream(surfaceCapOutputFile, surfaceCapOutputStream);
					TriangleMeshWriter(*this).writeVTKFile(surfaceCapOutputStream, surfaceCap);
					if(closeStream) surfaceCapOutputStream.close();
				}

				// Dump dislocations to output file.
				if(dislocationsOutputFile.empty() == false) {
					msgLogger() << "Writing dislocations to output file '" << dislocationsOutputFile << "' " << flush;
					bool closeStream = openOutputStream(dislocationsOutputFile, dislocationsOutputStream);
					DislocationsWriter(*this).writeVTKFile(dislocationsOutputStream, network);
					if(closeStream) dislocationsOutputStream.close();
				}
			}

			msgLogger() << "Analysis time: " << analysisTimer << "." << endl;
		}

		// Close logging stream.
		if(closeLogStream) logStream.close();
	}
	catch(const exception& ex) {
		msgLogger() << "****************************************************************************************" << endl;
		msgLogger() << "Crystal analysis failed at timestep " << update->ntimestep << "." << endl;
		msgLogger() << ex.what() << endl;
		msgLogger() << "****************************************************************************************" << endl;
		error->one(FLERR, ex.what());
	}
}

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function must be called by all processors simultaneously.
**********************************************************************/
void CrystalAnalysisFix::raiseErrorAllImpl(const char* errorMessage)
{
	throw runtime_error(errorMessage);
}

/*********************************************************************
* Aborts the analysis and reports an error before exiting the program.
* This function can be called by a single processor.
**********************************************************************/
void CrystalAnalysisFix::raiseErrorOneImpl(const char* errorMessage)
{
	throw runtime_error(errorMessage);
}

/*********************************************************************
 * This function aborts the program after printing an error message
 * when an assertion error occurs.
 *********************************************************************/
void CrystalAnalysisFix::handleFatalAssertionError(const char* file, int line, const char* location, const char* msg)
{
	ostringstream ss;
	ss << "ASSERTION FAILURE at ";
	if(location && msg)
		ss << "source location: " << file << ", line " << line << ", " << location << ": " << msg;
	else
		ss << "source location: " << file << ", line " << line;
	ss << endl;
	ss << "The crystal analysis code stumbled across something unexpected. Please contact the author." << endl;
	ss << "The bug report should include the program output above and, if possible," << endl;
	ss << "the the LAMMPS script and the necessary data file(s).";

	throw runtime_error(ss.str());
}

/*********************************************************************
 * Reports the memory usage of this fix to LAMMPS.
 *********************************************************************/
double CrystalAnalysisFix::memory_usage()
{
	return 0;	// Still need to develop a reasonable estimate for this.
}


