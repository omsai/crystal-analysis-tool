///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/context/CAContext.h>
#include <calib/util/Timer.h>
#include <calib/atomic_structure/neighbor_list/NeighborList.h>
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/input/atoms/AtomicStructureParser.h>
#include <calib/hardcoded/HardcodedCoordinationPatternAnalysis.h>
#include <calib/hardcoded/HardcodedSuperPatternAnalysis.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/tessellation/DelaunayTessellation.h>
#include <calib/tessellation/ElasticMapping.h>
#include <calib/dislocations/InterfaceMesh.h>
#include <calib/dislocations/DislocationTracer.h>
#include <calib/output/dislocations/DislocationsWriter.h>
#include <calib/output/cell/SimulationCellWriter.h>

using namespace CALib;
using namespace std;

int main(int argc, char* argv[])
{
	try {

		// Set the locale to the standard "C" locale, which is independent of the user's system settings.
		// This is needed to parse numbers with a decimal dot on non-english systems.
		locale::global(locale::classic());

		/// The context object of the CA library.
		CAContext context;
		context.init(argc, argv, true);

		// Print banner.
		if(context.isMaster()) {
			cout << "QuickDXA Analysis Tool" << endl;
			cout << "Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)" << endl;
			cout << "Version: " << CA_LIB_VERSION_STRING << endl;

			if(argc != 3) {
				cerr << "Missing command line parameter(s)." << endl;
				cerr << "Usage: " << argv[0] << " <inputfile> <cutoff>" << endl;
				return 1;
			}
		}

		// Parse cutoff radius parameter.
		CAFloat cutoff = atof(argv[2]);
		if(cutoff <= 0) context.raiseErrorAll("Invalid cutoff radius.");

		// Take a reasonable ghost layer thickness.
		CAFloat ghostCutoff = cutoff * CA_DEFAULT_GHOST_ATOM_LAYER_THICKNESS;
		// Take reasonable tessellation helper point distance.
		CAFloat helperPointDistance = cutoff * CA_DEFAULT_HELPER_POINT_DISTANCE;

		const char* catalogFilename = "quickdxa.db";
		PatternCatalog catalog(context);
		if(context.isMaster()) {
			// Load pattern catalog (only on master processor).
			ifstream stream(catalogFilename, ios::binary);
			if(!stream.is_open())
				context.raiseErrorOne("Failed to open pattern catalog file '%s'.", catalogFilename);
			boost::archive::binary_iarchive archive(stream);
			archive >> catalog;
		}

		// Broadcast the catalog's data from the master to other processors.
		catalog.broadcast();

		/// The input structure.
		AtomicStructure structure(context, AtomicStructure::GHOST_ATOMS);

		// Parse input file.
		AtomicStructureParser(structure).readAtomsFile(argv[1], &cin);

		// Measure runtime.
		Timer analysisTimer;

		// Create ghost atoms.
		context.msgLogger() << "Creating ghost atoms (ghost cutoff: " << ghostCutoff << ")." << endl;
		structure.setGhostCutoff(ghostCutoff);
		structure.communicateGhostAtoms();

		// Create tessellation helper points.
		context.msgLogger() << "Creating tessellation helper points (distance = " << helperPointDistance << ")." << endl;
		structure.createExtraTessellationPoints(helperPointDistance);

		// Build neighbor list for the input structure.
		context.msgLogger() << "Building neighbor lists (neighbor cutoff: " << cutoff << ")." << endl;
		NeighborList neighborList(structure);
		neighborList.buildNeighborLists(cutoff, false, true);

		// Perform coordination pattern analysis.
		context.msgLogger() << "Performing coordination pattern analysis." << endl;
		HardcodedCoordinationPatternAnalysis coordinationAnalysis(structure, neighborList, catalog);
		coordinationAnalysis.searchHardcodedCoordinationPatterns();

		// Perform super pattern analysis.
		context.msgLogger() << "Performing super pattern analysis." << endl;
		HardcodedSuperPatternAnalysis superPatternAnalysis(coordinationAnalysis);
		superPatternAnalysis.searchHardcodedSuperPatterns();

		context.msgLogger() << "Identifying fcc-hcp coherent interfaces." << endl;
		superPatternAnalysis.identifyClusterInterfaces();

		// Prepare cluster graph.
		context.msgLogger() << "Preparing cluster graph." << endl;
		ClusterGraph& clusterGraph = superPatternAnalysis.clusterGraph();
		clusterGraph.mirrorAdjacentProcClusters(catalog);
		superPatternAnalysis.connectDistributedClusterGraph();
		clusterGraph.buildFullGraph();
		clusterGraph.replicateGraphOnAllProcs();
		superPatternAnalysis.synchronizeClusterAssignments();

		// Build Delaunay tessellation.
		context.msgLogger() << "Generating Delaunay tessellation." << endl;
		DelaunayTessellation tessellation(context, structure);
		tessellation.generateTessellation(true);

		// Compute elastic mapping from current configuration to stress-free configuration.
		context.msgLogger() << "Computing elastic mapping." << endl;
		ElasticMapping elasticMapping(tessellation, superPatternAnalysis);
		elasticMapping.generate(true);

		// Make sure all edges are consistent across processors.
		context.msgLogger() << "Synchronizing elastic mapping across processors." << endl;
		elasticMapping.synchronize();

		// Build local part of interface mesh on each processor.
		context.msgLogger() << "Building interface mesh." << endl;
		InterfaceMesh mesh(elasticMapping);
		mesh.build();

		// Merge interface meshes from different processors.
		// Only the master processor will receive the complete mesh.
		context.msgLogger() << "Stitching interface mesh." << endl;
		InterfaceMesh stitchedMesh(elasticMapping);
		mesh.mergeParallel(stitchedMesh);

		if(context.isMaster()) {

			// Finish interface mesh.
			context.msgLogger() << "Finalizing mesh." << endl;
			stitchedMesh.finalizeMesh();

			// Identify dislocation segments.
			context.msgLogger() << "Extracting dislocations." << endl;
			DislocationTracer tracer(stitchedMesh);
			pair<size_t,size_t> dislocationInfo = tracer.traceDislocationSegments();
			tracer.finishDislocationSegments();
			context.msgLogger() << "Found " << dislocationInfo.first << " segments and " << dislocationInfo.second << " junctions." << endl;

			// Coarsen and smooth extracted dislocation lines.
			context.msgLogger() << "Coarsening and smoothing dislocation lines." << endl;
			DislocationNetwork& network = tracer.network();
			network.coarsenDislocationSegments(CA_DEFAULT_LINE_POINT_INTERVAL);
			network.smoothDislocationSegments(CA_DEFAULT_LINE_SMOOTHING_LEVEL);
			network.wrapDislocationSegments();

			context.msgLogger() << "Total analysis time: " << analysisTimer << "." << endl;

			// Output simulation cell and extracted dislocations to disk.
			ofstream cell_stream("cell.vtk");
			SimulationCellWriter(context).writeVTKFile(cell_stream, structure);
			ofstream disloc_stream("dislocations.vtk");
			DislocationsWriter(context).writeVTKFile(disloc_stream, network);
		}
	}
	catch(const std::bad_alloc& ex) {
		cerr << endl << "ERROR: Out of memory" << endl;
		return 1;
	}
	catch(const exception& ex) {
		cerr << endl << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
