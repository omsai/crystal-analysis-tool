///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/context/CAContext.h>
#include <calib/atomic_structure/SimulationCell.h>
#include <calib/trianglemesh/TriangleMesh.h>
#include <calib/output/triangle_mesh/TriangleMeshWriter.h>
#include <calib/input/triangle_mesh/TriangleMeshReader.h>

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#ifdef CALIB_USE_ZLIB
	#include <boost/iostreams/filter/gzip.hpp>
#endif
#include <boost/iostreams/filtering_stream.hpp>

using namespace CALib;
using namespace std;

int main(int argc, char* argv[])
{
	try {
		// Check if program has been invoked without parameters.
		// In this case just print usage instructions and quit.
		if(argc < 4) {
			cerr << "Usage: SurfaceSmoothingTool <inputfile> <outputfile> <capoutputfile>" << endl;
			return 1;
		}

		/// The context object of the CA library.
		CAContext context;
		context.init(argc, argv, true);

		// Open compressed input file for reading.
		boost::iostreams::stream<boost::iostreams::file_source> file_stream(argv[1], std::ios_base::in | std::ios_base::binary);
		if(!file_stream)
			context.raiseErrorOne("Failed to open input file %s.", argv[1]);
		boost::iostreams::filtering_stream<boost::iostreams::input> decompression_stream;
#ifdef CALIB_USE_ZLIB
		decompression_stream.push(boost::iostreams::gzip_decompressor());
#endif
		decompression_stream.push(file_stream);
		boost::archive::binary_iarchive archive(decompression_stream);

		// Load simulation cell geometry.
		SimulationCell cell(context);
		CALib::AffineTransformation cellMatrix;
		boost::array<bool,3> pbcFlags;
		archive >> cellMatrix >> pbcFlags;
		cell.setSimulationCell(cellMatrix, pbcFlags);

		// Load surface mesh.
		TriangleMesh surface(context);
		CALib::TriangleMeshReader(context).readBinaryFile(archive, surface);

		// Smooth surface mesh:
		surface.smoothMesh(CA_DEFAULT_SURFACE_SMOOTHING_LEVEL, cell);

		// Compute surface area
		context.msgLogger() << "Surface area: " << surface.computeTotalArea(cell) << endl;

		// Calculate vertex normals for visualization:
		surface.calculateNormals(cell);

		// Wrap surface mesh at simulation cell boundaries:
		TriangleMesh capMesh(context);
		surface.wrapMesh(cell, &capMesh);

		// Output wrapped surface and cap meshes to VTK files for visualization.
		ofstream surface_stream(argv[2]);
		TriangleMeshWriter(context).writeVTKFile(surface_stream, surface);
		ofstream cap_stream(argv[3]);
		TriangleMeshWriter(context).writeVTKFile(cap_stream, capMesh);
	}
	catch(const std::bad_alloc& ex) {
		cerr << endl << "ERROR: Out of memory" << endl << flush;
		return 1;
	}
	catch(const std::exception& ex) {
		cerr << endl << "ERROR: " << ex.what() << endl << flush;
		return 1;
	}

	return 0;
}
