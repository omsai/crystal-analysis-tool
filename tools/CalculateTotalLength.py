#!/usr/bin/env python

# Calculates the total length of all dislocation lines.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile [cellfile]")
options, args = parser.parse_args()
if len(args) < 1:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file.")

# Read the dislocation input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)

totalLength = 0.0
perTypeLengths = []
for segment in network.segments:
	totalLength += segment.length
	if segment.burgersVectorFamily != None:
		while len(perTypeLengths)<=segment.burgersVectorFamily: perTypeLengths.append(0)
		perTypeLengths[segment.burgersVectorFamily] += segment.length

stdout.write("%.4f" % totalLength)

# Read the cell file.
if len(args) >= 2:
    instream = stdin
    if args[1] != '-': instream = open(args[1], "r")
    if instream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
    infoLine = instream.readline()
    if infoLine.startswith("# Simulation cell") == False: raise IOError("Invalid simulation cell file.")    
    dxaVersionString = infoLine.strip()
    if instream.readline() != "ASCII\n" or instream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid simulation cell file.")
    tokens = instream.readline().split()
    if tokens[0] != "POINTS": raise IOError("Invalid simulation cell file.")
    numPoints = int(tokens[1])
    points = []
    for i in range(numPoints):
        tokens = instream.readline().split()
        points.append(Vector(float(tokens[0]), float(tokens[1]), float(tokens[2])))
        
    # Calculate cell volume
    v1 = points[1] - points[0]
    v2 = points[3] - points[0]
    v3 = points[4] - points[0]
    volume = v1.dot(v2.cross(v3))
    
    # Calculate dislocation density
    stdout.write(" %.6e %.6e" % (volume, totalLength / volume * 1e20))    

stdout.write("\n")

sum = 0.0
for i in range(len(perTypeLengths)):
	if perTypeLengths[i] != 0:
		stdout.write(" Burgers vector family %i: %.4f\n" % (i, perTypeLengths[i]))
		sum += perTypeLengths[i]
stdout.write(" Other: %.4f\n" % (totalLength-sum))


