#!/usr/bin/env python

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile [patternfilter]")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input clusters file and an output VRML file.")
    
if len(args) > 2:
	filterPatterns = []
	for p in args[2:]:		
		filterPatterns.append(int(p))
else:
	filterPatterns = None

def skipToKeyword(stream, keyword):
    while True:
        line = stream.readline()
        if line == "": raise IOError("Unexpected end of surface file.")
        if line.startswith(keyword):
            return line

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")

if instream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
infoLine = instream.readline()
if infoLine.startswith("# Clusters") == False: raise IOError("Invalid clusters file.")    
dxaVersionString = infoLine.strip()
if instream.readline() != "ASCII\n" or instream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid clusters file.")

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("#VRML V2.0 utf8\n")
outstream.write("Group {\n")
outstream.write("  children [\n")
outstream.write("  Shape {\n")
outstream.write("appearance Appearance {\n")
outstream.write("material Material {\n")
outstream.write("ambientIntensity .075\n")
outstream.write("diffuseColor 1 1 1\n")
outstream.write("specularColor 1 1 1\n")
outstream.write("shininess .02\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("IndexedFaceSet {\n")

# Read in point coordinates
tokens = instream.readline().split()
if tokens[0] != "POINTS": raise IOError("Invalid clusters file.")
numPoints = int(tokens[1])

clusters = []
for i in range(numPoints):
    tokens = instream.readline().split()
    clusters.append((float(tokens[0]), float(tokens[1]), float(tokens[2])))
    
skipToKeyword(instream, "SCALARS superpattern int")
instream.readline()
instream.readline()
filteredClusters = []
if filterPatterns != None:
	for c in clusters:
		t = int(instream.readline())
		for p in filterPatterns:
		    if t == p:
		    	filteredClusters.append(c)
else:
	filteredClusters = clusters
	
print "Number of filtered clusters: %i" % len(filteredClusters)

outstream.write("coord Coordinate {\n")
outstream.write("point [\n")

for c in filteredClusters:
    outstream.write("%s %s %s,\n" % c)
outstream.write("]\n")
outstream.write("}\n")

outstream.write("coordIndex [\n")

outstream.write("]\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("]\n")
outstream.write("}\n")
outstream.close()
