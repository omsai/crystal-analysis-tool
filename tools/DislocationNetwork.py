""" 
Dislocation data structures.

Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
"""

from sys import *
from LinAlg import *

class DislocationSegment:
	""" A single dislocation segment having a Burgers vector """

	def __init__(self):
		self.burgersVectorLocal = Vector(0,0,0)
		self.burgersVectorWorld = Vector(0,0,0)
		self.length = 0.0
		self.points = []
		self.id = None
		self.cluster_type = None
		self.cluster_id = None
		self.burgersVectorFamily = None

	def appendPoint(self, p):
		self.points.append(p)

	def flipOrientation(self):
		self.burgersVectorLocal = -self.burgersVectorLocal
		self.burgersVectorWorld = -self.burgersVectorWorld
		self.points.reverse()
	
class DislocationNetwork:
	""" A network of dislocation segments """

	def __init__(self):
		self.segments = []
		self.dxaVersionString = "# Dislocations (Python generated)"

	def appendSegment(self, segment):
		self.segments.append(segment)

def loadDislocationNetwork(stream):

	network = DislocationNetwork()
	
	if stream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
	infoLine = stream.readline()
	if infoLine.startswith("# Dislocations") == False: raise IOError("Invalid dislocations file.")	
	network.dxaVersionString = infoLine.strip()
	if stream.readline() != "ASCII\n" or stream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid dislocations file.")

	def skipToKeyword(stream, keyword):
		while True:
			line = stream.readline()
			if line == "": raise IOError("Unexpected end of dislocations file.")
			if line.startswith(keyword):
				return line

	# Read in point coordinates
	tokens = stream.readline().split()
	if tokens[0] != "POINTS": raise IOError("Invalid dislocations file.")
	numPoints = int(tokens[1])
	print "Loading %i dislocation points." % numPoints

	pointList = []
	for i in range(numPoints):
		tokens = stream.readline().split()
		pointList.append(Vector(float(tokens[0]), float(tokens[1]), float(tokens[2])))

	numDislocationSegments = int(skipToKeyword(stream, "CELLS").split()[1])
	print "Loading %i dislocation segments." % numDislocationSegments
	
	# Read in dislocation segments
	for i in range(numDislocationSegments):
		segment = DislocationSegment()
		tokens = stream.readline().split()
		numSegmentPoints = int(tokens[0])
		for index in tokens[1:]:
			segment.appendPoint(pointList[int(index)])
		network.appendSegment(segment)

	print "Loading dislocation segment properties."

	# Read in Burgers vectors in lattice space
	skipToKeyword(stream, "VECTORS burgers_vector_local double")
	for segment in network.segments:
		tokens = stream.readline().split()
		segment.burgersVectorLocal = Vector(float(tokens[0]), float(tokens[1]), float(tokens[2]))

	# Read in Burgers vectors in world space
	skipToKeyword(stream, "VECTORS burgers_vector_world double")
	for segment in network.segments:
		tokens = stream.readline().split()
		segment.burgersVectorWorld = Vector(float(tokens[0]), float(tokens[1]), float(tokens[2]))

	skipToKeyword(stream, "SCALARS burgers_vector_family int")
	skipToKeyword(stream, "LOOKUP_TABLE default")
	for segment in network.segments:
		segment.burgersVectorFamily = int(stream.readline())

	skipToKeyword(stream, "SCALARS segment_length double")
	skipToKeyword(stream, "LOOKUP_TABLE default")
	for segment in network.segments:
		segment.length = float(stream.readline())

	skipToKeyword(stream, "SCALARS segment_id int")
	skipToKeyword(stream, "LOOKUP_TABLE default")
	for segment in network.segments:
		segment.id = int(stream.readline())
		if segment.id < 0: segment.id = None

	skipToKeyword(stream, "SCALARS cluster_structure_type int")
	skipToKeyword(stream, "LOOKUP_TABLE default")
	for segment in network.segments:
		segment.cluster_type = int(stream.readline())

	skipToKeyword(stream, "SCALARS cluster_id int")
	skipToKeyword(stream, "LOOKUP_TABLE default")
	for segment in network.segments:
		segment.cluster_id = int(stream.readline())

	return network

def saveDislocationNetwork(stream, network):

	stream.write("# vtk DataFile Version 3.0\n")
	stream.write(network.dxaVersionString + "\n")
	stream.write("ASCII\n")
	stream.write("DATASET UNSTRUCTURED_GRID\n")

	# Write point coordinates
	numPoints = 0
	for segment in network.segments:
		numPoints += len(segment.points)
	stream.write("POINTS %i float\n" % numPoints)

	for segment in network.segments:
		for p in segment.points:
			stream.write("%g %g %g\n" % (p.x(), p.y(), p.z()))

	stream.write("\nCELLS %i %i\n" % (len(network.segments), numPoints + len(network.segments)))
	
	# Write dislocation segments
	pointIndex = 0
	for segment in network.segments:
		stream.write("%i" % len(segment.points))
		for i in range(len(segment.points)):
			stream.write(" %i" % pointIndex)
			pointIndex += 1
		stream.write("\n")
	stream.write("\nCELL_TYPES %i\n" % len(network.segments))
	for segment in network.segments:
		stream.write("4\n")

	stream.write("\nCELL_DATA %i\n" % len(network.segments))

	# Write Burgers vectors in lattice space
	stream.write("\nVECTORS burgers_vector_local double\n")
	for segment in network.segments:
		stream.write("%g %g %g\n" % (segment.burgersVectorLocal.x(), segment.burgersVectorLocal.y(), segment.burgersVectorLocal.z()))

	# Write Burgers vectors in world space
	stream.write("\nVECTORS burgers_vector_world double\n")
	for segment in network.segments:
		stream.write("%g %g %g\n" % (segment.burgersVectorWorld.x(), segment.burgersVectorWorld.y(), segment.burgersVectorWorld.z()))

	stream.write("\nSCALARS burgers_vector_family int\n")
	stream.write("LOOKUP_TABLE default\n")
	for segment in network.segments:
		if segment.burgersVectorFamily != None:
			stream.write("%i\n" % segment.burgersVectorFamily)
		else:
			stream.write("0\n")

	stream.write("\nSCALARS segment_length double\n")
	stream.write("LOOKUP_TABLE default\n")
	for segment in network.segments:
		stream.write("%g\n" % segment.length)

	stream.write("\nSCALARS segment_id int\n")
	stream.write("LOOKUP_TABLE default\n")
	for segment in network.segments:
		if segment.id != None:
			stream.write("%i\n" % segment.id)
		else:
			stream.write("-1\n")

	stream.write("\nSCALARS cluster_structure_type int\n")
	stream.write("LOOKUP_TABLE default\n")
	for segment in network.segments:
		stream.write("%i\n" % segment.cluster_type)

	stream.write("\nSCALARS cluster_id int\n")
	stream.write("LOOKUP_TABLE default\n")
	for segment in network.segments:
		stream.write("%i\n" % segment.cluster_id)
			