#!/usr/bin/env python

# Creates a POV-Ray file for rendering dislocation lines.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output POV-Ray file.")

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)

# Write the output file.
stream = stdout
if args[1] != '-': stream = open(args[1], "w")

for segment in network.segments:
	stream.write("sphere_sweep {\n")
	stream.write("  linear_spline %i" % len(segment.points))
	for p in segment.points:	
		stream.write(",\n  <%g, %g, %g>, 1" % (p.x(), p.y(), p.z()))
	stream.write("\n}\n")

stream.close()