#!/usr/bin/env python

# Creates a VRML file for rendering defect surfaces.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *
from LinAlg import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output VRML file.")

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("#VRML V2.0 utf8\n")
outstream.write("Group {\n")
outstream.write("  children [\n")

dislocationTypes = [
    {"BurgersVector" : Vector(1,1,2)/6.0, "Name" : "Partials"},
    {"BurgersVector" : Vector(1,1,0)/2.0, "Name" : "Perfect"},
    {"BurgersVector" : Vector(1,1,0)/6.0, "Name" : "StairRod"},
    {"BurgersVector" : Vector(0,0,0), "Name" : "Other"}
]

otherSegments = list(network.segments)

for dtype in dislocationTypes:

    blength2 = dtype["BurgersVector"].lengthSquared()
    
    segments = []
    if blength2 != 0:
        for segment in network.segments:
            if abs(segment.burgersVector.lengthSquared() - blength2) < 1e-5:
                segments.append(segment)
                otherSegments.remove(segment)
    else:
        segments = otherSegments
            
    if len(segments) == 0: continue

    outstream.write("  DEF %s Shape {\n" % dtype["Name"])
    outstream.write("appearance Appearance {\n")
    outstream.write("material Material {\n")
    outstream.write("ambientIntensity .075\n")
    outstream.write("diffuseColor 1 1 1\n")
    outstream.write("specularColor 1 1 1\n")
    outstream.write("shininess .02\n")
    outstream.write("}\n")
    outstream.write("}\n")
    outstream.write("geometry IndexedLineSet {\n")
    
    outstream.write("coord Coordinate {\n")
    outstream.write("point [\n")
    
    for segment in segments:
        for p in segment.points:
            outstream.write("%g %g %g, \n" % (p.x(), p.y(), p.z()))
                
    outstream.write("]\n")
    outstream.write("}\n")
    
    outstream.write("coordIndex [\n")
    
    counter = 0
    for segment in segments:
        for p in segment.points:
            outstream.write("%i " % counter)
            counter += 1
        outstream.write("-1\n")   
    
    outstream.write("]\n")
    outstream.write("}\n")
    outstream.write("}\n")
    
   
outstream.write("]\n")
outstream.write("}\n")
outstream.close()
